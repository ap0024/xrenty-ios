//
//  TabScreen.swift
//  XRentY
//
//  Created by user on 26/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import Foundation
import UIKit
import IBLocalizable

enum TabScreen: String {
    
    case search = "Search"
    case cars = "Cars"
    case rent = "Rents"
    case sellerProfile = "Profile"
    case customerProfile = " Profile "
    case bookings = "Bookings"
    case cart = "Cart"
    case message = "Chat"
    
    var controller: UINavigationController {
        
        let navigationController = UINavigationController()
        var controller: UIViewController?
        print(self.rawValue)
        
        switch self {
        case .search:
            controller = SearchViewController.initiatefromStoryboard(.customer)
            controller?.tabBarItem = UITabBarItem(title:"Search".localized, image:#imageLiteral(resourceName: "tabFindBlue_ic") , selectedImage: #imageLiteral(resourceName: "tabFindBlue_ic"))
        case .cars:
            controller = MyCarsViewController.initiatefromStoryboard(.seller)
            controller?.tabBarItem = UITabBarItem(title: "Cars".localized, image: #imageLiteral(resourceName: "MyCarBlue_ic"), selectedImage: #imageLiteral(resourceName: "MyCarBlue_ic"))
        case .message:
            controller = MessagesViewController.initiatefromStoryboard(.message)
            controller?.tabBarItem = UITabBarItem(title: "Chat".localized, image:#imageLiteral(resourceName: "messages") , selectedImage: #imageLiteral(resourceName: "messages"))
        case .rent:
            controller = RentsViewController.initiatefromStoryboard(.seller)
            controller?.tabBarItem = UITabBarItem(title: "Rents".localized, image:#imageLiteral(resourceName: "myrent_ic") , selectedImage: #imageLiteral(resourceName: "myrent_ic"))
        case .sellerProfile:
            controller = SellerProfileViewController.initiatefromStoryboard(.seller)
            controller?.tabBarItem = UITabBarItem(title: "Profile".localized, image:#imageLiteral(resourceName: "tabUser_ic") , selectedImage: #imageLiteral(resourceName: "tabUser_ic"))
        case .customerProfile:
            controller = CustomerProfileViewController.initiatefromStoryboard(.customer)
            controller?.tabBarItem = UITabBarItem(title: "Profile".localized, image:#imageLiteral(resourceName: "tabUser_ic") , selectedImage: #imageLiteral(resourceName: "tabUser_ic"))
        case .bookings:
            controller = MyBookingViewController.initiatefromStoryboard(.bookings)
            controller?.tabBarItem = UITabBarItem(title: "Booking".localized, image:#imageLiteral(resourceName: "myBookingBlue_ic") , selectedImage: #imageLiteral(resourceName: "myBookingBlue_ic"))
        case .cart:
            controller = CheckoutViewController.initiatefromStoryboard(.cart)
            (controller as! CheckoutViewController).isPushed = false
            controller?.tabBarItem = UITabBarItem(title: "Cart".localized, image:#imageLiteral(resourceName: "new_cart") , selectedImage: #imageLiteral(resourceName: "new_cart"))

        }
        
        navigationController.viewControllers = [controller!]
        return navigationController
    }
}
