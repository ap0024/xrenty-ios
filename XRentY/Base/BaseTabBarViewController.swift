//
//  BaseTabBarViewController.swift
//  XRentY
//
//  Created by user on 26/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit
import IBLocalizable

enum UserMode {
    
    case seller
    case customer
    
    
    var tabs: [UIViewController]? {
        switch self {
        case .seller:
            return setTabbar([.search,.rent,.message,.cars,.sellerProfile])
        case .customer:
            return setTabbar([.search,.bookings,.message,.cart,.customerProfile])
        }
    }
    
    func setTabbar(_ tabs: [TabScreen]) -> [UIViewController] {
        let controller = tabs.compactMap{ return $0.controller }
        
        controller.forEach {
            if !Constants.hasTopNotch {
                
                $0.tabBarItem.imageInsets =  UIEdgeInsets(top: -2, left: 0, bottom: 2, right: 0)
                $0.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -4)
        $0.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 12)!], for: .normal)
            }
            else {
                
                $0.tabBarItem.imageInsets =  UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
                $0.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 6)
                $0.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 12)!], for: .normal)
            }
        }
        return controller
    }
}

class BaseTabBarViewController: UITabBarController {
    
    lazy var drawerNavigationController: UINavigationController =  {
        let drawerNavigator = UINavigationController()
        let drawer = DrawerViewController.initiatefromStoryboard(.drawer)
        drawer.selectItem = { self.switchToDrawer(item: $0) }
        drawerNavigator.viewControllers = [drawer]
        drawerNavigator.modalPresentationStyle = .overFullScreen
        return drawerNavigator
    }()
    
    var drawerController: DrawerViewController {
        return drawerNavigationController.viewControllers.first as! DrawerViewController
    }
    
    var activeMode: UserMode {
        return isSellerActive ? .seller : .customer
    }
    
    var index = 0
    var isSellerActive: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.toggleUser(activeMode)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        
        if !Constants.hasTopNotch {
            var tabFrame: CGRect = self.tabBar.frame
            tabFrame.size.height = 60
            tabFrame.origin.y = self.view.frame.size.height - 60
            self.tabBar.itemPositioning = .automatic
            self.tabBar.frame = tabFrame
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NavigationHandler.hideNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NavigationHandler.showNavigationBar()
    }
    
    func toggleUser(_ mode: UserMode) {
        self.viewControllers = mode.tabs
        self.selectedIndex = index
        self.drawerController.reloadMenu(mode)
    }

    
    // Update UI on main thread
    func switchToDrawer(item drawer: Drawer) {
        DispatchQueue.main.async {
            switch drawer.basehandler {
            case .tab(let index):
                self.selectTab(index)
            case .push(let controller):
                self.pushToController(controller)
            case .logout:
                self.logout()
            case .mode(let mode):
                self.toggleUser(mode)
            case .becomeSeller:
                self.becomeSeller()
            }
        }
    }
    
    // select tab
    func selectTab(_ number: Int) {
        self.selectedIndex = number
    }
    
    // push to controller
    func pushToController(_ controller: UIViewController) {
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // logout
    func logout() {
        AlertManager.showAlert(type: .custom("Are you sure you want to logout?".localized), actionTitle: "Logout".localized) {
            
            WebService.clearDeviceToken { (result) in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let response):
                        Authentication.clearData()
                        NavigationHandler.logOut()
                        TrackList.removeAllUsers()
                        PubNubManager.sharedInstance.unSubscribe()
                    case .failure(let error):
                        AlertManager.showAlert(on: self, type: .custom(error.message))
                    }
                }
            }
        }
    }
    
    // logout
    func expireLogout() {
        AlertManager.showAlert(type: .custom("Your session is Expired press ok to logout the app ".localized), actionTitle: "Logout".localized) {
            Authentication.clearData()
            NavigationHandler.logOut()
        }
    }
    
    // logout
    func becomeSeller() {
        
        AlertManager.showAlert(type: .custom("Are you sure you want to become a seller?".localized), actionTitle: "Yes".localized) {
            
            self.showLoader(true)
            WebService.becomeSeller { result in
                DispatchQueue.main.async {
                    self.showLoader(false)
                    switch result {
                    case .success(let response):
                        Authentication.isSeller = true
                        self.toggleUser(.seller)
                        AlertManager.showAlert(on: self, type: .custom(response.message!))
                    case .failure(let error):
                        AlertManager.showAlert(on: self, type: .custom(error.message))
                    }
                }
            }
        }
    }
    
    func showLoader(_ show: Bool) {
        show ? ((self.selectedViewController as? UINavigationController)?.viewControllers[0] as? BaseViewController)?.showLoader() : ((self.selectedViewController as? UINavigationController)?.viewControllers[0] as? BaseViewController)?.hideLoader()
    }
    
    func presentMenuItems() {
        self.present(drawerNavigationController, animated: false, completion: nil)
    }
}
