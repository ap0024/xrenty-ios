//
//  BaseViewController.swift
//  Chnen
//
//  Created by user on 01/06/18.
//  Copyright © 2018 navjot_sharma. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    lazy var alertControllerLoader: UIAlertController = {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = .gray
        alert.view.addSubview(loadingIndicator)
        return alert
    }()
    
    lazy private var loaderView: ActivityIndicatorView = {
        let view  = ActivityIndicatorView.instanceFromNib()
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.clipsToBounds = true
        view.frame = self.view.bounds
        return view
    }()
    
    var isLoaderAnimating : Bool! {
       return loaderView.isSpinnerAnimating
    }
    
    var isLoaderWithTitleAnimating : Bool {
        var animating = false
        for view in alertControllerLoader.view.subviews {
            if let indicator = view as? UIActivityIndicatorView {
                animating = indicator.isAnimating
            }
        }
        return animating
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addTitleView() {
        
        let mainView = UIView()
        mainView.frame = CGRect.init(x: -8, y: 0, width: 148, height: 40)
        let image = UIImage(named: "topbar_Logo_ic")
        let view = UIImageView(image: image)
        
        view.clipsToBounds = true
        mainView.addSubview(view)
        view.contentMode = .scaleAspectFit
        view.frame = CGRect(x: -8, y: 0, width: 148, height: 40)
        navigationItem.titleView = mainView
    }
    
    func showNavigation() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func hideNavigation() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func addCustomNavigationView(_ view: UIView) {
        
        self.navigationItem.titleView = view
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    func showLoader(color: UIColor? = nil) {
        if !isLoaderAnimating {
            self.loaderView.frame = self.view.bounds
            self.loaderView.backgroundColor = color ?? loaderView.backgroundColor
            self.loaderView.startLoading()
            self.view.addSubview(self.loaderView)
        }
    }
    
    func showLoaderWithTitle(title: String) {
        self.alertControllerLoader.message = title
        for view in alertControllerLoader.view.subviews {
            if let indicator = view as? UIActivityIndicatorView {
                indicator.startAnimating()
            }
        }
        present(alertControllerLoader, animated: true, completion: nil)
    }
    
    func hideLoaderWithTitle() {
        for view in alertControllerLoader.view.subviews {
            if let indicator = view as? UIActivityIndicatorView {
               indicator.stopAnimating()
            }
        }
        alertControllerLoader.dismiss(animated: true, completion: nil)
    }
    
    func hideLoader() {
        self.loaderView.stopLoading()
        self.loaderView.removeFromSuperview()
        self.loaderView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    func showLoaderOnWindow() {
        guard let window = UIApplication.shared.delegate?.window else { return }
        self.loaderView.frame = window?.bounds ?? self.view.bounds
        self.loaderView.startLoading()
        window?.addSubview(loaderView)
    }
    
    func blockUI(_ flag: Bool) {
        self.view.isUserInteractionEnabled = !flag
    }
    
    @IBAction func pop(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuItems(_ sender: UIBarButtonItem) {
        guard let tabBarController = tabBarController as? BaseTabBarViewController else {
            return
        }
        tabBarController.presentMenuItems()
    }
}

extension BaseViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
