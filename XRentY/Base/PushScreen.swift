//
//  PushScreen.swift
//  XRentY
//
//  Created by user on 07/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import Foundation
import UIKit

enum PushScreen {

    case mainScreen
    case login
    case register
    case forgotPassword
    case tab(Bool,Int)
    case addCar
    case carDocumentation(AddCar)
    case filter(Search)
    case productDetail(Car)
    case bookingHistory
    case wishList(Bool)
    case googleMap
    case orderSummary
    case cart
    case pickupCar(Booking,(String)->())
    case returnCar(Booking,()->())
    case vieworder(Booking)
    case sellerReturnCar(Rent,()->())
    
    case review(Booking)
    case bookingConfiguration
    case chat
    case document
    case messagelist
    case customerprofile
    case orderInfo
    
    var controller: UIViewController {
        switch self {
        case .mainScreen:
            let controller = MainViewController.initiatefromStoryboard(.login)
            return controller
        case .login:
            let controller = LoginViewController.initiatefromStoryboard(.login)
            return controller
        case .register:
            let controller = RegisterViewController.initiatefromStoryboard(.login)
            return controller
        case .forgotPassword:
            let controller = ForgetPasswordViewController.initiatefromStoryboard(.login)
            return controller
        case .addCar:
            let controller = AddCarViewController.initiatefromStoryboard(.seller)
            return controller
        case .carDocumentation(let addcar):
            let controller = CarDocumentationViewController.initiatefromStoryboard(.seller)
            controller.addCar = addcar
            return controller
        case .filter(let search):
            let controller = ApplyFilterViewController.initiatefromStoryboard(.customer)
            controller.search = search
            return controller
        case .productDetail(let car):
            let controller = ProductDetailViewController.initiatefromStoryboard(.carDetail)
            controller.car = car
            return controller
        case .wishList:
            let controller = WishListViewController.initiatefromStoryboard(.customer)
            return controller
        case .bookingHistory:
            let controller = BookingHistoryViewController.initiatefromStoryboard(.bookings)
            return controller
        case .googleMap:
            let controller = GoogleMapViewController.initiatefromStoryboard(.customer)
            return controller
        case .cart:
            let controller = CheckoutViewController.initiatefromStoryboard(.cart)
            controller.isPushed = true
            return controller
        case .pickupCar(let booking ,let completion):
            let controller = PickupCarViewController.initiatefromStoryboard(.customer)
            controller.returnCarHandler = completion
            controller.booking = booking
            return controller
        case .returnCar(let booking ,let completion):
            let controller = ReturnCarViewController.initiatefromStoryboard(.customer)
            controller.returnCarHandler = completion
            controller.booking = booking
            return controller
        case .vieworder(let booking):
            let controller = BookingDetailsViewController.initiatefromStoryboard(.bookings)
            controller.booking = booking
            return controller
        case .orderSummary:
            let controller = OrderSummaryViewController.initiatefromStoryboard(.orderSummary)
            return controller
        case .sellerReturnCar(let rents,let completion):
            let controller = sellerReturnUIViewController.initiatefromStoryboard(.seller)
            controller.returnCarHandler = completion
            controller.Rent = rents
            return controller
        case .review(let booking):
            let controller = ReviewToCustomerViewController.initiatefromStoryboard(.customer)
            controller.booking = booking
            return controller
        case .tab(let active, let index):
            let controller = BaseTabBarViewController.initiatefromStoryboard(.base)
            controller.isSellerActive = active
            controller.index = index
            return controller
        case .bookingConfiguration:
            let controller = BookingConfigurationViewController.initiatefromStoryboard(.seller)
            return controller
        case .chat:
            let controller = ChatViewController.initiatefromStoryboard(.message)
            return controller
        case .messagelist:
            let controller = MessagesViewController.initiatefromStoryboard(.message)
            return controller
        case .document:
            let controller = DocumentViewController.initiatefromStoryboard(.seller)
            return controller
        case .customerprofile:
            let controller = CustomerProfileViewController.initiatefromStoryboard(.customer)
            return controller
        case .orderInfo:
            let controller = OrderViewController.initiatefromStoryboard(.orderSummary)
            return controller
            
        }
    }
}
