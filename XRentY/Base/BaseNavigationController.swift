//
//  BaseNavigationController.swift
//  XRentY
//
//  Created by user on 07/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit

class  NavigationHandler {
    
    class func pushTo(_ screen: PushScreen) {
        BaseNavigationController.sharedInstance.push(to: screen)
    }
    
    class func hideNavigationBar() {
        BaseNavigationController.sharedInstance.isNavigationBarHidden = true
    }
    
    class func showNavigationBar() {
        BaseNavigationController.sharedInstance.isNavigationBarHidden = false
    }
    
    class func pop() {
        BaseNavigationController.sharedInstance.popViewController(animated: true)
    }
    
    class func popTo(_ controller: UIViewController.Type) {
        BaseNavigationController.sharedInstance.viewControllers.forEach {
            if $0.isKind(of: controller) {
                BaseNavigationController.sharedInstance.popToViewController($0, animated: true)
            }
        }
    }
    
    class func setRoot(_ controller: UIViewController) {
        BaseNavigationController.sharedInstance.viewControllers = [controller]
    }
    
    class func setRoot(_ screen: PushScreen) {
        BaseNavigationController.sharedInstance.viewControllers = [screen.controller]
    }
    
    class var stack: [UIViewController] {
        return BaseNavigationController.sharedInstance.viewControllers
    }
    
    class func loggedIn() {
        let isSeller = Authentication.isSeller
        BaseNavigationController.sharedInstance.setRoot(.tab(isSeller,0))
    }
    
    class func logOut() {
        BaseNavigationController.sharedInstance.setRoot(.mainScreen)
    }
}

class BaseNavigationController: UINavigationController {
    
    static var sharedInstance = BaseNavigationController()
    var configureMannual: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        type(of: self).sharedInstance = self
        !configureMannual ? self.configure() : nil
        self.setDefaultAppearance()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func configure() {
        
        if Authentication.isUserLoggedIn! {
            self.setLanguage()
            
            let isSeller = Authentication.isSeller
            self.setRoot(.tab(isSeller,0))
        }
        else {
            self.setRoot(.mainScreen)
        }
    }
    
    func setLanguage()  {
        
        var langCode : String = ""
        let language = NSLocale.current.languageCode
        
        if language == "en"
        {
            langCode = "es_US"
        }
        else if language == "es"
        {
            langCode = "es_ES"
        }
        else
        {
            langCode = "es_US"
        }
        
        let queryItems = ["language":langCode]
        
        WebService.setLanguage(queryItems: queryItems) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    if let user = response.object {
                        print(user)
                    }
                    else {
                        print(Error.self.self)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    

    
    public func configureManually(with activeSeller: Bool, tabIndex: Int) {
        self.setRoot(.tab(activeSeller,tabIndex))
    }
    
    // set default navigation appearance
    func setDefaultAppearance() {
        UINavigationBar.appearance().isOpaque = false
//        UINavigationBar.appearance().barTintColor = Color.barTintColor
        let image = UIImage(named: "logo_bg")
        UINavigationBar.appearance().setBackgroundImage(image, for: .default)
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 22),NSAttributedString.Key.foregroundColor : UIColor.white]
        
        // set default uisearchbar tint color to white
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    // set root controller
    func setRoot(_ screen: PushScreen) {
        viewControllers = [screen.controller]
    }
    
    // push to view controller
    func push(to screen: PushScreen) {
        self.pushViewController(screen.controller, animated: true)
    }
}
