//
//  LTError.swift
//  XRentY
//
//  Created by user on 15/01/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation

enum LTError: Error {
    case invalidChannel
}
