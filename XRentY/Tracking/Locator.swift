//
//  Locator.swift
//  XRentY
//
//  Created by user on 15/01/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation
import CoreLocation

/*Locator enables location services*/
typealias LocationUpdater = (CLLocationCoordinate2D?) -> Void

class Locator: NSObject  {

    public var locationUpdater: LocationUpdater? = nil
    //private var locationEnabled: Bool
    private let locationManager: CLLocationManager
    var currentLocation: CLLocationCoordinate2D? = nil
    
    // initialization
    override init() {
       // self.locationEnabled = false
        self.locationManager = CLLocationManager()
        super.init()
    }
    
    // setup configuration for default location manager
    public func enableLocationUpdates() {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = 5
       self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
        
        
    }
    
    public func disableLocationUpdates() {
        self.locationManager.delegate = nil
        self.locationManager.stopUpdatingLocation()
        self.locationManager.stopMonitoringSignificantLocationChanges()
        
    }
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                self.currentLocation = nil
            case .authorizedAlways, .authorizedWhenInUse:
                self.enableLocationUpdates()
            }
        }
    }
    
    // enable location services
    /*func enableServices() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                self.locationEnabled = false
            case .authorizedAlways, .authorizedWhenInUse:
                self.locationEnabled = true
            }
        } else {
            self.locationManager.requestAlwaysAuthorization()
        }
    }*/
}

// location manager delegate
extension Locator: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations.last?.coordinate
        if let coordinate = locations.last?.coordinate,let updater = locationUpdater {
            updater(coordinate)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("updated location error : \(error.localizedDescription)")
        if let updater = locationUpdater {
            updater(nil)
        }
    }
}
