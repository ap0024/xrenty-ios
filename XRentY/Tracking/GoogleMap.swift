//
//  GoogleMap.swift
//  XRentY
//
//  Created by user on 28/01/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

enum GoogleMapError: Error {
    case invalidCoordinate
    case addMarkerError
}

protocol GoogleMapDelegate {
    func didEndTileRendering()
    func willMove()
}

class GoogleMap: NSObject {
    
    //static var sharedInstance = GoogleMap()
    var delegate: GoogleMapDelegate?
    var bounds: GMSCoordinateBounds?
    //var animationPolyline = GMSPolyline()
    var insets: UIEdgeInsets = UIEdgeInsets(top: 130, left: 130, bottom: 130, right: 130)
    fileprivate var carAnotation: GMSMarker?
    fileprivate var carCoordinate: CLLocationCoordinate2D?
    fileprivate var mapView: GMSMapView!
    fileprivate var finishRendering: Bool = false {
        didSet {
            finishRendering ? delegate?.didEndTileRendering() : nil
        }
    }
    
    var polyline = GMSPolyline()
    var gmsPath = GMSMutablePath()
    var markers: [GMSMarker] = []
    var coordinates: [CLLocationCoordinate2D] = []
    var carAnnotationImage: UIImage?
    var wayPoint: String?
    
    private var coordinate: CLLocationCoordinate2D? {
        didSet {
            if let coordinate = self.coordinate {
                self.mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 16)
            }
        }
    }
    
    private var mapStyleURL: URL? {
        didSet {
            if let url = mapStyleURL {
                self.mapView.mapStyle = try? GMSMapStyle(contentsOfFileURL: url)
            }
        }
    }
    
    // configure map on target with map style url
    public func configureMap(on target: UIViewController, mapStyleURL: URL? = nil, coordinate: CLLocationCoordinate2D) {
        self.mapView = GMSMapView()
        self.coordinate = coordinate
        self.mapStyleURL = mapStyleURL
        self.mapView.delegate = self
        self.mapView.settings.rotateGestures = false
        //self.mapView.settings.scrollGestures = false
        //self.mapView.setMinZoom(12, maxZoom: 20)
        self.mapView.settings.tiltGestures = false
        self.mapView.alpha = 0
        self.mapView.isBuildingsEnabled = false
        self.delegate = target as? GoogleMapDelegate
    }
    
    // add mapview on target view
    public func addMap(on view: UIView) {
        
        if let _ = mapView.superview {
            self.mapView.removeFromSuperview()
        }
        
        self.mapView.frame = view.bounds
        view.addSubview(mapView)
        view.sendSubviewToBack(mapView)
    }
    
    // move to current saved coordinates
    public func moveToCoordinate() throws {
        if let coordinate = self.coordinate {
            let marker = GMSMarker()
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.position = coordinate
            marker.icon = UIImage(named: "marker")
            marker.map = self.mapView
            let position = GMSCameraPosition(target: coordinate, zoom: 16, bearing: 2.0, viewingAngle: 45)
            self.mapView.animate(to: position)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                let position = GMSCameraPosition(target: coordinate, zoom: 18, bearing: 4.0, viewingAngle: 50)
                self.mapView.animate(to: position)
            }
        } else {
            throw  GoogleMapError.invalidCoordinate
        }
    }
    
    // add marker with image
    public func addMarker(on location: CLLocationCoordinate2D?, image: UIImage?, animation: Bool = true, center: Bool = false) throws {
        if let coordinate = location {
            //let iconView = UIImageView(image: image)
            let marker = GMSMarker()
            marker.appearAnimation = animation ? .pop : .none
            marker.position = coordinate
            marker.map = mapView
            marker.icon = image
            if center {
                 marker.groundAnchor = CGPoint(x: 0.5,y: 0.5)
            }
            markers.append(marker)
        } else {
            throw GoogleMapError.addMarkerError
        }
    }
    
    // update camera
    public func updateCamera(for bounds: GMSCoordinateBounds, insets: UIEdgeInsets) {
//        self.bounds = bounds
//        self.insets = insets
//        let cameraUpdate = GMSCameraUpdate.fit(bounds, with: insets)
//        self.mapView.moveCamera(cameraUpdate)
        
        if let bounds = self.bounds {
            
            self.insets = insets
            let cameraUpdate = GMSCameraUpdate.fit(bounds, with: insets)
            self.mapView.moveCamera(cameraUpdate)
        }
    }
    
    public func moveCamera(for bounds: GMSCoordinateBounds, insets: UIEdgeInsets) {
        
        if let bounds = self.bounds {
            let cameraUpdate = GMSCameraUpdate.fit(bounds, with: insets)
            self.mapView.moveCamera(cameraUpdate)
        }
        
        
//        let cameraUpdate = GMSCameraUpdate.fit(bounds, with: insets)
//        
//        
//        self.mapView.moveCamera(cameraUpdate)
        //self.mapView.animate(with: cameraUpdate)
        
    }
    
    public func animateCamera(for bounds: GMSCoordinateBounds, insets: UIEdgeInsets) {
        let cameraUpdate = GMSCameraUpdate.fit(bounds, with: insets)
        self.mapView.animate(with: cameraUpdate)
    }
    
    public func moveCameraToDriver() {
        if let location =  self.carAnotation?.position {
            let position = GMSCameraPosition.camera(withTarget: location, zoom: 16)
            self.mapView.animate(to: position)
        }
    }
    
    func updateInsets(_ insets: UIEdgeInsets) {
        if let bounds = self.bounds {
            let cameraUpdate = GMSCameraUpdate.fit(bounds, with: insets)
            self.mapView.moveCamera(cameraUpdate)
        }
    }
    
    // draw route
    public func drawRoute(_ wayPoints: String?, animatePath: Bool = true, completion: @escaping (Bool)->())  {
        
        
        if let points = wayPoints {

            gmsPath = GMSMutablePath(fromEncodedPath: points)!
            polyline = GMSPolyline()
            polyline.path = gmsPath
            polyline.strokeColor = UIColor(red: 37/255, green: 120/255, blue: 197/255, alpha: animatePath ? 0.4 : 0.8)
            polyline.strokeWidth = 2.5
            polyline.map = self.mapView
            
            self.coordinates = []
            for index in 0...gmsPath.count() - 1 {
                coordinates.append(gmsPath.coordinate(at: index))
            }
            
            if animatePath {
                
                let animationPolyline = GMSPolyline()
                let animationPath = GMSMutablePath()
                var i: UInt = 0
                
                let interval = 1/Double(gmsPath.count())
                Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { (timer) in
                    if (i < self.gmsPath.count()) {
                        animationPath.add(self.gmsPath.coordinate(at: i))
                        animationPolyline.path = animationPath
                        animationPolyline.strokeColor = UIColor(red: 37/255, green: 120/255, blue: 197/255, alpha: 1)
                        animationPolyline.strokeWidth = 3.0
                        animationPolyline.map = self.mapView
                        i += 1
                    }
                    else {
                        timer.invalidate()
                        completion(true)
                    }
                })
            } else {
                completion(true)
            }
            
            self.updateCameraPosition(coordinates)
            
        } else {
            completion(false)
        }
    }
    
    func updateCameraPosition(_ coordinates: [CLLocationCoordinate2D]) {
        
        let bounds = GMSCoordinateBounds()
        for coordinate in coordinates {
           bounds.includingCoordinate(coordinate)
        }
        if bounds.isValid {
            let cameraUpdate = GMSCameraUpdate.fit(bounds, with: insets)
            self.mapView.moveCamera(cameraUpdate)
        }
    }
    
    // configure and update car anotation
    public func configureUpdateCar(on location: CLLocationCoordinate2D, moveCamera: Bool = false) {
        
        guard let _ = self.carAnotation?.map else {
            self.carAnotation = GMSMarker()
            self.carAnotation?.appearAnimation = .pop
            self.carAnotation?.map = mapView
            self.carAnotation?.icon = UIImage(named: "car")
            self.carAnotation?.groundAnchor = CGPoint(x: 0.5,y: 0.5)
            self.carAnotation?.position = location
            self.carCoordinate = location
            if moveCamera {
                self.moveCameraToDriver()
            }
            return
        }
        
        if let coordinate = self.carCoordinate {
            let head = getAngle(fromLoc: location, toLoc: coordinate) * (180/Double.pi)
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.5)
            self.carAnotation?.rotation = head
            self.carAnotation?.position = coordinate
            CATransaction.commit()
            self.carCoordinate = location
           // print("head value: \(head)")
        }
    }
    
   // var count: Int = 0
    
    func updatePolyline(_ coordinate: CLLocationCoordinate2D, directionHandler: (()->())? = nil) {
        
        if coordinates.isEmpty {
            return
        }
        
//        if !GMSGeometryIsLocationOnPathTolerance(coordinate, gmsPath, true, 50) {
//            wayPoint = "\(coordinate.latitude)/\(coordinate.longitude)"
//            wayPoint = wayPoint?.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
//            print(wayPoint)
//        }
        
        self.configureUpdateCar(on: coordinate)
        
        if GMSGeometryDistance(coordinate, coordinates.last!) < 20 {
            coordinates.removeLast()
        } else  {
            coordinates.append(coordinate)
        }
        
        let path = GMSMutablePath()
        for coordinate in coordinates {
            path.add(coordinate)
        }
        path.add(coordinate)
        self.gmsPath = path
        self.polyline.path = path
        
        print("Distance: \(gmsPath.length(of: GMSLengthKind.geodesic))")
    }
    
    func emptyCoordinateBuffer() {
        self.coordinates.removeAll()
    }
    
    func clearAddedCoordinates() {
        self.gmsPath.removeAllCoordinates()
        self.polyline.path = nil
        self.polyline.map = nil
    }
    
    public func clearCarAnnotation() {
        if self.carAnotation?.map != nil {
            self.carAnotation?.map = nil
        }
    }
    
    // get angle for direction
    private func getAngle(fromLoc: CLLocationCoordinate2D, toLoc: CLLocationCoordinate2D)-> Double {
        let deltaLongitude = fromLoc.longitude - toLoc.longitude
        let deltaLatitude = fromLoc.latitude - toLoc.latitude
        let angle = (.pi * 0.5) - atan(deltaLatitude / deltaLongitude)
        if deltaLongitude > 0 {
            return angle + .pi
        }
        else if deltaLongitude < 0 {
            return angle
        }
        else if deltaLatitude < 0 {
            return .pi;
        }
        return  0.0
    }
    
    // clear memory
    public func clearMemory() {
        self.mapView.removeFromSuperview()
        self.mapView.delegate = nil
        self.mapView.clear()
        self.mapView = nil
        self.coordinate = nil
        self.mapStyleURL = nil
        self.finishRendering = false
        self.carCoordinate = nil
    }
    
    // clear map
    public func clearPolyline() {
        self.polyline.map = nil
    }
    
    public func clearMarkers() {
        for marker in markers {
            marker.map  = nil
            marker.icon = nil
        }
    }
    
    public func clearMap() {
        self.mapView.clear()
    }
}

extension GoogleMap: GMSMapViewDelegate {
    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {
        if !finishRendering {
            UIView.animate(withDuration: 0.3, animations: {
                self.mapView.alpha = 1
            })
            self.finishRendering = true
        }
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        delegate?.willMove()
    }
}
