//
//  LTCoordinator.swift
//  XRentY
//
//  Created by user on 15/01/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation
import PubNub
import CoreLocation

// Location Tracking Coordinator Delegate
protocol LTCoordinatorDelegate {
    func updateLocation(info: LTInformation)
    func currentLocation(coordinate: CLLocationCoordinate2D?)
}

class LTCoordinator: NSObject {
    
    static let instance = LTCoordinator()
    class func sharedInstance() -> LTCoordinator {
        return LTCoordinator.instance
    }
    
    private let locator: Locator
    private let pubnub: PubNubManager
    private var portChannel: String?
    var currentLocationCoordinate: CLLocationCoordinate2D?
    var locationUpdate: [String:Any] = [:]
    var trackChannel: String?
    var permissionHandler: ((CLLocationCoordinate2D, Bool) ->())? = nil
    var delegate: LTCoordinatorDelegate? = nil
    var lastLocation: CLLocationCoordinate2D? {
        return locator.currentLocation
    }
    var locatorTimer = Timer()
    var isLocationUpdating: Bool = false
    
    // [0] = Publish Key, [1] = Subscribe key

    override init() {
        
        self.locator = Locator()
        self.pubnub = PubNubManager()

        super.init()
        
        self.pubnub.listner = { (location,trackId) in
            self.listen(location, channelId: trackId)
        }
    }
    
    // configure pubnub keys
    func configure(pubnubKeys: (String,String)) {
        self.pubnub.configure(publishKey: pubnubKeys.0, subscribeKey: pubnubKeys.1)
    }
    
    // add observer method
    func addObserver(for channel: String?) throws {
        
        guard let channelId = channel else {
            throw LTError.invalidChannel
        }

        self.trackChannel = channelId
        let ch = "track:" + channelId
        self.pubnub.subscribe(to: ch)
        
        let data = ["id": channelId,
                    "description": "askForLocation"]
        self.pubnub.sendMessage(to: "group", message: data)
    }
    
    func addDelegate(_ target: LTCoordinatorDelegate) {
        self.delegate = target
    }
    
    // remove observer method 
    func removeObserver() {
        
        if let channel  = trackChannel {
            let ch = "track:" + channel
            self.pubnub.unSubscribe(from: ch)
        }
        
        self.delegate = nil
    }
    
    // enable services and location upudates
    public func enableServices(on channel: String?) throws {
        if let portChannel = channel {
            self.pubnub.subscribe(to: "group")
            self.portChannel = "track:" + portChannel
            self.locator.enableLocationUpdates()
            self.locator.locationUpdater = {
                self.updateLocation($0)
            }
        } else {
            throw LTError.invalidChannel
        }
    }
    
    public func checkLocationServices() {
        if let _ = portChannel {
            self.locator.checkLocationServices()
        }
    }
    
    // disbale services from pubnub and location updates
    public func disableServices() {
        self.locator.disableLocationUpdates()
        self.locator.locationUpdater = nil
        self.pubnub.unSubscribe()
        //self.pubnub.listner = nil
        self.portChannel = nil
    }
    
    // update location block
    private func updateLocation(_ coordinate: CLLocationCoordinate2D?) {
        
        if let channel = portChannel {
            self.isLocationUpdating = true
            let jsonObject = LTInformation.serialize(with: coordinate, id: channel)
            self.pubnub.sendMessage(to: channel, message: jsonObject)
            self.locatorTimer.invalidate()
            locatorTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { (_) in
                self.isLocationUpdating = false
            })
        }
        
        self.delegate?.currentLocation(coordinate: coordinate)
    }
    
    // listen to calls from other channels
    private func listen(_ message: Any, channelId: String) {
        
        if channelId.contains("track") {
            if let object = message as? [String:Any], !object.isEmpty {
               // print(object)
                if let lat = object["lat"] as? String, let long = object["long"] as? String {
                    var info: LTInformation?
                    let locationEnabled : Bool = object["locationEnabled"] as? Bool ?? true
                    info =  LTInformation(locationEnabled: locationEnabled, id: channelId, lat: lat, long: long)
                    if let information = info {
                        delegate?.updateLocation(info: information)
                    }
                }
            }
        }
        
        if channelId.contains("group") {
            if let data = message as? [String:Any] {
                if let id = data["id"] as? String, let description = data["description"] as? String, description == "askForLocation" {
                    if id == Authentication.customerId {
                        if let channel = portChannel {
                            if !isLocationUpdating {
                                let jsonObject = LTInformation.serialize(with: locator.currentLocation, id: channelId)
                                self.pubnub.sendMessage(to: channel, message: jsonObject)
                            }
                        }
                    }
                }
            }
        }
    }
}


/*if let data = try? JSONSerialization.data(withJSONObject: object, options: .prettyPrinted) {
 if let info = try? JSONDecoder().decode(LTInformation.self, from: data) {
 delegate?.updateLocation(info: info)
 }
 }*/
