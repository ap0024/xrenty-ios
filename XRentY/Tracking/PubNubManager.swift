//
//  PubNubManager.swift
//  Hider
//
//  Created by user on 27/12/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation
import PubNub

typealias PBListnerHandler = (Any, String) -> Void

class PubNubManager: NSObject {
    
    static let sharedInstance = PubNubManager()
    private var client : PubNub?
    public var listner: PBListnerHandler? = nil
    
    // configure pubnub
    func configure(publishKey: String, subscribeKey: String) {
        let configuration = PNConfiguration(publishKey: publishKey, subscribeKey: subscribeKey)
        self.client = PubNub.clientWithConfiguration(configuration)
        self.client?.addListener(self)
    }
    
    // subscribe to channel
    func subscribe(to channel: String) {
        if let client = self.client, !client.channels().contains(channel) {
           self.client?.subscribeToChannels([channel], withPresence: false)
        }
    }
    
    // unsubscribe from channel 
    func unSubscribe(from channel: String) {
        if let client = self.client, client.channels().contains(channel) {
            self.client?.unsubscribeFromChannels([channel], withPresence: false)
        }
    }
    
    func unSubscribe() {
        self.client?.unsubscribeFromAll()
    }
    
    // publish message
    func sendMessage(to channel: String, message: Any) {
        self.client?.publish(message, toChannel: channel, storeInHistory: false, withCompletion: { (status) in
            print(status)
        })
    }
}

extension PubNubManager: PNObjectEventListener {
    
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        //print(message.data.message)
        if let listner = self.listner, let text = message.data.message {
            /*let messageToken = message.data.timetoken
            let currentToken = Date().toMillis()
            print(messageToken)
            print(currentToken)*/
            //print(Date(timeIntervalSince1970: TimeInterval(message.data.timetoken)))
            //print(Date(timeIntervalSince1970: TimeInterval(Date().toMillis())))
            listner(text,message.data.channel)
        }
    }
    
    func client(_ client: PubNub, didReceivePresenceEvent event: PNPresenceEventResult) {
        //print(event)
    }
    
    func client(_ client: PubNub, didReceive status: PNStatus) {
       //print(status)
    }
}

extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 10000000)
    }
}

