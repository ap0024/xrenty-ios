//
//  LTInformation.swift
//  XRentY
//
//  Created by user on 15/01/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation
import CoreLocation

typealias Codable = Encodable & Decodable

struct LTInformation: Codable {
    
    static func serialize(with coordinate: CLLocationCoordinate2D?, id: String) -> [String:Any] {
        let locationEnabled = coordinate != nil
        let latitude = locationEnabled ? "\(coordinate!.latitude)" : ""
        let longitude = locationEnabled ? "\(coordinate!.longitude)" : ""
        let data = LTInformation(locationEnabled: locationEnabled, id: id, lat: latitude, long: longitude).data
        let jsonObject = try! JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! [String:Any]
        //let string = String(data: data, encoding: .utf8)!
        return jsonObject
    }
    
    var coordinate: CLLocationCoordinate2D? {
        if let latitude = CLLocationDegrees(lat), let longitude = CLLocationDegrees(long) {
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        return nil
    }
    
    var data: Data {
        let encoder = JSONEncoder()
        return try! encoder.encode(self)
    }
    
    let locationEnabled: Bool
    let id: String
    let lat: String
    let long: String
}
