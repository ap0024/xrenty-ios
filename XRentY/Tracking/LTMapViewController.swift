//
//  TrackViewController.swift
//  XRentY
//
//  Created by user on 15/01/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces

class LTMapViewController: BaseViewController {
    
    func addLocation(isLocationShared: Bool,channel: String?, location: CLLocationCoordinate2D, startTracking: Bool) {
        
        self.trackingEnable = startTracking
        self.location = location
        self.trackerId = channel
        self.isLocationShared = isLocationShared
    }
    
    // initialize self
    class func instantiate() -> LTMapViewController {
        let storyBoard = UIStoryboard(name: "LocationTracking", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "LTMapViewController") as! LTMapViewController
        return controller
    }
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnTrack: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnClose: UIButton!
    
    var trackingEnable: Bool = false
    var trackerId: String?
    var location: CLLocationCoordinate2D!
    var trackerLocation: CLLocationCoordinate2D?
    var gmData: GMData?
    var googleMap = GoogleMap()
    var bounds: GMSCoordinateBounds?
    var mapMoved: Bool = false
    var locationUpdateBufferCount = 5
    var isLocationShared: Bool = false
    var isLocationFetched: Bool = false
    var timer : Timer?
    var messageId: String?
    var isPathFetched: Bool = false
    var trackingStopHandler: (()->())? = nil
    var trackOnly: Bool = false
    var delay = 0
    var updateLocationtimer : Timer?
    var locationBuffer: [LTInformation] = []
     var locationManager:CLLocationManager?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        self.configureDefaultMapView()
        self.btnTrack.isHidden = true
        self.bottomViewConstraint.constant = 210
        if trackOnly {
            self.updateBottomViewConstraint(180)
        }
    }
    
    func configureDefaultMapView() {
        let url = Bundle.main.url(forResource: "style", withExtension: "json")
        googleMap.configureMap(on: self, mapStyleURL: url, coordinate: location)
        googleMap.addMap(on: self.view)
    }
    
    // MARK: Get Directions
    func getDirections(meetPoint: CLLocationCoordinate2D, trackerLocation: CLLocationCoordinate2D, completion: @escaping (GMData)->()) {
        if isPathFetched { return }
        self.isPathFetched = true
        GMServices.directions(source: meetPoint, destination: trackerLocation, key: Constants.kGOOGLE_API_KEY,wayPoint: googleMap.wayPoint) { (data, error) in
            DispatchQueue.main.async {
                self.isPathFetched = false
                if let gmData = data {
                    completion(gmData)

                } else {
                    self.showAlert(title: "Route Failed".localized, message: error!.localizedDescription)
                }
            }
        }
    }
    
    // update map view
    func updateMapView(_ data: GMData)  {
        
        bounds = GMSCoordinateBounds(coordinate: data.startLocation!.coordinate!, coordinate: data.endLocation!.coordinate!)
        googleMap.moveCamera(for: bounds!, insets: UIEdgeInsets.init(top: 120, left: 120, bottom: 120, right: 120))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            
            self.addMarkers(data.endLocation?.coordinate, data.startLocation?.coordinate)
            self.googleMap.drawRoute(data.points, completion: { success in
                if success {

                    self.googleMap.moveCamera(for: self.bounds!, insets: UIEdgeInsets.init(top: 180, left: 120, bottom: 180, right: 120))
                    self.addDistance(data.distance ?? "0")
                    self.addDuration(data.time ?? "0")
                    self.updateBottomViewConstraint(30)
                    //                    if GMSGeometryDistance(data.startLocation!.coordinate!, data.endLocation!.coordinate!) < 15 {
                    //                        self.finishTrip()
                    //                    } else {
                    //                        self.updateBottomViewConstraint(30)
                    //                    }
                } else {
                    self.showAlert(title: "Direction Error".localized, message: "No Directions are available for this location".localized)

                }
            })
        }
    }
    
    func addMarkers(_ origin: CLLocationCoordinate2D?, _ destination: CLLocationCoordinate2D?) {
        
        let pin = UIImage(named: "myLocationPin")
        let pulse = UIImage(named: "map_marker_pulse")

        try? googleMap.addMarker(on: origin, image: pin)
        try? googleMap.addMarker(on: destination, image: pulse, center: true)
    }
    
    func finishTrip() {

        if trackOnly {

            self.trackingEnable = false
            let message = isLocationShared ? "Car has been arrived to your place".localized : "You have reached to your destination".localized
            let alert = UIAlertController(title: "Tracking Finished".localized, message: message, preferredStyle: .alert)
            let action = UIAlertAction.init(title: "OK".localized, style: .default) { _ in
                self.navigationController?.popViewController(animated: true)
            }
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)

        }else
        {
            self.trackingEnable = false

            let message = isLocationShared ? "Car has been arrived to your place".localized : "You have reached to your destination".localized
            let alert = UIAlertController(title: "Tracking Finished".localized, message: message, preferredStyle: .alert)
            let action = UIAlertAction.init(title: "Continue Chat".localized, style: .default) { _ in
                self.changeTrackingStatus()
            }
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func changeTrackingStatus() {
        self.showLoaderWithTitle(title: "Stop tracking...".localized)
        let query = ["message_id": messageId!,
                     "message_status": "1"]
        WebService.updateMessageStatus(queryItems: query) { response in
            DispatchQueue.main.async {
                self.hideLoaderWithTitle()
                switch response {
                case .success(_):
                    self.trackingStopHandler?()
                    self.dismiss(animated: true, completion: nil)
                case .failure(let error):
                    self.showAlert(error.message)
                }
            }
        }
    }
    
    // add makers and draw route
    func addMarkersAndDrawPath(_ data: GMData, completion: @escaping (Bool)->()) {
        googleMap.drawRoute(data.points, completion: completion)

    }
    
    // add duration
    func addDuration(_ duration: String) {
        var attributes = [NSAttributedString.Key.font:
            UIFont(name: "Helvetica", size: 13.0)!]
        let attributedString1 = NSMutableAttributedString(string: "Estimate time to reach: ".localized, attributes: attributes)
        attributes = [NSAttributedString.Key.font:
            UIFont(name: "Helvetica-Bold", size: 13.0)!]
        let attributedString2 = NSAttributedString(string: duration, attributes: attributes)
        attributedString1.append(attributedString2)
        self.lblTime.attributedText = attributedString1
    }
    
    // add distance 
    func addDistance(_ distance: String) {
        var attributes = [NSAttributedString.Key.font:
            UIFont(name: "Helvetica-Bold", size: 15.0)!]
        let attributedString1 = NSMutableAttributedString(string: distance, attributes: attributes)
        attributes = [NSAttributedString.Key.font:
            UIFont(name: "Helvetica", size: 15.0)!]
        let attributedString2 = NSAttributedString(string: " Distance".localized, attributes: attributes)
        attributedString1.append(attributedString2)
        self.lblDistance.attributedText = attributedString1
    }
    
    // add duration
    func locationName(_ name: String) {
        var attributes = [NSAttributedString.Key.font:
            UIFont(name: "Helvetica", size: 13.0)!]
        let attributedString1 = NSMutableAttributedString(string: "Location: ".localized, attributes: attributes)
        attributes = [NSAttributedString.Key.font:
            UIFont(name: "Helvetica-Bold", size: 13.0)!]
        let attributedString2 = NSAttributedString(string: name, attributes: attributes)
        attributedString1.append(attributedString2)
        self.lblTime.attributedText = attributedString1
    }
    
    @objc func getLocationName() {
        if self.trackerLocation != nil {
            GMSGeocoder().reverseGeocodeCoordinate(self.trackerLocation!) { (response, error) in
                guard let address = response?.firstResult() else {
                    return
                }
                let name = address.subLocality ?? ""
                let city = address.locality ?? ""
                let administrativeArea = address.administrativeArea ?? ""
                let location = "\(name + " " + city + " " + administrativeArea)"
                print(location)
                self.locationName(location)
                self.updateBottomViewConstraint(110)
                self.lineView.isHidden = true
                self.lblDistance.isHidden = true
                self.delay = 10
            }
        }
    }
    
    fileprivate func updateBottomViewConstraint(_ value: CGFloat) {
        bottomViewConstraint.constant = value
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func moveDown(_ sender: UIButton) {
        let moveDown = bottomViewConstraint.constant == 30
        sender.transform = moveDown ? CGAffineTransform(rotationAngle: CGFloat(Double.pi)) : CGAffineTransform.init(rotationAngle: CGFloat(0))
        bottomViewConstraint.constant = moveDown ? 155 : 30
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.4, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func close(_ sender: UIButton) {
        googleMap.clearMemory()
        LTCoordinator.sharedInstance().removeObserver()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func moveToCurrentPosition(_ sender: UIButton) {
        if trackOnly {
            self.googleMap.moveCameraToDriver()
        } else {
            googleMap.animateCamera(for: bounds!, insets: UIEdgeInsets(top: 120, left: 120, bottom: 120, right: 120))
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.btnTrack.isHidden = true
        }
    }
    
    @IBAction func track(_ sender: UIButton) {
        
        guard let driverLocation = trackerLocation else {
            return
        }
        
        TrackList.addUser(messageId!)
        
        bounds = GMSCoordinateBounds(coordinate: location, coordinate: driverLocation)
        googleMap.moveCamera(for: bounds!, insets: UIEdgeInsets.init(top: 120, left: 120, bottom: 160, right: 120))
        
        self.trackingEnable = true
        self.updateBottomViewConstraint(98)
        
        self.showLoader()
        self.getDirections(meetPoint: location, trackerLocation: driverLocation) { gmdata in
            DispatchQueue.main.async {
                self.googleMap.clearMap()
                self.googleMap.configureUpdateCar(on: driverLocation)
                self.hideLoader()
                self.updateRoute(gmdata)
            }
        }
    }
    
    func updateRoute(_ gmdata: GMData?) {
        
        guard let data = gmdata else {
            return
        }
        
        googleMap.clearPolyline()
        googleMap.clearMarkers()
        let sourceImage = UIImage(named: "map_marker_pulse")
        let coordinate = data.startLocation?.coordinate
        try? googleMap.addMarker(on: coordinate, image: sourceImage, animation: false, center: true)
        
        bounds = GMSCoordinateBounds(coordinate: data.startLocation!.coordinate!, coordinate: data.endLocation!.coordinate!)
        
        googleMap.drawRoute(data.points, animatePath: false, completion: { _ in
            self.addDistance(data.distance ?? "0")
            self.addDuration(data.time ?? "0")
            if self.bottomViewConstraint.constant == 210 {
                self.updateBottomViewConstraint(98)
            }
        })
    }
    
    func updatePathInfo(driverLocation: CLLocationCoordinate2D) {

        
        if GMSGeometryDistance(location, driverLocation) < 15 {
            self.finishTrip()
        } else {
            if locationUpdateBufferCount == 5 {
                self.locationUpdateBufferCount = 0
                googleMap.emptyCoordinateBuffer()
                print("---getDirections api----")
                self.getDirections(meetPoint: location, trackerLocation: driverLocation) { gmdata in
                    DispatchQueue.main.async {
                        self.googleMap.configureUpdateCar(on: driverLocation)
                        self.updateRoute(gmdata)
                        if self.btnTrack.isHidden {
                            self.bounds = GMSCoordinateBounds(coordinate: self.location, coordinate: driverLocation)
                            self.googleMap.moveCamera(for: self.bounds!, insets: UIEdgeInsets.init(top: 120, left: 120, bottom: 120, right: 120))
                        }
                    }
                }
            }
            else {
                locationUpdateBufferCount += 1
                googleMap.updatePolyline(driverLocation)
            }
        }
    }
    
    var getLatCout = 0
   
}

extension LTMapViewController: LTCoordinatorDelegate {
    
    func currentLocation(coordinate: CLLocationCoordinate2D?) {
        
        self.trackerLocation = coordinate
        print(self.trackerLocation)
        
        if let origin = coordinate, self.trackingEnable, !self.isLocationShared {
            self.updatePathInfo(driverLocation: origin)
        }
    }
    
    // update location 
    func updateLocation(info: LTInformation) {
        
        if self.isLoaderAnimating {
            self.hideLoader()
        }
        
        self.timer?.invalidate()
        self.trackerLocation = info.coordinate
        
        self.locationBuffer.append(info)
        
        /*if !(self.updateLocationtimer!.isValid) {
             self.updateLocationtimer?.fire()
             self.getLocationName()
        }*/
       
        if info.locationEnabled {
            if trackOnly {
                if let location = info.coordinate {
                    if locationBuffer.count <  2 {
                        self.locationBuffer.removeAll()
                        self.googleMap.configureUpdateCar(on: location, moveCamera: true)
                    } else {
                        self.locationBuffer.removeAll()
                    }
                    self.getLocationName()
                    guard updateLocationtimer == nil else {
                        return
                    }
                    self.updateLocationtimer = Timer.scheduledTimer(timeInterval: 10 , target: self, selector: #selector(getLocationName), userInfo: nil, repeats: true)
                }
            } else {
                if !isLocationFetched {
                    self.isLocationFetched = true
                    self.getDirections(meetPoint: location, trackerLocation: info.coordinate!) {
                        self.updateMapView($0)
                    }
                }
                if let origin = info.coordinate, trackingEnable, isLocationShared  {
                    self.updatePathInfo(driverLocation: origin)
                }
            }
        } else {
           // self.showAlert(title: "Location Update Failed", message: "Location services are disabled by other user.\nPlease ask to enable the location.")
        }
    }
    
    func showAlert(title: String? = nil, message: String) {
        let controller = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok".localized, style: .cancel, handler: nil)
        controller.addAction(action)
        self.present(controller, animated: true, completion: nil)
    }
}

extension LTMapViewController: GoogleMapDelegate {
    
    func didEndTileRendering() {
        
        self.activityIndicator.stopAnimating()
        
        if trackingEnable {
            self.isLocationFetched = true
            self.btnTrack.isHidden = true
        }
        
        LTCoordinator.sharedInstance().addDelegate(self)
        
        if isLocationShared {
            self.showLoader()
            try? LTCoordinator.sharedInstance().addObserver(for: trackerId)
            timer = Timer.scheduledTimer(withTimeInterval: 20, repeats: false, block: { timer in
                timer.invalidate()
                if self.isLoaderAnimating {
                    self.hideLoader()
                }
                self.showAlert(title: "Location Update Failed".localized, message: "Location services are disabled by other user.Please ask to enable the location.".localized)
                
                
            })
        }
        else {
            if let currentCoordinates = LTCoordinator.sharedInstance().lastLocation {

                self.trackerLocation = currentCoordinates
                if !trackingEnable {
                    self.getDirections(meetPoint: location, trackerLocation: currentCoordinates) {
                        self.updateMapView($0)
                    }
                } else {
                    bounds = GMSCoordinateBounds(coordinate: location, coordinate: currentCoordinates)
                    googleMap.moveCamera(for: bounds!, insets: UIEdgeInsets.init(top: 120, left: 120, bottom: 120, right: 120))
                    self.updatePathInfo(driverLocation: currentCoordinates)
                }
            } else {
                self.showAlert(title: "Location Update Failed".localized, message: "Your location services are disabled.\nPlease enable the services from settings.".localized)
                
            }
        }
    }
    
    func willMove() {
        if self.btnTrack.isHidden && trackingEnable {
            self.btnTrack.isHidden = false
        }
    }
}
