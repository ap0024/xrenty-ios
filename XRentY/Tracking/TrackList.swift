//
//  TrackList.swift
//  XRentY
//
//  Created by user on 07/02/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation

class TrackList {
    
    class var users: [String] {
        return UserDefaults.standard.value(forKey: "start_tracking_list") as? [String] ?? []
    }
    
    class func addUser(_ id: String) {
        if var trackList = UserDefaults.standard.value(forKey: "start_tracking_list") as? [String]  {
            trackList.append(id)
            UserDefaults.standard.set(trackList, forKey: "start_tracking_list")
        }
        else {
            let list = [id]
            UserDefaults.standard.set(list, forKey: "start_tracking_list")
        }
    }
    
    class func removeAllUsers() {
        UserDefaults.standard.set([], forKey: "start_tracking_list")
    }
    
    class func contains(_ user: String) -> Bool {
        if let trackList = UserDefaults.standard.value(forKey: "start_tracking_list") as? [String]  {
            return trackList.contains(user)
        } else {
            return false
        }
    }
}
