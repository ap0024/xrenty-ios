//
//  GMService.swift
//  XRentY
//
//  Created by user on 24/01/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation
import CoreLocation

enum GMError: Error {
    
    case invalidWayPoint
    case noDirectionFound
    case invaidDirectionURL
    case responseError(String)
    case noInternet
    case noRouteFound
    
    var localizedDescription: String {
        switch self {
        case .invalidWayPoint:
            return "Unable to find the direction from your location to destination".localized
        case .noDirectionFound:
             return "No directions available.".localized
        case .invaidDirectionURL:
            return "There is a problem in retrieving directions may be due to some technical issue.\nPlease try again later".localized
        case .noInternet:
            return "No internet connection".localized
        case .responseError(let error):
            return error
        case .noRouteFound:
            return "No active route is found".localized
        }
    }
}

struct GMPoint {
    
    let lat: Double?
    let long: Double?
    
    var coordinate: CLLocationCoordinate2D? {
        if let lat = self.lat, let lng = self.long {
            return CLLocationCoordinate2D.init(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))
        }
        else {
            return nil
        }
    }
}

struct GMData {
    let points: String?
    let distance: String?
    let time: String?
    let startLocation: GMPoint?
    let endLocation: GMPoint?

}

//class ABD {
//
//
//    lazy var outStandingAmount: Int = {
//        let am = amount + 8
//        return am
//    }()
//
//    var amount : Int = 5
//}

class GMServices {
    
    func add() {
        
    }
    func add(x: Int) {
        
    }
    
    //weak var data: ABD? = nil
    
    typealias GMServiceData = (data: GMData?,error: GMError?)
    
    class func directions(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, key: String,wayPoint: String? = nil, completion:@escaping (GMServiceData) -> Void) {
        
//        print(wayPoint)
        
        if !(Reachability()!.isReachable) {
            completion((nil,GMError.noInternet))
            return
        }
        
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&key=\(key)"
        
//        if let point = wayPoint  {
//            urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&waypoints=via:\(point)&key=\(key)"
//        }
        
        if let url = URL(string: urlString) {
            
            URLSession.shared.dataTask(with: url, completionHandler: {
                (data, response, error) in
                do {
                    let jsonObject = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    if let routes = jsonObject["routes"] as? NSArray, routes.count > 0  {
                        let routeDict = routes[0] as! Dictionary<String, Any>
                        
                        let routeOverviewPolyline = routeDict["overview_polyline"] as? Dictionary<String, Any>
                        var points: String? = nil
                        var totalDistance: String? = nil
                        var totalDuration: String? = nil
                        var startPoint: GMPoint? = nil
                        var endPoint: GMPoint? = nil
                        
                        if let point = routeOverviewPolyline?["points"] as? String {
                            points = point
                        }
                        
                        if let legs = routeDict["legs"] as? NSArray, legs.count > 0 {
                            let leg = legs[0] as? Dictionary<String, Any>
                            if let distance = leg?["distance"] as? Dictionary<String, Any> {
                                totalDistance = distance["text"] as? String
                            }
                            if let duration = leg?["duration"] as? Dictionary<String, Any> {
                                totalDuration = duration["text"] as? String
                            }
                            if let distance = leg?["start_location"] as? Dictionary<String, Any> {
                                let latitude = distance["lat"] as? Double
                                let longitude = distance["lng"] as? Double
                                startPoint = GMPoint(lat: latitude, long: longitude)
                            }
                            if let distance = leg?["end_location"] as? Dictionary<String, Any> {
                                let latitude = distance["lat"] as? Double
                                let longitude = distance["lng"] as? Double
                                endPoint = GMPoint(lat: latitude, long: longitude)
                            }
                        }
                        
                        let gmdata = GMData(points: points, distance: totalDistance, time: totalDuration, startLocation: startPoint, endLocation: endPoint)
                        
                        completion((gmdata,nil))
                    }
                    else {
                        completion((nil,GMError.noRouteFound))
                    }
                }
                catch {
                    completion((nil,GMError.responseError(error.localizedDescription)))
                }
            }).resume()
            
        } else {
            completion((nil,GMError.invaidDirectionURL))
        }
    }
}
