//
//  Country.swift
//  XRentY
//
//  Created by user on 05/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class Country: NSObject, Decodable {
    
    var country_id: String = ""
    var iso2_code: String = ""
    var iso3_code: String = ""
    var name: String = ""
}

extension Country: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Country? {

        let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        guard data != nil else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let country = try decoder.decode(Country.self, from: data!)
            return country
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}
