//
//  Cart.swift
//  XRentY
//
//  Created by user on 24/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class Cart: NSObject, Decodable {
    
    var DiscountPrice: String {
        guard let price = discount else { return "N/A" }
        return "\(price.roundOff())"
    }
    
    var cars: [Car]?
    var grandtotal: String?
    var subTotal: String?
    var discount: String?
    var isdiscountBool : Int?
    var dipositAmount: String?
    var tax : String?
    var shipCost: String?
    var shipMethod: String?
    var couponApplied: String?
    var quoteId: String?
    var isVirtual: Bool?
    var quoteCount: Int?
    var customerHasAddress: Bool?
    var extraFee: String?
    var paymentMethod: String?
    var paymentStatus: Int?
    var paypal_deposit_order_id: String?
    
    var order_placed_from: String?
    var currencyCode: String?
    
    var isOrderFromWeb: Bool {
        return order_placed_from == "1"
    }
    var gold_amount : String?
    var platinum_amount : String?
    var gold_amount_deposit : String?
    var platinum_amount_deposit : String?
    
    
    var deposit_confirmed : Int?
    var type : String?
    
    // init decoder
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: DecodingKeys.self)
        print("cars")
        
        self.deposit_confirmed = try values.decodeIfPresent(Int.self, forKey: .deposit_confirmed) ?? 0
        print("deposit_confirmed")
        self.type = try values.decodeIfPresent(String.self, forKey: .type) ?? ""
        print("type")
        
        self.cars = (try! values.decodeIfPresent([Car].self, forKey: .products)) ?? []
        print("grandtotal")
        self.grandtotal = try values.decodeIfPresent(String.self, forKey: .grandtotal) ?? ""
        print("subTotal")
        self.subTotal = try values.decodeIfPresent(String.self, forKey: .subtotal) ?? ""
        print("discount")
        self.discount = try values.decodeIfPresent(String.self, forKey: .discount) ?? ""
        print("isdiscountBool")
        self.isdiscountBool = try values.decodeIfPresent(Int.self, forKey: .isdiscount) ?? 0
        print("dipositAmount")
        self.dipositAmount = try values.decodeIfPresent(String.self, forKey: .deposit_amount) ?? ""
        print("tax")
        self.tax = try values.decodeIfPresent(String.self, forKey: .tax) ?? ""
        print("shipCost")
        self.shipCost = try values.decodeIfPresent(String.self, forKey: .ship_cost) ?? ""
        print("shipMethod")
        self.shipMethod = try values.decodeIfPresent(String.self, forKey: .ship_method) ?? ""
        print("couponApplied")
        self.couponApplied = try values.decodeIfPresent(String.self, forKey: .coupon_applied) ?? ""
        print("quoteId")
        self.quoteId = try values.decodeIfPresent(String.self, forKey: .quote_id) ?? ""
        print("quoteCount")
        self.quoteCount = try values.decodeIfPresent(Int.self, forKey: .quote_count) ?? 0
        print("customerHasAddress")
        self.customerHasAddress = try values.decodeIfPresent(Bool.self, forKey: .cust_has_addr) ?? false
        print("isVirtual")
        self.isVirtual = try values.decodeIfPresent(Bool.self, forKey: .is_virtual) ?? false
        print("extraFee")
        self.extraFee = try values.decodeIfPresent(String.self, forKey: .extra_fee) ?? ""
        print("paymentMethod")
        self.paymentMethod = try values.decodeIfPresent(String.self, forKey: .payment_method)
        print("paymentStatus")
        self.paymentStatus = try values.decodeIfPresent(Int.self, forKey: .payment_status)
        print("paypal_deposit_order_id")
        
        self.gold_amount = try values.decodeIfPresent(String.self, forKey: .gold_amount)
        self.platinum_amount = try values.decodeIfPresent(String.self, forKey: .platinum_amount)
        
        self.gold_amount_deposit = try values.decodeIfPresent(String.self, forKey: .gold_amount_deposit)
        self.platinum_amount_deposit = try values.decodeIfPresent(String.self, forKey: .platinum_amount_deposit)

        self.paypal_deposit_order_id = try values.decodeIfPresent(String.self, forKey: .paypal_deposit_order_id)
        self.order_placed_from = try values.decodeIfPresent(String.self, forKey: .order_placed_from)
        self.currencyCode = try values.decodeIfPresent(String.self, forKey: .show_currency_deposit_amount)
        print("success")
    }
}

// Model Coding Keys
extension Cart {
    
    enum DecodingKeys: String, CodingKey {
        case products = "products"
        case grandtotal = "grandtotal"
        case subtotal = "subtotal"
        case discount = "discount"
        case isdiscount = "is_discount"
        case deposit_amount = "deposit_amount"
        case tax = "tax"
        case ship_cost = "ship_cost"
        case ship_method = "ship_method"
        case coupon_applied = "coupon_applied"
        case quote_id = "quote_id"
        case is_virtual = "is_virtual"
        case quote_count = "quote_count"
        case cust_has_addr = "cust_has_addr"
        case extra_fee = "extra_fee"
        case payment_method = "payment_method"
        case payment_status = "payment_status"
        case paypal_deposit_order_id = "paypal_deposit_order_id"
        case order_placed_from = "order_placed_from"
        case show_currency_deposit_amount = "show_currency_deposit_amount"
        case gold_amount = "gold_amount"
        case platinum_amount = "platinum_amount"
        case gold_amount_deposit = "gold_amount_deposit"
        case platinum_amount_deposit = "platinum_amount_deposit"
        case deposit_confirmed = "deposit_confirmed"
        case type = "type"
    }
}

extension Cart: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Cart? {
        print(dictionary)
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let cartResponse = try decoder.decode(Cart.self, from: data)
            return cartResponse
        }
        catch {
            print("unable to decode model: cart")
            return nil
        }
    }
}
