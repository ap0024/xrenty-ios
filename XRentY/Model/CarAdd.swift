//
//  AddCar.swift
//  XRentY
//
//  Created by user on 26/02/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation

final class CarAdd: NSObject, Decodable {

    var id: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decodeIfPresent(String.self, forKey: .cust_id) ?? ""
    }
}

// Model Coding Keys
extension CarAdd {
    
    enum CodingKeys: String, CodingKey {
        case cust_id = "id"
    }
}

extension CarAdd: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> CarAdd? {
        
        let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        guard data != nil else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(CarAdd.self, from: data!)
            return user
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}
