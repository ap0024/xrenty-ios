//
//  GetSearch.swift
//  XRentY
//
//  Created by user on 09/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class GetSearch: NSObject, Decodable {
    
    var address: String?
    var finaldisc: String?
    var image: String?
    var Wishlist: String?
    var instock: String?
    var name: String?
    var price: String?
    var pricehtml: String?
    let productid: String?
    var rating: String?
    var sku: String?
    var typeid: String?
}

extension GetSearch: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> GetSearch? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let search = try? decoder.decode(GetSearch.self, from: data)
        return search ?? nil
    }
}
