//
//  Profile.swift
//  XRentY
//
//  Created by user on 24/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class Profile : NSObject, Decodable {
    
    var imageURL: URL? {
        guard let urlString = image else { return nil }
        return URL(string: urlString)
    }
    
    var firstname: String?
    var lastname: String?
    var email: String?
    var telephone: String?
    var street: String?
    var city: String?
    var state: String?
    var iseditable : String?
    var pincode: String?
    var country: String?
    var image: String?
    var company: String?
    var accountHolderName: String?
    var accountNumber: String?
    var bank: String?
    var address : String?
    var countryName: String?
    var clabeAccountDetail : Int?
    var bankCode : String?
    var licenseNumber : String?
    var licencedate : String?
    var licenceFile : String?
    var customerLicenceDetail : Int?
    var current_currency : String?

    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        firstname = try? values.decodeIfPresent(String.self, forKey: .firstname) ?? ""
        lastname = try? values.decodeIfPresent(String.self, forKey: .lastname) ?? ""
        email = try? values.decodeIfPresent(String.self, forKey: .email) ?? ""
        telephone = try? values.decodeIfPresent(String.self, forKey: .telephone) ?? ""
        telephone = try? values.decodeIfPresent(String.self, forKey: .telephone) ?? ""
        street = try? values.decodeIfPresent(String.self, forKey: .street) ?? ""
        city = try? values.decodeIfPresent(String.self, forKey: .city) ?? ""
        state = try? values.decodeIfPresent(String.self, forKey: .state) ?? ""
        pincode = try? values.decodeIfPresent(String.self, forKey: .pincode) ?? ""
        country = try? values.decodeIfPresent(String.self, forKey: .country) ?? ""
        image = try? values.decodeIfPresent(String.self, forKey: .image) ?? ""
        company = try? values.decodeIfPresent(String.self, forKey: .company) ?? ""
        iseditable = try? values.decodeIfPresent(String.self, forKey: .iseditable) ?? ""
        accountHolderName = try? values.decodeIfPresent(String.self, forKey: .accountHolderName) ?? ""
        accountNumber = try? values.decodeIfPresent(String.self, forKey: .accountNumber) ?? ""
        bank = try? values.decodeIfPresent(String.self, forKey: .bank) ?? ""
        address = try? values.decodeIfPresent(String.self, forKey: .address) ?? ""
        countryName = try? values.decodeIfPresent(String.self, forKey: .countryname) ?? ""
        clabeAccountDetail = try? values.decodeIfPresent(Int.self, forKey: .clabeAccountDetail) ?? 0
        bankCode = try? values.decodeIfPresent(String.self, forKey: .bankCode) ?? ""
        licenseNumber = try? values.decodeIfPresent(String.self, forKey: .licenseNumber) ?? ""
        licencedate = try? values.decodeIfPresent(String.self, forKey: .licencedate) ?? ""
        licenceFile = try? values.decodeIfPresent(String.self, forKey: .licenceFile) ?? ""
        customerLicenceDetail = try? values.decodeIfPresent(Int.self, forKey: .customerLicenceDetail) ?? 0
        current_currency = try? values.decodeIfPresent(String.self, forKey: .current_currency) ?? ""
    }
}

// Model Coding Keys
extension Profile {
    
    enum CodingKeys: String, CodingKey {
        case firstname = "firstname"
        case lastname = "lastname"
        case email = "email"
        case telephone = "telephone"
        case street = "street"
        case city = "city"
        case state = "state"
        case pincode = "pincode"
        case country = "country"
        case image = "image"
        case company = "company"
        case iseditable = "is_editable"
        case accountHolderName = "account_holder_name"
        case accountNumber = "account_number"
        case bank = "bank"
        case address = "address"
        case countryname = "country_name"
        case clabeAccountDetail = "clabe_account_detail"
        case bankCode = "bank_code"
        case licenceFile = "licence_file"
        case licencedate = "licencedate"
        case licenseNumber = "license_number"
        case customerLicenceDetail = "customer_licence_detail"
        case current_currency = "current_currency"
    }
}

extension Profile: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Profile? {
        
        let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        guard data != nil else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(Profile.self, from: data!)
            return user
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}
