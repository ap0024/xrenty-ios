//
//  Type.swift
//  XRentY
//
//  Created by user on 05/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation


final class CarType: NSObject, Decodable {
    
    var value: String = ""
    var label: String = ""
    
    init(_ name: String) {
        // initailization
        self.label = name
    }
}

extension CarType: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> CarType? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }

        let decoder = JSONDecoder()
        let category = try? decoder.decode(CarType.self, from: data)
        print(category?.value ?? "no value")
        return category ?? nil
    }
}
