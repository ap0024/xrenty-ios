//
//  CarDetail.swift
//  XRentY
//
//  Created by user on 03/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class CarDetail: NSObject, Decodable {
    
    var id: String?
    var quoteId: String?
    var quoteCount: String?
    var message: String?
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decodeIfPresent(String.self, forKey: .cust_id) ?? ""
        message = try? values.decodeIfPresent(String.self, forKey: .message) ?? ""
        quoteId = try? values.decodeIfPresent(String.self, forKey: .message) ?? ""
        quoteCount = try? values.decodeIfPresent(String.self, forKey: .message) ?? ""
    }
}

// Model Coding Keys
extension CarDetail {
    
    enum CodingKeys: String, CodingKey {
        case cust_id = "cust_id"
        case message = "message"
        case quote_count = "quote_count"
        case quote_id = "quote_id"
    }
}

extension CarDetail: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> CarDetail? {
        print(dictionary)
        let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        guard data != nil else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(CarDetail.self, from: data!)
            return user
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}
