//
//  User.swift
//  XRentY
//
//  Created by user on 25/06/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation

final class User: NSObject, Decodable {
    
    var imageURL: URL? {
        guard let urlString = image else { return nil }
        return URL(string: urlString)
    }
        
    var id: String?
    var quoteId: String?
    var quoteCount: String?
    var token: String?
    var isSeller: Int?
    var userName: String?
    var image : String?
    var openpayId: String?
    var clabeAccount : Int?
    var customerLicenceDetail : Bool?
    
    init(from decoder: Decoder) throws {

        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decodeIfPresent(String.self, forKey: .cust_id) ?? "" 
        quoteId = try? values.decodeIfPresent(String.self, forKey: .quote_id) ?? ""
        quoteCount = try? values.decodeIfPresent(String.self, forKey: .quote_count) ?? ""
        token = try? values.decodeIfPresent(String.self, forKey: .token) ?? ""
        isSeller = try? values.decodeIfPresent(Int.self, forKey: .isSeller) ?? 0
        clabeAccount = try? values.decodeIfPresent(Int.self, forKey: .clabeAccountDetail) ?? 0
        userName = try? values.decodeIfPresent(String.self, forKey: .userName) ?? ""
        image = try? values.decodeIfPresent(String.self, forKey: .image) ?? ""
        openpayId = try? values.decodeIfPresent(String.self, forKey: .openpayId) ?? ""
        customerLicenceDetail = try? values.decodeIfPresent(Bool.self, forKey: .customerLicenceDetail) ?? false
    }
}

// Model Coding Keys
extension User {
    
    enum CodingKeys: String, CodingKey {
        case cust_id = "customer_id"
        case quote_count = "quote_count"
        case quote_id = "quote_id"
        case token = "token"
        case isSeller = "is_seller"
        case userName = "name"
        case image = "image"
        case openpayId = "openpay_id"
        case clabeAccountDetail = "clabe_account_detail"
        case customerLicenceDetail = "customer_licence_detail"
    }
}

extension User: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> User? {
        
        let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        guard data != nil else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(User.self, from: data!)
            return user
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}
