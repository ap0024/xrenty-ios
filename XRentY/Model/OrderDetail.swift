//
//  OrderDetail.swift
//  XRentY
//
//  Created by user on 25/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class orderDetail : NSObject, Decodable {
    
    var imageURL: URL? {
        guard let urlString = image else { return nil }
        return URL(string: urlString)
    }
    
    var orderTotal: String {
        guard let price = OrderTotal else { return "N/A" }
        return "\(price.roundOff())"
    }
    
    var OrderTotal: String?
    var billingAddress: String?
    var bookingSubtotal: String?
    var customerEmail: String?
    var customerFirstname: String?
    var customerLastname: String?
    var deposit: String?
    var finalPrice: String?
    var image: String?
    var orderId: String?
    var orderStatus: String?
    var pickupBool: Int?
    var pickupDate: String?
    var productId: String?
    var productName: String?
    var returnBool: Int?
    var returnDate: String?
    var rmaId: String?
    var telephone: String?
    var unitPrice: String?
    var discountAmount : String?
    var extraFee : String?
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.OrderTotal = try? values.decodeIfPresent(String.self, forKey: .OrderTotal) ?? "0.00"
        self.billingAddress = try? values.decodeIfPresent(String.self, forKey: .billingAddress) ?? "N/A"
        self.bookingSubtotal = try? values.decodeIfPresent(String.self, forKey: .bookingSubtotal) ?? "0.00"
        self.customerEmail = try? values.decodeIfPresent(String.self, forKey: .customerEmail) ?? "N/A"
        self.customerFirstname = try? values.decodeIfPresent(String.self, forKey: .customerFirstname) ?? "N/A"
        self.customerLastname = try? values.decodeIfPresent(String.self, forKey: .customerLastname) ?? "N/A"
        self.deposit = try! values.decodeIfPresent(String.self, forKey: .deposit) ?? "0.00"
        self.finalPrice = try? values.decodeIfPresent(String.self, forKey: .finalPrice) ?? "0.00"
        self.image = try? values.decodeIfPresent(String.self, forKey: .image) ?? ""
        self.orderId = try! values.decodeIfPresent(String.self, forKey: .orderId) ?? ""
        self.orderStatus = try? values.decodeIfPresent(String.self, forKey: .orderStatus) ?? "N/A"
        self.pickupBool = try! values.decodeIfPresent(Int.self, forKey: .pickupBool) ?? nil
        self.returnBool = try! values.decodeIfPresent(Int.self, forKey: .returnBool) ?? nil
        self.pickupDate = try? values.decodeIfPresent(String.self, forKey: .pickupDate) ?? "N/A"
        self.productId = try values.decodeIfPresent(String.self, forKey: .productId) ?? ""
        self.productName = try? values.decodeIfPresent(String.self, forKey: .productName) ?? "N/A"
        self.returnDate = try? values.decodeIfPresent(String.self, forKey: .returnDate) ?? "N/A"
        self.rmaId = try values.decodeIfPresent(String.self, forKey: .rmaId) ?? ""
        self.telephone = try values.decodeIfPresent(String.self, forKey: .telephone) ?? nil
        self.unitPrice = try? values.decodeIfPresent(String.self, forKey: .unitPrice) ?? "N/A"
        self.discountAmount = try? values.decodeIfPresent(String.self, forKey: .discountAmount) ?? ""
        self.extraFee = try? values.decodeIfPresent(String.self, forKey: .extraFee) ?? ""
    }
}

// Model Coding Keys
extension orderDetail {
    
    enum CodingKeys: String, CodingKey {
        case OrderTotal = "Order_total"
        case billingAddress = "billing_address"
        case bookingSubtotal = "booking_subtotal"
        case customerEmail = "customer_email"
        case customerFirstname = "customer_firstname"
        case customerLastname = "customer_lastname"
        case deposit = "deposit"
        case finalPrice = "final_price"
        case image = "image"
        case orderId = "order_id"
        case orderStatus = "order_status"
        case pickupBool = "pickup_bool"
        case pickupDate = "pickup_date"
        case productId = "product_id"
        case productName = "product_name"
        case returnBool = "return_bool"
        case returnDate = "return_date"
        case rmaId = "rma_id"
        case telephone = "telephone"
        case unitPrice = "unit_price"
        case discountAmount = "discount_amount"
        case extraFee = "extra_fee"
    }
}

extension orderDetail: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> orderDetail? {
        print(dictionary)
        let test =  dictionary["deposit"]
        print(type(of: test))
        let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        guard data != nil else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(orderDetail.self, from: data!)
            return user
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}
