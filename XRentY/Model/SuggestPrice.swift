
//
//  SuggestPrice.swift
//  XRentY
//
//  Created by user on 11/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation
final class SuggestPrice : NSObject , Decodable{
    var price: String?
    var specialPrice: String?
   
    
    
    init(from decoder: Decoder) throws{
        let values = try decoder.container(keyedBy: CodingKeys.self)
        price = try? values.decodeIfPresent(String.self, forKey: .price) ?? ""
        specialPrice = try? values.decodeIfPresent(String.self, forKey: .specialPrice) ?? ""
    }
    
}
extension SuggestPrice{
    enum CodingKeys: String, CodingKey{
        case price = "price"
        case specialPrice = "special_price"
       
    }
}
extension SuggestPrice : Parceable{
    static func parseObject(dictionary: [String : AnyObject]) -> SuggestPrice? {
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse")
            return nil
        }
        let decoder = JSONDecoder()
        let category = try? decoder.decode(SuggestPrice.self, from: data)
        return category ?? nil
    }
}

