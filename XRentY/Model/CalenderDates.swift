final class CalenderDates : NSObject {
    
    var data: [String:Any]?
    
    override init() {
        // initialization
    }
}

extension CalenderDates: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> CalenderDates? {
        let category = CalenderDates()
        category.data = dictionary
        return category
    }
}
