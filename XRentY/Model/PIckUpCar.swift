//
//  PIckUpCar.swift
//  XRentY
//
//  Created by user on 17/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class PickUpCar: NSObject, Decodable {
    var rma_id: String = ""
}

extension PickUpCar: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> PickUpCar? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let category = try? decoder.decode(PickUpCar.self, from: data)
        return category ?? nil
    }
}
