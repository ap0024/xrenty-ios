//
//  City.swift
//  XRentY
//
//  Created by user on 11/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class City: NSObject, Decodable {
    
    var value: String = ""
    var label: String = ""
}

extension City: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> City? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let make = try? decoder.decode(City.self, from: data)
        return make ?? nil
    }
}
