//
//  Year.swift
//  XRentY
//
//  Created by user on 05/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation


final class Year: NSObject, Decodable {
    
    var value: String = ""
    var label: String = ""
    
    init(_ name: String) {
        // initailization
        self.label = name
    }
}

extension Year: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Year? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let category = try? decoder.decode(Year.self, from: data)
        print(category?.value ?? "no value")
        return category ?? nil
    }
}
