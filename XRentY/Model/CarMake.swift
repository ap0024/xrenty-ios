//
//  CarMake.swift
//  XRentY
//
//  Created by user on 04/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class CarMake: NSObject, Decodable {
    
    var value: String = ""
    var label: String = ""
    
    init(_ name: String) {
        // initailization
        self.label = name
    }
}

extension CarMake: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> CarMake? {
            
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let make = try? decoder.decode(CarMake.self, from: data)
        return make ?? nil
    }
}
