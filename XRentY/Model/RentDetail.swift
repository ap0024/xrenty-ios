//
//  RentDetail.swift
//  XRentY
//
//  Created by user on 15/04/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation


final class RentDetail : NSObject, Decodable {
    
    
    var telephone: String?
    var customerEmail : String?
    var customerFirstname : String?
    var customerLastname : String?
    var orderId : String?
    var orderStatus : String?
    var customerLicenseDate : String?
    var customerlicenseNumber : String?
    var licencedoc : String?
    
    
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.customerEmail = try values.decodeIfPresent(String.self, forKey: .customerEmail) ?? nil
        self.customerFirstname = try values.decodeIfPresent(String.self, forKey: .customerFirstname) ?? nil
        self.customerLastname = try values.decodeIfPresent(String.self, forKey: .customerLastname) ?? nil
        self.orderId = try values.decodeIfPresent(String.self, forKey: .orderId) ?? nil
        self.orderStatus = try values.decodeIfPresent(String.self, forKey: .orderStatus) ?? nil
        self.customerLicenseDate = try values.decodeIfPresent(String.self, forKey: .customerLicenseDate) ?? nil
        self.customerlicenseNumber = try values.decodeIfPresent(String.self, forKey: .customerlicenseNumber) ?? nil
        self.telephone = try values.decodeIfPresent(String.self, forKey: .telephone) ?? nil
        self.licencedoc = try values.decodeIfPresent(String.self, forKey: .licencedoc) ?? ""
        
    }
}

// Model Coding Keys
extension RentDetail {
    
    enum CodingKeys: String, CodingKey {
        
        case customerEmail = "customer_email"
        case customerFirstname = "customer_firstname"
        case customerLastname = "customer_lastname"
        case orderId = "order_id"
        case orderStatus = "order_status"
        case customerLicenseDate = "customer_license_Date"
        case customerlicenseNumber = "customer_license_number"
        case telephone = "telephone"
        case licencedoc =  "licence_doc"
    }
}

extension RentDetail: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> RentDetail? {
        print(dictionary)
        let test =  dictionary["deposit"]
        print(type(of: test))
        let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        guard data != nil else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(RentDetail.self, from: data!)
            return user
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}
