//
//  Car.swift
//  XRentY
//
//  Created by user on 13/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import Foundation

extension String {
    
     func roundOff() -> String {
        guard let number = Double(self) else {
            return "00.0"
        }
        return String(format: "%.2f", number)
    }
}

final class Car: NSObject, Decodable {
    
    var imageURL: URL? {
        guard let urlString = image else { return nil }
        return URL(string: urlString)
    }
    
    var pricePerDay: String {
        guard let price = perDayPrice else { return "N/A" }
        return "\(price.roundOff())"
    }
    
    var promolessPrice: String {
        
        guard let price = promoPrice else { return "N/A" }
        if price == "" { return pricePerDay }
        return "\(price.roundOff())"
    }
    
    var Price: String {
        guard let price = price else { return "N/A" }
        return "\(price.roundOff())"
    }

    var name: String?
    var perDayPrice: String?
    var address: String?
    var discount: String?
    var promoPrice: String?
    var image: String?
    var wishlist: Int?
    var inStock: String?
    var priceHtml: String?
    var productId: String?
    var producturl : String?
    var ismycar : Int?
    var reviewCount : Int?
    var reviewNumber: String?
    var sellerId : String?
    var discountMsg : String?
    var ratingPercent : String?
    var starCount : Int?
    var id: String?
    var sku: String?
    var typeId: String?
    var detail: String?
    var images: [CarImage]?
    var latitude: String?
    var longitude: String?
    var sellerInfo:  SellerInfo?
    var attributes: [MainAttributes]?
    var price: String?
    var checkin: String?
    var checkout: String?
    var totalDays: String?
    var prodId: String?
    var itemId : String?
    var location : String?
    var approval : Int?
    var booking_lat : String?
    var booking_lon : String?
    var protectionFee : String?
    var subtotal : String?
    
    override init() {
        // initialization
    }

    
    init(_ url: String, name: String, price: String, address: String) {
        self.image = url
        self.name = name
        self.perDayPrice = price
        self.address = address
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try? values.decodeIfPresent(String.self, forKey: .name) ?? ""
        perDayPrice = try? values.decodeIfPresent(String.self, forKey: .perDayPrice) ?? "N/A"
        address = try? values.decodeIfPresent(String.self, forKey: .address) ?? ""
        discount = try? values.decodeIfPresent(String.self, forKey: .finaldisc) ?? ""
        sellerId = try? values.decodeIfPresent(String.self, forKey: .sellerId) ?? ""
        wishlist = try? values.decodeIfPresent(Int.self, forKey: .wishlist) ?? 0
        approval = try? values.decodeIfPresent(Int.self, forKey: .approval) ?? 0

        inStock = try? values.decodeIfPresent(String.self, forKey: .instock) ?? ""
        priceHtml = try? values.decodeIfPresent(String.self, forKey: .pricehtml) ?? ""
        productId = try? values.decodeIfPresent(String.self, forKey: .productid) ?? ""
        image = try? values.decodeIfPresent(String.self, forKey: .image) ?? ""
        ratingPercent = try? values.decodeIfPresent(String.self, forKey: .ratingPercent) ?? ""
        reviewCount = try? values.decodeIfPresent(Int.self, forKey: .reviewCount) ?? 0
        reviewNumber = try? values.decodeIfPresent(String.self, forKey: .numberReview) ?? "0"

        ismycar = try? values.decodeIfPresent(Int.self, forKey: .ismycar) ?? 0
        starCount = try? values.decodeIfPresent(Int.self, forKey: .starCount) ?? 0
        sku = try? values.decodeIfPresent(String.self, forKey: .sku) ?? ""
        typeId = try? values.decodeIfPresent(String.self, forKey: .typeid) ?? ""
        promoPrice = try? values.decodeIfPresent(String.self, forKey: .promoprice) ?? ""
        images = (try? values.decodeIfPresent([CarImage].self, forKey: .images)) ?? nil
        detail = try? values.decodeIfPresent(String.self, forKey: .detail) ?? ""
        latitude = try? values.decodeIfPresent(String.self, forKey: .lat) ?? ""
        longitude = try? values.decodeIfPresent(String.self, forKey: .long) ?? ""
        sellerInfo = (try? values.decodeIfPresent(SellerInfo.self, forKey: .sellerInfo)) ?? nil
        attributes = (try? values.decodeIfPresent([MainAttributes].self, forKey: .mainAttributes)) ?? nil
        id = try? values.decodeIfPresent(String.self, forKey: .id) ?? ""
        price = try? values.decodeIfPresent(String.self, forKey: .price) ?? ""
        checkin = try? values.decodeIfPresent(String.self, forKey: .checkin) ?? ""
        checkout = try? values.decodeIfPresent(String.self, forKey: .checkout) ?? ""
        totalDays = try? values.decodeIfPresent(String.self, forKey: .totaldays) ?? ""
        prodId = try? values.decodeIfPresent(String.self, forKey: .prodId) ?? ""
        itemId = try? values.decodeIfPresent(String.self, forKey: .itemId) ?? ""
        producturl = try? values.decodeIfPresent(String.self, forKey: .productUrl) ?? ""
        location = try? values.decodeIfPresent(String.self, forKey: .location) ?? ""
         booking_lat = try? values.decodeIfPresent(String.self, forKey: .booking_lat) ?? ""
         booking_lon = try? values.decodeIfPresent(String.self, forKey: .booking_lon) ?? ""
         subtotal = try? values.decodeIfPresent(String.self, forKey: .carttotal) ?? ""
        discountMsg = try? values.decodeIfPresent(String.self, forKey: .discountMsg) ?? ""
        protectionFee = try? values.decodeIfPresent(String.self, forKey: .extraFee) ?? ""
    }
}

// Model Coding Keys
extension Car {
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case perDayPrice = "per_day_price"
        case address = "address"
        case finaldisc = "final_disc"
        case sellerId = "seller_id"
        case promoprice = "promo_price"
        case image = "image"
        case wishlist = "inWishlist"
        case instock = "in_stock"
        case pricehtml = "price_html"
        case productid = "product_id"
        case productUrl = "product_url"
        case location = "location"
        case ratingPercent = "rating_percent"
        case reviewCount = "review_count"
        case numberReview = "reviewsCount"
        case starCount = "star_count"
        case ismycar = "is_my_car"
        case sku = "sku"
        case typeid = "type_id"
        case images = "images"
        case detail = "description"
        case lat = "lat"
        case long = "long"
        case sellerInfo = "seller_info"
        case mainAttributes = "main_attributes"
        case id = "id"
        case price = "price"
        case checkin = "checkin"
        case checkout = "checkout"
        case totaldays = "totaldays"
        case prodId = "prod_Id"
        case itemId = "item_id"
        case approval = "Approval"
        case booking_lat = "booking_lat"
        case booking_lon = "booking_lon"
        case discountMsg = "discount_msg"
        case carttotal = "subTotal"
        case extraFee = "extra_fee"
    }
}

extension Car: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Car? {
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        let decoder = JSONDecoder()
        let car = try? decoder.decode(Car.self, from: data)
        return car ?? nil
    }
}

/*extension Car {
    static var list : [Car] = [Car("https://images.pexels.com/photos/50704/car-race-ferrari-racing-car-pirelli-50704.jpeg?auto=compress&cs=tinysrgb&h=350", name: "Audi A1", price: "US$200.00", address: "Sector 43 Chadigarh India"),
                               Car("https://stimg.cardekho.com/images/carexteriorimages/360x240/Maruti/Maruti-Swift/047.jpg", name: "Audi A2", price: "US$200.00", address: "Karvenagar, karnvenagar karvenagar karvenagar"),
                               Car("https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/body-image/public/range-rover_0.jpg?itok=CXm1nSek", name: "Audi A3", price: "US$200.00", address: "Karvenagar, karnvenagar karvenagar karvenagar"),
                               Car("https://jaguar.ssl.cdn.sdlmedia.com/636567329476144975NH.jpg?v=45", name: "Audi A4", price: "US$200.00", address: "Karvenagar, karnvenagar karvenagar karvenagar"),
                               Car("https://stimg.cardekho.com/images/carexteriorimages/360x240/Maruti/Maruti-Swift/047.jpg", name: "Audi A5", price: "US$200.00", address: "Karvenagar, karnvenagar karvenagar karvenagar"),
                               Car("https://images.pexels.com/photos/50704/car-race-ferrari-racing-car-pirelli-50704.jpeg?auto=compress&cs=tinysrgb&h=350", name: "Audi A6", price: "US$200.00", address: "Karvenagar, karnvenagar karvenagar karvenagar"),
                               Car("https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/body-image/public/range-rover_0.jpg?itok=CXm1nSek", name: "Audi A7", price: "US$200.00", address: "Karvenagar, karnvenagar karvenagar karvenagar")]
    
}*/

//MARK: car image
struct CarImage: Decodable {
    
    var name: String?
    var url: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .imgname) ?? ""
        url = try values.decodeIfPresent(String.self, forKey: .url) ?? ""
    }
    
    enum CodingKeys: String, CodingKey {
        case imgname = "imgname"
        case url = "url"
    }
}

//MARK: seller info
struct SellerInfo: Decodable {
    
    var image: String?
    var name: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        image = try values.decodeIfPresent(String.self, forKey: .image) ?? nil
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? nil
    }
    
    enum CodingKeys: String, CodingKey {
        case image = "seller_logo_image"
        case name = "seller_nmae"
    }
}

//MARK: main attributes
struct MainAttributes: Decodable {
    
    var label: String?
    var value: String?
    var code: String?
    
    func image() -> UIImage? {
        if code == "ac" {
            return #imageLiteral(resourceName: "ac_attribute")
        }
        if code == "passengers" {
            return #imageLiteral(resourceName: "passengers_attribute")
        }
        if code == "transmission" {
            if label == "Yes" {
                return #imageLiteral(resourceName: "transmissionManul_ic")
            }
            else {
                return #imageLiteral(resourceName: "transmissionAuto_ic")
            }
        }
        if code == "brand_make" {
            return #imageLiteral(resourceName: "brand_make_attribute")
        }
        if code == "sound_systems" {
            return #imageLiteral(resourceName: "sound_attribute")
        }
        if code == "engine_type" {
            if label == "Electric" {
               return #imageLiteral(resourceName: "EngineE_ic")
            }
            if label == "Gasoline" {
                return #imageLiteral(resourceName: "EngineG_ic")
            }
            if label == "Hydrogen" {
                return #imageLiteral(resourceName: "EngineH_ic")
            }
            return #imageLiteral(resourceName: "engine_type_attribute")
        }
        return nil
    }
    
    mutating func labelName() -> String? {
        if code == "ac" {
          label = label == "Yes" ? "Air Conditioner".localized : "Non Air Conditioner".localized
        }
        if code == "passengers" {
            label = label != nil ?  "\(label!) " + "passengers".localized : "N/A"
        }
        if code == "transmission" {
            label = label == "Yes" ? "Manual Transmission".localized : "Automatic Transmission".localized
        }
        if code == "brand_make" {
            // same
        }
        if code == "sound_systems" {
            label = label == "Yes" ? "Sound System".localized : "No Sound System".localized
        }
        if code == "engine_type" {
            label = label != nil ?  "Engine".localized + "\(label!) " : "N/A"
        }
        return label
    }
}
