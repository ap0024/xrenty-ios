//
//  Engine.swift
//  XRentY
//
//  Created by user on 05/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation


final class Engine: NSObject, Decodable {
    
    var value: String = ""
    var label: String = ""
    
    init(_ name: String) {
        // initailization
        self.label = name
    }
}

extension Engine: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Engine? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let category = try? decoder.decode(Engine.self, from: data)
        print(category?.value ?? "no value")
        return category ?? nil
    }
}
