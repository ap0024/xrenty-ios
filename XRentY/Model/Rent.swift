//
//  Rent.swift
//  XRentY
//
//  Created by user on 17/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class Rent: NSObject, Decodable {
    
    var imageURL: URL? {
        guard let urlString = productImage else { return nil }
        return URL(string: urlString)
    }
    
    var billingAddress: String?
    var customerEmail: String?
    var customerName: String?
    var customerPhone: String?
    var grandTotal: String?
    var orderId: String?
    var pickupDate: String?
    var producQty: String?
    var productRowPrice: String?
    var productName: String?
    var productUnitPrice: String?
    var productImage: String?
    var returnDate: String?
    var status: String?
    var return_bool: Int?
    var charge: String?
    var chargeTransactionId: String?
    var deposit: String?
    var convertedCustomerDepositAmount : String?
    var depositTransactionId: String?
    var customerOpenpayId: String?
    var exchangeRate: String?
    var userId: String?
    var mybookingPickupStatus: Bool?
    var mybookingReturnStatus: Bool?
    var payment_method: String?
    var paypal_charge_order_id: String?
    var paypal_deposit_order_id: String?
    var order_placed_from: String?
    var currencyCode: String?
    
    var isOrderFromWeb: Bool {
        return order_placed_from == "1"
    }
    
    var depositeValue: String? {
        if let deposit = self.deposit, let double = Double(deposit) {
            let stringAmount = String(format: "%.2f", double)
            return stringAmount
        } else {
            return "0.00"
        }
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.billingAddress = try? values.decodeIfPresent(String.self, forKey: .billing_address) ?? ""
        self.customerEmail = try? values.decodeIfPresent(String.self, forKey: .customer_email) ?? ""
        self.customerName = try? values.decodeIfPresent(String.self, forKey: .customer_name) ?? ""
        self.customerPhone = try? values.decodeIfPresent(String.self, forKey: .customer_phone) ?? ""
        self.grandTotal = try? values.decodeIfPresent(String.self, forKey: .grand_total) ?? ""
        self.orderId = try? values.decodeIfPresent(String.self, forKey: .order_id) ?? ""
        self.pickupDate = try? values.decodeIfPresent(String.self, forKey: .pickup_date) ?? ""
        self.producQty = try? values.decodeIfPresent(String.self, forKey: .produc_qty) ?? ""
        self.productRowPrice = try? values.decodeIfPresent(String.self, forKey: .produc_row_price) ?? ""
        self.productName = try? values.decodeIfPresent(String.self, forKey: .produc_tname) ?? ""
        self.productUnitPrice = try? values.decodeIfPresent(String.self, forKey: .produc_unit_price) ?? ""
        self.productImage = try? values.decodeIfPresent(String.self, forKey: .product_image) ?? ""
        self.returnDate = try? values.decodeIfPresent(String.self, forKey: .return_date) ?? ""
        self.status = try? values.decodeIfPresent(String.self, forKey: .status) ?? ""
        self.return_bool = try values.decodeIfPresent(Int.self, forKey: .checketurn) ?? nil
        self.charge = try values.decodeIfPresent(String.self, forKey: .charge) ?? ""
        self.chargeTransactionId = try values.decodeIfPresent(String.self, forKey: .chargeTransactionId) ?? ""
        self.deposit = try values.decodeIfPresent(String.self, forKey: .deposit) ?? ""
        self.depositTransactionId = try values.decodeIfPresent(String.self, forKey: .depositTransactionId) ?? ""
        self.customerOpenpayId = try values.decodeIfPresent(String.self, forKey: .customerOpenpayId) ?? ""
        self.exchangeRate = try values.decodeIfPresent(String.self, forKey: .exchangeRate) ?? ""
        self.convertedCustomerDepositAmount = try values.decodeIfPresent(String.self, forKey: .convertedCustomerDepositAmount) ?? ""
        self.userId = try values.decodeIfPresent(String.self, forKey: .userId) ?? ""
        self.mybookingPickupStatus = try values.decodeIfPresent(Bool.self, forKey: .mybookingPickupStatus) ?? nil
        self.mybookingReturnStatus = try values.decodeIfPresent(Bool.self, forKey: .mybookingReturnStatus) ?? nil
        self.payment_method = try values.decodeIfPresent(String.self, forKey: .payment_method)
        self.paypal_charge_order_id = try values.decodeIfPresent(String.self, forKey: .paypal_charge_order_id)
        self.paypal_deposit_order_id = try values.decodeIfPresent(String.self, forKey: .paypal_deposit_order_id)
        self.order_placed_from = try values.decodeIfPresent(String.self, forKey: .order_placed_from)
        self.currencyCode = try values.decodeIfPresent(String.self, forKey: .show_currency_customer_charge_amount)
    }
}

// Model Coding Keys
extension Rent {
    
    enum CodingKeys: String, CodingKey {
        case billing_address = "billing_address"
        case customer_email = "customer_email"
        case customer_name = "customer_name"
        case customer_phone = "customer_phone"
        case grand_total = "grand_total"
        case order_id = "order_id"
        case checketurn = "return_bool"
        case pickup_date = "pickup_date"
        case produc_qty = "produc_qty"
        case produc_row_price = "produc_row_price"
        case produc_tname = "produc_tname"
        case produc_unit_price = "produc_unit_price"
        case product_image = "product_image"
        case return_date = "return_date"
        case status = "status"
        case chargeTransactionId = "charge_transaction_id"
        case charge = "customer_charge_amount"
        case deposit = "customer_deposit_amount"
        case depositTransactionId = "deposit_transaction_id"
        case exchangeRate = "openpay_exchange_rate"
        case customerOpenpayId = "customer_openpay_id"
        case userId = "user_id"
        case mybookingPickupStatus = "mybooking_pickup_status"
        case mybookingReturnStatus = "mybooking_return_status"
        case convertedCustomerDepositAmount = "converted_customer_deposit_amount"
        
        case payment_method = "payment_method"
        case paypal_charge_order_id = "paypal_charge_order_id"
        case paypal_deposit_order_id = "paypal_deposit_order_id"
        case order_placed_from = "order_placed_from"
        case show_currency_customer_charge_amount = "show_currency_customer_charge_amount"
    }
}

extension Rent: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Rent? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(Rent.self, from: data)
            return user
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}
