//
//  ReturnCar.swift
//  XRentY
//
//  Created by user on 17/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation


final class ReturnCar: NSObject, Decodable {
    var id: String = ""
    var name: String = ""
}

extension ReturnCar: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> ReturnCar? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let category = try? decoder.decode(ReturnCar.self, from: data)
        return category ?? nil
    }
}
