//
//  sellerCar.swift
//  XRentY
//
//  Created by user on 13/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class sellerCar: NSObject, Decodable {
    
    var id : String?
    var name : String?
    var status : String?
    var numberofseatslabel : String?
    var numberofseatsvalue : String?
    var enginetypelabel : String?
    var enginetypevalue : String?
    var allwheeldrivelabel : String?
    var allwheeldrivevalue : String?
    var childseatlabel : String?
    var childseatvalue : String?
    var aclabel : String?
    var aclvalue : String?
    var categorylabel : String?
    var categoryvalue : String?
    var brandmakelabel : String?
    var brandmakevalue : String?
    var brandmodellabel : String?
    var brandmodelvalue : String?
    var typelabel : String?
    var typevalue : String?
    var subcategorylabel : String?
    var subcategoryvalue : String?
    var brandyearlabel : String?
    var brandyearvalue : String?
    var soundsystemslabel : String?
    var soundsystemsvalue : String?
    var productimage : [String]?
    var productImagePath : [String]?

    var urlkey : String?
    var insurancecompany : String?
    var insurancecompanywebsite : String?
    
    var insurancenumber : String?
    var carregistrationnumber : String?
    var insurancepolicydoc : String?
    var driveid : String?
    var carregistration : String?
    var price : String?
    //var promoprice : String?
    var insuranceexpirydate : String?
    var transmission : Int?
    var carDescription : String?

    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decodeIfPresent(String.self, forKey: .id) ?? ""
        name = try? values.decodeIfPresent(String.self, forKey: .name) ?? ""
        status = try? values.decodeIfPresent(String.self, forKey: .status) ?? ""
        numberofseatslabel = try? values.decodeIfPresent(String.self, forKey: .numberofseatslabel) ?? ""
        numberofseatsvalue = try? values.decodeIfPresent(String.self, forKey: .numberofseatsvalue) ?? ""
        enginetypelabel = try? values.decodeIfPresent(String.self, forKey: .enginetypelabel) ?? ""
        enginetypevalue = try? values.decodeIfPresent(String.self, forKey: .enginetypevalue) ?? ""
        allwheeldrivelabel = try? values.decodeIfPresent(String.self, forKey: .allwheeldrivelabel) ?? ""
        allwheeldrivevalue = try? values.decodeIfPresent(String.self, forKey: .allwheeldrivevalue) ?? ""
        childseatlabel = try? values.decodeIfPresent(String.self, forKey: .childseatlabel) ?? ""
        childseatvalue = try? values.decodeIfPresent(String.self, forKey: .childseatvalue) ?? ""
        aclabel = try? values.decodeIfPresent(String.self, forKey: .aclabel) ?? ""
        aclvalue = try? values.decodeIfPresent(String.self, forKey: .aclvalue) ?? ""
        brandmakelabel = try? values.decodeIfPresent(String.self, forKey: .brandmakelabel) ?? ""
        brandmakevalue = try? values.decodeIfPresent(String.self, forKey: .brandmakevalue) ?? ""
        brandmodellabel = try? values.decodeIfPresent(String.self, forKey: .brandmodellabel) ?? ""
        brandmodelvalue = try? values.decodeIfPresent(String.self, forKey: .brandmodelvalue) ?? ""
        categorylabel = try? values.decodeIfPresent(String.self, forKey: .categorylabel) ?? ""
        categoryvalue = try? values.decodeIfPresent(String.self, forKey: .categoryvalue) ?? ""
        typelabel = try? values.decodeIfPresent(String.self, forKey: .typelabel) ?? ""
        typevalue = try? values.decodeIfPresent(String.self, forKey: .typevalue) ?? ""
        subcategorylabel = try? values.decodeIfPresent(String.self, forKey: .subcategorylabel) ?? ""
        subcategoryvalue = try? values.decodeIfPresent(String.self, forKey: .subcategoryvalue) ?? ""
        brandyearlabel = try? values.decodeIfPresent(String.self, forKey: .brandyearlabel) ?? ""
        brandyearvalue = try? values.decodeIfPresent(String.self, forKey: .brandyearvalue) ?? ""
        soundsystemslabel = try? values.decodeIfPresent(String.self, forKey: .soundsystemslabel) ?? ""
        soundsystemsvalue = try? values.decodeIfPresent(String.self, forKey: .soundsystemsvalue) ?? ""
        productimage = (try! values.decodeIfPresent([String].self, forKey: .productimage)) ?? []
        urlkey = try? values.decodeIfPresent(String.self, forKey: .urlkey) ?? ""
        insurancecompany = try? values.decodeIfPresent(String.self, forKey: .insurancecompany) ?? ""
        insurancecompanywebsite = try? values.decodeIfPresent(String.self, forKey: .insurancecompanywebsite) ?? ""
        insurancenumber = try? values.decodeIfPresent(String.self, forKey: .insurancenumber) ?? ""
        carregistrationnumber = try? values.decodeIfPresent(String.self, forKey: .carregistrationnumber) ?? ""
        insurancepolicydoc = try? values.decodeIfPresent(String.self, forKey: .insurancepolicydoc) ?? ""
        driveid = try? values.decodeIfPresent(String.self, forKey: .driveid) ?? ""
        carregistration = try? values.decodeIfPresent(String.self, forKey: .carregistration) ?? ""
        price = try? values.decodeIfPresent(String.self, forKey: .price) ?? ""
        insuranceexpirydate = try? values.decodeIfPresent(String.self, forKey: .insuranceexpirydate) ?? ""
        transmission = try? values.decodeIfPresent(Int.self, forKey: .transmission) ?? 0
        carDescription = try? values.decodeIfPresent(String.self, forKey: .carDescription) ?? ""
        productImagePath = (try! values.decodeIfPresent([String].self, forKey: .productImagePath)) ?? []
    }
}

// Model Coding Keys
extension sellerCar {
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case status = "status"
        case numberofseatslabel = "number_of_seats_label"
        case numberofseatsvalue = "number_of_seats_value"
        case enginetypelabel = "engine_type_label"
        case enginetypevalue = "engine_type_value"
        case allwheeldrivelabel = "all_wheel_drive_label"
        case allwheeldrivevalue = "all_wheel_drive_value"
        case childseatlabel = "child_seat_label"
        case childseatvalue = "child_seat_value"
        case aclabel = "ac_label"
        case aclvalue = "acl_value"
        case brandmakelabel = "brand_make_label"
        case brandmakevalue = "brand_make_value"
        case brandmodellabel = "brand_model_label"
        case brandmodelvalue = "brand_model_value"
        case categorylabel = "category_label"
        case categoryvalue = "category_value"
        case typelabel = "type_label"
        case typevalue = "type_value"
        case subcategorylabel = "sub_category_label"
        case subcategoryvalue = "sub_category_value"
        case brandyearlabel = "brand_year_label"
        case brandyearvalue = "brand_year_value"
        case soundsystemslabel = "sound_systems_label"
        case soundsystemsvalue = "sound_systems_value"
        case productimage = "product_image"
        case urlkey = "url_key"
        case insurancecompany = "insurance_company"
        case insurancecompanywebsite = "insurance_company_website"
        case insurancenumber = "insurance_number"
        case carregistrationnumber = "car_registration_number"
        case insurancepolicydoc = "insurance_policy_doc"
        case driveid = "drive_id"
        case carregistration = "car_registration"
        case price = "price"
        case promoprice = "promo_price"
        case carDescription = "description"
        case insuranceexpirydate = "insurance_expiry_date"
        case transmission = "transmission"
        case productImagePath = "product_image_path"
    }
}

extension sellerCar: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> sellerCar? {
        print(dictionary)
        let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        guard data != nil else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(sellerCar.self, from: data!)
            return user
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}
