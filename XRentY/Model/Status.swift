//
//  Status.swift
//  XRentY
//
//  Created by user on 11/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation
final class Status: NSObject, Decodable {
    
    var value: String = ""
    var label: String = ""
}

extension Status: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Status? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let make = try? decoder.decode(Status.self, from: data)
        return make ?? nil
    }
}
