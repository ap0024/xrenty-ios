//
//  AddtoCart.swift
//  XRentY
//
//  Created by user on 15/11/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class AddtoCart: NSObject, Decodable {
    

    var price: String = ""
    
    init(_ name: String) {
        // initailization
        self.price = name
    }
}

extension AddtoCart: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> AddtoCart? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let category = try? decoder.decode(AddtoCart.self, from: data)
        return category ?? nil
    }
}
