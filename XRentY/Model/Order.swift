//
//  Order.swift
//  XRentY
//
//  Created by user on 31/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class Order: NSObject, Decodable {
    
    var chargeValue: String? {
        guard let gtotal = grandTotal, let total = Double(gtotal)  else {
            return nil
        }
        let amount = total - Double(depositeValue ?? "0.00")!
        let stringAmount = String(format: "%.2f", amount)
        return stringAmount
    }
    
    var depositeValue: String? {
        if let deposit = self.deposit, let double = Double(deposit) {
            let stringAmount = String(format: "%.2f", double)
            return stringAmount
        } else {
            return "0.00"
            
        }
    }


    var products: [Car]?
    var deposit: String?
    var discount: String?
    var extraFee: String?
    var grandTotal: String?
    var quoteId : String?
    var subtotal: String?
    var tax: String?
    var city : String?
    var countryId : String?
    var countryName : String?
    var email : String?
    var dob : String?
    var firstName : String?
    var lastName : String?
    var Pincode : String?
    var state : String?
    var street : String?
    var telephone : String?
    var couponApplied: String?
    var paymentMethod: String?
    var depositConfirmed: Int?
    var convertedDeposit: String?
    var itemsInCart: Int?
    var depositTransactionId: String?
    var exchangeRate: String?
    var orderid : String?
    var grand_total_amount : String?
    var paypal_deposit_order_id: String?
    
    // init decoder
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: DecodingKeys.self)
        self.products = (try! values.decodeIfPresent([Car].self, forKey: .products)) ?? []
        self.deposit = try values.decodeIfPresent(String.self, forKey: .deposit_amount)
        self.discount = try values.decodeIfPresent(String.self, forKey: .discount)
        self.grandTotal = try values.decodeIfPresent(String.self, forKey: .grandtotal)
        self.quoteId = try values.decodeIfPresent(String.self, forKey: .quote_id)
        self.subtotal = try values.decodeIfPresent(String.self, forKey: .subtotal)
        self.extraFee = try values.decodeIfPresent(String.self, forKey: .extraFee)
        self.tax = try values.decodeIfPresent(String.self, forKey: .tax)
        self.city = try values.decodeIfPresent(String.self, forKey: .city)
        self.countryId = try values.decodeIfPresent(String.self, forKey: .coutryId)
        self.countryName = try values.decodeIfPresent(String.self, forKey: .countryName)
        self.email = try values.decodeIfPresent(String.self, forKey: .email)
        self.dob = try values.decodeIfPresent(String.self, forKey: .dob)
        self.firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        self.lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        self.Pincode = try values.decodeIfPresent(String.self, forKey: .pincode)
        self.state = try values.decodeIfPresent(String.self, forKey: .state)
        self.street = try values.decodeIfPresent(String.self, forKey: .street)
        self.telephone = try values.decodeIfPresent(String.self, forKey: .telephone)
        self.couponApplied = try values.decodeIfPresent(String.self, forKey: .coupon_applied) ?? ""
        self.exchangeRate = try values.decodeIfPresent(String.self, forKey: .exchangeRate)
        self.depositConfirmed = try values.decodeIfPresent(Int.self, forKey: .depositConfirmed)
        self.convertedDeposit = try values.decodeIfPresent(String.self, forKey: .convertedDeposit)
        self.itemsInCart = try values.decodeIfPresent(Int.self, forKey: .itemsInCart)
        self.depositTransactionId = try values.decodeIfPresent(String.self, forKey: .depositTransactionId)
        self.paypal_deposit_order_id = try values.decodeIfPresent(String.self, forKey: .paypal_deposit_order_id)
        self.paymentMethod = try values.decodeIfPresent(String.self, forKey: .payment_method)
        self.orderid = try values.decodeIfPresent(String.self, forKey: .order_id)
        self.grand_total_amount = try values.decodeIfPresent(String.self, forKey: .grand_total_amount)
    }
}

// Model Coding Keys
extension Order {
    
    enum DecodingKeys: String, CodingKey {
        case grand_total_amount = "grand_total_amount"
        case deposit_amount = "deposit_amount"
        case discount = "discount"
        case grandtotal = "grandtotal"
        case products = "products"
        case quote_id = "quote_id"
        case subtotal = "subtotal"
        case extraFee = "extra_fee"
        case tax = "tax"
        case city = "city"
        case coutryId = "country"
        case countryName = "country_name"
        case email = "email"
        case dob = "dob"
        case firstName = "firstname"
        case lastName = "lastname"
        case pincode = "pincode"
        case coupon_applied = "coupon_applied"
        case state = "state"
        case street = "street"
        case telephone = "telephone"
        case payment_method = "payment_method"
        case payment_status = "payment_status"
        case depositConfirmed = "deposit_confirmed"
        case convertedDeposit = "converted_customer_deposit_amount"
        case itemsInCart = "items_in_cart"
        case depositTransactionId  = "openpay_transaction_id"
        case exchangeRate = "openpay_exchange_rate"
        case paypal_deposit_order_id = "paypal_deposit_order_id"
        case order_id = "order_id"
    }
}

extension Order: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Order? {
        
        print(dictionary)
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) , let cartResponse = try? JSONDecoder().decode(Order.self, from: data) else {
            print("unable to decode json")
            return nil
        }
        
        return cartResponse
    }
}
