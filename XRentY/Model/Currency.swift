//
//  Currency.swift
//  XRentY
//
//  Created by user on 27/06/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation

final class Currency: NSObject, Decodable {
    
    var customer_currency: String?
    var customer_lang : String?
    
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        customer_currency = try? values.decodeIfPresent(String.self, forKey: .customer_currency) ?? ""
        customer_lang = try? values.decodeIfPresent(String.self, forKey: .customer_lang) ?? ""
        
        
    }
}

// Model Coding Keys
extension Currency {
    
    enum CodingKeys: String, CodingKey {
        case customer_currency = "customer_currency"
        case customer_lang = "customer_lang"
    }
}

extension Currency: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Currency? {
        
        let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        guard data != nil else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(Currency.self, from: data!)
            return user
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}

