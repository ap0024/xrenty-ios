//
//  Bank.swift
//  XRentY
//
//  Created by user on 01/11/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class Bank: NSObject, Decodable {
    
    var id: String = ""
    var name: String = ""
}

extension Bank: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Bank? {
        
        let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        guard data != nil else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let country = try decoder.decode(Bank.self, from: data!)
            return country
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}
