//
//  Price.swift
//  XRentY
//
//  Created by user on 10/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation
final class Price : NSObject , Decodable{
    var price: String?
    var specialPrice: String?
    var calendar_id: String?
    var start_date: String?
    var end_date: String?
    var qty: String?
    var city_value : String?
    var city_label : String?
    var status_label : String?
    var status_value: String?
    var main_special_price : String?
    var  main_price : String?
    
    init(from decoder: Decoder) throws{
        let values = try decoder.container(keyedBy: CodingKeys.self)
        price = try? values.decodeIfPresent(String.self, forKey: .price) ?? ""
        specialPrice = try? values.decodeIfPresent(String.self, forKey: .specialPrice) ?? ""
        calendar_id = try? values.decodeIfPresent(String.self, forKey: .calendarId) ?? ""
        start_date = try? values.decodeIfPresent(String.self, forKey: .startDate) ?? ""
        end_date = try? values.decodeIfPresent(String.self, forKey: .endDate) ?? ""
        qty = try? values.decodeIfPresent(String.self, forKey: .qty) ?? ""
        city_label = try? values.decodeIfPresent(String.self, forKey: .city_label) ?? ""
        city_value = try? values.decodeIfPresent(String.self, forKey: .city_value) ?? ""
        status_label = try? values.decodeIfPresent(String.self, forKey: .status_label) ?? ""
        status_value = try? values.decodeIfPresent(String.self, forKey: .status_value) ?? ""
        main_special_price = try? values.decodeIfPresent(String.self, forKey: .main_special_price) ?? ""
        main_price = try? values.decodeIfPresent(String.self, forKey: .main_price) ?? ""
    }
    
}
extension Price{
    enum CodingKeys: String, CodingKey{
        case price = "price"
        case specialPrice = "special_price"
        case calendarId = "calendar_id"
        case startDate = "start_date"
        case endDate = "end_date"
        case qty = "qty"
        case city_label = "city_label"
        case city_value = "city_value"
        case status_label = "status_label"
        case status_value = "status_value"
        case main_price = "main_price"
        case main_special_price = "main_special_price"
        
    }
}
extension Price : Parceable{
    static func parseObject(dictionary: [String : AnyObject]) -> Price? {
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse")
            return nil
        }
        let decoder = JSONDecoder()
        let category = try? decoder.decode(Price.self, from: data)
        return category ?? nil
    }
}

