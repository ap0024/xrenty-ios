//
//  BookingResponse.swift
//  XRentY
//
//  Created by user on 17/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class BookingResponse: NSObject, Decodable {
    
    var bookings: [Booking]?
    var more: Int
    
    enum DecodingKeys: String, CodingKey {
        case bookings = "order"
        case more = "more"
    }
    // init decoder
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: DecodingKeys.self)
        self.bookings = (try! values.decodeIfPresent([Booking].self, forKey: .bookings)) ?? []
        self.more = try values.decodeIfPresent(Int.self, forKey: .more) ?? 0
    }
}

extension BookingResponse: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> BookingResponse? {
        
        let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        guard data != nil else {
            debugPrint("unable to parse: user")
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let bookingResponse = try decoder.decode(BookingResponse.self, from: data!)
            return bookingResponse
        }
        catch {
            print("unable to decode model: user")
            return nil
        }
    }
}
