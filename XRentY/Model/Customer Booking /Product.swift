//
//  Products.swift
//  XRentY
//
//  Created by user on 17/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation
//
//  MyBookings.swift
//  XRentY
//
//  Created by user on 16/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class Product: NSObject, Decodable {
    
    var imageURL: URL? {
        guard let urlString = image else { return nil }
        return URL(string: urlString)
    }
    
    var sellerImageURL: URL? {
        guard let urlString = sellerimage else { return nil }
        return URL(string: urlString)
    }

    var address: String?
    var contactnumber: String?
    var id: String?
    var image: String?
    var name: String?
    var price: String?
    var qty: String?
    var low_price: String?
    var sellername: String?
    var unit_price: String?
    var sellerimage: String?
    var show_currency_price: String?

    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: DecodingKeys.self)
        self.address = try values.decodeIfPresent(String.self, forKey: .address) ?? "N/A"
        self.contactnumber = try values.decodeIfPresent(String.self, forKey: .phone) ?? "N/A"
        self.id = try values.decodeIfPresent(String.self, forKey: .id) ?? ""
        self.image = try values.decodeIfPresent(String.self, forKey: .image) ?? ""
        self.name = try values.decodeIfPresent(String.self, forKey: .name) ?? "N/A"
        self.price = try values.decodeIfPresent(String.self, forKey: .price) ?? ""
        self.qty = try values.decodeIfPresent(String.self, forKey: .qty) ?? ""
        self.low_price = try values.decodeIfPresent(String.self, forKey: .low_price) ?? ""
        self.sellername = try values.decodeIfPresent(String.self, forKey: .sellername) ?? ""
        self.unit_price = try values.decodeIfPresent(String.self, forKey: .unit_price) ?? ""
        self.sellerimage = try values.decodeIfPresent(String.self, forKey: .sellerimage) ?? ""
        self.show_currency_price = try values.decodeIfPresent(String.self, forKey: .show_currency_price)
    }
    
    enum DecodingKeys: String, CodingKey {
        case address = "address"
        case phone = "contactnumber"
        case id = "id"
        case image = "image"
        case name = "name"
        case price = "unit_price"
        case qty = "qty"
        case low_price = "row_price"
        case sellername = "Sellername"
        case unit_price = "price"
        case sellerimage = "Seller_image"
        case show_currency_price = "show_currency_price"
    }
}

