//
//  MyBookings.swift
//  XRentY
//
//  Created by user on 16/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class Booking: NSObject, Decodable {
    
    var pickupCar: Bool {
        return pickup_bool == 1
    }
    
    var returnCar: Bool {
        return return_bool == 1
    }
    
    var cancelCar: Bool {
        return cancelStatus == 1
    }
    
    var order_id: String?
    var grand_total: String?
    var created_at: Int?
    var pickup_date: String?
    var return_date: String?
    var status: String?
    var pickup_bool: Int?
    var return_bool: Int?
    var cancelStatus : Int?
    var rma_id: String?
    var productId: Int?
    var charge: String?
    var chargeTransactionId: String?
    var deposit: String?
    var depositTransactionId: String?
    var exchangeRate: String?
    var product: [Product]?
    var convertedCustomerDepositAmount : String?
    var payment_method: String?
    var paypal_charge_order_id: String?
    var paypal_deposit_order_id: String?
    var order_placed_from: String?
    
    var isOrderFromWeb: Bool {
        return order_placed_from == "1"
    }
    
    enum DecodingKeys: String, CodingKey {
        
        case order_id = "order_id"
        case grand_total = "grand_total"
        case created_at = "created_at"
        case pickup_date = "pickup_date"
        case return_date = "return_date"
        case status = "status"
        case product = "products"
        case checkPickup = "pickup_bool"
        case checketurn = "return_bool"
        case rma_id = "rma_id"
        case productId = "product_id"
        case cancelStatus = "cancel_status"
        case chargeTransactionId = "charge_transaction_id"
        case charge = "customer_charge_amount"
        case deposit = "customer_deposit_amount"
        case depositTransactionId = "deposit_transaction_id"
        case exchangeRate = "openpay_exchange_rate"
        case convertedCustomerDepositAmount = "converted_customer_deposit_amount"
        
        case payment_method = "payment_method"
        case paypal_charge_order_id = "paypal_charge_order_id"
        case paypal_deposit_order_id = "paypal_deposit_order_id"
        case order_placed_from = "order_placed_from"
    }
    
    // init decoder
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: DecodingKeys.self)
        self.order_id = try values.decodeIfPresent(String.self, forKey: .order_id) ?? ""
        self.grand_total = try values.decodeIfPresent(String.self, forKey: .grand_total) ?? ""
        self.created_at = try values.decodeIfPresent(Int.self, forKey: .created_at) ?? nil
        self.return_date = try values.decodeIfPresent(String.self, forKey: .return_date) ?? ""
        self.pickup_date = try values.decodeIfPresent(String.self, forKey: .pickup_date) ?? ""
        self.rma_id = try values.decodeIfPresent(String.self, forKey: .rma_id) ?? ""
        self.status = try values.decodeIfPresent(String.self, forKey: .status) ?? ""
        self.pickup_bool = try values.decodeIfPresent(Int.self, forKey: .checkPickup) ?? nil
        self.return_bool = try values.decodeIfPresent(Int.self, forKey: .checketurn) ?? nil
        self.cancelStatus = try values.decodeIfPresent(Int.self, forKey: .cancelStatus) ?? nil
        self.productId = try values.decodeIfPresent(Int.self, forKey: .productId) ?? nil
        self.product = (try! values.decodeIfPresent([Product].self, forKey: .product)) ?? []
        
        self.charge = try values.decodeIfPresent(String.self, forKey: .charge) ?? ""
        self.chargeTransactionId = try values.decodeIfPresent(String.self, forKey: .chargeTransactionId) ?? ""
        self.deposit = try values.decodeIfPresent(String.self, forKey: .deposit) ?? ""
        self.depositTransactionId = try values.decodeIfPresent(String.self, forKey: .depositTransactionId) ?? ""
        self.exchangeRate = try values.decodeIfPresent(String.self, forKey: .exchangeRate) ?? ""
        self.convertedCustomerDepositAmount = try values.decodeIfPresent(String.self, forKey: .convertedCustomerDepositAmount) ?? ""
        
        self.payment_method = try values.decodeIfPresent(String.self, forKey: .payment_method)
        self.paypal_charge_order_id = try values.decodeIfPresent(String.self, forKey: .paypal_charge_order_id)
        self.paypal_deposit_order_id = try values.decodeIfPresent(String.self, forKey: .paypal_deposit_order_id)
        self.order_placed_from = try values.decodeIfPresent(String.self, forKey: .order_placed_from)
    }
}
