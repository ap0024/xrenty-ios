//
//  Wishlist.swift
//  XRentY
//
//  Created by user on 05/11/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

    final class WishList: NSObject, Decodable {
        
        var address : String?
        var image : String?
        var name : String?
        var price : String?
        var id : String?
    }
    
    extension WishList: Parceable {
        
        static func parseObject(dictionary: [String : AnyObject]) -> WishList? {
            
            guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
                debugPrint("unable to parse: user")
                return nil
            }
            
            let decoder = JSONDecoder()
            let category = try? decoder.decode(WishList.self, from: data)
            return category ?? nil
        }
}
