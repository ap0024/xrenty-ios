//
//  Category.swift
//  XRentY
//
//  Created by user on 04/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class Category: NSObject, Decodable {
    
    var id: String = ""
    var name: String = ""
    
    init(_ name: String) {
        // initailization
        self.name = name
    }
}

extension Category: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Category? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let category = try? decoder.decode(Category.self, from: data)
        return category ?? nil
    }
}
