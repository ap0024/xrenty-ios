//
//  Chat.swift
//  XRentY
//
//  Created by user on 13/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation
import CoreLocation

final class Chat: NSObject, Codable {
    
    var dayTime: (String, String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        if let messageTime = chatDate, let date = dateFormatter.date(from: messageTime) {
            
            let calendar = NSCalendar.current
            dateFormatter.dateFormat = "H:mm"
            dateFormatter.timeZone = TimeZone.current
            let time = dateFormatter.string(from: date)
            
            if calendar.isDateInToday(date) {
                return ("Today",time)
            }
            else if calendar.isDateInYesterday(date) {
                return ("Yesterday",time)
            }
            else {
                dateFormatter.dateFormat = "dd MMMM"
                let strDate = dateFormatter.string(from: date)
                return (strDate,time)
            }
        } else {
            return ("-","-")
        }
    }
    
    var currentDate: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = formatter.string(from: Date())
        return date
    }
    
    var isReceived: Bool {
        return isSender == 1
    }
    
    var islocationIncluded: Bool {
        return (descriptionValue == "" || (lat != "" && lat != nil && long != "" && long != nil))
    }
    
    var coordinate: CLLocationCoordinate2D? {
        if islocationIncluded, let latitude = Double(lat!), let longitude = Double(long!)  {
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        return nil
    }
    
    var name: String?
    var date: String?
    var descriptionValue : String?
    var product_name: String?
    var isSender: Int?
    var messageId: String?
    var lat: String?
    var long: String?
    var chatDate: String?
    var userId: String?
    
    var json: [String:Any]? {
        let encoder = JSONEncoder()
        if let data = try? encoder.encode(self),
            let json =  try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] {
            return json
        }
        return nil
    }
    
    init(description: String, location: CLLocationCoordinate2D?, chat: Chat?, sending: Bool, userId: String) {
        super.init()
        self.descriptionValue = description
        self.name = chat?.name ?? ""
        self.date = chat?.date ?? ""
        self.chatDate = currentDate
        self.lat = location != nil ? "\(location!.latitude)" : ""
        self.long = location != nil ? "\(location!.longitude)" : ""
        self.product_name = chat?.product_name ?? ""
        self.isSender = sending ? 1 : 0
        self.userId = "\(userId)"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(date, forKey: .date)
        try container.encode(descriptionValue, forKey: .descriptionValue)
        try container.encode(product_name, forKey: .product_name)
        try container.encode(isSender, forKey: .isSender)
        try container.encode(lat, forKey: .lat)
        try container.encode(long, forKey: .long)
        try container.encode(chatDate, forKey: .chatDate)
        try container.encode(userId, forKey: .userId)
    }
    
    init(from decoder : Decoder) throws{
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try? values.decodeIfPresent(String.self, forKey: .name) ?? ""
        date = try? values.decodeIfPresent(String.self, forKey: .date) ?? ""
        descriptionValue = try? values.decodeIfPresent(String.self, forKey: .descriptionValue) ?? ""
        product_name = try? values.decodeIfPresent(String.self, forKey: .product_name) ?? ""
        isSender = try? values.decodeIfPresent(Int.self, forKey: .isSender) ?? 0
        lat = try? values.decodeIfPresent(String.self, forKey: .lat) ?? ""
        long = try? values.decodeIfPresent(String.self, forKey: .long) ?? ""
        chatDate = try? values.decodeIfPresent(String.self, forKey: .chatDate) ?? ""
        userId = try? values.decodeIfPresent(String.self, forKey: .userId) ?? ""
    }
}
extension Chat{
    enum CodingKeys : String, CodingKey {
        case name = "name"
        case date = "date"
        case descriptionValue = "description"
        case product_name = "product_name"
        case isSender = "isSender"
        case lat = "lat"
        case long = "long"
        case chatDate = "chat_date"
        case userId = "userid"
    }
}
extension Chat: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Chat? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let make = try? decoder.decode(Chat.self, from: data)
        return make ?? nil
    }
}

