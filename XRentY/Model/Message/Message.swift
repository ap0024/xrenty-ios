//
//  Message.swift
//  XRentY
//
//  Created by user on 12/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class Message: NSObject, Decodable {
    
    var messages_id: String?
    var user_id: String?
    var usercontact_id : String?
    var licence_doc : String?
    var title: String?
    var product_id: String?
    var order_id: String?
    var is_active: String?
    var date: String?
    var product_name : String?
    var sender_name : String?
    var descriptionValue : String?
    var count_of_message : Int?
    var sender_image: String?
    var receiver_id: String?
    var message_status: String?
    
    
    init(from decoder : Decoder) throws{
        let values = try decoder.container(keyedBy: CodingKeys.self)
        messages_id = try? values.decodeIfPresent(String.self, forKey: .messages_id) ?? ""
        user_id = try? values.decodeIfPresent(String.self, forKey: .user_id) ?? ""
        usercontact_id = try? values.decodeIfPresent(String.self, forKey: .usercontact_id) ?? ""
        title = try? values.decodeIfPresent(String.self, forKey: .title) ?? ""
        product_id = try? values.decodeIfPresent(String.self, forKey: .product_id) ?? ""
        order_id = try? values.decodeIfPresent(String.self, forKey: .order_id) ?? ""
        is_active = try? values.decodeIfPresent(String.self, forKey: .is_active) ?? ""
        date = try? values.decodeIfPresent(String.self, forKey: .date) ?? ""
        product_name = try? values.decodeIfPresent(String.self, forKey: .product_name) ?? ""
        sender_name = try? values.decodeIfPresent(String.self, forKey: .sender_name) ?? ""
        descriptionValue = try? values.decodeIfPresent(String.self, forKey: .descriptionValue) ?? ""
        count_of_message = try? values.decodeIfPresent(Int.self, forKey: .count_of_message) ?? 0
        sender_image = try? values.decodeIfPresent(String.self, forKey: .sender_image) ?? ""
        receiver_id = try? values.decodeIfPresent(String.self, forKey: .receiver_id) ?? ""
        message_status = try? values.decodeIfPresent(String.self, forKey: .message_status) ?? ""
        licence_doc = try? values.decodeIfPresent(String.self, forKey: .licencedoc) ?? ""
    }
}

extension Message{
    enum CodingKeys : String, CodingKey {
        case messages_id = "messages_id"
        case user_id = "user_id"
        case usercontact_id = "usercontact_id"
        case title = "title"
        case product_id = "product_id"
        case order_id = "order_id"
        case is_active = "is_active"
        case date = "date"
        case product_name = "product_name"
        case sender_name = "sender_name"
        case descriptionValue = "description"
        case count_of_message = "count_of_message"
        case sender_image = "sender_image"
        case receiver_id = "receiver_id"
        case message_status = "message_status"
        case licencedoc = "licence_doc"
        
    }
}
extension Message: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Message? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let make = try? decoder.decode(Message.self, from: data)
        return make ?? nil
    }
}

