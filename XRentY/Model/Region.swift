//
//  Region.swift
//  XRentY
//
//  Created by user on 01/11/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

//{"region_id":"1","code":"AL","name":"Alabama"}

final class Region: NSObject, Decodable {
    
    var region_id: String?
    var code: String?
    var name: String?
    
    // init decoder
    /*init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: DecodingKeys.self)
        self.cars = (try! values.decodeIfPresent([Car].self, forKey: .products)) ?? []
        self.grandtotal = try values.decodeIfPresent(String.self, forKey: .grandtotal)
        self.subTotal = try values.decodeIfPresent(String.self, forKey: .subtotal)
        self.discount = try values.decodeIfPresent(String.self, forKey: .discount)
        self.dipositAmount = try values.decodeIfPresent(String.self, forKey: .deposit_amount)
        self.tax = try values.decodeIfPresent(String.self, forKey: .tax)
        self.shipCost = try values.decodeIfPresent(String.self, forKey: .ship_cost)
        self.shipMethod = try values.decodeIfPresent(String.self, forKey: .ship_method)
        self.couponApplied = try values.decodeIfPresent(String.self, forKey: .coupon_applied)
        self.quoteId = try values.decodeIfPresent(String.self, forKey: .quote_id)
        self.isVirtual = try values.decodeIfPresent(String.self, forKey: .is_virtual)
        self.quoteCount = try values.decodeIfPresent(String.self, forKey: .quote_count)
        self.customerHasAddress = try values.decodeIfPresent(Int.self, forKey: .cust_has_addr)
    }*/
}

// Model Coding Keys
/*extension Cart {
    
    enum DecodingKeys: String, CodingKey {
        case products = "products"
        case grandtotal = "grandtotal"
        case subtotal = "subtotal"
        case discount = "discount"
        case deposit_amount = "deposit_amount"
        case tax = "tax"
        case ship_cost = "ship_cost"
        case ship_method = "ship_method"
        case coupon_applied = "coupon_applied"
        case quote_id = "quote_id"
        case is_virtual = "is_virtual"
        case quote_count = "quote_count"
        case cust_has_addr = "cust_has_addr"
    }
}*/

extension Region: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Region? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            return nil
        }
        do {
            let decoder = JSONDecoder()
            let region = try decoder.decode(Region.self, from: data)
            return region
        }
        catch {
            return nil
        }
    }
}
