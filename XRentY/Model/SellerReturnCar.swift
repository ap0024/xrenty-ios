 //
//  SellerReturnCar.swift
//  XRentY
//
//  Created by user on 26/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class SellerReturnCar: NSObject, Decodable {
    var id: String = ""
    var name: String = ""
}

extension SellerReturnCar: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> SellerReturnCar? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }
        
        let decoder = JSONDecoder()
        let category = try? decoder.decode(SellerReturnCar.self, from: data)
        return category ?? nil
    }
}
