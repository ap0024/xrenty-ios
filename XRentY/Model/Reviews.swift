//
//  Reviews.swift
//  XRentY
//
//  Created by user on 17/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

final class Review: NSObject, Decodable {
    
    var created_at : String?
    var detail : String?
    var image : String?
    var nickname : String?
    var star_count : Int?
    var title : String?
    var address : String?
    var name : String?
    
}

extension Review: Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Review? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            debugPrint("unable to parse: user")
            return nil
        }

        let decoder = JSONDecoder()
        let category = try? decoder.decode(Review.self, from: data)
        return category ?? nil
    }
}
