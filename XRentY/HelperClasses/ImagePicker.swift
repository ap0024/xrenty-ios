//
//  ImagePickerButton.swift
//  XRentY
//
//  Created by user on 06/07/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
import Photos

enum UploadFile {
    case file
    case dropBox
    case googleDrive
}

class UIImagePicker: NSObject {
    
    var pickerHandler: ((UIImage,String)->Void)? = nil
    fileprivate var currentVC: UIViewController?
    
    var allowEditing = false
    var currentView: UIViewController? {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return delegate.window?.rootViewController
    }
    
    lazy var pickerController: UIImagePickerController = {
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.allowsEditing = self.allowEditing
        return controller
    }()
    
    // show source type sheet 
    func showSheet(_ isDocument : Bool, completion: ((UploadFile)->())? = nil) {

        // add action sheet instance to display action sheet
        let actionSheet = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // photos action instance
        let photos = UIAlertAction(title: "Photo", style: .default, handler: { (_) in
            actionSheet.dismiss(animated: false, completion: nil)
            let sourceType =  UIImagePickerController.SourceType.photoLibrary
            if  UIImagePickerController.isSourceTypeAvailable(sourceType)  {
                self.pickerController.sourceType = sourceType
                guard let view = self.currentView else { return }
                view.present(self.pickerController, animated: true, completion: nil)
            }
        })
        
        // camera action instance
        let camera = UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            actionSheet.dismiss(animated: false, completion: nil)
            let sourceType = UIImagePickerController.SourceType.camera
            if UIImagePickerController.isSourceTypeAvailable(sourceType)   {
                self.pickerController.sourceType = sourceType
                guard let view = self.currentView else { return }
                view.present(self.pickerController, animated: true, completion: nil)
            }
        })

        if isDocument
        {
            // camera action instance
            let document = UIAlertAction(title: "File", style: .default, handler: { (_) in
                completion?(.file)
            })
            actionSheet.addAction(document) // add file action
            
            let DropBox = UIAlertAction(title: "DropBox", style: .default, handler: { (_) in
                completion?(.dropBox)
    
            })
            actionSheet.addAction(DropBox) // add file action
            
            let googledrive = UIAlertAction(title: "GoogleDrive", style: .default, handler: { (_) in
                 completion?(.googleDrive)
            })
            
            actionSheet.addAction(googledrive) // add file action
        }
        
        // cancel action instance
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        
        actionSheet.addAction(photos) // add photos action
        actionSheet.addAction(camera) // add camera action
        actionSheet.addAction(cancel) // add cancel action
        
        // present action sheet 
        guard let view = self.currentView else { return }
        view.present(actionSheet, animated: true, completion: nil)
    }
    
    func openCamera() {
        let sourceType =  UIImagePickerController.SourceType.camera
        if  UIImagePickerController.isSourceTypeAvailable(sourceType)  {
            self.pickerController.sourceType = sourceType
            guard let view = self.currentView else { return }
            view.present(self.pickerController, animated: true, completion: nil)
        }
        else {
            Toast(text: "Don't have camera access".localized).show()
        }
    }
    
    func openLibrary() {
        let sourceType =  UIImagePickerController.SourceType.photoLibrary
        if  UIImagePickerController.isSourceTypeAvailable(sourceType)  {
            self.pickerController.sourceType = sourceType
            guard let view = self.currentView else { return }
            view.present(self.pickerController, animated: true, completion: nil)
        }
        else {
            Toast(text: "Don't have library access".localized).show()
        }
    }
}

extension UIImagePicker: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            DispatchQueue.main.async {
                if let image = info[.originalImage] as? UIImage {
                   let test =  image.fixedOrientation()
                    guard self.pickerHandler != nil else { return }
                    let imgUrl = info[.referenceURL] as? URL
                    let imgName = imgUrl?.lastPathComponent
                    let documentDirectory = NSTemporaryDirectory()
                    let localPath = documentDirectory.appending(imgName ?? "")
                    var imageName = (localPath as NSString).lastPathComponent
                    
                    if imageName.fileExtension() == "" || imageName.fileExtension() == "HEIC" || imageName.fileExtension() == "MOV"
                    {
                        imageName = "asset.JPG"
                    }
                    self.pickerHandler!(test!, imageName)
                } else if let image = info[.originalImage] as? UIImage {
                    let test = image.fixedOrientation()
                    guard self.pickerHandler != nil else { return }
                    self.pickerHandler!(test!,"")
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
