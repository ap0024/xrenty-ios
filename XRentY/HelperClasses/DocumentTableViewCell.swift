//
//  DocumentTableViewCell.swift
//  XRentY
//
//  Created by user on 03/01/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit

class DocumentTableViewCell: UITableViewCell {

    @IBOutlet weak var fileimageImg: UIImageView!
    @IBOutlet weak var fileNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
