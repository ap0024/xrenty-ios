//
//  Document.swift
//  XRentY
//
//  Created by user on 09/01/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation
import GoogleAPIClientForREST
import GoogleSignIn
import UIKit
import SwiftyDropbox
import iCloudDocumentSync

enum Document {
    
    typealias DataHandler = (Data?, String , String)->Void
    
    case google([GTLRDrive_File],GTLRDriveService)
    case dropBox([Files.Metadata])
    case none
    
    var title: String {
        switch self {
        case .google(_,_):
            return "Google Drive"
        case .dropBox(_):
            return "DropBox"
        case .none:
            return "Document"
        }
    }
    
    var fileCount: Int {
        switch self {
        case .google(let list,_):
            return list.count
        case .dropBox(let list):
            return list.count
        case .none:
            return 0
        }
    }
    
    func fileName(for index: Int) -> String? {
        switch self {
        case .google(let list,_):
            return list[index].name
        case .dropBox(let list):
            return list[index].name
        case .none:
            return nil
        }
    }
    
    func dropBoxfolderName(_ index: Int) -> String? {
        switch self {
        case .dropBox(let metadata):
            if metadata[index] is Files.FolderMetadata {
                return metadata[index].pathLower
            } else {
                return nil
            }
        default:
            return nil
        }
    }
    
//    func fileImage(_ index: Int) -> UIImage? {
//        switch self {
//        case .dropBox(let metadata):
//            if metadata[index] is Files.FolderMetadata {
//                return UIImage.init("")
//            } else {
//                return nil
//                
//            }
//        default:
//            return nil
//        }
//    }

    func downloadFile(for index: Int, completion: @escaping DataHandler) {
        
        switch self {
            
        case .google(let list,let service):
            if let name = list[index].name, !name.contains("No File Found") {
                self.downloadGoogleDriveFile(list[index], service: service, completion: completion)
            }
        case .dropBox(let list):
            if !list[index].name.contains("No File Found") {
                self.downloadDropboxFile(list[index], completion: completion)
            }
        case .none:
            break
        }
    }
    
    private func downloadDropboxFile(_ path: Files.Metadata, completion: @escaping DataHandler)  {
        let client = DropboxClientsManager.authorizedClient
        
        client?.files.download(path: path.pathLower ?? path.name).response { response, error in
            let fileExtension = path.name.fileExtension()
            let filename = path.name.fileName()
            //self.testDropBoxResponse(response)
            completion(response?.1,fileExtension, filename)
            Observer.uploadDoc()
        }
    }
    
    private func downloadGoogleDriveFile(_ file: GTLRDrive_File,service: GTLRDriveService, completion: @escaping DataHandler) {
        let url = "https://www.googleapis.com/drive/v3/files/\(file.identifier!)?alt=media"
        let fetcher = service.fetcherService.fetcher(withURLString: url)
        fetcher.beginFetch { (data, error) in
            let fileExtension = file.fileExtension ?? ""
            let filename = file.name?.fileName() ?? ""
            //self.testGoogleDriveResponse(response)
            completion(data, fileExtension, filename)
            Observer.uploadDoc()
        }
    }
    
//    private func testDropBoxResponse(_ resonse: (Files.FileMetadata, Data)?) {
//        print("data:\(resonse?.0)")
//        print("meta data: \(resonse?.1)")
//
//    }
    
//    private func testGoogleDriveResponse(_ resonse: (Data?,Error?)) {
//        print("data length: \(resonse.0)")
//        print("error:\(resonse.1)")
//    }
}
