//
//  DocumentViewController.swift
//  XRentY
//
//  Created by user on 03/01/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
import GoogleAPIClientForREST
import SwiftyDropbox
import GoogleSignIn
import IBLocalizable

class DocumentViewController: BaseViewController {
    
    typealias DataHandler = (Data?, String , String)->()
    var dropBoxList: [Files.Metadata] = []
    var googleDriveList: [GTLRDrive_File] = []
    var dataHandler: DataHandler? = nil
    var document: Document = .none {
        didSet {
            switch document {
            case .google(let list,_):
                self.googleDriveList = list
            case .dropBox(let list):
                self.dropBoxList = list
            case .none:
                assertionFailure("invalid selection")
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = document.title
    }

    func showFiles(_ document: Document, dataHandler: @escaping ((Data?, String , String) -> Void)) {
        self.document = document
        self.dataHandler = dataHandler
    }
    
    @IBAction func logout(_ sender: UIBarButtonItem) {
        switch document {
        case .google(_, _):
            self.showAlert("Are you sure you want to logout from your google drive account?".localized) {
                GIDSignIn.sharedInstance().signOut()
            }
        case .dropBox(_):
            self.showAlert("Are you sure you want to logout from your dropbox account?".localized) {
                DropboxClientsManager.resetClients()
                DropboxClientsManager.unlinkClients()
                DropboxMobileOAuthManager.sharedOAuthManager.clearStoredAccessTokens()
            }
        case .none:
            break
        }
    }
    
    func showAlert(_ title: String, completion: (()->())? = nil) {
        AlertManager.showAlert(type: .custom(title), actionTitle: "logout".localized) {
            for controller in self.navigationController!.viewControllers {
                if controller.isKind(of: CarDocumentationViewController.self) || controller.isKind(of: CustomerProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    completion?()
                }
            }
        }
    }
}

extension DocumentViewController: UITableViewDelegate, UITableViewDataSource,UINavigationControllerDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return document.fileCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : DocumentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DocumentTableViewCell", for: indexPath) as! DocumentTableViewCell
        
        if document.dropBoxfolderName(indexPath.row) != nil {
            cell.fileimageImg.image = UIImage(named: "folder_ic")
        } else {
            cell.fileimageImg.image = UIImage(named: "file_ic")
        }
        
        cell.fileNameLbl?.text = document.fileName(for: indexPath.row)
        
        
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.font = UIFont(name: "HelveticaNeue", size: 19.0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        
        if let path = document.dropBoxfolderName(indexPath.row) {
            self.dropboxListFiles(path)
        } else {
            guard let handler = dataHandler else {
                hideLoader()
                return
            }
            self.document.downloadFile(for: indexPath.row, completion: handler)
        }
    }
    
    //MARK:- DropBox Api
    func dropboxListFiles(_ path: String?) {
        guard let pathString = path else {
            AlertManager.showAlert(type: .custom("Invalid file path".localized))
            return
        }
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        DropboxClientsManager.authorizedClient?.files.listFolder(path: pathString).response { response, error in
            DispatchQueue.main.async {
                self.hideLoader()
                if let result = response {
                    
                    if !result.entries.isEmpty  {
                        let vc = UIStoryboard(name: "Seller", bundle: nil).instantiateViewController(withIdentifier: "DocumentViewController") as? DocumentViewController
                        vc?.showFiles(.dropBox(result.entries), dataHandler: self.dataHandler!)
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                    else {
                        AlertManager.showAlert(type: .custom("This folder is empty".localized))
                    }
                }
            }
        }
    }
}
