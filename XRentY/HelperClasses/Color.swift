//
//  Color.swift
//  SM-Markets
//
//  Created by user on 24/05/18.
//  Copyright © 2018 Navjot Sharma. All rights reserved.
//

import UIKit

enum Color {
    
    static var barTintColor = UIColor.init(red: 2/255, green: 115/255, blue: 204/255, alpha: 1.0)
    
    static var textColor = UIColor.init(red: 37.0/255.0, green: 120.0/255.0, blue: 198.0/255.0, alpha: 1.0)
    
    static var baseColourLightBlue = UIColor.init(red: 148/255, green: 180/255, blue: 229/255, alpha: 1.0)
    
    static var greenColour = UIColor.init(red: 0/255, green: 220/255, blue: 0/255, alpha: 1.0)
}
