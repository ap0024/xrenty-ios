//
//  RatingHistoryCell.swift
//  XRentY
//
//  Created by Harish on 27/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit
import Kingfisher

class RatingHistoryCell: UITableViewCell {

    //MARK:- outlet
    @IBOutlet var carNameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var commentLabel: UILabel!
    @IBOutlet var pickUpDate: UILabel!
    @IBOutlet var returnDate: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var xrentName: UILabel!
    @IBOutlet var imageCar: UIImageView!
    @IBOutlet weak var starRatingView: StarRatingView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- cell configure
    func configure(_ review: Review) {
        self.carNameLabel.text = review.name ?? ""
        self.commentLabel.text = review.detail ?? ""
        self.addressLabel.text = review.address ?? ""
        self.xrentName.text = review.nickname ?? ""
        self.pickUpDate.text = review.created_at ?? ""
        starRatingView.setRate(review.star_count ?? 0)
        imageCar.kf.indicatorType = .activity
        imageCar.kf.setImage(with: URL.init(string: review.image ?? "")  ,placeholder: UIImage(named: "car_placeholder_images"))
    }
}
