//
//  RatingHistoryViewController.swift
//  XRentY
//
//  Created by Harish on 27/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.

import UIKit

class RatingHistoryViewController: BaseViewController {
    
    @IBOutlet var tableRents: UITableView!
    @IBOutlet var noRecordsView: UIView!
    var review: [Review] = [] {
        didSet {
            tableRents.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTitleView()
        self.rentHistoryWebService()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func rentHistoryWebService() {
        self.showLoader()
        WebService.SellerReview { result  in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    self.review = response.objects ?? []
                    if let review = response.objects,!review.isEmpty  {
                        self.tableRents.backgroundView = nil
                        self.review = review
                    } else if self.review.isEmpty {
                        self.tableRents.backgroundView = self.noRecordsView
                    }
                    else {
                        AlertManager.showAlert(on: self, type: .custom(response.message!))
                    }
                case .failure(let error):
                    print(error.message)
                }
            }
        }
    }
}

extension RatingHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(review.count)
        return review.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RatingHistory", for: indexPath) as! RatingHistoryCell
        cell.configure(review[indexPath.row])
        return cell
    }
}
