//
//  RentsViewController.swift
//  XRentY
//
//  Created by user on 26/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit
import Kingfisher
import IBLocalizable

class RentsViewController: BaseViewController {

    @IBOutlet var tableRents: UITableView!
    @IBOutlet var noRecordsView: UIView!
    let refreshControl = UIRefreshControl()
    var rents: [Rent] = [] {
        didSet {
            tableRents.reloadData()
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getRents()
        self.tableRents.separatorColor = UIColor.clear
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        let attributes = [NSAttributedString.Key.foregroundColor: Color.barTintColor, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing please wait...".localized, attributes: attributes)
        refreshControl.tintColor = Color.barTintColor
        tableRents.refreshControl = refreshControl
        Observer.upDateList(target: self, selector: #selector(refreshMyRents))
    }
    
    @objc func refreshMyRents() {
        self.getRents(false)
    }
    
    @objc func doSomething(refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        self.getRents(false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getRents(_ showLoader: Bool = true) {
        showLoader ? self.showLoader() : self.refreshControl.beginRefreshing()
        WebService.rentHistory { [weak self] (result) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.refreshControl.endRefreshing()
                switch result {
                case .success(let response):
                    if let rents = response.objects,!rents.isEmpty  {
                        self?.tableRents.backgroundView = nil
                        self?.tableRents.separatorColor = UIColor.lightGray
                        self?.rents = rents
                    } else if self?.rents.isEmpty ?? true {
                        self?.tableRents.separatorColor = UIColor.clear
                        self?.tableRents.backgroundView = self?.noRecordsView
                    }
                    else {
                        AlertManager.showAlert(on: self, type: .custom(response.message!))
                    }
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
}

extension RentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MyRentCell
        cell?.configure(rents[indexPath.row])
        cell?.tag = indexPath.row
        cell?.returnCar = {self.returnCar(self.rents[$0])}
        cell?.trackCar = {self.trackMap(self.rents[$0])}
        cell?.customerDetail = {self.customerDetail(self.rents[$0])}
        return cell!
    }
    
    func returnCar(_ rent: Rent) {
        
            self.pushTo(.sellerReturnCar(rent, {
                var row = 0
                self.rents.forEach{
                    if $0.orderId == rent.orderId {
                        $0.return_bool = 1
                        row = self.rents.index(of: $0)!
                    }
                }
                self.tableRents.reloadRows(at: [IndexPath.init(row: row, section: 0)], with: .fade)
            }))
    }
    
    func trackMap(_ rent: Rent) {
        
        if let location = LTCoordinator.sharedInstance().lastLocation {
            let controller = LTMapViewController.instantiate()
            controller.addLocation(isLocationShared: true, channel: rent.userId!, location: location, startTracking: true)
            controller.trackOnly = true
            self.present(controller, animated: true, completion: nil)
        }
        else{
            AlertManager.showAlert(type: .custom("Location services are disabled by other user.Please ask to enable the location.".localized))
        }
    }
    
    func customerDetail(_ rent: Rent) {
        let query = ["user_id": rent.userId!]
        self.showLoader(color: .clear)
        WebService.getRentDetail(queryItems: query) { result in
            DispatchQueue.main.async {
            self.hideLoader()
            switch result {
                case .success(let response):
                    let controller = CustomerDetailViewController.initiatefromStoryboard(.seller)
                    controller.detail = response.object
                    self.presentDetail(controller)
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                
                }
            }
        }
        }
}
