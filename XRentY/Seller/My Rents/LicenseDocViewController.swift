//
//  LicenseDocViewController.swift
//  XRentY
//
//  Created by user on 17/04/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
import WebKit

class LicenseDocViewController: BaseViewController,UIWebViewDelegate {

    //MARK:- outlet
    @IBOutlet weak var helpsupportWebView: WKWebView!
    var docLink : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        helpsupportWebView.uiDelegate = self
        helpsupportWebView.navigationDelegate = self
        self.addTitleView()
        
        if docLink.isStringLink() {
            let url = URL (string: docLink)
            let requestObj = URLRequest(url: url!)
            helpsupportWebView.load(requestObj)
        } else {
          print("there is no doc")
        }
        
        
    }
    
    @IBAction func back(_ sender: Any) {
       dismiss(animated: true, completion: nil)
    }
}

extension LicenseDocViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.showLoader()
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        self.hideLoader()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        self.hideLoader()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.hideLoader()
    }
    
    /*func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
     let app = UIApplication.shared
     // Handle phone and email links
     if let url = navigationAction.request.url {
     if url.scheme == "tel" || url.scheme == "mailto" {
     if app.canOpenURL(url) {
     app.open(url, options: [:], completionHandler: nil)
     decisionHandler(.cancel)
     return
     }
     }
     }
     
     decisionHandler(.allow)
     }*/
}

extension LicenseDocViewController: WKUIDelegate {
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        return nil
    }
}
