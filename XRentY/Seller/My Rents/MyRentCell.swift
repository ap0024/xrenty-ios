//
//  MyRentCell.swift
//  XRentY
//
//  Created by user on 27/09/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit

class MyRentCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblCustomerName: UILabel!
    @IBOutlet var lblCustomerPhone: UILabel!
    @IBOutlet var lblPickupDate: UILabel!
    @IBOutlet var lblReturnDate: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet weak var imageCar: UIImageView!
    @IBOutlet weak var btnRetun: UIButton!
    @IBOutlet weak var btntrack: UIButton!
    
    var returnCar : ((Int)->())? = nil
    var trackCar : ((Int) ->())? = nil
    var customerDetail : ((Int) ->())? = nil
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(_ rent: Rent) {
        
        btnRetun.alpha = 0.4
        btnRetun.isUserInteractionEnabled = false
        btntrack.alpha = 0.4
        btntrack.isUserInteractionEnabled = false

        if rent.mybookingPickupStatus! {
            btntrack.alpha = 1
            btntrack.isUserInteractionEnabled = true
        }

        if rent.mybookingReturnStatus! {
            btnRetun.alpha = 1
            btnRetun.isUserInteractionEnabled = true
            btntrack.alpha = 0.4
            btntrack.isUserInteractionEnabled = false
        }
        
        if rent.return_bool == 1 {
            btnRetun.alpha = 0.4
            btnRetun.isUserInteractionEnabled = false
            btntrack.alpha = 0.4
            btntrack.isUserInteractionEnabled = false
        }
        
        lblName.text = rent.productName ?? "N/A"
        lblAddress.text = rent.billingAddress ?? "N/A"
        let price = rent.grandTotal ?? "N/A"
        
        let price1 = Double(price.roundOff()) ?? 0
        let price2 = CGFloat(price1).formattedWithSeparator
        lblPrice.text = symbol + "\(price2)"
        lblCustomerName.text = rent.customerName ?? "N/A"
        lblCustomerPhone.text = rent.customerPhone ?? "N/A"
        lblPickupDate.text = rent.pickupDate ?? "N/A"
        lblReturnDate.text = rent.returnDate ?? "N/A"
        lblStatus.text = rent.status
        imageCar.kf.indicatorType = .activity
        imageCar.kf.setImage(with: rent.imageURL  , placeholder: #imageLiteral(resourceName: "car_placeholder_images"))
    }
    
    @IBAction func ReturnCar(_ sender: Any) {
        
        if let handler = returnCar {
            handler(tag)
        }
    }
    
    @IBAction func trackUser(_ sender: Any) {
        if let handler = trackCar {
            handler(tag)
        }
    }
    
    @IBAction func customerDetail(_ sender: Any) {
        if let handler = customerDetail {
            handler(tag)
        }
    }
    
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
}

extension UIImageView {
    public func imageFromServerURL(urlString: String) {
        self.image = UIImage(named: "car_placeholder.jpg")
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
            
        }).resume()
    }
}
