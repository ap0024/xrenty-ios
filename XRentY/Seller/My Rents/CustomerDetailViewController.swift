//
//  CustomerDetailViewController.swift
//  XRentY
//
//  Created by user on 15/04/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
typealias LicensePopOver = (_ message :String) ->()

class CustomerDetailViewController: BaseViewController,UIGestureRecognizerDelegate  {
    
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var lastNameTf: UITextField!
    @IBOutlet weak var phoneTf: UITextField!
    @IBOutlet weak var expiryDateLicenseTf: UITextField!
    @IBOutlet weak var EmailTf: UITextField!
    @IBOutlet weak var licenseDocument: UITextField!
    var detail: RentDetail?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showDetail()
    }
    
    func showDetail() {
        nameTf.text = detail?.customerFirstname?.capitalized ?? "N/A"
        lastNameTf.text = detail?.customerLastname ?? "N/A"
        let title = detail?.licencedoc ?? "No doc found".localized
        licenseDocument.text = (title as NSString).lastPathComponent
        
//        var Msg_Date_ = detail?.customerLicenseDate ?? ""
//        let dateFormatterGet = DateFormatter()
//        dateFormatterGet.dateFormat = "yy-MM-dd"
//        let dateFormatterPrint = DateFormatter()
//        dateFormatterPrint.dateFormat = "dd MMM yyyy"
//        let datee = dateFormatterGet.date(from: Msg_Date_)
//        if datee == nil {
//            Msg_Date_ =  "N/A"
//        }
//        else
//        {
//            Msg_Date_ =  dateFormatterPrint.string(from: datee!)
//        }
//        Msg_Date_ =  dateFormatterPrint.string(from: datee!)
        
        expiryDateLicenseTf.text = detail?.customerLicenseDate ?? ""
        phoneTf.text = detail?.telephone ?? "N/A"
        EmailTf.text = detail?.customerEmail ?? "N/A"
    }
    
    @IBAction func closeView(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func docPriview(_ sender: Any) {
        
        if  !(detail?.licencedoc == "") &&  !(detail?.licencedoc == nil) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LicenseDocViewController") as! LicenseDocViewController
        vc.docLink = detail?.licencedoc ?? ""
            let link = detail?.licencedoc ?? ""
        
            if link.isStringLink() {
                let navigationController = UINavigationController(rootViewController: vc)
                self.present(navigationController, animated: true, completion: nil)
            }
        }
    }
}

extension Date {
    func monthAsString() -> String {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("MMM")
        return df.string(from: self)
}
}
