//
//  HelpSupportViewController.swift
//  XRentY
//
//  Created by user on 28/09/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit

class HelpSupportViewController: BaseViewController,UIWebViewDelegate {

    //MARK:- outlet
    @IBOutlet weak var helpsupportWebView: UIWebView!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.addTitleView()
        
        let urlen = URL (string: "https://xrenty.uvdesk.com/en/folder/148/category")
        let urles = URL (string: "https://xrenty.uvdesk.com/es/folder/148/category")
        
        let language = NSLocale.current.languageCode
        
        if language == "en"
        {
            let requestObj = URLRequest(url: urlen!)
            helpsupportWebView.loadRequest(requestObj)
        }
        else if language == "es"
        {
            let requestObj = URLRequest(url: urles!)
            helpsupportWebView.loadRequest(requestObj)
        }
        else
        {
            let requestObj = URLRequest(url: urlen!)
            helpsupportWebView.loadRequest(requestObj)
        }
    }
    
    //MARK:- Webview Delegate
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        self.showLoader()

    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        self.hideLoader()

    }

    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
