//
//  CategoryCollectionViewCell.swift
//  XRentY
//
//  Created by user on 09/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var btnSelect: UIButton!

    var selectCategory : ((Int)->())? = nil

    func configure(_ category: Category) {
        self.lblCategoryName.text = category.name
    }
}
