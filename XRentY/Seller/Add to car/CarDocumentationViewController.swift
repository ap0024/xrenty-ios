//
//  CarDocumentationViewController.swift
//  XRentY
//  Created by user on 13/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.

import UIKit
import MobileCoreServices
import iCloudDocumentSync
import SwiftyDropbox
import GoogleAPIClientForREST
import GoogleSignIn
import IBLocalizable

enum uploadImage: String {
    case registration = "registration"
    case policy = "policy"
    case driveId = "driveId"
    case carImage = "carImage"
}

class CarDocumentationViewController: BaseViewController,URLSessionDelegate, URLSessionTaskDelegate, URLSessionDataDelegate {
    
    @IBOutlet weak var collectionViewhightConstraints: NSLayoutConstraint!
    @IBOutlet weak var txtCarRegistration: UITextField!
    @IBOutlet weak var lblRegistrationName: UILabel!
    @IBOutlet weak var lblInsurancePolicy: UILabel!
    @IBOutlet weak var lblDriveId: UILabel!
    @IBOutlet weak var txtInsuranceNumber: UITextField!
    @IBOutlet weak var txtXpireDate: UITextField!
    @IBOutlet weak var txtInsuranceCompany: UITextField!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var imageCar: UIImageView!
    @IBOutlet weak var txtInsuranceWebsite: UITextField!
    @IBOutlet weak var lblDocTitle: UILabel!
    @IBOutlet weak var uploadValue: UILabel!
    @IBOutlet weak var docProgressView: UIProgressView!
    @IBOutlet weak var uploadProgressView: UIView!
    
    
    @IBOutlet weak var lblRegNUmber: UILabel!
    @IBOutlet weak var lblCarReg: UILabel!
    @IBOutlet weak var lblinsNumber: UILabel!
    @IBOutlet weak var lblinsComny: UILabel!
    @IBOutlet weak var lblcompnyWbsite: UILabel!
    @IBOutlet weak var lblinsPolicy: UILabel!
    @IBOutlet weak var lblDriveID: UILabel!
    
    
    
    var addCar: AddCar?
    
    var imageType : uploadImage = .registration
    
    let scopes = [kGTLRAuthScopeDriveReadonly]
    let service = GTLRDriveService()
    var csvList = [GTLRDrive_File]()
    
    var listArray = [String]()
    var googledata : Data? = nil
    var filetype : String = ""
    var selectedIndex = Int()
    var imagesData = [String: Any]()
    var profileImages = [String]()
    var showImage = [String]()
    var filePath : String = ""
    var product_id : String = ""
    var minetype : String = ""
    var fileName : String = ""
    var docName : String = ""
    var fileUrl : URL? = nil
    var height = 98
    
    var imageView = UIImageView()
    let datePicker = UIDatePicker()
    
    lazy var picker: UIImagePicker = {
        let imagePicker = UIImagePicker()
        imagePicker.pickerHandler = {self.addImages($0, $1)}
        return imagePicker
    }()
    
    var reachability1 : Reachability {
        let reachability = Reachability()!
        return reachability
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.addTitleView()
        showDatePicker()
        self.setValues()
        self.setLabel()
        Authentication.isedit = true
        self.uploadProgressView.isHidden = true
        self.docProgressView.progress = 0
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.dbDelegate = self
        
        Observer.uploadDoc(target: self, selector: #selector(uploadDoc))
    }
    
    func setLabel() {
        
        lblRegNUmber.setLocalizedAshtrik()
        lblCarReg.setLocalizedAshtrik()
        lblinsNumber.setLocalizedAshtrik()
        lblinsComny.setLocalizedAshtrik()
        lblcompnyWbsite.setLocalizedAshtrik()
        lblinsPolicy.setLocalizedAshtrik()
        lblDriveID.setLocalizedAshtrik()
    }
    
    
    
    @objc func uploadDoc() {
        if !(googledata == nil){
            
            if let data =  googledata {
                self.myImageUploadRequest(data, isData: true, name: "")
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func setValues() {
        
        if Authentication.isedit
        {
            let detail = addCar?.car
            
            self.txtCarRegistration.text = detail?.carregistrationnumber ?? ""
            self.txtInsuranceNumber.text = detail?.insurancenumber ?? ""
            self.txtInsuranceCompany.text = detail?.insurancecompany ?? ""
            self.txtInsuranceWebsite.text = detail?.insurancecompanywebsite ?? ""
            self.product_id = detail?.id ?? ""
            let registration = detail?.carregistration ?? ""
            if !(registration == "") {
                
                let registrationName = (registration as NSString ).lastPathComponent
                self.lblRegistrationName.addImageAfterText(text: registrationName + " ", image: #imageLiteral(resourceName: "greenTick_ic"))
                
                imagesData["registration"] = detail?.carregistration ?? ""
            }
            
            let driveid = detail?.driveid ?? ""
            if !(driveid == "") {
                let driveName = (driveid as NSString).lastPathComponent
                self.lblDriveId.addImageAfterText(text: driveName + " ", image: #imageLiteral(resourceName: "greenTick_ic"))
                
                imagesData["driveId"] = detail?.driveid ?? ""
            }
            
            let policy = detail?.insurancepolicydoc ?? ""
            if !(policy ==  "") {
                let policyName = (policy as NSString).lastPathComponent
                self.lblInsurancePolicy.addImageAfterText(text: policyName + " ", image: #imageLiteral(resourceName: "greenTick_ic"))
                
                imagesData["policy"] = detail?.insurancepolicydoc ?? ""
            }
            
            let images  = detail?.productimage
            //self.product_id = detail?.id ?? ""
            let path = detail?.productImagePath
            self.profileImages = path ?? []
            self.showImage = images ?? []
            imagesData["carImage"] = path
            let heightConstant = self.showImage.count % 3 != 0 ? (CGFloat(self.showImage.count/3) + 1) *  CGFloat(self.height) : CGFloat(self.showImage.count/3) *  CGFloat(self.height)
            self.collectionViewhightConstraints.constant = heightConstant
            self.imageCollectionView.reloadData()
        }
    }
    
    func addImages(_ image: Any, _ name : String) {
        switch imageType {
        case .registration:
            self.myImageUploadRequest(image, isData: false, name: name)
            
        case .policy:
            self.myImageUploadRequest(image, isData: false, name: name)
            
        case .driveId:
            self.myImageUploadRequest(image, isData: false, name: name)
            
        case .carImage:
            self.myImageUploadRequest(image, isData: false, name: name)
            
        }
    }
    
    func imageString(_ image: UIImage) -> String {
        let (base64,format) = image.convertImageTobase64()
        let imageString = "data:image/\(format.rawValue);base64," + base64!
        return imageString
    }
    
    func myImageUploadRequest(_ image : Any , isData : Bool , name : String)
    {
        //showLoader(color: UIColor.black.withAlphaComponent(0.3))
        self.uploadProgressView.isHidden = false
        let myUrl = NSURL(string: APIConstants.pathLive+APIConstants.uploadproductimages.rawValue);
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var imageData : Data?
        
        print(imageData?.count)
        
        if isData
        {
            if self.filetype.contains("pdf")
            {
                imageData = googledata ?? image as! Data
                minetype = "application/pdf"
                fileName =  self.docName + "." + self.filetype
            }
            else if self.filetype.contains("docx")
            {
                imageData = googledata ?? image as! Data
                minetype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("doc")
            {
                imageData = googledata ?? image as! Data
                minetype = "application/msword"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("csv")
            {
                imageData = googledata ?? image as! Data
                minetype = "text/csv"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("ppt")
            {
                imageData = googledata ?? image as! Data
                minetype = "application/vnd.ms-powerpoint"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("png")
            {
                imageData = googledata ?? image as! Data
                minetype = "image/png"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("PNG")
            {
                imageData = googledata ?? image as! Data
                minetype = "image/png"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("jpeg")
            {
                imageData = googledata ?? image as! Data
                minetype = "image/jpeg"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("jpg")
            {
                imageData = googledata ?? image as! Data
                minetype = "image/jpeg"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("JPG")
            {
                imageData = googledata ?? image as! Data
                minetype = "image/jpeg"
                fileName = self.docName + "." + self.filetype
            }
            else{
                self.uploadProgressView.isHidden = true
                self.docProgressView.progress = 0
                self.uploadValue.text = "0% Complete"
                self.showToast(message: "This file format is not supported . Please use this file format  jpg,jpeg,png,csv,doc,docx,pdf.".localized)
            }
        }
        else
        {
            if let imageType = image as? UIImage {
                
                let max = 2097152
                imageData = imageType.pngData()
                if (imageData?.count)! > max {
                    imageData = imageType.jpegData(compressionQuality: 0.5)
                }
                
                print(imageData?.count as Any)
                minetype = "image/jpeg"
                fileName = name
                docName = name.fileName()
                filetype = name.fileExtension()
                
            } else {
                do {
                    imageData =  try Data.init(contentsOf: image as! URL)
                    
                    minetype = "image/jpeg"
                    fileName = name
                    docName = name.fileName()
                    filetype = name.fileExtension()
                }
                catch { }
            }
        }
        
        switch self.imageType {
            
        case .registration:
            self.filePath = "car_registration"
        case .policy:
            self.filePath = "insurance_policy_doc"
        case .driveId:
            self.filePath = "drive_id"
        case .carImage:
            self.filePath = "product_image"
        }
        
        if(imageData==nil)  { return; }
        
        if reachability1.isReachable {
            
            request.allHTTPHeaderFields = ["Authorization": "Bearer \(Authentication.token ?? "")"]
            request.httpBody = createBodyWithParameters( parameters: [:], filePathKey: self.filePath, imageDataKey: imageData! as NSData, boundary: boundary) as Data
            
            print(request.httpBody)
        }
        else
        {
            self.hideLoader()
            AlertManager.showAlert(type: .custom("No Internet Connection".localized))
            self.uploadProgressView.isHidden = true
            self.docProgressView.progress = 0
            self.uploadValue.text = "0% Complete".localized
            return
        }
        
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 30
        config.timeoutIntervalForResource = 60
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        config.urlCache = nil
        
        let session = Foundation.URLSession(configuration: config, delegate: self, delegateQueue: nil)
        
        let task = session.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.uploadProgressView.isHidden = true
                    self.docProgressView.progress = 0
                    self.uploadValue.text = "0% Complete"
                    print("error=\(error)")
                    AlertManager.showAlert(type: .custom("Connection error".localized))
                }
                return
            }
            
            // You can print out response object
            print("******* response = \(response)")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                
                print(json!)
                
                DispatchQueue.main.async {
                    if let response = json!["data"] as? [String:Any]
                    {
                        let imagepath = response["image_url"] as? String
                        print(imagepath!)
                        let imgpath = response["img_url"] as? String
                        print(imagepath!)
                        self.uploadProgressView.isHidden = true
                        self.docProgressView.progress = 0
                        self.uploadValue.text = "0% Complete".localized
                        switch self.imageType {
                        case .registration:
                            self.imagesData[self.imageType.rawValue] = imagepath
                            self.lblRegistrationName.addImageAfterText(text: (self.docName + "." + self.filetype ) + " ", image: #imageLiteral(resourceName: "greenTick_ic"))
                            //self.hideLoader()
                        //AlertManager.showAlert(type: .custom(self.docName + "." + self.filetype + " " + "is uploaded successfully"))
                        case .policy:
                            self.imagesData[self.imageType.rawValue] = imagepath
                            self.lblInsurancePolicy.addImageAfterText(text: (self.docName + "." + self.filetype ) + " ", image: #imageLiteral(resourceName: "greenTick_ic"))
                            //self.hideLoader()
                        //AlertManager.showAlert(type: .custom(self.docName + "." + self.filetype + " " + "is uploaded successfully"))
                        case .driveId:
                            self.imagesData[self.imageType.rawValue] = imagepath
                            self.lblDriveId.addImageAfterText(text: (self.docName + "." + self.filetype ) + " ", image: #imageLiteral(resourceName: "greenTick_ic"))
                            //self.hideLoader()
                        //AlertManager.showAlert(type: .custom(self.docName + "." + self.filetype + " " + "is uploaded successfully"))
                        case .carImage:
                            self.profileImages.append(imagepath ?? "")
                            self.showImage.append(imgpath ?? "")
                            let heightConstant = self.showImage.count % 3 != 0 ? (CGFloat(self.showImage.count/3) + 1) *  CGFloat(self.height) : CGFloat(self.showImage.count/3) *  CGFloat(self.height)
                            self.collectionViewhightConstraints.constant = heightConstant
                            self.imageCollectionView.reloadData()
                            self.imagesData[self.imageType.rawValue] = self.profileImages
                            //self.hideLoader()
                        }
                    }
                }
                
            }catch
            {
            }
        }
        task.resume()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?)
    {
        
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64)
    {
        let uploadProgress:Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        let progressPercent = Int(uploadProgress*100)
        DispatchQueue.main.async {
            self.docProgressView.setProgress(uploadProgress, animated: true)
            self.uploadValue.text = "\(progressPercent)% Complete"
            if  progressPercent == 100 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.uploadProgressView.isHidden = true
                    self.docProgressView.progress = 0
                    self.uploadValue.text = "0% Complete".localized
                })
            }
        }
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: (URLSession.ResponseDisposition) -> Void)
    {
        print(session)
        print(dataTask)
    }
    
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(fileName)\"\r\n")
        body.appendString(string: "Content-Type: \(minetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func carRegistrationBtn(_ sender: Any) {
        
        
        if reachability1.isReachable {
            
            
            imageType = .registration
            self.picker.showSheet(true) { self.openFileType($0) }
            self.lblDocTitle.text = "Registration document".localized
        }
        else
        {
            self.showToast(message: "No Internet Connection".localized)
            self.hideLoader()
            self.uploadProgressView.isHidden = true
            self.docProgressView.progress = 0
            self.uploadValue.text = "0% Complete".localized
        }
    }
    
    @IBAction func insurancePolicyUpload(_ sender: Any) {
        
        if reachability1.isReachable {
            
            
            imageType = .policy
            self.picker.showSheet(true) { self.openFileType($0) }
            self.lblDocTitle.text = "Policy document".localized
        }
        else
        {
            self.hideLoader()
            self.showToast(message: "No Internet Connection".localized)
            self.uploadProgressView.isHidden = true
            self.docProgressView.progress = 0
            self.uploadValue.text = "0% Complete".localized
        }
    }
    
    @IBAction func driveIDUpload(_ sender: Any) {
        
        if reachability1.isReachable {
            
            
            imageType = .driveId
            self.picker.showSheet(true) { self.openFileType($0) }
            self.lblDocTitle.text = "DriveId document".localized
        }
        else
        {
            self.hideLoader()
            self.showToast(message: "No Internet Connection".localized)
            self.uploadProgressView.isHidden = true
            self.docProgressView.progress = 0
            self.uploadValue.text = "0% Complete".localized
        }
    }
    
    func openFileType(_ type: UploadFile) {
        switch type {
        case .file:
            self.openiCloudDrive()
        case .dropBox:
            self.dropboxSignIn(self)
            self.selectedIndex = 0
        case .googleDrive:
            self.googleSignIn()
            self.selectedIndex = 1
        }
    }
    
    @IBAction func addImage(_ sender: Any) {
       
        self.lblDocTitle.text = "Car images".localized
        imageType = .carImage
        if profileImages.count == 20
        {AlertManager.showAlert(type: .custom("You can upload upto 20 images".localized))}
        else
        {
            
            self.picker.showSheet(false)
            
        }
    }
    
    
    @IBAction func poptoMycar(_ sender: Any) {
        self.navigationController?.viewControllers.forEach {
            if $0.isKind(of: MyCarsViewController.self) {
                self.navigationController?.popToViewController($0, animated: true)
                return
            }
        }
    }
    
    @IBAction func sendToarovel(_ sender: Any) {
        self.carAddQuery()
    }
    
    func carAddQuery() {
        
        if !(self.txtCarRegistration.text!.isEmpty || self.txtInsuranceNumber.text!.isEmpty || self.txtInsuranceCompany.text!.isEmpty || self.txtInsuranceWebsite.text!.isEmpty || imagesData["registration"] == nil || imagesData["policy"] == nil || imagesData["driveId"] == nil || imagesData["carImage"] == nil)
        {
            self.showLoader(color: UIColor.black.withAlphaComponent(0.3))
            addCar?.query["car_registration_number"] = txtCarRegistration.text ?? ""
            addCar?.query["insurance_number"] = txtInsuranceNumber.text ?? ""
            addCar?.query["insurance_company"] = txtInsuranceCompany.text ?? ""
            addCar?.query["insurance_company_website"] = txtInsuranceWebsite.text ?? ""
            addCar?.query["insurance_expiry_date"] =  ""
            addCar?.query["car_registration"] = imagesData["registration"]
            addCar?.query["insurance_policy_doc"] = imagesData["policy"]
            addCar?.query["drive_id"] = imagesData["driveId"]
            addCar?.query["product_image"] = imagesData["carImage"]
            
            self.selleraddCar()
        }
        else
        {
            AlertManager.showAlert(type: .custom("Please fill all information before proceeding to next step.".localized))
        }
    }
    
    func selleraddCar() {
        
        if Authentication.isedit
        {
            let id = addCar?.productId
            print(id!)
            
            addCar?.query["product_id"] = id ?? ""
            WebService.editCar(queryItems: addCar?.query ?? [:]) { result in
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch result {
                    case .success(let response):
                        print(response)
                        Observer.updateList()
                        self.navigationController?.viewControllers.forEach {
                            if $0.isKind(of: MyCarsViewController.self) {
                                self.navigationController?.popToViewController($0, animated: true)
                                return
                            }
                        }
                    case .failure(let error):
                        AlertManager.showAlert(on: self, type: .custom(error.message))
                    }
                }
            }
        }
        else
        {
            WebService.addCar(queryItems: addCar?.query ?? [:]) { result in
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch result {
                    case .success(let response):
                        print(response)
                        Observer.updateList()
                        self.navigationController?.viewControllers.forEach {
                            if $0.isKind(of: MyCarsViewController.self) {
                                self.navigationController?.popToViewController($0, animated: true)
                                return
                            }
                        }
                    case .failure(let error):
                        AlertManager.showAlert(on: self, type: .custom(error.message))
                    }
                }
            }
        }
    }
}

extension CarDocumentationViewController
{
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.setDate(Date(), animated: false)
        datePicker.minimumDate = Date()
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        txtXpireDate.inputAccessoryView = toolbar
        txtXpireDate.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        txtXpireDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}

extension CarDocumentationViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return showImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! imagesCollectionViewCell
        
        let image = URL.init(string: showImage[indexPath.row])
        cell.imgView.kf.indicatorType = .activity
        cell.imgView.kf.setImage(with: image,placeholder: UIImage(named: "car_placeholder_images"))
        cell.removeImage = { self.remove(indexPath.row) }
        return cell
    }
    
    func remove(_ tag: Int) {
        AlertManager.showAlert(type: .custom("Do you want to delete car image ?".localized), actionTitle: "Delete".localized) {
            
            let detail = self.addCar?.productId
            let url = self.showImage[tag]
            
            if Authentication.isedit
            {
                let query = ["image_url" : url,
                             "product_id" : detail ?? ""] as [String:Any]
                
                self.showLoader(color: UIColor.black.withAlphaComponent(0.3))
                WebService.removeimage(queryItems: query) { result in
                    DispatchQueue.main.async {
                        self.hideLoader()
                        switch result {
                        case .success(_):
                            self.showImage.remove(at: tag)
                            self.profileImages.remove(at: tag)
                            
                            self.imagesData["carImage"] = self.profileImages
                            let heightConstant = self.showImage.count % 3 != 0 ? (CGFloat(self.showImage.count/3) + 1) *  CGFloat(self.height) : CGFloat(self.showImage.count/3) *  CGFloat(self.height)
                            self.collectionViewhightConstraints.constant = heightConstant
                            self.imageCollectionView.reloadData()
                            self.imagesData[self.imageType.rawValue] = self.profileImages
                            
                        case .failure(let error):
                            AlertManager.showAlert(type: .custom(error.message))
                        }
                    }
                }
            }
            else
            {
                self.showImage.remove(at: tag)
                self.profileImages.remove(at: tag)
                self.imagesData["carImage"] = self.profileImages
                let heightConstant = self.showImage.count % 3 != 0 ? (CGFloat(self.showImage.count/3) + 1) *  CGFloat(self.height) : CGFloat(self.showImage.count/3) *  CGFloat(self.height)
                self.collectionViewhightConstraints.constant = heightConstant
                self.imageCollectionView.reloadData()
                self.imagesData[self.imageType.rawValue] = self.profileImages
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width : CGFloat = collectionView.bounds.width/2
        return CGSize(width: width, height: CGFloat(height))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

extension CarDocumentationViewController {
    
    func openiCloudDrive() {
        self.setupiCloud()
        let cloudIsAvailable: Bool = iCloud.shared().checkAvailability()
        if cloudIsAvailable {
            let picker = UIDocumentPickerViewController(documentTypes:["public.content"], in: UIDocumentPickerMode.import)
            picker.delegate = self
            present(picker, animated: true, completion: nil)
            
        } else {
            self.showToast(message: "Sorry! Your ICloud Drive is Turn OFF. Download .pdf File from ICloud Drive, you need to turn it ON".localized)
        }
    }
    
    func dropboxSignIn(_ controller: UIViewController) {
        if (DropboxClientsManager.authorizedClient == nil) {
            DropboxClientsManager.authorizeFromController(UIApplication.shared,
                                                          controller: self,
                                                          openURL: { (url: URL) -> Void in
                                                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            })
        } else {
            
            
            dropboxListFiles()
        }
    }
    
    //intialize iCloud
    func setupiCloud() {
        
        iCloud.shared().delegate = self
        iCloud.shared().verboseLogging = true
        iCloud.shared().setupiCloudDocumentSync(withUbiquityContainer: nil)
    }
    
    func googleSignIn() {
        
        if let user =  GIDSignIn.sharedInstance()?.currentUser {
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            self.googledriveListFiles()
        }
        else {
            GIDSignIn.sharedInstance().clientID = Constants.kGOOGLE_SIGNIN_CLIENT_KEY
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().scopes = scopes
            GIDSignIn.sharedInstance().signIn()
        }
    }
    
    //MARK:- Google Drive Api
    func googledriveListFiles() {
        
        self.showLoader(color: UIColor.black.withAlphaComponent(0.3))
        let root = "(mimeType = 'application/vnd.google-apps.folder' or  mimeType = 'application/pdf ' or mimeType = 'image/jpeg' or mimeType = 'image/png' or mimeType = 'text/csv'  or mimeType = 'application/msword' or mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' or mimeType = 'application/pdf')"
        
        let query = GTLRDriveQuery_FilesList.query()
        query.pageSize = 100
        query.q = root
        query.fields = "files(id,name,mimeType,modifiedTime,createdTime,fileExtension,size,parents,kind),nextPageToken"
        service.executeQuery(query,
                             delegate: self,
                             didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:))
        )
    }
    
    // Process the response and display list of files in TableView
    @objc func displayResultWithTicket(ticket: GTLRServiceTicket,
                                       finishedWithObject result : GTLRDrive_FileList,
                                       error : NSError?) {
        self.hideLoader()
        if let error = error {
            AlertManager.showAlert(type: .custom(error.localizedDescription))
        }
        else if let files = result.files, !files.isEmpty {
            self.pushToListing(.google(files,service))
        }
    }
    
    func pushToListing(_ document: Document) {
        if let vc = UIStoryboard(name: "Seller", bundle: nil).instantiateViewController(withIdentifier: "DocumentViewController") as? DocumentViewController {
            vc.showFiles(document, dataHandler: { (data,etension,name) in
                if let responseData = data {
                    self.googledata = data
                    self.filetype = etension
                    self.docName = name
                    for controller in self.navigationController!.viewControllers {
                        if controller.isKind(of: CarDocumentationViewController.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                        }
                    }
                } else {
                    self.showToast(message: "The file type is not valid".localized)
                }
            })
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- DropBox Api
    func dropboxListFiles() {
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        DropboxClientsManager.authorizedClient?.files.listFolder(path:"").response { response, error in
            DispatchQueue.main.async {
                self.hideLoader()
                if let result = response {
                    if !result.entries.isEmpty  {
                        self.pushToListing(.dropBox(result.entries))
                    }
                    else {
                        self.showToast(message: "No file found".localized)
                    }
                } else {
                    AlertManager.showAlert(type: .custom("authentication error".localized))
                    self.hideLoader()
                }
            }
        }
    }
}

extension CarDocumentationViewController: UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        if controller.documentPickerMode == UIDocumentPickerMode.import {
            
            self.fileName = url.lastPathComponent
            self.filetype = self.fileName.fileExtension()
            
            if self.filetype == "jpg" {
                self.filetype = url.pathExtension
                minetype = "image/jpeg"
                self.addImages(url, self.fileName)
            }
            else if self.filetype == "jpeg" {
                self.filetype = url.pathExtension
                minetype = "image/jpeg"
                self.addImages(url, self.fileName)
            }
                
            else if self.filetype == "png" {
                self.filetype = url.pathExtension
                minetype = "image/png"
                self.addImages(url, self.fileName)
            }
            else if self.filetype == "csv" {
                self.filetype = url.pathExtension
                minetype = "text/csv"
                self.addImages(url, self.fileName)
            }
            else if self.filetype == "doc" {
                self.filetype = url.pathExtension
                minetype = "application/msword"
                self.addImages(url, self.fileName)
            }
            else if self.filetype == "docx" {
                self.filetype = url.pathExtension
                minetype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                self.addImages(url, self.fileName)
            }
            else if self.filetype == "pdf" {
                self.filetype = url.pathExtension
                self.minetype = "application/pdf"
                self.addImages(url, self.fileName)
            }
                
            else {
                self.showToast(message: "This file format is not supported . Please use this file format  jpg,jpeg,png,csv,doc,docx,pdf.".localized)
            }
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
    }
}

extension CarDocumentationViewController : iCloudDelegate {
    
    func iCloudDidFinishInitializingWitUbiquityToken(_ cloudToken: Any!, withUbiquityContainer ubiquityContainer: URL!) {
        debugPrint("ubiquityContainer == \(ubiquityContainer)")
        let cloud = iCloud.shared().checkUbiquityContainer()
        debugPrint("ubiquity container \(cloud)")
    }
    
    func iCloudAvailabilityDidChange(toState cloudIsAvailable: Bool, withUbiquityToken ubiquityToken: Any!, withUbiquityContainer ubiquityContainer: URL!) {
        debugPrint("UbiquityToken == \(ubiquityToken)")
    }
}

extension CarDocumentationViewController : GIDSignInDelegate, GIDSignInUIDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        
        guard (error == nil) else {
            service.authorizer = nil
            return
        }
        service.authorizer = user.authentication.fetcherAuthorizer()
        googledriveListFiles()
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        print("dismissing Google SignIn")
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        print("presenting Google SignIn")
    }
}

extension CarDocumentationViewController: DropBoxResponseDelegate {
    
    func didAuthenticateDropBoxUser(_ url: URL) {
        self.dropboxListFiles()
    }
}
