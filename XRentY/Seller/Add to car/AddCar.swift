//
//  AddCar.swift
//  XRentY
//
//  Created by user on 07/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

//MARK:- addcar struct
struct AddCar {
    var car: sellerCar?
    var query: [String:Any] = [:]
    var productId : String
}
