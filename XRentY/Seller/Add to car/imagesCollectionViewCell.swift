//
//  imagesCollectionViewCell.swift
//  XRentY
//
//  Created by user on 10/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit

class imagesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var btnRemoveImage: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    var removeImage : (()->())? = nil

    @IBAction func removeImageBtn(_ sender: Any) {
       removeImage?()
    }
}
