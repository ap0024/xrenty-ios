//
//  AddCarViewController.swift
//  XRentY
//
//  Created by user on 13/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit

class AddCarViewController: BaseViewController {
    
    //MARK:- Outlet
    @IBOutlet var txtType: UITextField!
    @IBOutlet var txtSubtype: UITextField!
    @IBOutlet var txtEngineType: UITextField!
    @IBOutlet var txtTransmission: UITextField!
    @IBOutlet var txtMake: UITextField!
    @IBOutlet var txtModel: UITextField!
    @IBOutlet var txtYear: UITextField!
    @IBOutlet var txtAllWheelDrive: UITextField!
    @IBOutlet var txtDescription: UITextView!
    @IBOutlet var txtAC: UITextField!
    @IBOutlet var txtSoundSystem: UITextField!
    @IBOutlet var txtNumberOfSeats: UITextField!
    @IBOutlet var txtChildSeats: UITextField!
    @IBOutlet var btnBack: UIBarButtonItem!
    @IBOutlet var dropDownButtons: [UIImageView]!
    @IBOutlet weak var ConstraintCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var categoryCollection: UICollectionView!
    
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblSubTye: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblEngineType: UILabel!
    @IBOutlet weak var lbltransmission: UILabel!
    @IBOutlet weak var lblWheelDrive: UILabel!
    @IBOutlet weak var lblMake: UILabel!
    @IBOutlet weak var lblmodel: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAC: UILabel!
    @IBOutlet weak var lblSoundSystem: UILabel!
    @IBOutlet weak var lblnumerSeats: UILabel!
    @IBOutlet weak var lblChildSeat: UILabel!
    
    //MARK:- properties
    var category : [Category] = []
    var model : [CarModel] = []
    var make : [CarMake] = []
    var type : [CarType] = []
    var subType : [CarSubCategory] = []
    var engine : [Engine] = []
    var soundSystem : [SoundSystem] = []
    var childSeat : [ChildSeat] = []
    var Ac : [AC] = []
    var numberSeat : [NumberSeat] = []
    var year : [Year] = []
    var wheel : [Wheel] = []
    var carAdd: CarAdd?
    
    //MARK:- empty variable
    var productid = String()
    var carInfo: [String:Any] = [:]
    var id: String?
    var label: String?
    var value : String?
    var typevalue,subtypeValue,engineValue,wheelValue,soundSystemValue,childValue,acValue,numberSeatValue,YearValue,transmissionValue : String?
    
    var count = 6
    var height = 42
    
    var selectedIndex = -1 {
        didSet {
            categoryCollection.reloadData()
        }
    }
    
    var sellercar: sellerCar? {
        didSet {
            if Authentication.isedit
            {
                self.txtType.text = sellercar?.typelabel ?? ""
                self.typevalue = sellercar?.typevalue ?? ""
                self.txtSubtype.text = sellercar?.subcategorylabel ?? ""
                self.subtypeValue = sellercar?.subcategoryvalue ?? ""
                self.txtEngineType.text = sellercar?.enginetypelabel ?? ""
                self.engineValue = sellercar?.enginetypevalue ?? ""
                if sellercar?.transmission == 0 {self.txtTransmission.text = "Automatic"}else{self.txtTransmission.text = "Manual"}
                self.transmissionValue = "\(sellercar?.transmission ?? 0)"
                self.txtEngineType.text = sellercar?.enginetypelabel ?? ""
                self.engineValue = sellercar?.enginetypevalue ?? ""
                self.txtAllWheelDrive.text = sellercar?.allwheeldrivelabel ?? ""
                self.wheelValue = sellercar?.allwheeldrivevalue ?? ""
                self.txtMake.text = sellercar?.brandmakelabel ?? ""
                self.label = sellercar?.brandmakevalue ?? ""
                self.txtModel.text = sellercar?.brandmodellabel ?? ""
                self.value = sellercar?.brandmodelvalue ?? ""
                self.txtYear.text = sellercar?.brandyearlabel ?? ""
                self.YearValue = sellercar?.brandyearvalue ?? ""
                self.txtDescription.text = sellercar?.carDescription ?? ""
                self.txtAC.text = sellercar?.aclabel ?? ""
                self.acValue = sellercar?.aclvalue ?? ""
                self.txtSoundSystem.text = sellercar?.soundsystemslabel ?? ""
                self.soundSystemValue = sellercar?.soundsystemsvalue ?? ""
                self.txtNumberOfSeats.text = sellercar?.numberofseatslabel ?? ""
                self.numberSeatValue = sellercar?.numberofseatsvalue ?? ""
                self.txtChildSeats.text = sellercar?.childseatlabel ?? ""
                self.childValue = sellercar?.childseatvalue ?? ""
                self.productid = sellercar?.id ?? ""
                for i in 0..<category.count {
                    if category[i].id == sellercar?.categoryvalue {
                        selectedIndex = i
                    }
                }
            }
        }
    }
    
    var transmission = [["label": "Automatic", "value": "0"],["label": "Manual", "value": "1"]]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLabel()
        self.showLoader()
        self.addTitleView()
        if Authentication.isedit {getCarDetail()}
        self.fetchCarProperties()
        self.fetchType {
            self.type =  $0
            self.type.insert(CarType("Select an option".localized), at: 0)
        }
        
        dropDownButtons.forEach {
            $0.tintColor = Color.baseColourLightBlue
        }
        transmission.insert(["label": "Select an option".localized, "value": ""], at: 0)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setLabel() {
        lblType.setLocalizedAshtrik()
        lblSubTye.setLocalizedAshtrik()
        lblCategory.setLocalizedAshtrik()
        lblEngineType.setLocalizedAshtrik()
        lbltransmission.setLocalizedAshtrik()
        lblWheelDrive.setLocalizedAshtrik()
        lblMake.setLocalizedAshtrik()
        lblmodel.setLocalizedAshtrik()
        lblYear.setLocalizedAshtrik()
        lblDescription.setLocalizedAshtrik()
        lblAC.setLocalizedAshtrik()
        lblSoundSystem.setLocalizedAshtrik()
        lblnumerSeats.setLocalizedAshtrik()
        lblChildSeat.setLocalizedAshtrik()
    }
    
    @IBAction func next(_ sender: UIButton) {
        self.searchResult()
    }
    
    func getCarDetail()  {
        
        let query = ["product_id":productid ]
        
        print(query)
        self.showLoader(color: UIColor.black.withAlphaComponent(0.3))
        WebService.GetSellerCarDetail(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    self.sellercar = response.object
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func searchResult() {
        
        if !(self.txtType.text!.isEmpty || self.txtSubtype.text!.isEmpty || self.txtEngineType.text!.isEmpty || self.txtTransmission.text!.isEmpty || self.txtMake.text!.isEmpty || self.txtModel.text!.isEmpty || self.txtYear.text!.isEmpty || self.txtAllWheelDrive.text!.isEmpty || self.txtDescription.text!.isEmpty || self.txtAC.text!.isEmpty || self.txtSoundSystem.text!.isEmpty || self.txtNumberOfSeats.text!.isEmpty || self.txtChildSeats.text!.isEmpty || selectedIndex < 0)  {
            let queryItems : [String : Any] = ["car_type": typevalue ?? "",
                                               "sub_category" : subtypeValue ?? "",
                                               "engine_type": engineValue ?? "",
                                               "transmission" : transmissionValue ?? "",
                                               "brand_make" : label ?? "",
                                               "brand_model" : value ?? "",
                                               "category_id":category[selectedIndex].id,
                                               "brand_year":YearValue ?? "",
                                               "description":txtDescription.text ?? "",
                                               "all_wheel_drive":wheelValue ?? "",
                                               "sound_systems":soundSystemValue ?? "",
                                               "child_seat": childValue ?? "",
                                               "ac":acValue ?? "",
                                               "number_of_seats" : numberSeatValue ?? "",
                                               "car_registration" : "",
                                               "insurance_policy_doc" : "",
                                               "drive_id" : "",
                                               "car_registration_number" : "",
                                               "insurance_number" : "",
                                               "insurance_company" : "",
                                               "insurance_company_website" : "",
                                               "product_image" : [],
                                               "insurance_expiry_date" : "",
                                               "product_id" : self.productid]
            
            self.carInfo = queryItems
            self.pushToSecondStep(queryItems)
            
        }
        else
        {
            self.showToast(message: "Please fill all information before proceeding to next step.".localized)
        }
    }
    
    func pushToSecondStep(_ query: [String:Any]) {
        //        let search = AddCar(car: sellercar, query: query)
        //        pushTo(.carDocumentation(search))
        selleraddCar(query)
    }
    
    func selleraddCar(_ query: [String:Any]) {
        
        if Authentication.isedit
        {
            self.carInfo = query
            print(self.carInfo)
            
            self.showLoader(color: UIColor.black.withAlphaComponent(0.3))
            WebService.editCar(queryItems: self.carInfo) { result in
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch result {
                    case .success(let response):
                        print(response)
                        if let user = response.object {
                            self.productid = user.id ?? ""
                            self.getCarDetail()
                            Observer.updateList()
                            let search = AddCar(car: self.sellercar, query: self.carInfo, productId: user.id ?? "")
                            self.pushTo(.carDocumentation(search))
                        }
                    case .failure(let error):
                        AlertManager.showAlert(on: self, type: .custom(error.message))
                    }
                }
            }
        }
        else
        {
            self.showLoader(color: UIColor.black.withAlphaComponent(0.3))
            WebService.addCar(queryItems: query) { result in
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch result {
                    case .success(let response):
                        if let user = response.object {
                            
                            self.productid = user.id ?? ""
                            self.carInfo["product_id"] = self.productid
                            self.getCarDetail()
                            Observer.updateList()
                            let search = AddCar(car: self.sellercar, query: self.carInfo, productId: user.id ?? "")
                            
                            self.pushTo(.carDocumentation(search))
                        }
                        
                    case .failure(let error):
                        AlertManager.showAlert(on: self, type: .custom(error.message))
                    }
                }
            }
        }
    }
}

extension AddCarViewController {
    
    func showPickerView(title: String, tag: Int, list: [String], id: [String] ) {
        let picker = CustomPickerView()
        
        print(list)
        picker.list = list
        picker.showPickerView(title: title, tag: tag)
        picker.didSelectRow = { self.updateFieldsValue($0,$1, list, id) }
    }
    
    // open picker
    func openPicker(for sender : Int) {
        
        switch sender {
        case 0:
            let names = type.compactMap {return $0.label}
            let value = type.compactMap {return $0.value}
            showPickerView(title: "Select Type".localized, tag: 0, list: names, id: value)
        case 1:
            let names = subType.compactMap { return $0.label}
            let value = subType.compactMap {return $0.value}
            showPickerView(title: "Select subType".localized, tag: 1, list: names, id: value)
        case 2:
            let names = engine.compactMap { return $0.label}
            let value = engine.compactMap {return $0.value}
            showPickerView(title: "Select engine".localized, tag: 2, list: names, id: value)
        case 3:
            let names = transmission.compactMap { return $0["label"]}
            let value = transmission.compactMap {return $0["value"]}
            showPickerView(title: "Select transmission".localized, tag: 3, list: names, id: value)
        case 4:
            let names = make.compactMap { return $0.label}
            let value = make.compactMap {return $0.value}
            showPickerView(title: "Select Make".localized, tag: 4, list: names, id: value)
        case 5:
            let names = model.compactMap { return $0.label}
            let value = model.compactMap {return $0.value}
            showPickerView(title: "Select Model".localized, tag: 5, list: names, id: value)
        case 6:
            let names = year.compactMap { return $0.label}
            let value = year.compactMap {return $0.value}
            showPickerView(title: "Select year".localized, tag: 6, list: names, id: value)
        case 7:
            let names = Ac.compactMap { return $0.label}
            let value = Ac.compactMap {return $0.value}
            showPickerView(title: "Select Ac".localized, tag: 7, list: names, id: value)
        case 8:
            let names = soundSystem.compactMap { return $0.label}
            let value = soundSystem.compactMap {return $0.value}
            showPickerView(title: "Select sound System".localized, tag: 8, list: names, id: value)
        case 9:
            let names = numberSeat.compactMap { return $0.label}
            let value = numberSeat.compactMap {return $0.value}
            showPickerView(title: "Select Seat Number".localized, tag: 9, list: names, id: value)
        case 10:
            let names = childSeat.compactMap { return $0.label}
            let value = childSeat.compactMap {return $0.value}
            showPickerView(title: "Select child Seat".localized, tag: 10, list: names, id: value)
        case 11:
            let names = wheel.compactMap { return $0.label}
            let value = wheel.compactMap {return $0.value}
            showPickerView(title: "Select All wheel Drive".localized, tag: 11, list: names, id: value)
        default:
            break
        }
    }
    
    func updateFieldsValue( _ row: Int,_ tag: Int,_ list : [String], _ id: [String]) {
        switch tag {
            
        case 0:
            self.txtType.text = list[row] == "Select an option".localized ? "" : list[row]
            self.typevalue = id[row]
        case 1:
            self.txtSubtype.text = list[row] == "Select an option".localized ? "" : list[row]
            self.subtypeValue = id[row]
        case 2:
            self.txtEngineType.text = list[row] == "Select an option".localized ? "" : list[row]
            self.engineValue = id[row]
        case 3:
            self.txtTransmission.text = list[row] == "Select an option".localized ? "" : list[row]
            self.transmissionValue = id[row]
        case 4:
            if self.txtMake.text == list[row] { return }
            self.txtMake.text = list[row] == "Select an option".localized ? "" : list[row]
            self.txtModel.text = ""
            self.value = ""
            self.fetchModel(self.txtMake.text!)
            self.label = id[row]
        case 5:
            self.txtModel.text = list[row] == "Select an option".localized ? "" : list[row]
            self.value = id[row]
        case 6:
            self.txtYear.text = list[row] == "Select an option".localized ? "" : list[row]
            self.YearValue = id[row]
        case 7:
            self.txtAC.text = list[row] == "Select an option".localized ? "" : list[row]
            self.acValue = id[row]
        case 8:
            self.txtSoundSystem.text = list[row] == "Select an option".localized ? "" : list[row]
            self.soundSystemValue = id[row]
        case 9:
            self.txtNumberOfSeats.text = list[row] == "Select an option".localized ? "" : list[row]
            self.numberSeatValue = id[row]
        case 10:
            self.txtChildSeats.text = list[row] == "Select an option".localized ? "" : list[row]
            self.childValue = id[row]
        case 11:
            self.txtAllWheelDrive.text = list[row] == "Select an option".localized ? "" : list[row]
            self.wheelValue = id[row]
            
        default:
            break
        }
    }
    
    // method to fetch car categories, make and model
    func fetchCarProperties() {
        self.fetchCategories {
            self.category =  $0
            DispatchQueue.main.async {
                self.fetchMake {
                    self.make = $0
                    self.make.insert(CarMake("Select an option".localized), at: 0)
                }
            }
        }
    }
    
    //MARK:- fetch car categories
    func fetchCategories(completion:@escaping ([Category])->()) {
        WebService.getCategory { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                    self.ConstraintCollectionHeight.constant = CGFloat(self.category.count/2) *  CGFloat(self.height)
                    self.categoryCollection.reloadData()
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- fetch car make
    func fetchMake(completion:@escaping ([CarMake])->()) {
        WebService.getMake { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- fetch car Type
    func fetchType(completion:@escaping ([CarType])->()) {
        WebService.getTpye { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                    self.fetchSubType {
                        self.subType =  $0
                        self.subType.insert(CarSubCategory("Select an option".localized), at: 0)
                    }
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- fetch car SubType
    func fetchSubType(completion:@escaping ([CarSubCategory])->()) {
        WebService.getSubType { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                    self.fetchEngine {
                        self.engine =  $0
                        self.engine.insert(Engine("Select an option".localized), at: 0)
                    }
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- car Engine
    func fetchEngine(completion:@escaping ([Engine])->()) {
        WebService.getEngineType { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                    self.fetchSoundSystem {
                        self.soundSystem =  $0
                        self.soundSystem.insert(SoundSystem("Select an option".localized), at: 0)
                    }
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- car Sound System
    func fetchSoundSystem(completion:@escaping ([SoundSystem])->()) {
        WebService.getSoundSystem { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                    self.getChildSeat {
                        self.childSeat =  $0
                        self.childSeat.insert(ChildSeat("Select an option".localized), at: 0)
                    }
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- car Child Seat
    func getChildSeat(completion:@escaping ([ChildSeat])->()) {
        WebService.getChildSeat { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                    self.getAc {
                        self.Ac =  $0
                        self.Ac.insert(AC("Select an option".localized), at: 0)
                    }
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- car AC
    func getAc(completion:@escaping ([AC])->()) {
        WebService.getAc { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                    
                    self.NumberofSeat {
                        self.numberSeat =  $0
                        self.numberSeat.insert(NumberSeat("Select an option".localized), at: 0)
                    }
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- car NumberSeat
    func NumberofSeat(completion:@escaping ([NumberSeat])->()) {
        WebService.getNumberSeat { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                    
                    self.getyear {
                        self.year =  $0
                        self.year.insert(Year("Select an option".localized), at: 0)
                    }
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- car getyear
    func getyear(completion:@escaping ([Year])->()) {
        WebService.getyear { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                    
                    self.getWheel {
                        self.wheel =  $0
                        self.wheel.insert(Wheel("Select an option".localized), at: 0)
                    }
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- car wheel
    func getWheel(completion:@escaping ([Wheel])->()) {
        WebService.getWheel { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- fetch car Model
    func fetchModel(_ name: String) {
        
        var queryItems : [String:Any] = [:]
        
        if name == "" {
            self.txtModel.text = ""
            return
        } else {
            queryItems = ["make": name]
        }
        
        txtModel.isUserInteractionEnabled = false
        WebService.getModel(queryItems: queryItems) { result in
            DispatchQueue.main.async {
                self.txtModel.isUserInteractionEnabled = true
                switch result {
                case .success(let response):
                    self.model = response.objects ?? []
                    self.model.insert(CarModel("Select an option".localized), at: 0)
                case .failure(_):
                    break
                }
            }
        }
    }
    
    // textfield delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtModel && (txtMake.text!.isEmpty || txtMake.text == "All")  {
            self.value = nil
            return false
        }
        if textField == txtMake && make.isEmpty { return false }
        if textField == txtType && type.isEmpty { return false }
        if textField == txtSubtype && subType.isEmpty { return false }
        if textField == txtEngineType && engine.isEmpty { return false }
        if textField == txtTransmission && transmission.isEmpty { return false }
        if textField == txtYear && year.isEmpty { return false }
        if textField == txtChildSeats && childSeat.isEmpty { return false }
        if textField == txtNumberOfSeats && numberSeat.isEmpty { return false }
        if textField == txtSoundSystem && soundSystem.isEmpty { return false }
        if textField == txtAC && Ac.isEmpty { return false }
        if textField == txtAllWheelDrive && wheel.isEmpty { return false }
        
        // open picker view for category, make and model
        let showPicker = (textField == txtMake || textField == txtModel || textField == txtType || textField == txtSubtype || textField == txtEngineType || textField == txtTransmission || textField == txtYear || textField == txtChildSeats || textField == txtAllWheelDrive  || textField == txtNumberOfSeats || textField == txtSoundSystem || textField == txtAC)
        showPicker ? self.openPicker(for: textField.tag) : nil
        
        return false
    }
}

extension AddCarViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return category.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCollectionViewCell
        cell.configure(category[indexPath.row])
        cell.btnSelect.setImage(UIImage(named: selectedIndex == indexPath.row ? "checkdark.png" : "uncheck.png") , for: .normal)
        self.hideLoader()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width : CGFloat = collectionView.bounds.width/2
        return CGSize(width: width, height: CGFloat(height))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

