//
//  sellerReturnUIViewController.swift
//  XRentY
//
//  Created by user on 26/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit
import IBLocalizable
import PaypalSDK

class sellerReturnUIViewController: BaseViewController,UITextViewDelegate {
    
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var txtDiscription: UITextView!
    @IBOutlet weak var imgAccept: UIImageView!
    @IBOutlet weak var imgNotAccept: UIImageView!
    @IBOutlet weak var returnDate: UILabel!
    @IBOutlet weak var lblPickUpDate: UILabel!
    @IBOutlet weak var btnReturn: UIButton!
    @IBOutlet weak var saveData : UIButton!
    @IBOutlet weak var returnTableView: UITableView!
    
    var savedFiles = [[String: AnyObject]]()
    var savedtableFiles = [[String: AnyObject]]()
    var newEntry = [String: AnyObject]()
    var newTableEntry = [String: AnyObject]()
    var Rent: Rent?
    var returnValue = 0
    var name: String?
    var PickUpCar : [PickUpCar] = []
    var returnCarHandler: (()->())? = nil
    var paypalAuthCompletion: (()->())? = nil
    
    
    
    // return required imageString for paramater to send
    var imageString: String? {
        if carImage.image == #imageLiteral(resourceName: "imageBg_ic") { return nil }
        
        var imageData : Data?
        var compressedImage : UIImage?
        let max = 2097152
        imageData = carImage.image!.pngData()
        if (imageData?.count)! > max {
            imageData = carImage.image!.jpegData(compressionQuality: 0.5)
            compressedImage = UIImage(data: imageData!)
        }
        else {
            compressedImage = carImage.image
        }
        let (base64,format) = compressedImage!.convertImageTobase64()
        let imageString = "data:image/\(format.rawValue);base64," + base64!
        return imageString
    }
    
    lazy var picker: UIImagePicker = {
        let imagePicker = UIImagePicker()
        imagePicker.pickerHandler = { self.addProfilePhoto($0, $1)}
        return imagePicker
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addTitleView()
        self.txtDiscription.delegate = self
        self.enableRegister(false)
        lblPickUpDate.text = Rent?.pickupDate ?? ""
        returnDate.text = Rent?.returnDate
        saveData.alpha = 0.4
        saveData.isUserInteractionEnabled = false
        btnReturn.alpha =  0.4
        btnReturn.isUserInteractionEnabled = false
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addProfilePhoto(_ image: UIImage,_ name : String) {
        carImage.image = image
        self.name = name
    }
    
    @IBAction func addDescription ( _ sender : UIButton)
    {
        if !(savedFiles.count < 10) {
            AlertManager.showAlert(type: .custom("You can upload maximum 10 images.".localized))
            return
        }
        
        if carImage.image == nil && txtDiscription.text == ""
        {
            AlertManager.showAlert(type: .custom("Please add car image and description".localized))
        }
        else if carImage.image == nil
        {
            AlertManager.showAlert(type: .custom("Please add car image ".localized))
        }
        else if txtDiscription.text == ""
        {
            AlertManager.showAlert(type: .custom("Please add car description".localized))
        }
        else
        {
            newEntry["desc"] = txtDiscription.text as AnyObject
            newEntry["image"] = imageString as AnyObject
            savedFiles.insert(newEntry, at: 0)
            
            newTableEntry["desc"] = txtDiscription.text as AnyObject
            newTableEntry["image"] =  carImage.image as AnyObject
            savedtableFiles.insert(newTableEntry, at: 0)
            returnTableView.reloadData()
            
            carImage.image = nil
            txtDiscription.text = ""
        }
    }
    
    @IBAction func carPhoto(_ sender: UIButton) {
        self.picker.showSheet(false)
    }
    
    @IBAction func returnOkBtn(_ sender: Any) {
        imgAccept.image = #imageLiteral(resourceName: "radio_select_ic")
        imgNotAccept.image = #imageLiteral(resourceName: "radiounselect_ic")
        returnValue = 2
        btnReturn.alpha =  1
        btnReturn.isUserInteractionEnabled = true
    }
    
    @IBAction func returnNotOkBtn(_ sender: Any) {
        imgAccept.image = #imageLiteral(resourceName: "radiounselect_ic")
        imgNotAccept.image = #imageLiteral(resourceName: "radio_select_ic")
        returnValue = 3
        btnReturn.alpha =  1
        btnReturn.isUserInteractionEnabled = true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        saveData.alpha = 1
        saveData.isUserInteractionEnabled = true
    }
    
    @IBAction func returnCar(_ sender: Any) {
        
        //        print("\n**********************************")
        //        print("orderId: \(Rent?.orderId)")
        //        print("charge: \(Rent?.charge)")
        //        print("charge transaction id: \(Rent?.chargeTransactionId)")
        //        print("deposite: \(Rent?.deposit)")
        //        print("deposite transaction id: \(Rent?.depositTransactionId)")
        //        print("**********************************\n")
        
        if savedFiles.isEmpty
        {
            AlertManager.showAlert(type: .custom("Please add car image description".localized))
        }
        else
        {
            if let rent =  self.Rent, let paymentMethod = rent.payment_method,  paymentMethod.contains("paypal") {
                if let orderid = rent.paypal_deposit_order_id {
                    if rent.isOrderFromWeb {
                        showLoaderOnWindow()
                        PaypalSDK.shared.delegate = self
                        if returnValue == 2 {
                            PaypalSDK.shared.cancelV1(transactionId: orderid)
                        } else if let currencyCode = rent.currencyCode, let deposit = rent.depositeValue   {
                            PaypalSDK.shared.captureV1(transactionId: orderid, total: deposit, currency: currencyCode)
                        } else {
                            print("amount should be upto two decimal places")
                        }
                    } else {
                        self.showLoaderOnWindow()
                        PaypalSDK.shared.delegate = self
                        PaypalSDK.shared.orderDetail(for: orderid)
                    }
                } else {
                    print("order id is nil")
                }
            } else {
                self.releaseChargeDeposit()
            }
        }
    }
    
    func releaseChargeDeposit() {
        
        guard let depositeTransactionId = self.Rent?.depositTransactionId else {
            return
        }
        
        if !(depositeTransactionId.isEmpty)  {
            if self.returnValue == 2 {
                self.releaseDeposit()
            }
            else if self.returnValue == 3 {
                self.chargeDeposit()
            }
        }
        else {
            self.carReturn()
        }
    }
    
    func releaseDeposit() {
        self.releaseDeposit { (success,message) in
            if success {
                self.carReturn()
            }
            else {
                AlertManager.showAlert(on: self, type: .custom(message))
            }
        }
    }
    
    func chargeDeposit() {
        self.chargeDeposit { (success,message) in
            if success {
                self.carReturn()
            }
            else {
                AlertManager.showAlert(on: self, type: .custom(message))
            }
        }
    }
    
    // desposite charge confirmation
    func chargeDeposit(completion: @escaping (Bool,String)->()) {
        
        guard let depositTransactionId = self.Rent?.depositTransactionId, let customerId = Rent?.customerOpenpayId,let convertedCustomerDepositAmount = Rent?.convertedCustomerDepositAmount, let deposit = Rent?.deposit else {
            completion(false,"Deposit transaction id not generated".localized)
            return
        }
        
        showLoaderOnWindow()
        OpenpayPayment.confirmCharge(amount: "\(convertedCustomerDepositAmount)", customerId: customerId, transactionId: depositTransactionId) { (json) in
            DispatchQueue.main.async {
                self.hideLoader()
                guard let response = json else {
                    return
                }
                if let status = response["status"] as? String,status == "completed" {
                    completion(true, "Charge deposit success".localized)
                }
                else {
                    if let description = response["description"] as? String {
                        completion(false, description)
                    } else {
                        completion(false, "Charge deposit fails".localized)
                    }
                }
            }
        }
    }
    
    // deposite charge refund
    func releaseDeposit(completion: @escaping (Bool,String)->()) {
        
        guard let deposit = self.Rent?.deposit, let exchangeRate = self.Rent?.exchangeRate, let depositTransactionId = self.Rent?.depositTransactionId, let customerId = Rent?.customerOpenpayId,let convertedCustomerDepositAmount = Rent?.convertedCustomerDepositAmount else {
            completion(false,"Deposit transaction id not generated".localized)
            return
        }
        
        var calculatedDeposit = String()
        if let calculativeDeposit = Float(deposit) , let calculativeRate = Float(exchangeRate){
            //            calculatedDeposit = String(calculativeDeposit*calculativeRate)
            //            calculatedDeposit = String(971.22)
            print(calculativeDeposit*calculativeRate)
            let test = String(format:"%.3f", calculativeDeposit*calculativeRate)
            print(test)
            //            calculatedDeposit = convertedCustomerDepositAmount
        }
        
        showLoaderOnWindow()
        
        print(convertedCustomerDepositAmount)
        print(depositTransactionId)
        
        OpenpayPayment.refundAmount(amount: convertedCustomerDepositAmount, customerId: customerId, transactionId: depositTransactionId) { (json,message) in
            DispatchQueue.main.async {
                self.hideLoader()
                guard let response = json else {
                    AlertManager.showAlert(type: .custom(message))
                    return
                }
                if let status = response["status"] as? String,status == "cancelled" {
                    completion(true, "Refund success".localized)
                } else {
                    
                    if let description = response["description"] as? String {
                        completion(false, description)
                    } else {
                        completion(false, "Refund deposit failed".localized)
                    }
                }
            }
        }
    }
    
    // returnCar
    func carReturn() {
        
        let query = ["post_image": savedFiles ,
                     "return_status": returnValue,
                     "order_id": (Rent?.orderId)!] as [String : Any]
        
        self.showLoaderOnWindow()
        WebService.returnCarSeller(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    AlertManager.showAlert(type: .custom(response.message!) , action: {
                        self.navigationController?.popToRootViewController(animated: true)
                        self.returnCarHandler?()
                        Observer.ListUpdate()
                    })
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    // enable register button if user accepts terms and condition
    func enableRegister(_ show: Bool) {
        btnReturn.alpha = show ? 1 : 0.6
        btnReturn.isUserInteractionEnabled = show
    }
    
    func setCheckUncheck(_ view: UIImageView) -> Bool {
        
        view.tag = view.tag == 1 ? 0 : 1
        let checkboxImage = UIImage(named: "radio_select_ic")
        view.image = view.tag == 1 ? checkboxImage : nil
        return view.tag == 1
    }
}

extension sellerReturnUIViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedtableFiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sellerReturnCell", for: indexPath) as? sellerReturnCell
        
        let info = savedtableFiles[indexPath.row]
        cell?.txtxDiscription.text = info["desc"] as? String
        cell?.imgCar.image = info["image"] as? UIImage
        
        cell?.zoomImage = { self.zoomImage((cell?.imgCar.image)!, (cell?.txtxDiscription.text)!) }
        
        return cell!
    }
    
    
    func zoomImage(_ zoomImage: UIImage, _ disc : String) {
        
        self.enlargeImage(view: self, profileImage: UIImageView(image: zoomImage), text: disc)
    }
}

extension sellerReturnUIViewController: PaymentDelegate {
    
    func didUpdateOrder(with state: PaymentState) {
        switch state {
        case .capture:
            self.hideLoader()
            self.carReturn()
        case .cancel:
            self.hideLoader()
            self.carReturn()
        case .detail(let order):
            self.hideLoader()
            if self.returnValue == 2 {
                if let authId = ((order.purchase_units?[0])?.payments?.authorizations?[0])?.id {
                    self.showLoaderOnWindow()
                    PaypalSDK.shared.cancel(authorizationId: authId)
                }
            }
            else if self.returnValue == 3 {
                if let link = ((order.purchase_units?[0])?.payments?.authorizations?[0])?.links.find(where: "capture") {
                    self.showLoaderOnWindow()
                    PaypalSDK.shared.capture(link: link)
                }
            }
        default:
            break
        }
    }
    
    func didFailUpdateOrder(with error: PaymentError) {
        self.hideLoader()
        AlertManager.showAlert(type: .custom(error.errorDescription))
    }
}


