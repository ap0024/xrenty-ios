//  SellerProfileViewController.swift
//  XRentY
//  Created by user on 26/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.

import UIKit
import IBLocalizable

class SellerProfileViewController: BaseViewController {
    
    @IBOutlet var imageProfile: UIImageView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtPincode: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var saveProfile: UIButton!
    @IBOutlet weak var txtBankName: UITextField!
    @IBOutlet weak var txtAccountHolderName: UITextField!
    @IBOutlet weak var txtClabeAccount: UITextField!
    @IBOutlet weak var txtCompany: UITextField!
    @IBOutlet weak var txtCurrency: UITextField!
    
    @IBOutlet weak var lblFname: UILabel!
    @IBOutlet weak var lblLName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lbladdress: UILabel!
    @IBOutlet weak var lblZipCode: UILabel!
    @IBOutlet weak var lblPhnNumber: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblAccountHldrName: UILabel!
    @IBOutlet weak var lblAccNumber: UILabel!
    @IBOutlet weak var lblBank: UILabel!
    
    var countries: [Country] = []
    var banks : [Bank] = []
    var userImage :  String = ""
    var isEmailEdit : String = ""
    var currency: [String] = []
    var name : String = ""
    var isEdit = true
    lazy var picker: UIImagePicker = {
        let imagePicker = UIImagePicker()
        imagePicker.pickerHandler = { self.addProfilePhoto($0, $1)}
        return imagePicker
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLabel()
        self.profile()
        self.getCountries()
        self.Banklist()
        self.currency = ["USD","MXN"]
        self.txtEmail.isUserInteractionEnabled = false
        userEnabled(isEdit: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        imageProfile.addShadow()
        cameraButton.bringSubviewToFront(self.view)
    }
    
    func setLabel() {
        
        lblFname.setLocalizedAshtrik()
        lblLName.setLocalizedAshtrik()
        lblEmail.setLocalizedAshtrik()
        lblCountry.setLocalizedAshtrik()
        lblState.setLocalizedAshtrik()
        lblCity.setLocalizedAshtrik()
        lbladdress.setLocalizedAshtrik()
        lblZipCode.setLocalizedAshtrik()
        lblPhnNumber.setLocalizedAshtrik()
        lblCompany.setLocalizedAshtrik()
        lblAccountHldrName.setLocalizedAshtrik()
        lblAccNumber.setLocalizedAshtrik()
        lblBank.setLocalizedAshtrik()
        
    }
    
    
    
    // enable profile button
    func enableRegister(_ show: Bool) {
        saveProfile.alpha = show ? 1 : 0.6
        saveProfile.isUserInteractionEnabled = show
    }
    
    func addProfilePhoto(_ image: UIImage, _ name : String) {
        self.imageProfile.image = image
        self.name = name
    }
    
    func userEnabled(isEdit:Bool) {
        
        self.txtFirstName.resignFirstResponder()
        self.enableRegister(isEdit)
        let txtFields = [txtFirstName,txtEmail,txtLastName,txtCity,txtState,txtCountry,txtPincode,txtPhone,txtBankName,txtAddress,txtCompany,txtAccountHolderName,txtClabeAccount,txtBankName,txtCurrency]
        for textField in txtFields {
            textField?.isUserInteractionEnabled = isEdit
        }
        imageProfile.isUserInteractionEnabled = isEdit
        imageProfile.isUserInteractionEnabled = isEdit
        cameraButton.isUserInteractionEnabled = isEdit
        
        if isEdit {
            
            if self.isEmailEdit.contains("1")
            {
                self.txtEmail.isUserInteractionEnabled = true
            } else
            {
                self.txtEmail.isUserInteractionEnabled = false
            }
        }
    }
    
    @IBAction func editprofileAction(_ sender: Any){
        isEdit = true
        userEnabled(isEdit: true)
        isEdit ? txtFirstName.becomeFirstResponder() : nil 
    }
    
    @IBAction func saveProfileAction(_ sender: Any) {
        if isEdit {
            if isValidForm {
                editProfileApi()
            }
        }
    }
    
    func editProfileApi () {
        
        var query = parameters
        if let image = imageString {
            query["image"] = image
        }
        
        showLoader(color: .clear)
        WebService.editProfile(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    self.userEnabled(isEdit: false)
                    AlertManager.showAlert(type: .custom("Profile is updated successfully".localized), action: {
                        self.profile()
                        Authentication.clabeAccountDetail = 1
                        Observer.updateList()
                        Observer.ListUpdate()
                        Observer.poptoSearch()
                        Observer.ListReview()
                        
                    })
                case .failure(let error):
                    self.userEnabled(isEdit: true)
                    AlertManager.showAlert(on: self, type: .custom(error.message ?? ""))
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func openCamera(_ sender: Any) {
        self.picker.showSheet(false)
    }
    
    func profile () {
        let query = ["is_seller": "1"]
        let grayColor = UIColor(red: 230, green: 230, blue: 241, alpha: 1)
        showLoader(color: grayColor)
        WebService.profile(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    let profile = response.object
                    self.txtCity.text = profile?.city ?? "N/A"
                    self.txtCountry.text = profile?.countryName ?? "N/A"
                    self.txtEmail.text = profile?.email ?? "N/A"
                    self.txtFirstName.text = profile?.firstname ?? "N/A"
                    self.imageProfile.kf.indicatorType = .activity
                    self.userImage = profile?.image ?? ""
                    let targetURL = URL(string: profile?.image ?? "")
                    self.imageProfile.kf.setImage(with: targetURL , placeholder: #imageLiteral(resourceName: "user_ic"))
                    self.txtLastName.text = profile?.lastname ?? "N/A"
                    self.txtPincode.text = profile?.pincode ?? "N/A"
                    Authentication.clabeAccountDetail = profile?.clabeAccountDetail
             //       print(Authentication.clabeAccountDetail)
                    self.txtState.text = profile?.state ?? "N/A"
                    self.isEmailEdit = profile?.iseditable ?? ""
                    let phone = profile?.telephone ?? "N/A"
                    self.txtCurrency.text = profile?.current_currency ?? ""
                    let currency = profile?.current_currency ?? ""
                    Authentication.currency = currency
                    let language = NSLocale.current.languageCode
                    Authentication.currency = currency
                    if currency == "USD"
                    {
                        Authentication.currencySymbol = "US$"
                    }
                    else if currency == "MXN"
                    {
                        Authentication.currencySymbol = "MX$"
                    }
                    else if currency == ""
                    {
                        if language == "en"
                        {
                            Authentication.currencySymbol = "US$"
                            Authentication.currency = "USD"
                        }
                        else if language == "es"
                        {
                            Authentication.currencySymbol = "MX$"
                            Authentication.currency  = "MXN"
                        }
                        else
                        {
                            Authentication.currencySymbol = "US$"
                            Authentication.currency  = "USD"
                        }
                    }
                    if phone.isEqualToString(find: "null")
                    {
                        self.txtPhone.text = ""
                    }else
                    {
                        self.txtPhone.text = phone
                    }
                    self.txtAddress.text = profile?.address ?? "N/A"
                    self.txtCompany.text = profile?.company ?? "N/A"
                    self.txtAccountHolderName.text = profile?.accountHolderName ?? "N/A"
                    self.txtClabeAccount.text = profile?.accountNumber ?? "N/A"
                    self.txtBankName.text = profile?.bank ?? "N/A"
                    Authentication.customerName = (profile?.firstname)! + " " + (profile?.lastname)!
                    Authentication.customerImage = profile?.image ?? ""
                    self.hideLoader()
                    
                    Authentication.clabeAccountDetail = self.txtClabeAccount.text! != "" ? 1 : 0
                    
                    
                    
                    
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    

    
    // get countries data
    func getCountries() {
        WebService.getCountries { result in
            switch result {
            case .success(let response):
                self.countries = response.objects ?? []
            case .failure(_):
                break
            }
        }
    }
    
    // get bank data
    func Banklist() {
        WebService.Banklist { result in
            switch result {
            case .success(let response):
                self.banks = response.objects ?? []
            case .failure(_):
                break
            }
        }
    }
    
    // show country picker
    func showCountryPicker() {
        self.view.endEditing(true)
        if self.countries.isEmpty { return }
        let picker = CustomPickerView()
        picker.list = self.countries.flatMap { return $0.name }
        picker.showPickerView(title: "Countries".localized, tag: 0)
        picker.didSelectRow = { index,_ in  self.txtCountry.text = self.countries[index].name }
    }
    
    // show country picker
    func showBankPicker() {
        self.view.endEditing(true)
        if self.banks.isEmpty { return }
        let picker = CustomPickerView()
        picker.list = self.banks.flatMap { return $0.name }
        picker.showPickerView(title: "Bank list", tag: 0)
        picker.didSelectRow = { index,_ in  self.txtBankName.text = self.banks[index].name
        }
    }
    
    // show country picker
    func showCurrencyPicker() {
        self.view.endEditing(true)
        if self.currency.isEmpty { return }
        let picker = CustomPickerView()
        picker.list = self.currency.compactMap { return $0 }
        picker.showPickerView(title: "Currency".localized, tag: 0)
        picker.didSelectRow = { index,_ in  self.txtCurrency.text = self.currency[index]}
    }
    
    // textfield should begin editing
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCountry
        {
            self.showCountryPicker()
        }
        else if  textField == txtBankName
        {
            self.showBankPicker()
        }
        else if  textField == txtCurrency
        {
            self.showCurrencyPicker()
        }

        return !(textField == txtCountry || textField == txtBankName || textField == txtCurrency)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtState
        {
            if txtCountry.text == ""
            {
                AlertManager.showAlert(type: .custom("Please select country.".localized))
            }
        }
        else if textField == txtCity
        {
            if txtCountry.text == ""
            {
                AlertManager.showAlert(type: .custom("Please select country.".localized))
            }
            else if txtState.text == ""
            {
                AlertManager.showAlert(type: .custom("Please select state.".localized))
            }
        }
    }
}

extension SellerProfileViewController {
    
    // return required imageString for paramater to send
    var imageString: String? {
        if imageProfile.image == #imageLiteral(resourceName: "user_ic") || imageProfile.image == nil { return nil }
        
        var imageData : Data?
        var compressedImage : UIImage?
        let max = 2097152
        imageData = imageProfile.image!.pngData()
        if (imageData?.count)! > max {
            imageData = imageProfile.image!.jpegData(compressionQuality: 0.5)
            compressedImage = UIImage(data: imageData!)
        }
        else {
            compressedImage = imageProfile.image
        }
        let (base64,format) = compressedImage!.convertImageTobase64()
        let imageString = "data:image/\(format.rawValue);base64," + base64!
        return imageString
    }

//    // return 0 for invalid form and 1 form valid form details
var isValidForm: Bool {
    guard let error = errorDescription else {
        return true
    }
    
    AlertManager.showAlert(on: self, type: .custom(error))
    return false
}

// return corresponding country code for selected country
var countryCode: String? {
    var code : String?
    countries.forEach {
        if $0.name == txtCountry.text! { code = $0.country_id }
    }
    return code
}
    
    var bankCode: String? {
        var code : String?
        banks.forEach {
            if $0.name == txtBankName.text! { code = $0.id }
        }
        return code
    }

// parameters to be used
var parameters: [String:Any] {
    return ["firstname":self.txtFirstName.text?.trimmingCharacters(in:
        CharacterSet.whitespaces) ?? "" ,
            "lastname":self.txtLastName.text?.trimmingCharacters(in:
                CharacterSet.whitespacesAndNewlines) ?? "",
            "email":self.txtEmail.text?.trimmingCharacters(in:
                CharacterSet.whitespacesAndNewlines) ?? "",
            "country":countryCode ?? "",
            "state":self.txtState.text?.trimmingCharacters(in:
                CharacterSet.whitespacesAndNewlines) ?? "",
            "city":self.txtCity.text?.trimmingCharacters(in:
                CharacterSet.whitespacesAndNewlines) ?? "",
            "pincode":self.txtPincode.text ?? "",
            "telephone":self.txtPhone.text ?? "",
            "address":self.txtAddress.text?.trimmingCharacters(in:
                CharacterSet.whitespacesAndNewlines) ?? "",
            "is_seller":1,
            "company":self.txtCompany.text?.trimmingCharacters(in:
                CharacterSet.whitespacesAndNewlines) ?? "",
            "account_number":self.txtClabeAccount.text ?? "",
            "account_holder_name":self.txtAccountHolderName.text ?? "",
            "bank":self.txtBankName.text ?? "",
            "bank_code": bankCode ?? "",
            "is_editable" :isEmailEdit,
            "customer_currencies" :txtCurrency.text ?? ""]
}

// register error
enum ProfileError: String {
    
    case empty = "Please enter your details"
    case firtsname = "First name should be of atleast 3 characters"
    case lastname = "Last name should be of atleast 3 characters"
    case email = "Please enter a valid email address. For example customer@xrenty.com."
    case city
    case state
    case pincode
    case phonenumber
    case accountnumber
    case accountholdername
    case company
    case address
    case image
    case country
    case bank
    
    var emptydescription: String {
        switch self {
        case .firtsname:
            return "Please enter your first name".localized
        case .lastname:
            return "Please enter your last name".localized
        case .email:
            return "Please enter your email address".localized
        case .city:
            return "Please enter your city".localized
        case .state:
            return "Please enter your state".localized
        case .pincode:
            return "Please enter your zipcode".localized
        case .phonenumber:
            return "Please enter your phone number".localized
        case .accountnumber:
            return "Please enter your account number".localized
        case .accountholdername:
            return "Please enter your CLABE account number".localized
        case .company:
            return "Please enter your company".localized
        case .address:
            return "Please enter your address".localized
        case .image:
            return "Please upload your image".localized
        case .empty:
            return "Please enter your details".localized
        case .country:
            return "Please select your Country".localized
        case .bank:
            return "Please select your bank".localized
        }
    }
}

// return error description to show
var errorDescription: String? {
    return txtFirstName.text!.empty && txtLastName.text!.empty && txtEmail.text!.empty &&   txtCity.text!.empty && txtState.text!.empty && (txtCountry.text?.empty)! && txtPincode.text!.empty && txtPhone.text!.empty && txtAddress.text!.empty  && txtAccountHolderName.text!.empty && txtClabeAccount.text!.empty ? ProfileError.empty.rawValue.localized :
        txtFirstName.text!.empty ? ProfileError.firtsname.emptydescription :
        txtFirstName.text!.count < 3 ? ProfileError.firtsname.rawValue.localized :
        txtLastName.text!.empty ? ProfileError.lastname.emptydescription :
        txtLastName.text!.count < 3 ? ProfileError.lastname.rawValue.localized :
        txtEmail.text!.empty ? ProfileError.email.emptydescription :
        !txtEmail.text!.isValidEmail() ? ProfileError.email.rawValue.localized :
        txtCity.text!.empty ? ProfileError.city.emptydescription :
        txtState.text!.empty ? ProfileError.state.emptydescription :
        txtPincode.text!.empty ? ProfileError.pincode.emptydescription :
        txtPhone.text!.empty ? ProfileError.phonenumber.emptydescription :
        txtAccountHolderName.text!.empty ? ProfileError.accountnumber.emptydescription :
        txtClabeAccount.text!.empty ? ProfileError.accountholdername.emptydescription :
        txtAddress.text!.empty ? ProfileError.address.emptydescription :
        txtCountry.text!.empty ? ProfileError.country.emptydescription :
        txtBankName.text!.empty ? ProfileError.bank.emptydescription :
        (imageProfile.image == nil) ? ProfileError.image.emptydescription :
    nil
}
}
