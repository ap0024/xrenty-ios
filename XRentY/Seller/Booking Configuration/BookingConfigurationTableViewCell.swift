//
//  BookingConfigurationTableViewCell.swift
//  XRentY
//
//  Created by user on 30/09/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit
enum Action{
    case edit
    case delete
}

class BookingConfigurationTableViewCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var specialPriceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    typealias ActionHandler = (Action, Price) -> ()
    var handler : ActionHandler?
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    var object : Price? {
        didSet{
            statusLabel.text = object?.status_label
            startDateLabel.text = object?.start_date
            endDateLabel.text = object?.end_date
            quantityLabel.text = object?.qty
            let mainPrice = object?.price ?? ""
            let specialPrice = object?.specialPrice ?? ""
            priceLabel.text = symbol + "\n" + mainPrice.roundOff()
            specialPriceLabel.text = symbol + "\n" + specialPrice.roundOff()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func didTapEditButton(_ sender: Any) {
        handler?(.edit, object!)
    }
    
    @IBAction func didTapDeletePrice(_ sender: Any) {
        handler?(.delete, object!)
    }
}
