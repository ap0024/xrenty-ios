//
//  BookingConfigurationViewController.swift
//  XRentY
//
//  Created by user on 30/09/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import CoreLocation
import IBLocalizable

//MARK:- Booking Configuration Form
enum BookingConfigurationForm : String{
    case empty = "Please enter the required fields."
    case valid
    
    var valid : Bool {
        switch self {
        case .empty:
            AlertManager.showAlert(type: .custom(rawValue.localized))
            return false
        case .valid:
            return true
        }
    }
}

class BookingConfigurationViewController: BaseViewController ,GMSMapViewDelegate {
    
    
    //MARK:- Outlet
    @IBOutlet weak var detailMapView: ProductDetailMapView!
    @IBOutlet weak var ConstraintTableHeight: NSLayoutConstraint!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var startDateTextfield: UITextField!
    @IBOutlet weak var endDateTextfield: UITextField!
    @IBOutlet weak var statusTextfield: UITextField!
    @IBOutlet weak var cityTextfield: UITextField!
    @IBOutlet weak var specialPriceTextfield: UITextField!
    @IBOutlet weak var priceTextfield: UITextField!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var priceTableView: UITableView!
    @IBOutlet weak var constraintTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtSearchLocation: UITextField!
    @IBOutlet weak var detailMapViewHeight: NSLayoutConstraint!
    @IBOutlet weak var savePriceButton: UIButton!
    @IBOutlet weak var constraintPriceViewTop: NSLayoutConstraint!
    @IBOutlet weak var btnClearfirstDates: UIButton!
    @IBOutlet weak var btnClearSecondDates: UIButton!
    

    //MARK:- Variable
    var bookingId = String()
    var isDefault = Bool()
    var longitude =  String()
    var latitude =  String()
    var address : String?
    var countryBk : String?
    var cityBk : String?
    var stateBk : String?
    var autoComplete =  String()
    var scrollOffsetY: CGFloat!
    var presentMap: Bool = false
    var arrDatesAvailability : [[String:Any]] = []
    var firstDate: Date?
    var lastDate: Date?
    var datesRange: [Date]?
    var isEdit : Bool = false
    var prices = [Price]()
    var status = [Status]()
    var city = [City]()
    var objectPrice : Price?
    var form : BookingConfigurationForm{
        return  statusTextfield.text!.isEmptyString() || cityTextfield.text!.isEmptyString() ? .empty :  .valid
    }
    var formSave : BookingConfigurationForm{
        return  statusTextfield.text!.isEmptyString() || cityTextfield.text!.isEmptyString() || priceTextfield.text!.isEmptyString() ? .empty :  .valid
    }
    
    var showClearfirstDatesButton: Bool = false  {
        didSet {
            self.btnClearfirstDates.alpha = self.showClearfirstDatesButton ? 1 : 0
        }
    }
    
    var showClearSecondDatesButton: Bool = false  {
        didSet {
            self.btnClearSecondDates.alpha = self.showClearSecondDatesButton ? 1 : 0
        }
    }
    
    
    fileprivate var lunar: Bool = false {
        didSet {
            self.calendar.reloadData()
        }
    }
    
    fileprivate let lunarFormatter = LunarFormatter()
    fileprivate var theme: Int = 0 {
        didSet {
            switch (theme) {
            case 0:
                self.calendar.appearance.weekdayTextColor = UIColor(red: 14/255.0, green: 69/255.0, blue: 221/255.0, alpha: 1.0)
                self.calendar.appearance.headerTitleColor = UIColor(red: 14/255.0, green: 69/255.0, blue: 221/255.0, alpha: 1.0)
                self.calendar.appearance.eventDefaultColor = UIColor(red: 31/255.0, green: 119/255.0, blue: 219/255.0, alpha: 1.0)
                self.calendar.appearance.selectionColor = UIColor(red: 31/255.0, green: 119/255.0, blue: 219/255.0, alpha: 1.0)
                self.calendar.appearance.headerDateFormat = "MMMM yyyy"
                self.calendar.appearance.todayColor = UIColor(red: 31/255.0, green: 119/255.0, blue: 219/255.0, alpha: 1.0)//UIColor(red: 198/255.0, green: 51/255.0, blue: 42/255.0, alpha: 1.0)
                self.calendar.appearance.borderRadius = 1.0
                self.calendar.appearance.headerMinimumDissolvedAlpha = 0.2
                
            case 1:
                self.calendar.appearance.weekdayTextColor = UIColor.red
                self.calendar.appearance.headerTitleColor = UIColor.darkGray
                self.calendar.appearance.eventDefaultColor = UIColor.green
                self.calendar.appearance.selectionColor = UIColor.blue
                self.calendar.appearance.headerDateFormat = "yyyy-MM";
                self.calendar.appearance.todayColor = UIColor.red
                self.calendar.appearance.borderRadius = 1.0
                self.calendar.appearance.headerMinimumDissolvedAlpha = 0.0
            case 2:
                self.calendar.appearance.weekdayTextColor = UIColor.red
                self.calendar.appearance.headerTitleColor = UIColor.red
                self.calendar.appearance.eventDefaultColor = UIColor.green
                self.calendar.appearance.selectionColor = UIColor.blue
                self.calendar.appearance.headerDateFormat = "yyyy/MM"
                self.calendar.appearance.todayColor = UIColor.orange
                self.calendar.appearance.borderRadius = 0
                self.calendar.appearance.headerMinimumDissolvedAlpha = 1.0
            default:
                break;
            }
        }
    }
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    fileprivate let gregorian: NSCalendar! = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)
    

    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTitleView()
        
        saveBooking()
        getCityApi()
        getStatusApi()
        getPriceListing()
        self.showClearfirstDatesButton = false
        self.showClearSecondDatesButton = false
        calendar.allowsMultipleSelection = false
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 700
        }
        else
        {
            self.calendarHeightConstraint.constant = 650
        }
        detailMapView.fullScreenHandler = { self.presentMap($0) }
        self.calendar.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesUpperCase]
        self.calendar.accessibilityIdentifier = "calendar"
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(self.latitude) ?? 0.0, longitude: CLLocationDegrees(self.longitude) ?? 0.0)
        self.detailMapView.longitude = longitude
        self.detailMapView.latitude = latitude
        self.detailMapView.configureMap(for: coordinates)
        self.txtSearchLocation.text = autoComplete
        
        Observer.poptoSearch(target: self, selector: #selector(poptosearch))
    }
    
    @objc func poptosearch() {
        self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.viewControllers.first?.tabBarController?.selectedIndex = 0
    }
    
    func getCityApi ()
    {
        WebService.getCity { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self.city = response.objects ?? []
                case .failure(_):
                    print("fail")
                }
            }
        }
    }
    
    func getStatusApi ()
    {
        WebService.getStatus{ (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self.status = response.objects ?? []
                case .failure(_):
                    print("fail")
                }
            }
        }
    }
    
    func presentMap(_ present: Bool) {
        self.presentMap = present
        let point = CGPoint(x: 0, y: present ? detailMapView.frame.origin.y : scrollOffsetY)
        scrollView.isScrollEnabled = !present
        detailMapViewHeight.constant = present ? view.bounds.height - (self.navigationController!.navigationBar.frame.size.height + 64) : 400
        print(detailMapViewHeight.constant)
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        scrollView.setContentOffset(point, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func clearFirstDates(_ sender: UIButton) {
        self.startDateTextfield.text = ""
        self.showClearfirstDatesButton = false
        self.endDateTextfield.text = ""
        self.showClearSecondDatesButton = false
    }
    
    @IBAction func clearsecondDates(_ sender: UIButton) {
        self.endDateTextfield.text = ""
        self.showClearSecondDatesButton = false
    }
    
    func saveBooking()
    {
        let arrbooking = [
            "booking_product_id": bookingId,
            "booking_temp_id": "0",
            "store_id": "0",
            "booking_temp_check_price_update": "0",
            "booking_type": "per_day",
            "booking_time": "1",
            "booking_min_days": "",
            "booking_max_days": "",
            "booking_type_intevals": "1",
            "booking_service_start": [
                "hour": "0",
                "minute": "0",
                "type": "1"
            ],
            "booking_service_end": [
                "hour": "0",
                "minute": "0",
                "type": "1"
            ],
            "booking_tour_type": "1",
            "booking_min_days_xx": "0",
            "booking_fee_night": "10",
            "booking_min_hours": "",
            "booking_max_hours": "",
            "booking_time_slot": "0",
            "booking_time_buffer": "0",
            "booking_show_finish": "1",
            "booking_show_qty": "1",
            "booking_phone": "",
            "booking_email": "",
            "booking_address": "",
            "booking_city": "",
            "booking_zipcode": "",
            "booking_country": "",
            "booking_state": "",
            "auto_address": "",
            "booking_lat": "0",
            "booking_lon": "0"
            ] as [String : Any]
        
        let queryItems : [String : Any] = ["booking_id": bookingId,
                                           "bookings": arrbooking ,
                                           "is_change_intervals": "1",
                                           "is_default_change_intervals": "0"]
        
        self.showLoader()
        WebService.savebooking(queryItems: queryItems) { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    self.saveAgainBooking()
                    print(response)
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func saveAgainBooking()
    {
        let arrbooking = [
            "booking_product_id": bookingId,
            "booking_temp_id": "0",
            "store_id": "0",
            "booking_temp_check_price_update": "0",
            "booking_type": "per_day",
            "booking_time": "1",
            "booking_min_days": "",
            "booking_max_days": "",
            "booking_type_intevals": "1",
            "booking_service_start": [
                "hour": "0",
                "minute": "0",
                "type": "1"
            ],
            "booking_service_end": [
                "hour": "0",
                "minute": "0",
                "type": "1"
            ],
            "booking_tour_type": "1",
            "booking_min_days_xx": "0",
            "booking_fee_night": "10",
            "booking_min_hours": "",
            "booking_max_hours": "",
            "booking_time_slot": "0",
            "booking_time_buffer": "0",
            "booking_show_finish": "1",
            "booking_show_qty": "1",
            "booking_phone": "",
            "booking_email": "",
            "booking_address": "",
            "booking_city": "",
            "booking_zipcode": "",
            "booking_country": "",
            "booking_state": "",
            "auto_address": "",
            "booking_lat": "0",
            "booking_lon": "0"
            ] as [String : Any]
        
        let queryItems : [String : Any] = ["booking_id": bookingId,
                                           "bookings": arrbooking ,
                                           "is_change_intervals": "1",
                                           "is_default_change_intervals": "0",
                                           "is_address": "1"]
        
        self.showLoader()
        WebService.savebooking(queryItems: queryItems) { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    print(response)
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func getPriceListing()
    {
        let queryItems = ["id": bookingId ]
        self.showLoader()
        WebService.getPriceListing(queryItems: queryItems) { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    if let price = response.objects {
                        self.prices = price
                        self.constraintTableViewHeight.constant = self.prices.count>0 ? 50 : 0
                        self.priceTableView.reloadData()
                    }
                    else {
                        AlertManager.showAlert(on: self, type: .custom(response.message!))
                    }
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
                self.getCalender()
            }
        }
    }
    
    @IBAction func approveCar(_ sender: UIButton) {
        self.saveaddress()
    }
    
    func saveaddress() {
        if latitude.isEmpty || longitude.isEmpty || autoComplete.isEmpty{
            AlertManager.showAlert(type: .custom("Please fill the address.".localized))
            return
        }
        let query = ["product_id" : bookingId,
                     "lat" : latitude,
                     "long" : longitude,
                     "auto_address" : autoComplete ] as [String:Any]
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        WebService.saveAddress(queryItems: query) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(_):
                    Observer.updateList()
                    self.navigationController?.viewControllers.forEach {
                        if $0.isKind(of: MyCarsViewController.self) {
                            self.navigationController?.popToViewController($0, animated: true)
                            return
                        }
                    }
                case .failure(let error):
                    AlertManager.showAlert(type: .custom(error.message))
                }
            }
        }
    }
    
    @IBAction func didTapAddNewPrice(_ sender: Any) {
        isEdit = false
        self.showClearfirstDatesButton = false
        self.showClearSecondDatesButton = false
        savePriceButton.setTitle("Save price".localized, for: .normal)
        UIView.animate(withDuration: 10) {
            self.constraintPriceViewTop.constant = 0
            self.priceView.isHidden = false
        }
        statusTextfield.accessibilityLabel = ""
        statusTextfield.text = ""
        cityTextfield.accessibilityLabel = ""
        cityTextfield.text = ""
        startDateTextfield.text = ""
        endDateTextfield.text = ""
        priceTextfield.text = ""
        specialPriceTextfield.text = ""
        isDefault = checkImageView.image == UIImage(named: "check")
        startDateTextfield.isUserInteractionEnabled = !isDefault
        endDateTextfield.isUserInteractionEnabled = !isDefault
    }
    
    @IBAction func didTapDefaultButton(_ sender: Any) {
        
        checkImageView.image =  checkImageView.image == UIImage(named: "check")  ? nil : UIImage(named: "check")
        isDefault = checkImageView.image == UIImage(named: "check")
        startDateTextfield.isUserInteractionEnabled = !isDefault
        endDateTextfield.isUserInteractionEnabled = !isDefault
        
        if isDefault
        {
            status.remove(at: 1)
           
            statusTextfield.accessibilityLabel = status[0].value
            statusTextfield.text = status[0].label
            //            cityTextfield.accessibilityLabel = city[0].value
            //            cityTextfield.text = city[0].label
        }
        else
        {
            self.getStatusApi()
            statusTextfield.accessibilityLabel = ""
            statusTextfield.text = ""
            cityTextfield.accessibilityLabel = city[0].label
            //            cityTextfield.text = city[0].label
        }
    }
    
    @IBAction func didTapResetMap(_ sender: Any) {
        txtSearchLocation.text = ""
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(0) , longitude: CLLocationDegrees(0))
        detailMapView.latitude = "0.0"
        detailMapView.longitude = "0.0"
        self.detailMapView.configureMap(for: coordinates)
    }
    
    @IBAction func didTapSavePrice(_ sender: Any){
        self.view.endEditing(true)
        if formSave.valid {
            guard let status = statusTextfield.accessibilityLabel ,
                let city = cityTextfield.accessibilityLabel,
                let startDate = startDateTextfield.text,
                let endDate = endDateTextfield.text,
                let price = priceTextfield.text,
                let specialPrice = specialPriceTextfield.text
                else {
                    return
            }
            
            print(startDate,endDate,status)
            var queryItems = ["bk_time_mode":"1","booking_id":bookingId,"booking_id_1":bookingId,"booking_time":"1","booking_type":"per_day","calendar_id":"0","is_inter_default":"2","item_day_city":city,"item_day_description":"","item_day_end_date": isDefault ? "" : endDate ,"item_day_price":price,"item_day_promo":specialPrice,"item_day_qty":"1","item_day_start_date": isDefault ? "" : startDate,"item_day_status":status,"ok_calendar":"0","temp_end_date":isDefault && endDate.isEmpty ? "" :endDate ,"temp_start_date":  isDefault && startDate.isEmpty ? "" : startDate]
            if isDefault {
                queryItems["item_day_default_value"]  =  "1" }
            self.showLoader()
            if isEdit {
                queryItems["calendar_id"] = objectPrice?.calendar_id
                WebService.editPrice(queryItems: queryItems) { (result) in
                    DispatchQueue.main.async {
                        self.hideLoader()
                        switch result {
                        case .success(let response):
                            UIView.animate(withDuration: 10) {
                                self.constraintPriceViewTop.constant = -570
                                self.priceView.isHidden = true
                            }
                            AlertManager.showAlert(on: self, type: .custom(response.message!))
                            self.getPriceListing()
                            Observer.updateList()
                        case .failure(let error):
                            AlertManager.showAlert(on: self, type: .custom(error.message))
                        }
                    }
                }
            }else{
                WebService.savePrice(queryItems: queryItems) { (result) in
                    DispatchQueue.main.async {
                        self.hideLoader()
                        switch result {
                        case .success(let response):
                            UIView.animate(withDuration: 10) {
                                self.constraintPriceViewTop.constant = -570
                                self.priceView.isHidden = true
                                Observer.updateList()
                                self.priceTableView.reloadData()
                            }
                            AlertManager.showAlert(on: self, type: .custom(response.message!))
                            self.getPriceListing()
                        case .failure(let error):
                            AlertManager.showAlert(on: self, type: .custom(error.message))
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func didTapClosePrice(_ sender: Any) {
        UIView.animate(withDuration: 10) {
            self.constraintPriceViewTop.constant = -570
            self.priceView.isHidden = true
        }
    }
    
    @IBAction func didTapSuggestPrice(_ sender: Any) {
        self.view.endEditing(true)
        if form.valid {
            guard let status = statusTextfield.accessibilityLabel ,
                let city = cityTextfield.accessibilityLabel,
                let startDate = startDateTextfield.text,
                let endDate = endDateTextfield.text else {
                    return
            }
            
            print(startDate,endDate)
            let queryItems = ["bk_time_mode":"1","booking_id":bookingId,"booking_id_1":bookingId,"booking_time":"1","booking_type":"per_day","calendar_id":"0","is_inter_default":"2","item_day_city":city,"item_day_description":"","item_day_end_date":endDate,"item_day_price":" ","item_day_promo":" ","item_day_qty":"1","item_day_start_date":startDate,"item_day_status":status,"ok_calendar":"0","temp_end_date":endDate,"temp_start_date":startDate]
            self.showLoader()
            WebService.suggestPrice(queryItems: queryItems) { (result) in
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch result {
                    case .success(let response):
                        if let price = response.object {
                            self.priceTextfield.text = price.price?.roundOff()
                            self.specialPriceTextfield.text = price.specialPrice?.roundOff()
                        }
                        else {
                            AlertManager.showAlert(on: self, type: .custom(response.message!))
                        }
                    case .failure(let error):
                        AlertManager.showAlert(on: self, type: .custom(error.message))
                    }
                }
            }
        }
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        let text: NSString = textField.text! as NSString
//        let resultString = text.replacingCharacters(in: range, with: string)
//        
//        
//        //Check the specific textField
//        if textField == priceTextfield {
//            let textArray = resultString.components(separatedBy: ".")
//            if textArray.count > 2 { //Allow only one "."
//                return false
//            }
//            if textArray.count == 2 {
//                let lastString = textArray.last
//                if lastString!.count > 2 { //Check number of decimal places
//                    return false
//                }
//            }
//        }
//        
//        if textField == specialPriceTextfield {
//            let textArray = resultString.components(separatedBy: ".")
//            if textArray.count > 2 { //Allow only one "."
//                return false
//            }
//            if textArray.count == 2 {
//                let lastString = textArray.last
//                if lastString!.count > 2 { //Check number of decimal places
//                    return false
//                }
//            }
//        }
//        
//        return true
//    }
    
    // getCalender
    func getCalender() {
        let query = ["product_id": bookingId ]
        self.showLoader()
        WebService.getCalender(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    guard let dict = response.objects else {
                        self.arrDatesAvailability = []
                        self.calendar.delegate = self
                        self.calendar.dataSource = self;
                        self.calendar.reloadData()
                        let arr = NSMutableArray(array: [])
                        self.calendar.availableDates(arr)
                        return }
                    let arrDatesAvailable : [[String:Any]] = dict.compactMap { return $0.data! }
                    self.arrDatesAvailability = arrDatesAvailable
                    self.calendar.delegate = self
                    self.calendar.dataSource = self;
                    self.calendar.reloadData()
                    let arr = NSMutableArray(array: arrDatesAvailable)
                    self.calendar.availableDates(arr)
                    break
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: Prompt.custom(error.message))
                    break
                }
            }
        }
    }
    
    func editDeletePrice(_ type: Action, _ object: Price){
        switch type {
        case .edit:
            editPrice(object)
        case .delete:
            deletePriceApi(object.calendar_id ?? "0")
            break
        }
    }
    
    func editPrice ( _ object: Price){
        objectPrice = object
        savePriceButton.setTitle("Update price".localized, for: .normal)
        UIView.animate(withDuration: 10) {
            self.constraintPriceViewTop.constant = 0
            self.priceView.isHidden = false
        }
        isEdit = true
        statusTextfield.accessibilityLabel = object.status_value
        statusTextfield.text = object.status_label
        cityTextfield.accessibilityLabel = object.city_value
        cityTextfield.text = object.city_label
        startDateTextfield.text = object.start_date == "Default" ? "" : object.start_date
        endDateTextfield.text = object.end_date == "Default" ? "" : object.end_date
        priceTextfield.text = object.price?.roundOff()
         specialPriceTextfield.text = object.specialPrice?.roundOff()
//        priceTextfield.text = object.main_price?.roundOff()
//        specialPriceTextfield.text = object.main_special_price?.roundOff()
        isDefault = object.start_date == "Default"
        checkImageView.image =  !isDefault  ? nil : UIImage(named: "check")
        startDateTextfield.isUserInteractionEnabled = !isDefault
        endDateTextfield.isUserInteractionEnabled = !isDefault
    }
    
    func deletePriceApi(_ calendarId: String){
        self.view.endEditing(true)
        let queryItems = ["calenderid": calendarId]
        self.showLoader()
        WebService.deletePrice(queryItems: queryItems) { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    AlertManager.showAlert(on: self, type: .custom(response.message!))
                    Observer.updateList()
                    self.getPriceListing()
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
}


extension BookingConfigurationViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingConfigurationTableViewCell", for: indexPath) as! BookingConfigurationTableViewCell
        cell.object = prices[indexPath.row]
        cell.titleLabel.text = "Price".localized + " \(indexPath.row+1):"
        cell.handler = { action, object in
            self.editDeletePrice(action, object)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        constraintTableViewHeight.constant = tableView.contentSize.height
    }
    
    func showCalender() {
        
        let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = self
        dateRangePickerViewController.minimumDate = Date()
        dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 2, to: Date())
        dateRangePickerViewController.selectedStartDate = Date()
        dateRangePickerViewController.selectedEndDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())
        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func openPicker(for sender : Int) {
        switch sender {
        case 3:
            let labels = status.compactMap { return $0.label }
            let values = status.compactMap {return $0.value}
            showPickerView(title: "Select Status".localized, tag: 3, list: labels, id: values)
        case 4:
            let labels = city.compactMap { return $0.label}
            let values = city.compactMap {return $0.value}
            showPickerView(title: "Select City".localized, tag: 4, list: labels, id: values)
        default:
            break
        }
    }
    
    func showPickerView(title: String, tag: Int, list: [String], id: [String] ) {
        let picker = CustomPickerView()
        print(list)
        picker.list = list
        picker.showPickerView(title: title, tag: tag)
        picker.didSelectRow = { self.updateFieldsValue($0,$1, list, id) }
    }
    
    func updateFieldsValue( _ row: Int,_ tag: Int,_ list : [String], _ id: [String]) {
        switch tag {
        case 3:
            self.statusTextfield.text = list[row]
            self.statusTextfield.accessibilityLabel = id[row]
            
        case 4:
            self.cityTextfield.text = list[row]
            self.cityTextfield.accessibilityLabel = id[row]
            
        default:
            break
        }
    }
    
}
extension BookingConfigurationViewController  : FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance
{
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        
        return Color.greenColour
    }
    
    // MARK:- FSCalendarDataSource
    
    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
        return (self.gregorian.isDateInToday(date) ? nil : nil)
    }
    
    func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {
        guard self.lunar else {
            return nil
        }
        return self.lunarFormatter.string(from: date)
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        let strmaxDate = "\(arrDatesAvailability.first?["date"] ?? result )"
        return self.formatter.date(from: strmaxDate)!
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        let strmaxDate = "\(arrDatesAvailability.last?["date"] ?? result )"
        return self.formatter.date(from: strmaxDate)!
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }

}
extension BookingConfigurationViewController : CalendarDateRangePickerViewControllerDelegate {
    
    func didTapCancel() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        self.didPickDateRange(startDate: startDate, endDate: endDate)
    }
    
    func didCancelPickingDateRange() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func didPickDateRange(startDate: Date!, endDate: Date!) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.showClearfirstDatesButton = true
        self.showClearSecondDatesButton = true
        
        startDateTextfield.text = dateFormatter.string(from: startDate)
        endDateTextfield.text = dateFormatter.string(from: endDate)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
extension BookingConfigurationViewController{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == statusTextfield && status.isEmpty { return false }
        if textField == cityTextfield && city.isEmpty { return false }
        
        /* check for textfield */
        // open calender for date selection
        textField == startDateTextfield ? self.showCalender() : nil
        textField == endDateTextfield ? self.showCalender() : nil
        
        //        // open google location picker view for add location
        let searchLocation = textField == txtSearchLocation
        searchLocation ? self.searchLocation() : nil
        
        // open picker view for category, make and model
        let showPicker = (textField == statusTextfield ||  textField == cityTextfield )
        showPicker ? self.openPicker(for: textField.tag) : nil
        
        return false
    }
    
    func searchLocation() {
        let locationController = GMSAutocompleteViewController()
        locationController.delegate = self
        present(locationController, animated: true, completion: nil)
    }
}
extension BookingConfigurationViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        longitude  = String(place.coordinate.longitude)
        latitude  = String(place.coordinate.latitude)
        
        GMSGeocoder().reverseGeocodeCoordinate(place.coordinate) { (response, error) in
            guard let address = response?.firstResult() else {
                return
            }
            
            self.address = address.subLocality ?? ""
            self.cityBk = address.locality ?? ""
            self.stateBk = address.administrativeArea ?? ""
            self.countryBk = address.country ?? ""
            self.autoComplete = place.formattedAddress ?? ""
            self.txtSearchLocation.text = place.formattedAddress
            self.detailMapView.latitude = self.latitude
            self.detailMapView.longitude = self.longitude
            let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(self.latitude) ?? 0.0, longitude: CLLocationDegrees(self.longitude) ?? 0.0)
            self.detailMapView.configureMap(for: coordinates)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
extension BookingConfigurationViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if presentMap { return }
        self.scrollOffsetY = scrollView.contentOffset.y
    }
}
