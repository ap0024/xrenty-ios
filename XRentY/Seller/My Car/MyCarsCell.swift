//
//  MyCarsCell.swift
//  XRentY
//
//  Created by user on 24/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit
import Kingfisher

class MyCarsCell: UITableViewCell {
    
    @IBOutlet var imageCar: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnCarConfig: UIButton!
    @IBOutlet weak var btnCarEdit: UIButton!
    @IBOutlet weak var btnCarDelete: UIButton!
    @IBOutlet var lblConfig: UILabel!
    
    
    var carConfig : ((Int)->())? = nil
    var carEdit : ((Int)->())? = nil
    var carDelete : ((Int)->())? = nil
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(_ car: Car) {
        
//      btnCarConfig.titleLabel?.adjustsFontSizeToFitWidth = true
        
        if car.approval == 0{
            btnCarConfig.isUserInteractionEnabled = false
            btnCarConfig.alpha = 0.4
            lblConfig.text = "Once approved you can configure your availability.".localized
        }
        else{
            btnCarConfig.isUserInteractionEnabled = true
            btnCarConfig.alpha = 1
            lblConfig.text = "Your car is approved from admin.".localized
        }
        
        self.lblName.text = car.name
        
        let price1 = Double(car.Price) ?? 0
        let price2 = CGFloat(price1).formattedWithSeparator
        
        self.lblPrice.text = symbol + "\(price2)"
        if car.location == ""
        {
            self.lblAddress.isHidden = false
            self.lblAddress.addImage(text: " N/A", image: #imageLiteral(resourceName: "blue_LocationMark_ic"))
            
        }else
        {
            self.lblAddress.isHidden = false
            self.lblAddress.addImage(text: " " + (car.location ?? "N/A"), image: #imageLiteral(resourceName: "blue_LocationMark_ic"))
        }
        imageCar.kf.indicatorType = .activity
        imageCar.kf.setImage(with: car.imageURL,placeholder: UIImage(named: "car_placeholder_images"))
    }
    
    @IBAction func carConfig(_ sender: Any) {
        
        if let handler = carConfig {
            handler(tag)
        }
    }
    
    @IBAction func carEdit(_ sender: Any) {

        if let handler = carEdit {
            handler(tag)
        }
    }
    
    @IBAction func carDelete(_ sender: Any) {
        
        if let handler = carDelete {
            handler(tag)
        }
    }
}
