//
//  MyCarsViewController.swift
//  XRentY
//
//  Created by user on 24/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit
import IBLocalizable

class MyCarsViewController: BaseViewController {
    
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var tableCars: UITableView!
    @IBOutlet var noRecordsView: UIView!
    let refreshControl = UIRefreshControl()

    
    var cars: [Car] = [] {
        didSet {
            tableCars.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.listCars()
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        let attributes = [NSAttributedString.Key.foregroundColor: Color.barTintColor, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing please wait...".localized, attributes: attributes)
        tableCars.refreshControl = refreshControl
        refreshControl.tintColor = Color.barTintColor
        // Do any additional setup after loading the view.

        Observer.addcar(target: self, selector: #selector(refreshcarlist))
    }
    
    @objc func refreshcarlist() {
        self.listCars(loaderColor: UIColor.black.withAlphaComponent(0.3))
        Authentication.clabeAccountDetail = 1
    }
    
    @objc func doSomething(refreshControl: UIRefreshControl) {
        self.listCars(false)
    }
    
    @IBAction func addCar(_ sender: UIButton) {
        
        if Authentication.clabeAccountDetail == 1 {
            Authentication.isedit = false
            self.pushTo(.addCar)
        }
        else
        {
            AlertManager.showAlert(type: .custom("Please fill valid CLABE number in profile".localized), actionTitle: "OK".localized) {
                for controller in BaseNavigationController.sharedInstance.viewControllers {
                    if controller.isKind(of: BaseTabBarViewController.self) {
                        if (controller as! BaseTabBarViewController).selectedIndex != 4 {
                            (controller as! BaseTabBarViewController).selectTab(4)
                        }
                    }
                }
            }
        }
        
//        Authentication.isedit = false
//        self.pushTo(.addCar)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func listCars(_ showLoader: Bool = true, loaderColor: UIColor = .white) {
        showLoader ? self.showLoader(color: loaderColor) : self.refreshControl.beginRefreshing()
        WebService.listCars { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                self.refreshControl.endRefreshing()
                switch result {
                case .success(let response):
                    if let cars = response.objects,!cars.isEmpty  {
                        self.tableCars.backgroundView = nil
                        self.cars = cars
                    } else if self.cars.isEmpty {
                        self.tableCars.backgroundView = self.noRecordsView
                    }
                    else {
                        AlertManager.showAlert(on: self, type: .custom(response.message!))
                    }
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
}

extension MyCarsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MyCarsCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyCarsCell
        
        cell.tag = indexPath.row
        cell.configure(cars[indexPath.row])
        cell.carConfig = { self.configure(self.cars[$0]) }
        cell.carEdit = { self.editCar(self.cars[$0]) }
        cell.carDelete = { self.deleteCar(self.cars[$0], indexPath.row) }
        return cell
    }
    
    func deleteCar(_ cars : Car,_ tag : Int) {
        
        AlertManager.showAlert(type: .custom("Do you want to delete car?".localized), actionTitle: "Delete".localized) {
        let query = ["product_id" : cars.id ?? ""] as [String:Any]
        self.showLoader(color: UIColor.black.withAlphaComponent(0.3))
        WebService.deleteCar(queryItems: query) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(_):
                    Observer.updateList()
                    self.cars.remove(at: tag)
                case .failure(let error):
                    AlertManager.showAlert(type: .custom(error.message))
                }
            }
        }
        }
    }
    
    func editCar(_ cars : Car) {
        AlertManager.showAlert(type: .custom("Do you want to edit car?".localized), actionTitle: "Edit".localized) {
            let controller = AddCarViewController.initiatefromStoryboard(.seller)
            controller.productid = cars.id ?? ""
            Authentication.isedit = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func configure (_ cars : Car){
       let controller = BookingConfigurationViewController.initiatefromStoryboard(.seller)
        controller.bookingId = cars.id ?? ""
        controller.latitude = cars.booking_lat ?? ""
        controller.longitude = cars.booking_lon ?? ""
        controller.autoComplete = cars.location ?? ""
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
