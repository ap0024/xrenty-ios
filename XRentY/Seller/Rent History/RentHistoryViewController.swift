//
//  RentHistoryViewController.swift
//  XRentY
//
//  Created by user on 28/09/18.
//  Copyright © 2018 Softprodigy. All rights reserved.

import UIKit

class RentHistoryViewController: BaseViewController {

    @IBOutlet var tableRents: UITableView!
    var rents: [Rent] = [] {
        didSet {
            tableRents.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTitleView()
        self.rentHistoryWebService()
        
        Observer.reviewUpdate(target: self, selector: #selector(refreshMyReview))
        
    }
    
    @objc func refreshMyReview() {
        self.rentHistoryWebService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func rentHistoryWebService() {
        self.showLoader()
        WebService.sellerOrderHistory { result  in
            switch result {
            case .success(let response):
                self.rents = response.objects ?? []
                self.tableRents.rowHeight = UITableView.automaticDimension
            case .failure(let error):
                print(error.message)
            }
            DispatchQueue.main.async {
                self.hideLoader()
                self.tableRents.reloadData()
            }
        }
    }
}

extension RentHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(rents.count)
        return rents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RentHistory", for: indexPath) as! RentHistoryCell
        cell.configure(rents[indexPath.row])
        return cell
    }
}
