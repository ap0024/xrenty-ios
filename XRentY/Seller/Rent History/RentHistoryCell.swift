//
//  RentHistory.swift
//  XRentY
//
//  Created by user on 28/09/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit
import Kingfisher

class RentHistoryCell: UITableViewCell {

    @IBOutlet var lblFrom: UILabel!
    @IBOutlet var lblTo: UILabel!
    @IBOutlet var lblTotal: UILabel!
    @IBOutlet var lblDeposite: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var imageCar: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(_ rent: Rent) {
        lblFrom.text = rent.pickupDate
        lblTo.text = rent.returnDate
        lblTotal.text = rent.grandTotal
        lblDeposite.text = rent.productRowPrice
        lblStatus.text = rent.status
        if let url = URL(string: rent.productImage!) {
            imageCar.kf.indicatorType = .activity
            imageCar.kf.setImage(with: url)
        }
    }
}
