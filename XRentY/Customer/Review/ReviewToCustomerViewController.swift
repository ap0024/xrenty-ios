//
//  ReviewToCustomerViewController.swift
//  XRentY
//
//  Created by user on 17/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit

class ReviewToCustomerViewController: BaseViewController {
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtReview: UITextView!
    @IBOutlet weak var star: STRatingControl!
    @IBOutlet weak var sellerName: UILabel!
    
    var booking: Booking?
    var product_id : String = ""
    var seller_image : String = ""
    var seler_name : String = ""
    var user: User!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addTitleView()
        
        self.imgUser.kf.indicatorType = .activity
        self.sellerName.text = seler_name ?? ""
        let targetURL = URL(string: seller_image)
        imgUser.kf.setImage(with: targetURL  , placeholder: #imageLiteral(resourceName: "user_ic"))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func sendReview(_ sender: Any) {
        if txtReview.text == ""
        {
            self.showToast(message: "Please add some reviews.".localized)
        }
        else {
            self.addReview()
        }
    }
}

extension ReviewToCustomerViewController {
    func addReview() {
        
        let dics = txtReview.text ?? ""
        
        let query = ["prod_id": product_id,
                     "store_id":"1",
                     "detail":dics,
                     "title":"Review : ",
                     "nickname": Authentication.customerName ?? "N/A",
                     "rating" :  star.rating
            ] as [String : Any]
        
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        WebService.addReview(queryItems: query) { result in
           DispatchQueue.main.async {
            self.hideLoader()
            switch result {
            case .success(let response):
                AlertManager.showAlert(type: .custom("Your review has been accepted for moderation".localized) , action: {
                    self.navigationController?.popToRootViewController(animated: true)
                })
                break
            case .failure(let error):
                AlertManager.showAlert(on: self, type: .custom(error.message))
                
                break
            }
            }
        }
    }
}
