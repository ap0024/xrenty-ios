//
//  SearchFilterViewController.swift
//  XRentY
//
//  Created by user on 11/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit
import NHRangeSlider

extension ApplyFilterViewController: NHRangeSliderViewDelegate {
    func sliderValueChanged(slider: NHRangeSlider?) {
        
        self.filter["price_high"] = slider?.upperValue
        self.filter["price_low"] = slider?.lowerValue
        self.searchResult()
    }
}

class ApplyFilterViewController: BaseViewController {
    
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var BtnPrice: UIButton!
    @IBOutlet weak var btnTransmission: UIButton!
    @IBOutlet weak var btnEngine: UIButton!
    @IBOutlet weak var btnPassenger: UIButton!
    @IBOutlet weak var btnTransAuto: UIButton!
    @IBOutlet weak var btntransManual: UIButton!
    @IBOutlet weak var btn7Seater: UIButton!
    @IBOutlet weak var btn5Seater: UIButton!
    @IBOutlet weak var btn2seater: UIButton!
    @IBOutlet weak var viewPassenger: UIView!
    @IBOutlet weak var ViewTransmision: UIView!
    @IBOutlet weak var btnEngineG: UIButton!
    @IBOutlet weak var btnEngineH: UIButton!
    @IBOutlet weak var btnEngineE: UIButton!
    @IBOutlet weak var viewEngine: UIView!
    @IBOutlet var selectFilterButtons: [UIButton]!
    @IBOutlet var sortButtons: [UIButton]!
    @IBOutlet var transmissionButtons: [UIButton]!
    @IBOutlet var passengerButtons: [UIButton]!
    @IBOutlet var engineButtons: [UIButton]!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var tblFilter: UITableView!
    @IBOutlet weak var noData: UILabel!
    @IBOutlet weak var slideprice: UILabel!
    var selectedFilterButton = UIButton()
    var filter: [String:Any] = [:]
    var search: Search!
    var sortButtonSelectedTag: Int = 0
    var startDate: String?
    var endDate: String?
    var lowPrice : Double = 0.0
    var highPrice : Double = 0.0
    let language = NSLocale.current.languageCode
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    
    //heading
    @IBOutlet weak var headingFilterBy: UILabel!
    @IBOutlet weak var headingsortBy: UILabel!
    @IBOutlet weak var headingpriceLtoH: UIButton!
    @IBOutlet weak var headingpriceHtoL: UIButton!
    @IBOutlet weak var headingprice5to1: UIButton!
    @IBOutlet weak var headingprice1to5: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblFilter.estimatedRowHeight = 200
        self.Rangeslider()
        self.addTitleView()
        
        Observer.poptoSearch(target: self, selector: #selector(poptosearch))
        self.noData.isHidden = true
        self.filter = ["price_low": lowPrice, "price_high": highPrice , "transmission": "", "passenger_count": "", "engine_type": "", "sort_by": "price", "sort_order": "ASC"]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func Rangeslider()
    {
        if Authentication.currency == "USD"
        {
            self.lowPrice = 10.00
            self.highPrice = 2000.00
        }
        else if Authentication.currency == "MXN"
        {
            self.lowPrice = 191.80
            self.highPrice = 38360.40
        }
        else if Authentication.currency == ""
        {
            if language == "en"
            {
                self.lowPrice = 10.00
                self.highPrice = 2000.00
            }
            else if language == "es"
            {
                self.lowPrice = 191.80
                self.highPrice = 38360.40
            }
            else
            {
                self.lowPrice = 10.00
                self.highPrice = 2000.00
            }
        }
        
        let sliderCustomStringView = NHRangeSliderView(frame: CGRect(x: 0, y: 0, width: self.sliderView.bounds.width-45, height: self.sliderView.bounds.height))
        sliderCustomStringView.trackHighlightTintColor = Color.textColor
        sliderCustomStringView.minimumValue = lowPrice
        sliderCustomStringView.maximumValue = highPrice
        sliderCustomStringView.lowerValue = lowPrice
        sliderCustomStringView.upperValue = highPrice
        
        sliderCustomStringView.thumbLabelStyle = .STICKY
        
        sliderCustomStringView.lowerDisplayStringFormat = "\(symbol)" + " %.2f"
        sliderCustomStringView.upperDisplayStringFormat = "\(symbol)" + " %.2f"
        sliderCustomStringView.delegate = self
        sliderCustomStringView.sizeToFit()
        self.sliderView.addSubview(sliderCustomStringView)
    }
    
    @IBAction func setTransmissionAutomatic(_ sender: UIButton) {
        
        let isSelected = self.setHighlightedtransmissionButton(sender.tag)
        self.filter["transmission"] = isSelected ? "0" : ""
        self.searchResult()
    }
    
    @IBAction func setTransmisionManual(_ sender: UIButton) {
        
        let isSelected = self.setHighlightedtransmissionButton(sender.tag)
        self.filter["transmission"] = isSelected ? "1" : ""
        self.searchResult()
    }
    
    @IBAction func setTwoSeater(_ sender: UIButton) {
        
        let isSelected = self.setHighlightedPassengerButton(sender.tag)
        self.filter["passenger_count"] = isSelected ? "2" : ""
        self.searchResult()
    }
    
    @IBAction func setFiveSeater(_ sender: UIButton) {
        
        let isSelected = self.setHighlightedPassengerButton(sender.tag)
        self.filter["passenger_count"] = isSelected ? "5" : ""
        self.searchResult()
    }
    
    @IBAction func setSevenSeater(_ sender: UIButton) {
        
        let isSelected = self.setHighlightedPassengerButton(sender.tag)
        self.filter["passenger_count"] = isSelected ? "7" : ""
        self.searchResult()
    }
    
    @IBAction func setEngineTypeG(_ sender: UIButton) {
        
        let isSelected = self.setHighlightedengineButton(sender.tag)
        self.filter["engine_type"] = isSelected ? "Gasoline" : ""
        self.searchResult()
    }
    
    @IBAction func setEngineTypeH(_ sender: UIButton) {
        
        let isSelected = self.setHighlightedengineButton(sender.tag)
        self.filter["engine_type"] = isSelected ? "Hybrid" : ""
        self.searchResult()
    }
    
    @IBAction func setEngineTypeE(_ sender: UIButton) {
        
        let isSelected = self.setHighlightedengineButton(sender.tag)
        self.filter["engine_type"] = isSelected ? "Electric" : ""
        self.searchResult()
    }
    
    func searchResult() {
        search.query["filter"] = filter
        
        showLoader()
        WebService.getSearch(queryItems: search.query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    self.search.cars = response.objects!
                    self.tblFilter.reloadData()
                    self.noData.isHidden = true
                    if self.search.cars.isEmpty
                    {
                        self.noData.isHidden = false
                    }
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    
    @objc func poptosearch() {
        self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.viewControllers.first?.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func PriceHighToLowButtons(_ sender: UIButton) {
        if sortButtonSelectedTag == sender.tag { return  }
        let isSelected = self.setHighlightedsortButton(sender.tag)
        self.filter["sort_by"] = isSelected  ? "price" : ""
        self.filter["sort_order"] = isSelected  ? "DESC" : ""
        self.searchResult()
    }
    
    @IBAction func PriceLowToHighButtons(_ sender: UIButton) {
        if sortButtonSelectedTag == sender.tag { return  }
        let isSelected = self.setHighlightedsortButton(sender.tag)
        self.filter["sort_by"] = isSelected  ? "price" : ""
        self.filter["sort_order"] = isSelected  ? "ASC" : ""
        self.searchResult()
    }
    
    @IBAction func star1to5Buttons(_ sender: UIButton) {
        if sortButtonSelectedTag == sender.tag { return  }
        let isSelected = self.setHighlightedsortButton(sender.tag)
        self.filter["sort_by"] = isSelected  ? "stars" : ""
        self.filter["sort_order"] = isSelected  ? "ASC" : ""
        self.searchResult()
    }
    
    @IBAction func star5to1Buttons(_ sender: UIButton) {
        if sortButtonSelectedTag == sender.tag { return  }
        let isSelected = self.setHighlightedsortButton(sender.tag)
        self.filter["sort_by"] = isSelected  ? "stars" : ""
        self.filter["sort_order"] = isSelected  ? "DESC" : ""
        self.searchResult()
    }
    
    //select Engine Type
    func setHighlightedsortButton(_ tag: Int) -> Bool {
        sortButtonSelectedTag = tag
        sortButtons.forEach {
            $0.borderColor = $0.tag == tag ? Color.textColor : nil
            $0.borderWidth = ($0.tag == tag && $0.borderWidth == 1) ? 0 : ($0.tag == tag ? 1 : 0)
            $0.setTitleColor( $0.tag == tag ? Color.textColor : UIColor.gray, for: .normal)
        }
        
        var selections: [Bool] = []
        sortButtons.forEach {
            selections.append($0.borderWidth == CGFloat(1))
        }
        return selections.contains(true)
    }
    
    @IBAction func selectFilterButtons(_ sender: UIButton) {
        if selectedFilterButton == sender { return }
        selectedFilterButton = sender
        selectFilterButtons.forEach {
            let selected = $0 == sender && sender.titleColor(for: .normal) != Color.textColor
            $0.setTitleColor( selected ? Color.textColor : UIColor.gray, for: .normal)
            $0.borderColor = selected ? Color.textColor : nil
            $0.borderWidth = selected ? 1 : 0
        }

        hideView(sender.tag)
    }
    
    func hideView(_ sender : Int)  {
        
        switch sender {
        case 0:
            self.priceView.isHidden = false
            self.ViewTransmision.isHidden = true
            self.viewPassenger.isHidden = true
            self.viewEngine.isHidden = true
        case 1:
            self.priceView.isHidden = true
            self.ViewTransmision.isHidden = false
            self.viewPassenger.isHidden = true
            self.viewEngine.isHidden = true
        case 2:
            self.priceView.isHidden = true
            self.ViewTransmision.isHidden = true
            self.viewPassenger.isHidden = false
            self.viewEngine.isHidden = true
        case 3:
            self.priceView.isHidden = true
            self.ViewTransmision.isHidden = true
            self.viewPassenger.isHidden = true
            self.viewEngine.isHidden = false
        default:
            print(sender)
        }
    }
    
    //select Engine Type
    func setHighlightedengineButton(_ tag: Int) -> Bool {
        
        engineButtons.forEach {
            $0.borderColor = $0.tag == tag ? Color.textColor : nil
            $0.borderWidth = ($0.tag == tag && $0.borderWidth == 1) ? 0 : ($0.tag == tag ? 1 : 0)
        }
        
        var selections: [Bool] = []
        engineButtons.forEach {
            selections.append($0.borderWidth == CGFloat(1))
        }
        
        return selections.contains(true)
    }
    
    //select Transmission Type
    func setHighlightedtransmissionButton(_ tag: Int) -> Bool {
        transmissionButtons.forEach {
            $0.borderColor = $0.tag == tag ? Color.textColor : nil
            $0.borderWidth = ($0.tag == tag && $0.borderWidth == 1) ? 0 : ($0.tag == tag ? 1 : 0)
        }
        
        var selections: [Bool] = []
        transmissionButtons.forEach {
            selections.append($0.borderWidth == CGFloat(1))
        }
        
        return selections.contains(true)
    }
    
    //select Passenger Count
    func setHighlightedPassengerButton(_ tag: Int) -> Bool {
        
        passengerButtons.forEach {
            $0.borderColor = $0.tag == tag ? Color.textColor : nil
            $0.borderWidth = ($0.tag == tag && $0.borderWidth == 1) ? 0 : ($0.tag == tag ? 1 : 0)
        }
        
        var selections: [Bool] = []
        passengerButtons.forEach {
            selections.append($0.borderWidth == CGFloat(1))
        }
        
        return selections.contains(true)
    }
}

extension ApplyFilterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return search.cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FilterCell
        cell.tag = indexPath.row
        cell.configure(search.cars[indexPath.row])
        cell.bookCar = { self.pushToPriductDetail(self.search.cars[$0]) }
        return cell
    }
    
    func pushToPriductDetail(_ car: Car) {
        self.pushTo(.productDetail(car))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
