//
//  searchViewController.swift
//  XRentY
//
//  Created by user on 14/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import CoreLocation
import IBLocalizable


enum selection: String {
    
    case selectCategory = "Please select some category for search cars"
    case valid
    
    var valid: Bool {
        switch self {
        case .selectCategory:
            AlertManager.showAlert(type: .custom(rawValue.localized))
            return false
        case .valid:
            return true
        }
    }
}


class SearchViewController: BaseViewController ,GMSMapViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var txtSearchLocation: UITextField!
    @IBOutlet weak var txtSelectDate: UITextField!
    @IBOutlet var dropDownButtons: [UIImageView]!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var txtMake: UITextField!
    @IBOutlet weak var txtModel: UITextField!
    @IBOutlet weak var categoryIndicator: UIActivityIndicatorView!
    @IBOutlet weak var makeIndicator: UIActivityIndicatorView!
    @IBOutlet weak var modelIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnClearLocation: UIButton!
    @IBOutlet weak var btnClearDate: UIButton!

    var myPickerView : UIPickerView!
    var locationManager:CLLocationManager?
    var lowPrice : Int = 0
    var highPrice : Int = 0
    let language = NSLocale.current.languageCode
    
    var showClearLocationButton: Bool = false  {
        didSet {
            self.btnClearLocation.alpha = self.showClearLocationButton ? 1 : 0
        }
    }
    
    var showClearDateButton: Bool = false  {
        didSet {
            self.btnClearDate.alpha = self.showClearDateButton ? 1 : 0
        }
    }
    
    var category : [Category] = []
    var model : [CarModel] = []
    var make : [CarMake] = []
    var search : [GetSearch] = []
    var longitude: String?
    var latitude: String?
    var address : String?
    var countryBk : String?
    var cityBk : String?
    var stateBk : String?
    var startDate: String?
    var endDate: String?
    var id: String?
    var label: String?
    var value : String?
    var autoComplete : String?
    
    var form: selection {
        return self.txtCategory.text!.isEmpty ? .selectCategory : .valid
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLanguage()
        
        if Authentication.firstTime {
            self.license()
//            Authentication.firstTime = false
        }
        
        determineMyCurrentLocation()
        dropDownButtons.forEach {
            $0.tintColor = Color.baseColourLightBlue
        }

        txtSearchLocation.clearButtonMode = UITextField.ViewMode.whileEditing
        self.showClearDateButton = false
        self.showClearLocationButton = false
        self.fetchCarProperties()
    }
    
    func license()  {
        if Authentication.LicenceDetail == false {

            if  Authentication.appMode == false {
                AlertManager.showAlert(type: .custom("Your driving license details are pending.Please fill it asap.".localized), actionTitle: "OK".localized) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        for controller in BaseNavigationController.sharedInstance.viewControllers {
                            if controller.isKind(of: BaseTabBarViewController.self) {
                                if (controller as! BaseTabBarViewController).selectedIndex != 4 {
                                    (controller as! BaseTabBarViewController).selectTab(4)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.checkCurrency()
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager?.requestWhenInUseAuthorization()
        locationManager?.requestAlwaysAuthorization()
        locationManager?.showsBackgroundLocationIndicator = false
        
        
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager?.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
        
        guard let locationManager = self.locationManager else {
            return
        }
        
        locationManager.delegate = nil
        locationManager.stopUpdatingLocation()
        latitude = "\(userLocation.coordinate.latitude)"
        longitude = "\(userLocation.coordinate.longitude)"
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    func showPickerView(title: String, tag: Int, list: [String], id: [String] ) {
        let picker = CustomPickerView()
        
        print(list)
        picker.list = list
        picker.showPickerView(title: title, tag: tag)
        picker.didSelectRow = { self.updateFieldsValue($0,$1, list, id) }
    }
    
    // open picker
    func openPicker(for sender : Int) {

        switch sender {
        case 0:
            let names = category.compactMap { return $0.name }
            let id = category.compactMap {return $0.id}
            showPickerView(title: "Select Category".localized, tag: 0, list: names, id: id)
        case 1:
            let names = make.compactMap { return $0.label}
            let value = make.compactMap {return $0.value}
            showPickerView(title: "Select Make".localized, tag: 1, list: names, id: value)
        case 2:
            let names = model.compactMap { return $0.label}
            let value = model.compactMap {return $0.value}
            showPickerView(title: "Select Model".localized, tag: 2, list: names, id: value)
        default:
            break
        }
    }
    
    func updateFieldsValue( _ row: Int,_ tag: Int,_ list : [String], _ id: [String]) {
        switch tag {
        case 0:
            self.txtCategory.text = list[row] == "Select an option".localized ? "" : list[row]
            self.id = id[row]

        case 1:
            if self.txtMake.text == list[row] { return }
            self.txtMake.text = list[row] == "Select an option".localized ? "" : list[row]
            self.txtModel.text = ""
            self.value = ""
            self.fetchModel(self.txtMake.text!)
            self.label = id[row]
        case 2:
            self.txtModel.text = list[row] == "Select an option".localized ? "" : list[row]
            self.value = id[row]

        default:
            break
        }
    }

    // method to fetch car categories, make and model
    func fetchCarProperties() {
        self.fetchCategories {
            self.category =  $0
            self.category.insert(Category("Select an option".localized), at: 0)
            DispatchQueue.main.async {
                self.fetchMake {
                    self.make = $0
                    self.make.insert(CarMake("Select an option".localized), at: 0)
                }
            }
        }
    }
    
    //MARK:- fetch car categories
    func fetchCategories(completion:@escaping ([Category])->()) {
        categoryIndicator.startAnimating()
        WebService.getCategory { (result) in
            DispatchQueue.main.async {
                self.categoryIndicator.stopAnimating()
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- fetch car make
    func fetchMake(completion:@escaping ([CarMake])->()) {
        makeIndicator.startAnimating()
        WebService.getMake { result in
            DispatchQueue.main.async {
                self.makeIndicator.stopAnimating()
                switch result {
                case .success(let response):
                    completion(response.objects ?? [])
                case .failure(_):
                    completion([])
                }
            }
        }
    }
    
    //MARK:- fetch car Model
    func fetchModel(_ name: String) {
        
        var queryItems : [String:Any] = [:]
        
        if name == "" {
            self.txtModel.text = ""
            return
        } else {
            queryItems = ["make": name]
        }
        
        txtModel.isUserInteractionEnabled = false
        modelIndicator.startAnimating()
        WebService.getModel(queryItems: queryItems) { result in
            DispatchQueue.main.async {
                self.txtModel.isUserInteractionEnabled = true
                self.modelIndicator.stopAnimating()
                switch result {
                case .success(let response):
                    self.model = response.objects ?? []
                    self.model.insert(CarModel("Select an option".localized), at: 0)
                case .failure(_):
                    break
                }
            }
        }
    }
    
    func searchResult() {

        if Authentication.currency == "USD"
        {
            self.lowPrice = 10
            self.highPrice = 2000
        }
        else if Authentication.currency == "MXN"
        {
            self.lowPrice = Int(191.80)
            self.highPrice = Int(38360.40)
        }
        else if Authentication.currency == ""
        {
            if language == "en"
            {
                self.lowPrice = 10
                self.highPrice = 2000
            }
            else if language == "es"
            {
                self.lowPrice = Int(191.80)
                self.highPrice = Int(38360.40)
            }
            else
            {
                self.lowPrice = 10
                self.highPrice = 2000
            }
        }

    
        Authentication.startDate = startDate
        Authentication.endDate = endDate
        
        let arrFilter = ["sort_order": "ASC",
                         "sort_by": "price",
                         "price_low": "\(lowPrice)",
                         "price_high": "\(highPrice)",
                         "transmission": "",
                         "passenger_count": "",
                         "engine_type": ""
            ] as [String : Any]
        let queryItems : [String : Any] = ["make": label ?? "",
                          "model" : value ?? "",
                          "category" : id ?? "",
                          "lat" : latitude ?? "",
                          "lng" : longitude ?? "",
                          "city_bk":cityBk ?? "",
                          "address":address ?? "",
                          "country_bk":countryBk ?? "",
                          "text_rent_auto_complete":autoComplete ?? "",
                          "state_bk_bk":stateBk ?? "",
                          "check_in": startDate ?? "",
                          "check_out":endDate ?? "",
                          "filter" : arrFilter]
        
        
        
        showLoader()
        WebService.getSearch(queryItems: queryItems) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    guard let cars = response.objects else {
                        AlertManager.showAlert(on: self, type: .custom(response.message!))
                        return
                    }
                    if !cars.isEmpty {
                        self.pushToFilter(cars, queryItems)
                    } else {
                        AlertManager.showAlert(on: self, type: .custom(response.message!))
                    }
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    func setLanguage()  {
        
        var langCode : String = ""
        let language = NSLocale.current.languageCode
        
        if language == "en"
        {
            langCode = "es_US"
        }
        else if language == "es"
        {
            langCode = "es_ES"
        }
        else
        {
            langCode = "es_US"
        }
        
        
        let queryItems = ["language":langCode]
        print(queryItems)
        showLoader()
        WebService.setLanguage(queryItems: queryItems) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self.checkCurrency()
                    if let user = response.object {
                        print(user)
                    }
                    else {
                        print(Error.self.self)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func checkCurrency()  {
        
        var langCode : String = ""
        let language = NSLocale.current.languageCode
        
        if language == "en"
        {
            langCode = "es_US"
        }
        else if language == "es"
        {
            langCode = "es_ES"
        }
        else
        {
            langCode = "es_US"
        }
        
        WebService.GetCurrency { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    let currency = response.object?.customer_currency
                    Authentication.currency = currency
                    if currency == "USD"
                    {
                        Authentication.currencySymbol = "US$"
                    }
                    else if currency == "MXN"
                    {
                        Authentication.currencySymbol = "MX$"
                    }
                    else if currency == ""
                    {
                        if language == "en"
                        {
                            Authentication.currencySymbol = "US$"
                            Authentication.currency = "USD"
                        }
                        else if language == "es"
                        {
                            Authentication.currencySymbol = "MX$"
                            Authentication.currency = "MXN"
                        }
                        else
                        {
                            Authentication.currencySymbol = "US$"
                            Authentication.currency = "USD"
                        }
                    }
                    self.hideLoader()
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func pushToFilter(_ cars: [Car],_ query: [String:Any]) {
        let search = Search(cars: cars, query: query)
        pushTo(.filter(search))
    }
    
    @IBAction func viewCarInMap(_ sender: Any) {
        self.pushTo(.googleMap)
    }
    
    @IBAction func clearLocation(_ sender: UIButton) {
        self.txtSearchLocation.text = ""
        self.showClearLocationButton = false
        self.address = nil
        self.autoComplete = nil
        self.cityBk = nil
        self.stateBk = nil
        self.countryBk = nil
        locationManager?.delegate = self
        locationManager?.startUpdatingLocation()
    }
    
    @IBAction func clearDate(_ sender: UIButton) {
        self.txtSelectDate.text = ""
        self.startDate = nil
        self.endDate = nil
        Authentication.startDate = ""
        Authentication.endDate = ""
        self.showClearDateButton = false
        self.address = nil
        self.autoComplete = nil
        self.cityBk = nil
        self.stateBk = nil
        self.countryBk = nil
    }
    
    @IBAction func searchCar(_ sender: UIButton) {
        self.searchResult()
    }
    
    func showCalender() {

        let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = self
        dateRangePickerViewController.minimumDate = Date()
        dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 2, to: Date())
        dateRangePickerViewController.selectedStartDate = Date()
        dateRangePickerViewController.selectedEndDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())
        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func searchLocation() {
        let locationController = GMSAutocompleteViewController()
        locationController.delegate = self
        present(locationController, animated: true, completion: nil)
    }
    
    // textfield delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtModel && (txtMake.text!.isEmpty || txtMake.text == "All")  {
            self.value = nil
            return false
        }
        if textField == txtMake && make.isEmpty { return false }
        if textField == txtCategory && category.isEmpty { return false }
        
        /* check for textfield */
        // open calender for date selection
        let showCalender = textField == txtSelectDate
        showCalender ? self.showCalender() : nil
        
        // open google location picker view for add location
        let searchLocation = textField == txtSearchLocation
        searchLocation ? self.searchLocation() : nil
        
        // open picker view for category, make and model
        let showPicker = (textField == txtCategory ||  textField == txtMake || textField == txtModel)
        showPicker ? self.openPicker(for: textField.tag) : nil

        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        let string = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
//
//        if textField == txtSearchLocation {
//            btnClearLocation.alpha = string.isEmpty ? 1 : 0
//        }
//        if textField == txtSelectDate {
//            btnClearDate.alpha = string.isEmpty ? 1 : 0
//        }

        return true
    }
}

extension SearchViewController : CalendarDateRangePickerViewControllerDelegate {
    
    func didTapCancel() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        self.didPickDateRange(startDate: startDate, endDate: endDate)
    }
    
    func didCancelPickingDateRange() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func didPickDateRange(startDate: Date!, endDate: Date!) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        txtSelectDate.text = dateFormatter.string(from: startDate) + " to " + dateFormatter.string(from: endDate)
        self.startDate = dateFormatter.string(from: startDate)
        self.endDate = dateFormatter.string(from: endDate)
        self.showClearDateButton = true
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension SearchViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        // Then display the name in textField
        txtSearchLocation.text = place.name
        

        self.showClearLocationButton = true

        longitude  = String(place.coordinate.longitude)
        latitude  = String(place.coordinate.latitude)
        
        GMSGeocoder().reverseGeocodeCoordinate(place.coordinate) { (response, error) in
            guard let address = response?.firstResult() else {
                return
            }

            self.address = address.subLocality
            self.cityBk = address.locality
            self.stateBk = address.administrativeArea
            self.countryBk = address.country
            self.autoComplete = place.name
        }
        
        // Dismiss the GMSAutocompleteViewController when something is selected
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
