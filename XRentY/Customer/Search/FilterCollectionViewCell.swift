//
//  FilterCollectionViewCell.swift
//  XRentY
//
//  Created by user on 12/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.

import UIKit
import Kingfisher
import IBLocalizable

class FilterCell: UITableViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblProductAddresss: UILabel!
    @IBOutlet weak var lblCurrentPrice: UILabel!
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var lbllastPrice: UILabel!
    @IBOutlet weak var lblreview: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgproduct: UIImageView!
    @IBOutlet weak var starRatingView: StarRatingView!
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    var bookCar : ((Int)->())? = nil

    override func awakeFromNib() {
        outerView.layer.borderColor = Color.textColor.cgColor
        outerView.layer.borderWidth = 1
    }
    
    
    func configure(_ car: Car) {
        lblProductName.text = car.name?.capitalized ?? "N/A"
        lblProductAddresss.addImage(text: " " + (car.address ?? "N/A"), image: UIImage(named: "LocationMark_ic")!)
        let pricePerDay1 = Double(car.pricePerDay.roundOff()) ?? 0
        let pricePerDay2 = CGFloat(pricePerDay1).formattedWithSeparator
        let promolessPrice1 = Double(car.promolessPrice.roundOff()) ?? 0
        let promolessPrice2 = CGFloat(promolessPrice1).formattedWithSeparator
        lbllastPrice.text = symbol + "\(pricePerDay2)"
        lblCurrentPrice.text = symbol + "\(promolessPrice2)"
        imgproduct.kf.indicatorType = .activity
        let targetURL = URL(string: car.image ?? "")
        imgproduct.kf.setImage(with: targetURL , placeholder:#imageLiteral(resourceName: "car_placeholder_images"))
        starRatingView.setRate(car.starCount ?? 0)
        
        let number = "\(car.reviewNumber ?? "0")"
        lblreview.text = "(" + number +  " reviews".localized + ")"
    }
    
    @IBAction func bookCar(_ sender: UIButton) {
        bookCar?(tag)
    }
}
