//
//  GoogleMapViewController.swift
//  XRentY
//
//  Created by user on 25/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit
import GoogleMaps

class GoogleMapViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTitleView()

        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView

        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView
}

override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
}
}

