//
//  Search.swift
//  XRentY
//
//  Created by user on 10/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

struct Search {
    var cars: [Car] = []
    var query: [String:Any] = [:]
}
