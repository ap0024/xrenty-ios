//
//  ReturnCarViewController.swift
//  XRentY
//
//  Created by user on 29/09/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit
import IBLocalizable

class ReturnCarViewController: BaseViewController,UITextViewDelegate {
    
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var txtDiscription: UITextView!
    @IBOutlet weak var imgreturn: UIImageView!
    @IBOutlet weak var Notnotreturn: UIImageView!
    @IBOutlet weak var returnDate: UILabel!
    @IBOutlet weak var lblPickUpDate: UILabel!
    var savedFiles = [[String: AnyObject]]()
    var savedtableFiles = [[String: AnyObject]]()
    var newEntry = [String: AnyObject]()
    var newTableEntry = [String: AnyObject]()
    var returnOk:Int = 0
    var booking: Booking?
    var name: String?
    
    var returnCarHandler: (()->())? = nil
    @IBOutlet weak var returnCar: UIButton!
    @IBOutlet weak var saveData : UIButton!
    @IBOutlet weak var returnTableView: UITableView!
    
    //heading
    @IBOutlet weak var lblTextheading: UILabel!
    @IBOutlet weak var headingreturncar: UILabel!
    @IBOutlet weak var headingreturn: UILabel!
    @IBOutlet weak var headingpickup: UILabel!
    
    
    lazy var picker: UIImagePicker = {
        let imagePicker = UIImagePicker()
        imagePicker.pickerHandler = { self.addProfilePhoto($0,$1) }
        return imagePicker
    }()
    
    // return required imageString for paramater to send
    var imageString: String? {
        if carImage.image == #imageLiteral(resourceName: "imageBg_ic") { return nil }
        
        var imageData : Data?
        var compressedImage : UIImage?
        let max = 2097152
        imageData = carImage.image!.pngData()
        if (imageData?.count)! > max {
            imageData = carImage.image!.jpegData(compressionQuality: 0.5)
            compressedImage = UIImage(data: imageData!)
        }
        else {
            compressedImage = carImage.image
        }
        let (base64,format) = compressedImage!.convertImageTobase64()
        let imageString = "data:image/\(format.rawValue);base64," + base64!
        return imageString
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTitleView()
        self.enableRegister(false)
        self.txtDiscription.delegate = self
        lblPickUpDate.text = booking?.pickup_date
        returnDate.text = booking?.return_date

        saveData.alpha = 0.4
        saveData.isUserInteractionEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addProfilePhoto(_ image: UIImage, _ name : String) {
        carImage.image = image
        self.name = name
    }
    
    @IBAction func addDescription ( _ sender : UIButton)
    {
        if !(savedFiles.count < 10) {
            AlertManager.showAlert(type: .custom("You can upload maximum 10 images.".localized))
            return
        }
        
        if carImage.image == nil && txtDiscription.text == ""
        {
            AlertManager.showAlert(type: .custom("Please add car image description".localized))
        }
        else if txtDiscription.text == ""
        {
            AlertManager.showAlert(type: .custom("Please add car description".localized))
        }
        else if carImage.image == nil
        {
            AlertManager.showAlert(type: .custom("Please add car image".localized))
        }
        else
        {
            newEntry["desc"] = txtDiscription.text as AnyObject
            newEntry["image"] = imageString as AnyObject
            savedFiles.insert(newEntry, at: 0)
            
            newTableEntry["desc"] = txtDiscription.text as AnyObject
            newTableEntry["image"] =  carImage.image as AnyObject
            savedtableFiles.insert(newTableEntry, at: 0)
            returnTableView.reloadData()
            carImage.image = nil
            txtDiscription.text = ""
        }
        
        

    }
    
    @IBAction func carPhoto(_ sender: UIButton) {
        self.picker.showSheet(false)
    }
    
    @IBAction func returnOkBtn(_ sender: Any) {
        let acceptTermsCondition = self.setCheckUncheck(imgreturn)
            self.enableRegister(acceptTermsCondition)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        saveData.alpha = 1
        saveData.isUserInteractionEnabled = true
    }
    
    
    // returnCar
    func carReturn() {
        
        let query = ["post_image": savedFiles,
                     "solved": 1,
                     "rma_id": booking?.rma_id ?? "" ] as [String : Any]
        
        self.showLoader()
        WebService.returnCar(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    AlertManager.showAlert(type: .custom(response.message!) , action: {
                        Observer.updateMyBookings()
                        let controller =  ReviewToCustomerViewController.initiatefromStoryboard(.customer)
                        controller.product_id = (self.booking?.product![0].id)!
                        controller.seller_image = (self.booking?.product![0].sellerimage)!
                        controller.seler_name = (self.booking?.product![0].sellername)!
                        self.navigationController?.pushViewController(controller, animated: true)
                        self.navigationController!.viewControllers.remove(at:  self.navigationController!.viewControllers.count - 2)
                        self.returnCarHandler?()
                    })

                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    @IBAction func returnCar(_ sender: Any) {
        
        if savedFiles.isEmpty
        {
            AlertManager.showAlert(type: .custom("Please add car image description".localized))
        }
        else
        {
            self.carReturn()
        }
    }
    
    // enable register button if user accepts terms and condition
    func enableRegister(_ show: Bool) {
        returnCar.alpha = show ? 1 : 0.6
        returnCar.isUserInteractionEnabled = show
    }
    
    func setCheckUncheck(_ view: UIImageView) -> Bool {

        view.tag = view.tag == 1 ? 0 : 1
        let checkboxImage = UIImage(named: "checkdark")
        let uncheckboxImage = UIImage(named: "uncheck")
        view.image = view.tag == 1 ? checkboxImage : uncheckboxImage
        return view.tag == 1
    }
}

extension ReturnCarViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedtableFiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReturnCarTableViewCell", for: indexPath) as? ReturnCarTableViewCell
        
        let info = savedtableFiles[indexPath.row]
        cell?.txtxDiscription.text = info["desc"] as? String
        cell?.imgCar.image = info["image"] as? UIImage
        
        cell?.zoomImage = { self.zoomImage((cell?.imgCar.image)!, (cell?.txtxDiscription.text)!) }
        return cell!
    }
    func zoomImage(_ zoomImage: UIImage, _ disc : String) {
        
        self.enlargeImage(view: self, profileImage: UIImageView(image: zoomImage), text: disc)
    }
}
