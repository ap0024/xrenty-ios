//
//  ReturnCarTableViewCell.swift
//  XRentY
//
//  Created by user on 29/09/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit

class ReturnCarTableViewCell: UITableViewCell {

    
    @IBOutlet weak var txtxDiscription: UITextView!
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var btnImageZoom: UIButton!

    var zoomImage : (()->())? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func zoomImage(_ sender: Any) {
        
        if let handler = zoomImage {
            handler()
        }
    }
}
