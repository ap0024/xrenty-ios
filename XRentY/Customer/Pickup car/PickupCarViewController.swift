//
//  PickupCarViewController.swift
//  XRentY
//
//  Created by user on 29/09/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit
import PaypalSDK

class PickupCarViewController: BaseViewController,UITextViewDelegate {
    
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var txtDiscription: UITextView!
    @IBOutlet weak var imgAccept: UIImageView!
    @IBOutlet weak var returnDate: UILabel!
    @IBOutlet weak var lblPickUpDate: UILabel!
    @IBOutlet weak var btnPickUp: UIButton!
    @IBOutlet weak var saveData : UIButton!
    
    //heading
    
    @IBOutlet weak var lblTextheading: UILabel!
    @IBOutlet weak var headingAccetpCar: UILabel!
    @IBOutlet weak var headingreturn: UILabel!
    @IBOutlet weak var headingpickup: UILabel!
    
    var savedFiles = [[String: AnyObject]]()
    var savedtableFiles = [[String: AnyObject]]()
    var newEntry = [String: AnyObject]()
    var newTableEntry = [String: AnyObject]()
    var returnCarHandler: ((String)->())? = nil
    var booking: Booking?
    var name: String?
    var PickUpCar : [PickUpCar] = []
    
    var paypalCaptureAmountHandler: ((Bool,String)->())? = nil
    // return required imageString for paramater to send
    var imageString: String? {
        if carImage.image == #imageLiteral(resourceName: "imageBg_ic") { return nil }
        
        var imageData : Data?
        var compressedImage : UIImage?
        let max = 2097152
        imageData = carImage.image!.pngData()
        if (imageData?.count)! > max {
            imageData = carImage.image!.jpegData(compressionQuality: 0.5)
            compressedImage = UIImage(data: imageData!)
        }
        else {
            compressedImage = carImage.image
        }
        let (base64,format) = compressedImage!.convertImageTobase64()
        let imageString = "data:image/\(format.rawValue);base64," + base64!
        return imageString
    }
    
    @IBOutlet weak var pickUpTableView: UITableView!
    
    
    lazy var picker: UIImagePicker = {
        let imagePicker = UIImagePicker()
        imagePicker.pickerHandler = { self.addProfilePhoto($0,$1) }
        return imagePicker
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.addTitleView()
        self.enableRegister(false)
        self.txtDiscription.delegate = self
        lblPickUpDate.text = booking?.pickup_date
        returnDate.text = booking?.return_date
        
        saveData.alpha = 0.4
        saveData.isUserInteractionEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addProfilePhoto(_ image: UIImage,_ name : String) {
        carImage.image = image
        self.name = name
    }
    
    @IBAction func addDescription ( _ sender : UIButton) {
        
        if !(savedFiles.count < 10) {
            AlertManager.showAlert(type: .custom("You can upload maximum 10 images.".localized))
            return
        }
        
        if carImage.image == nil && txtDiscription.text == "" {
            
            AlertManager.showAlert(type: .custom("Please add car image description".localized))
        }
            
        else if txtDiscription.text == "" {
            AlertManager.showAlert(type: .custom("Please add car description".localized))
        }
            
        else if carImage.image == nil {
            AlertManager.showAlert(type: .custom("Please add car image".localized))
        }
        else  {
            newEntry["desc"] = txtDiscription.text as AnyObject
            newEntry["image"] = imageString as AnyObject
            savedFiles.insert(newEntry, at: 0)
            
            newTableEntry["desc"] = txtDiscription.text as AnyObject
            newTableEntry["image"] =  carImage.image as AnyObject
            savedtableFiles.insert(newTableEntry, at: 0)
            pickUpTableView.reloadData()
            
            carImage.image = nil
            txtDiscription.text = ""
            
//            btnPickUp.alpha = 1
//            btnPickUp.isUserInteractionEnabled = true
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        saveData.alpha = 1
        saveData.isUserInteractionEnabled = true
    }
    
    @IBAction func carPhoto(_ sender: UIButton) {
        self.picker.showSheet(false)
    }
    
    @IBAction func pickupCar(_ sender: Any) {
        
        if savedFiles.isEmpty
        {
            AlertManager.showAlert(type: .custom("Please add car image description".localized))
        }
        else
        {
            self.pickUpCar()
        }
    }
    
    @IBAction func AcceptCarBtn(_ sender: Any) {
        
        let acceptTermsCondition = self.setCheckUncheck(imgAccept)
        self.enableRegister(acceptTermsCondition)
    }
    
    // pickup
    func pickUpCar() {
        
        self.confirmCharge { (success, message) in
            DispatchQueue.main.async {
                if success {
                    let query = ["post_image": self.savedFiles,
                                 "return": 1,
                                 "order_id": (self.booking?.order_id)! ] as [String : Any]
                    
                    self.showLoaderOnWindow()
                    WebService.pickUpCar(queryItems: query) { result in
                        DispatchQueue.main.async {
                            self.hideLoader()
                            switch result {
                            case .success(let response):
                                let rmaId = response.object?.rma_id
                                AlertManager.showAlert(type: .custom(response.message!) , action: {
                                    self.returnCarHandler?(rmaId!)
                                    self.navigationController?.popToRootViewController(animated: true)
                                })
                            case .failure(let error):
                                AlertManager.showAlert(on: self, type: .custom(error.message))
                            }
                        }
                    }
                }
                else {
                    AlertManager.showAlert(on: self, type: .custom(message))
                }
            }
        }
    }
    
    // desposite charge confirmation
    func confirmCharge( completion: @escaping (Bool,String)->()) {
        
        if let payment_method = booking?.payment_method, payment_method == "paypal_express" {
            if let orderid = booking?.paypal_charge_order_id {
                if !orderid.isEmpty {
                    
                    self.showLoaderOnWindow()
                    PaypalSDK.shared.delegate = self
                    paypalCaptureAmountHandler = completion

                    if let orderPlacedFromWeb = booking?.isOrderFromWeb, orderPlacedFromWeb {
                        if let customer_charge_amount = booking?.charge, let currencyCode = booking?.product?.first?.show_currency_price  {
                             PaypalSDK.shared.captureV1(transactionId: orderid, total: customer_charge_amount, currency: currencyCode)
                        }
                        else {
                            completion(false, "internal server error")
                        }
                    } else {
                        PaypalSDK.shared.orderDetail(for: orderid)
                    }
                }
                else{
                    completion(true, "success")
                }
            } else {
                completion(false, "invalid order id")
            }
        }
        else {
            
            guard let charge = booking?.charge, let chargeTransactionId = booking?.chargeTransactionId,let convertedCustomerDepositAmount = booking?.convertedCustomerDepositAmount, let customerId = Authentication.openpayCustomerId else {
                completion(false,"Charge transaction id not generated")
                return
            }
            
            if !charge.isEmpty {
                showLoaderOnWindow()
                OpenpayPayment.confirmCharge(amount: charge, customerId: customerId, transactionId: chargeTransactionId) { (json) in
                    DispatchQueue.main.async {
                        self.hideLoader()
                        guard let response = json else {
                            return
                        }
                        //print(json)
                        if let status = response["status"] as? String,status == "completed" {
                            completion(true, "Confirm charge success")
                        }
                        else {
                            if let description = response["description"] as? String {
                                completion(false, description)
                            } else {
                                completion(false, "Confirm charge fails")
                            }
                        }
                    }
                }
            }
            else {
                completion(true, "Confirm charge success")
            }
        }
    }
    
    //func
    
    // enable register button if user accepts terms and condition
    func enableRegister(_ show: Bool) {
        btnPickUp.alpha = show ? 1 : 0.6
        btnPickUp.isUserInteractionEnabled = show
    }
    
    func setCheckUncheck(_ view: UIImageView) -> Bool {
        view.tag = view.tag == 1 ? 0 : 1
        let checkboxImage = UIImage(named: "checkdark")
        let uncheckboxImage = UIImage(named: "uncheck")
        view.image = view.tag == 1 ? checkboxImage : uncheckboxImage
        return view.tag == 1
    }
}

extension PickupCarViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return savedtableFiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PickupCarCell", for: indexPath) as? PickupCarCell
        let info = savedtableFiles[indexPath.row]
        
        if  (info["desc"] as! String == "")
        {
            cell?.txtxDiscription.isHidden = true
        }
        else
        { 
            cell?.txtxDiscription.text = info["desc"] as? String
        }
        
        cell?.imgCar.image = (info["image"] as! UIImage)
        cell?.zoomImage = { self.zoomImage((cell?.imgCar.image)!, (cell?.txtxDiscription.text)!) }
        
        return cell!
    }
    
    func zoomImage(_ zoomImage: UIImage, _ disc : String) {
        self.enlargeImage(view: self, profileImage: UIImageView(image: zoomImage), text: disc)
    }
}

extension PickupCarViewController: PaymentDelegate {
    
    func didUpdateOrder(with state: PaymentState) {
        switch state {
        case .capture:
            self.hideLoader()
            self.paypalCaptureAmountHandler?(true,"success")
        case .detail(let order):
            self.hideLoader()
            if let link = ((order.purchase_units?[0])?.payments?.authorizations?[0])?.links.find(where: "capture") {
                self.showLoaderOnWindow()
                PaypalSDK.shared.capture(link: link)
            }
        default:
            break
        }
    }
    
    func didFailUpdateOrder(with error: PaymentError) {
        self.hideLoader()
        AlertManager.showAlert(type: .custom(error.errorDescription))
    }
}
