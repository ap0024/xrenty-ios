//
//  ProductDetailMapView.swift
//  XRentY
//
//  Created by user on 11/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class ProductDetailMapView: UIView {

    let fontBold = UIFont(name: "Roboto-Bold", size: 11)
    let fontRegular = UIFont(name: "Roboto-Regular", size: 11)
    
    var fullScreenHandler: ((_ showFullScreen: Bool)->())? = nil
    var latitude : String = ""
    var longitude : String = ""
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mapBtn: UIButton!
    @IBOutlet weak var satelliteBtn: UIButton!
    @IBOutlet weak var zoomIn: UIButton!
    @IBOutlet weak var zoomOut: UIButton!
    var zoomScale: Float = 12.0
    
    // configure
    func configureMap(for location: CLLocationCoordinate2D? = nil) {
        
        let zoomScale: Float = 12.0
        latitude = latitude == "" ? "0" : latitude
        longitude = longitude == "" ? "0" : longitude
        
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude ?? "")!, longitude:CLLocationDegrees(longitude ?? "")!)

        let marker = GMSMarker(position: coordinates)
        marker.map = mapView
        mapView.delegate = self
        mapView.animate(toLocation: coordinates)
        mapView.animate(toZoom: zoomScale)

        marker.position = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
        marker.map = mapView
    }
    
    @IBAction func zoomIn(_ sender: Any) {
        
        zoomScale += 0.2
        mapView.animate(toZoom: zoomScale)
    }
    
    @IBAction func zoomOut(_ sender: Any) {
        
        zoomScale -= 0.2
        mapView.animate(toZoom: zoomScale)
    }
    
    @IBAction func mapView(_ sender: Any) {
        
        mapBtn.titleLabel?.font = fontBold
        satelliteBtn.titleLabel?.font = fontRegular
        mapView.mapType = .normal
    }
    
    @IBAction func satelliteView(_ sender: Any) {
        
        mapBtn.titleLabel?.font = fontRegular
        satelliteBtn.titleLabel?.font = fontBold
        mapView.mapType = .satellite
    }
    
    @IBAction func streetView(_ sender: Any) {
        
    }
    
    @IBAction func fullScreenView(_ sender: UIButton) {
        
        sender.tag = sender.tag == 1 ? 0 : 1
        mapView.isMyLocationEnabled = sender.tag == 1
        mapView.isUserInteractionEnabled =  sender.tag == 1
        fullScreenHandler?(sender.tag == 1)
    }
}

extension ProductDetailMapView: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        print("will move")
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("change camera position")
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("begin dragging")
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("end dragging")
    }
}
