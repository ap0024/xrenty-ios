//
//  productDetailViewController.swift
//  XRentY
//
//  Created by user on 13/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit
import Social
import CoreLocation
import FBSDKShareKit

class ProductDetailViewController: BaseViewController,STRatingControlDelegate {
    
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var nameTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var reviewTxt: UIButton!
    @IBOutlet weak var lblcarName: UILabel!
    @IBOutlet weak var lblcarAddress: UILabel!
    @IBOutlet weak var lblcarReview: UILabel!
    @IBOutlet weak var imgCarowner: UIImageView!
    @IBOutlet weak var lblCarOwnerName: UILabel!
    @IBOutlet weak var lblavalibleDiscount: UILabel!
    @IBOutlet weak var lblPerDayPrice: UILabel!
    @IBOutlet weak var txtSelectDate: UITextField!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgWishlish: UIImageView!
    @IBOutlet weak var tableDescription: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var detailMapView: ProductDetailMapView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewTopConstrains: NSLayoutConstraint!
    @IBOutlet weak var detailMapViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionCarImages: UICollectionView!
    @IBOutlet weak var pageControl: CHIPageControlJalapeno!
    @IBOutlet weak var starRatingView: StarRatingView!
    @IBOutlet weak var wishlistBtn: UIButton!
    @IBOutlet weak var askmeQuestionBtn: UIButton!
    @IBOutlet weak var addTocartBtn: UIButton!
    @IBOutlet weak var checkoutBtn: UIButton!
    @IBOutlet weak var askMeQuestionStack: UIStackView!
    @IBOutlet weak var wishlistStack: UIStackView!
    @IBOutlet weak var btnClearDates: UIButton!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var shareinTopConstarints: NSLayoutConstraint!
    @IBOutlet weak var discountLabel: UILabel!
    
    var firstDate: Date?
    var lastDate: Date?
    var datesRange: [Date]?
    var sellerId: String?
    var carDetail: String = ""
    var fromDate : String = ""
    var toDate : String = ""
    var productUrl : String = ""
    var currentDate : String = ""
    var differenceOfDate  : String = ""
    var starCount : Int = 0
    var wishlist : Int = 0
    var isMycar : Int = 0
    var reviewCount : Int = 0
    var reviewPercentage : Int = 0
    var presentMap: Bool = false
    var presentNotification: Bool = false
    var carImages: [CarImage] = []
    var attributes: [MainAttributes] = []
    var scrollOffsetY: CGFloat!
    var car: Car?
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    var arrDatesAvailability : [[String:Any]] = []
    
    var showClearDatesButton: Bool = false  {
        didSet {
            self.btnClearDates.alpha = self.showClearDatesButton ? 1 : 0
        }
    }
    fileprivate var lunar: Bool = false {
        didSet {
            self.calendar.reloadData()
        }
    }
    fileprivate let lunarFormatter = LunarFormatter()
    fileprivate var theme: Int = 0 {
        didSet {
            switch (theme) {
            case 0:
                self.calendar.appearance.weekdayTextColor = UIColor(red: 14/255.0, green: 69/255.0, blue: 221/255.0, alpha: 1.0)
                self.calendar.appearance.headerTitleColor = UIColor(red: 14/255.0, green: 69/255.0, blue: 221/255.0, alpha: 1.0)
                self.calendar.appearance.eventDefaultColor = UIColor(red: 31/255.0, green: 119/255.0, blue: 219/255.0, alpha: 1.0)
                self.calendar.appearance.selectionColor = UIColor(red: 31/255.0, green: 119/255.0, blue: 219/255.0, alpha: 1.0)
                self.calendar.appearance.headerDateFormat = "MMMM yyyy"
                self.calendar.appearance.todayColor = UIColor(red: 31/255.0, green: 119/255.0, blue: 219/255.0, alpha: 1.0)//UIColor(red: 198/255.0, green: 51/255.0, blue: 42/255.0, alpha: 1.0)
                self.calendar.appearance.borderRadius = 1.0
                self.calendar.appearance.headerMinimumDissolvedAlpha = 0.2
                
            case 1:
                self.calendar.appearance.weekdayTextColor = UIColor.red
                self.calendar.appearance.headerTitleColor = UIColor.darkGray
                self.calendar.appearance.eventDefaultColor = UIColor.green
                self.calendar.appearance.selectionColor = UIColor.blue
                
                self.calendar.appearance.headerDateFormat = "yyyy-MM";
                self.calendar.appearance.todayColor = UIColor.red
                self.calendar.appearance.borderRadius = 1.0
                self.calendar.appearance.headerMinimumDissolvedAlpha = 0.0
                
            case 2:
                self.calendar.appearance.weekdayTextColor = UIColor.red
                self.calendar.appearance.headerTitleColor = UIColor.red
                self.calendar.appearance.eventDefaultColor = UIColor.green
                self.calendar.appearance.selectionColor = UIColor.blue
                self.calendar.appearance.headerDateFormat = "yyyy/MM"
                self.calendar.appearance.todayColor = UIColor.orange
                self.calendar.appearance.borderRadius = 0
                self.calendar.appearance.headerMinimumDissolvedAlpha = 1.0
            default:
                break;
            }
        }
    }
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    fileprivate let gregorian: NSCalendar! = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        addTitleView()
        getCarDetail()
        
        notificationView.isHidden = true
        nameTopConstraints.constant = -80
        if Authentication.LicenceDetail == false {
        notificationView.isHidden = false
            self.checkoutBtn.isUserInteractionEnabled = false
            self.checkoutBtn.alpha = 0.3
            nameTopConstraints.constant = 10
        }
        
        calendar.allowsMultipleSelection = true
        self.showClearDatesButton = false
        tableDescription.estimatedRowHeight = 100
        detailMapView.fullScreenHandler = { self.presentMap($0) }
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 700
        }
        else
        {
            self.calendarHeightConstraint.constant = 650
        }
        self.calendar.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesUpperCase]
        self.calendar.accessibilityIdentifier = "calendar"
        Observer.poptoSearch(target: self, selector: #selector(poptosearch))

    }
    
    @IBAction func clearDates(_ sender: UIButton) {
        self.txtSelectDate.text = ""
        self.showClearDatesButton = false
    }
    
    @objc func poptosearch() {
        self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.viewControllers.first?.tabBarController?.selectedIndex = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func viewDetail(_ sender: Any) {

        if  Authentication.appMode == false {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                for controller in BaseNavigationController.sharedInstance.viewControllers {
                    if controller.isKind(of: BaseTabBarViewController.self) {
                        if (controller as! BaseTabBarViewController).selectedIndex != 4 {
                            (controller as! BaseTabBarViewController).selectTab(4)
                        }
                    }
                }
            }
        }
        else
        {
            self.showToast(message: "please switch to customer for fill license detail.".localized)
        }
    }
    
    
    func presentMap(_ present: Bool) {
        self.presentMap = present
        let point = CGPoint(x: 0, y: present ? detailMapView.frame.origin.y : scrollOffsetY)
        scrollView.isScrollEnabled = !present
        detailMapViewHeight.constant = present ? view.bounds.height - (self.navigationController!.navigationBar.frame.size.height + 64) : 400
        print(detailMapViewHeight.constant)
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        scrollView.setContentOffset(point, animated: true)
    }
    
    @IBAction func addReview(_ sender: Any) {
        
    }
    
    @IBAction func googleShare(_ sender: Any) {
        
        let myWebsite = NSURL(string: self.productUrl)
        let activityViewController = UIActivityViewController(activityItems: [myWebsite as Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func facebookShare(_ sender: Any) {
        let content = FBSDKShareLinkContent()
        content.contentURL = URL(string: self.productUrl)
        FBSDKShareDialog.show(from: self, with: content, delegate: nil)
    }
    
    @IBAction func twitterShare(_ sender: Any) {
        let tweetText = ""
        let tweetUrl = self.productUrl
        let shareString = "https://twitter.com/intent/tweet?text=\(tweetText)&url=\(tweetUrl)"
        
        // encode a space to %20 for example
        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        // cast to an url
        let url = URL(string: escapedShareString)
        
        // open in safari
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    
    @IBAction func addtoWishlist(_ sender: UIButton) {
        self.wishlist = setCheckUncheck(imgWishlish) ? 1 : 0
        self.addToWishlist()
    }
    
    func setCheckUncheck(_ view: UIImageView) -> Bool {
        view.tag = view.tag == 1 ? 0 : 1
        let checkboxImage = UIImage(named: "wishlist_ic")
        let unCheckboxImage = UIImage(named: "unselect_wishlist_ic")
        view.image = view.tag == 1 ? checkboxImage : unCheckboxImage
        return view.tag == 1
    }
    
    func adjustUITextViewHeight(arg : UITextView) {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    @IBAction func selectDateforOrder(_ sender: Any) {
        self.datePickerTapped()
    }
        
    func datePickerTapped() {
        
        let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = self
        dateRangePickerViewController.minimumDate = Date()
        dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 2, to: Date())
        dateRangePickerViewController.selectedStartDate = Date()
        dateRangePickerViewController.selectedEndDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())
        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func addtoCart(_ sender: Any) {
        
        
        if Authentication.LicenceDetail == false {
        AlertManager.showAlert(type: .custom("Please fill license information for book car".localized))
        }
        else
        {
            if txtSelectDate.text == ""
            {
                AlertManager.showAlert(type: .custom("Please select a Date first".localized))
            }
            else {
                // check if cart is empty or not
                self.checkCartStatus { isCleared in
                    if isCleared {
                        // add car to cart
                        self.addToCart()
                    }
                    else {
                        // show alert if cart is not empty
                        AlertManager.showAlert(on: self, type: .custom("You can add only one car at a time.".localized))
                    }
                }
            }
        }
    }
    
    @IBAction func checkout(_ sender: Any) {
        self.pushTo(.cart)
    }
    
    @IBAction func didTapAskQuestion(_ sender: Any) {
        
        let controller = PopOverViewController.initiatefromStoryboard(.carDetail)
        controller.productId = car?.productId ?? ""
        controller.userId = sellerId
        controller.completionBlock = {message in
            AlertManager.showAlert(on: self, type: .custom(message))
            self.hideLoader()
        }
        self.presentDetail(controller)

    }
    
    // check cart status
    func checkCartStatus(completion:@escaping (Bool)->()) {
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        WebService.cartStatus { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    completion(response.object?.itemsInCart == 0)
                    /*
                     print(response.object?.depositConfirmed)
                     print(response.object?.convertedDeposit)
                     print(response.object?.itemsInCart)
                     print(response.object?.depositTransactionId)
                     */
                case .failure(let error):
                    AlertManager.showAlert(on: self, type:  .custom(error.message))
                }
            }
        }
    }
}

extension ProductDetailViewController {
    
    func getCarDetail() {
        
        guard let id = car?.productId else { return }
        var sdate: String = ""
        var edate: String = ""
        
        if Authentication.startDate == nil
        {
            sdate = ""
            edate = ""
        }
        else
        {
            sdate = Authentication.startDate ?? ""
            edate = Authentication.endDate ?? ""
        }
        
        let query = ["id": id,
                     "check_in": sdate ,
                     "check_out": edate]
        print(query)
        let grayColor = UIColor(red: 230, green: 230, blue: 241, alpha: 1)
        showLoader(color: grayColor)
        WebService.getCarDetail(queryItems: query) { result in
            DispatchQueue.main.async {
                //                self.hideLoader()
                switch result {
                case .success(let response):
                    self.addDescription(response.object)
                    self.getCalender()
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    func addDescription(_ car: Car?) {
        
        guard let product = car else {
            print("----no data----"); return
        }
        
        self.sellerId = product.sellerId
        
        //print(String(c, Double(1230.0)))
        
        self.carDetail = product.detail == "" ? "N/A" : product.detail ?? "N/A"
        self.attributes = sortAttributes(product.attributes)
        self.tableDescription.reloadData()
        self.productUrl = product.producturl ?? "https://www.xrenty.com"
        self.lblcarName.text = product.name?.capitalized ?? "N/A"
        self.lblcarAddress.addImage(text: " " + (product.address ?? "N/A"), image: #imageLiteral(resourceName: "map_marker.png"))
        self.lblCarOwnerName.text = product.sellerInfo?.name?.capitalized ?? "N/A"
        
        let pricePerDay1 = Double(product.pricePerDay) ?? 0
        let pricePerDay2 = CGFloat(pricePerDay1).formattedWithSeparator
        
        let promolessPrice1 = Double(product.promolessPrice) ?? 0
        let promolessPrice2 = CGFloat(promolessPrice1).formattedWithSeparator
        
        
        self.lblPerDayPrice.text =  symbol + "\(pricePerDay2)"
        
        self.lblPrice.text = symbol + "\(promolessPrice2)" == "" ?  symbol + "\(pricePerDay2)" : symbol + "\(promolessPrice2)"
        
        starRatingView.setRate(product.starCount ?? 0)
        
        if product.discountMsg == ""
        {
            shareinTopConstarints.constant = -100
            discountLabel.isHidden = true
            lblavalibleDiscount.isHidden = true
        }
        else
        {
            shareinTopConstarints.constant = 30
            discountLabel.isHidden = false
            lblavalibleDiscount.isHidden = false
            self.lblavalibleDiscount.text = product.discountMsg ?? ""
        }
        
        if Authentication.customerId == product.sellerId
        {
            self.addTocartBtn.isUserInteractionEnabled = false
            self.addTocartBtn.alpha = 0.3
            
            self.askmeQuestionBtn.isUserInteractionEnabled = false
            self.askMeQuestionStack.alpha = 0.3
            
            self.wishlistBtn.isUserInteractionEnabled = false
            self.wishlistStack.alpha = 0.3
            
            self.checkoutBtn.isUserInteractionEnabled = false
            self.checkoutBtn.alpha = 0.3
            
            self.txtSelectDate.isUserInteractionEnabled = false
        }

        self.isMycar = product.ismycar ?? 0
        self.wishlist = product.wishlist ?? 0
        if wishlist == 1 {
            self.imgWishlish.image = #imageLiteral(resourceName: "wishlist_ic")
            self.imgWishlish.tag = 1
        }
        else {
            self.imgWishlish.image = #imageLiteral(resourceName: "unselect_wishlist_ic")
            self.imgWishlish.tag = 0
        }
        detailMapView.latitude = product.latitude!
        detailMapView.longitude = product.longitude!
        
        
        
        self.addImages(product.images)
        self.lblcarReview.text = "\(product.ratingPercent ?? "")" + "%" + " " + "Positive Review".localized
        self.starCount = product.starCount ?? 0
        if let sellerImageUrl = product.sellerInfo?.image {
            let url = URL(string: sellerImageUrl)
            self.imgCarowner.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "user_ic"))
        }
        
        guard let lat = car!.latitude, let long = car!.longitude, let latitude = CLLocationDegrees(lat), let longitude = CLLocationDegrees(long) else {
            return
        }
        
        let coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.detailMapView.configureMap(for: coordinates)
    }
    
    func addImages(_ images: [CarImage]?) {
        self.carImages = images ?? []
        self.pageControl.numberOfPages = images?.count ?? 0
        collectionCarImages.reloadData()
    }
    
    // add to wishlist
    func addToWishlist() {
        guard let id = car?.productId else { return }
        let query = ["prod_id" : id ,
                     "status":wishlist] as [String : Any]
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        WebService.addWishlist(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    AlertManager.showAlert(on: self, type: .custom(response.message!))
                    Observer.updateWishlist()
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    // add to cart
    func addToCart() {
        
        guard let productId = car?.productId else {
            print("null product id")
            return
        }
        
        let query = ["prod_id": productId,
                     "check_in": fromDate,
                     "check_out": toDate,
                     "total_days": differenceOfDate,
                     "currency" : Authentication.currency! ] as [String:Any]
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        WebService.addtocart(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    Observer.updateCart()
                    self.getCalender()
                    let totalPrice = response.object
                    var subTotal = "\(totalPrice?.price ?? "" )"
                    
                    let subTotal1 = Double(subTotal.roundOff()) ?? 0
                    let subTotal2 = CGFloat(subTotal1).formattedWithSeparator
                    
                    self.lblPrice.text = self.symbol + "\(subTotal2)"
                    AlertManager.showAlert(on: self, type: .custom(response.message ?? ""))
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    // add to cart
    func proceedTocheckout() {
        let query = ["prod_id": car!.productId! ,
                     "check_in": fromDate,
                     "check_out": toDate,
                     "total_days": differenceOfDate] as [String:Any]
        self.showLoader()
        WebService.addtocart(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    
                    AlertManager.showAlert(type: .custom(response.message!) , action: {
                        Observer.updateCart()
                        self.pushTo(.cart)
                    })
                case .failure(let error):
                    
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    func sortAttributes(_ attributes: [MainAttributes]?) -> [MainAttributes] {
        
        guard let mainAttributes = attributes else { return [] }
        
        func sortAttributes(_ key: String) -> MainAttributes? {
            for i in 0..<mainAttributes.count {
                if mainAttributes[i].code == key {
                    return mainAttributes[i]
                }
            }
            return nil
        }
        
        let sortedArr = ["brand_make","passengers","engine_type","transmission","ac","sound_systems"]
        var arr: [MainAttributes] = []
        for i in 0..<sortedArr.count {
            if let sortedAttribute = sortAttributes(sortedArr[i]) {
                arr.append(sortedAttribute)
            }
        }
        return arr
    }
    
    // getCalender
    func getCalender() {
        guard let id = car?.productId else { return }
        let query = ["product_id":id ]
        WebService.getCalender(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    guard let dict = response.objects else { return }
                    let arrDatesAvailable : [[String:Any]] = dict.compactMap { return $0.data! }
                    // print("CalenderData",arrDatesAvailable)
                    self.arrDatesAvailability = arrDatesAvailable
                    self.calendar.delegate = self
                    self.calendar.dataSource = self;
                    self.calendar.reloadData()
                    let arr = NSMutableArray(array: arrDatesAvailable)
                    self.calendar.availableDates(arr)
                    break
                case .failure:
                    break
                }
            }
        }
    }
}

extension ProductDetailViewController : CalendarDateRangePickerViewControllerDelegate {
    
    func didTapCancel() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        self.didPickDateRange(startDate: startDate, endDate: endDate)
    }
    
    func didCancelPickingDateRange() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func didPickDateRange(startDate: Date, endDate: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        txtSelectDate.text = dateFormatter.string(from: startDate) + " to " + dateFormatter.string(from: endDate)
        self.showClearDatesButton = true
        self.fromDate = dateFormatter.string(from: startDate)
        self.toDate = dateFormatter.string(from: endDate )
        let components = Calendar.current.dateComponents([.day], from: startDate, to: endDate).day
        differenceOfDate = "\(components!)"
        if fromDate == toDate
        {
            differenceOfDate = "\(components! + 1)"
            
        }
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension ProductDetailViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if presentMap { return }
        self.scrollOffsetY = scrollView.contentOffset.y
    }
}

extension ProductDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return carImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CarImageCell
        if let getUrl = carImages[indexPath.row].url {
            cell.carImageView.kf.indicatorType = .activity
            let url = URL(string: getUrl)
            cell.carImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "car_placeholder_images"))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let indexPath = collectionCarImages.indexPathForItem(at: scrollView.contentOffset) {
            pageControl.progress = Double(indexPath.row)
        }
    }
}

extension ProductDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : attributes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return indexPath.section == 0 ? descriptionCell(tableView, indexPath: indexPath) : attributesCell(tableView, indexPath: indexPath)
    }
    
    func descriptionCell(_ tableview: UITableView, indexPath: IndexPath) -> DescriptionCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "descriptionCell", for: indexPath) as! DescriptionCell
        cell.lblDescription.text = carDetail
        cell.lblDescription.setLineSpacing(lineSpacing: 1.3, lineHeightMultiple: 1.3)
        return cell
    }
    
    func attributesCell(_ tableview: UITableView, indexPath: IndexPath) -> AttributeCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "attributeCell", for: indexPath) as! AttributeCell
        cell.attributeImage.image = attributes[indexPath.row].image()
        cell.attributeValue.text = attributes[indexPath.row].labelName()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableHeight.constant = tableView.contentSize.height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 30)
        let header = UIView(frame: frame)
        
        let labelFrame = CGRect(x: 15, y: 0, width: tableView.bounds.width - 30, height: 30)
        let label = UILabel(frame: labelFrame)
        label.font = UIFont(name: "Roboto-Bold", size: 20)
        label.textColor = Color.barTintColor
        label.text = section == 0 ? "Description Car".localized : "Main Attributes".localized
        header.addSubview(label)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}

extension ProductDetailViewController : FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance
{
    // MARK:- FSCalendarDataSource
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        
        return Color.greenColour
    }
    
    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
        return (self.gregorian.isDateInToday(date) ? nil : nil)
    }
    
    func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {
        guard self.lunar else {
            return nil
        }
        return self.lunarFormatter.string(from: date)
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        let strmaxDate = "\(arrDatesAvailability.first?["date"] ?? result )"
        return self.formatter.date(from: strmaxDate)!
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        let strmaxDate = "\(arrDatesAvailability.last?["date"] ?? result )"
        return self.formatter.date(from: strmaxDate)!
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    
    // MARK:- FSCalendarDelegate
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("change page to \(self.formatter.string(from: calendar.currentPage))")
    }
    
    //    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
    //        print("calendar did select date \(self.formatter.string(from: date))")
    //        if monthPosition == .previous || monthPosition == .next {
    //            calendar.setCurrentPage(date, animated: true)
    //        }
    //    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // nothing selected:
        if firstDate == nil {
            firstDate = date
            datesRange = [firstDate!]
            
            print("datesRange contains: \(datesRange!)")
            didPickDateRange(startDate: (datesRange?.first)!, endDate: (datesRange?.last)!)
            return
        }
        
        // only first date is selected:
        if firstDate != nil && lastDate == nil {
            // handle the case of if the last date is less than the first date:
            if date <= firstDate! {
                calendar.deselect(firstDate!)
                firstDate = date
                datesRange = [firstDate!]
                
                print("datesRange contains: \(datesRange!)")
                didPickDateRange(startDate: (datesRange?.first)!, endDate: (datesRange?.last)!)
                return
            }
            
            let range = datesRange(from: firstDate!, to: date)
            
            lastDate = range.last
            
            for d in range {
                calendar.select(d)
            }
            
            datesRange = range
            
            print("datesRange contains: \(datesRange!)")
            didPickDateRange(startDate: (datesRange?.first)!, endDate: (datesRange?.last)!)
            return
        }
            
        else if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            
            datesRange = []
            print("datesRange contains: \(datesRange!)")
            self.txtSelectDate.text = ""
        }
    }
    
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // both are selected:
        
        // NOTE: the is a REDUANDENT CODE:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            
            datesRange = []
            print("datesRange contains: \(datesRange!)")
            self.txtSelectDate.text = ""
        }
    }
}

extension UILabel {
    
    func addImage(text: String, image: UIImage) {
        let attachment: NSTextAttachment = NSTextAttachment()
        attachment.image = image
        let attachmentString: NSAttributedString = NSAttributedString(attachment: attachment)
        let text = NSAttributedString(string: text)
        let mutableAttachmentString = NSMutableAttributedString(attributedString: attachmentString)
        mutableAttachmentString.append(text)
        self.attributedText = mutableAttachmentString
    }
    
    func addImageAfterText(text: String, image: UIImage) {
        let attachment: NSTextAttachment = NSTextAttachment()
        attachment.image = image
        let attachmentString: NSAttributedString = NSAttributedString(attachment: attachment)
        let text = NSMutableAttributedString(string: text)
        let mutableAttachmentString = NSMutableAttributedString(attributedString: attachmentString)
        text.append(mutableAttachmentString)
        //mutableAttachmentString.append(text)
        self.attributedText = text
    }
}
