//
//  StarRatingView.swift
//  XRentY
//
//  Created by user on 02/11/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit

class StarRatingView: UIStackView {

    @IBOutlet var starsView: [UIImageView]!
    var enableTap: Bool = false
    
    var rating: Int {
        var rating = 0
        starsView.forEach {
            rating = $0.image == #imageLiteral(resourceName: "return star") ? rating + 1 : rating
        }
        return rating
    }
    
    override func awakeFromNib() {
        starsView.forEach {
            $0.image = #imageLiteral(resourceName: "return empty star")
            $0.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapStar))
            $0.addGestureRecognizer(tapGesture)
        }
    }
    
    @objc func tapStar(_ gesture: UITapGestureRecognizer) {
        if !enableTap { return }
        if let view = gesture.view {
            self.setRate(view.tag)
        }
    }
    
    func setRate(_ value: Int, enabletap: Bool = false) {
        self.enableTap = enabletap
        if value < 6 {
            starsView.forEach {
                $0.image = $0.tag > value ? #imageLiteral(resourceName: "WhiteStar_ic") : #imageLiteral(resourceName: "return star")
            }
        }
    }
    
}
