//
//  CarImageCell.swift
//  XRentY
//
//  Created by user on 12/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit

class CarImageCell: UICollectionViewCell {
    @IBOutlet var carImageView: UIImageView!
}
