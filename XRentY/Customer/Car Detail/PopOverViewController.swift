//
//  PopOverViewController.swift
//  SM-Markets
//
//  Created by Administrator on 6/4/18.
//  Copyright © 2018 Navjot Sharma. All rights reserved.
//

import UIKit
typealias PopOverCompletionBlock = (_ message :String) ->()

class PopOverViewController: BaseViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var subjectTextField: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    var productId = String()
    typealias  Navigate = (UIViewController) -> ()
    var completionBlock:PopOverCompletionBlock?
    var userId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func didTapPickUpCar(_ sender: Any) {
        
        self.view.endEditing(true)
        if messageTextView.text?.isEmptyString() ?? true && subjectTextField.text?.isEmptyString() ?? true { return}
        guard let subject = subjectTextField.text,
            let message = messageTextView.text
        
        else {
                return
        }
        showLoader()
        
        //print(Authentication.deviceToken)
        let queryItems = ["subject":subject, "ask": message, "product_id":productId, "order_id":"","device_token" : Authentication.deviceToken ?? "" , "device_type" : "iphone"]
        
        WebService.sendMessage(queryItems: queryItems) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self.publishMessage(message)
                    guard let completionBlock = self.completionBlock else {return}
                    completionBlock(response.message ?? "")
                case .failure(let error):
                    guard let completionBlock = self.completionBlock else {return}
                    completionBlock(error.message )
                }
                self.dismiss(animated: false, completion: nil)
                
            }
        }
    }
    
    func publishMessage(_ description: String) {
        if let id = userId {
            if let message = Chat(description: description,location: nil, chat: nil, sending: true, userId: "\(Authentication.customerId!)").json {
                let dict = ["message_id": "",
                            "message": message] as [String : Any]
                PubNubManager.sharedInstance.sendMessage(to: id, message: dict)
            }
            
            Observer.refreshmessage()
        }
    }
}

