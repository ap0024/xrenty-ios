//
//  DescriptionCell.swift
//  XRentY
//
//  Created by user on 11/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit

class DescriptionCell: UITableViewCell {

    @IBOutlet var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
