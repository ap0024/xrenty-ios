//
//  NotificationObserver.swift
//  XRentY
//
//  Created by user on 03/11/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

class Observer {
    
    // Add to cart observer
    class func addUpdateCart<T: Any>(target: T, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name("addToCart"), object: nil)
    }
    
    class func updateCart() {
         NotificationCenter.default.post(name: Notification.Name("addToCart"), object: nil)
    }
    
    // Bookings observer
    class func addMyBookings<T: Any>(target: T, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name("myBookings"), object: nil)
    }
    
    class func updateMyBookings() {
        NotificationCenter.default.post(name: Notification.Name("myBookings"), object: nil)
    }
    
    // Add to cart observer
    class func connectWishlist<T: Any>(target: T, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name("wishlist"), object: nil)
    }
    
    class func updateWishlist() {
        NotificationCenter.default.post(name: Notification.Name("wishlist"), object: nil)
    }
    
    // Add car
    class func addcar<T: Any>(target: T, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name("addCar"), object: nil)
    }
    
    class func updateList() {
        NotificationCenter.default.post(name: Notification.Name("addCar"), object: nil)
    }
    
    // pop to search
    class func poptoSearch<T: Any>(target: T, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name("poptoSearch"), object: nil)
    }
    
    class func poptoSearch() {
        NotificationCenter.default.post(name: Notification.Name("poptoSearch"), object: nil)
    }
    
    // refresh message
    class func refreshmessage<T: Any>(target: T, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name("refreshmessage"), object: nil)
    }
    
    class func refreshmessage() {
        NotificationCenter.default.post(name: Notification.Name("refreshmessage"), object: nil)
    }
    
    // upload
    class func uploadDoc<T: Any>(target: T, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name("uploaddoc"), object: nil)
    }
    
    class func uploadDoc() {
        NotificationCenter.default.post(name: Notification.Name("uploaddoc"), object: nil)
    }
    
    // updateList
    class func upDateList<T: Any>(target: T, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name("updateList"), object: nil)
    }
    
    class func ListUpdate() {
        NotificationCenter.default.post(name: Notification.Name("updateList"), object: nil)
    }
    
    // playVideo
    class func playVideo<T: Any>(target: T, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name("playVideo"), object: nil)
    }
    
    class func playvideo() {
        NotificationCenter.default.post(name: Notification.Name("playVideo"), object: nil)
    }
    
    // reviewUpdate
    class func reviewUpdate<T: Any>(target: T, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name("reviewUpdate"), object: nil)
    }
    
    class func ListReview() {
        NotificationCenter.default.post(name: Notification.Name("reviewUpdate"), object: nil)
    }
}
