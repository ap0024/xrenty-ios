//
//  AttributesCell.swift
//  XRentY
//
//  Created by user on 11/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit

class AttributeCell: UITableViewCell {

    @IBOutlet var attributeImage: UIImageView!
    @IBOutlet var attributeValue: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
