//
//  CheckoutViewController.swift
//  XRentY
//
//  Created by user on 18/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit
import PaypalSDK
import Toast_Swift
import Alamofire

class CheckoutViewController: BaseViewController {
    
    @IBOutlet weak var ConstraintTableHeight: NSLayoutConstraint!
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var checkoutScroll: UIScrollView!
    @IBOutlet weak var ApplyFilterView: UIView!
    @IBOutlet weak var leftBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var lblOrderTotal: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var txtcode: UITextField!
    @IBOutlet weak var ViewEstimateTax: UIView!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var lblShowDepositePrice: UILabel!
    @IBOutlet weak var lblShowDiscountPrice: UILabel!
    @IBOutlet weak var lblShowProtectionFee: UILabel!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtZipCode: UITextField!
    @IBOutlet weak var lblApplyCode: UILabel!
    @IBOutlet weak var topConstraintsEstimateTax: NSLayoutConstraint!
    @IBOutlet weak var btnApplyBtn: UIButton!
    @IBOutlet weak var labelTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var viewtopDic: UIView!
    @IBOutlet weak var labelDis: UILabel!
    @IBOutlet weak var lblbotmDis: UIView!
    @IBOutlet weak var btnCheckout: UIButton!
   // @IBOutlet weak var btnPaypalCheckout: UIButton!
    @IBOutlet weak var viewDipositAmt: UIView!
    
    @IBOutlet weak var gold_btn: UIButton!
    @IBOutlet weak var platinum_btn: UIButton!
    
    @IBOutlet weak var lbl_gold: UILabel!
    @IBOutlet weak var lbl_platinum: UILabel!
    
    
    var selected_now = -1
    var total_order = 0.0
    var protection_fee_tbl = Float()
    
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    var selectedRegionId: String {
        var regionId = ""
        regions.forEach {
            if $0.name == txtState.text! {
                regionId = $0.region_id!
            }
        }
        return regionId
    }
    var selectedCountryId: String {
        var countryId = ""
        countries.forEach {
            if $0.name == txtCountry.text! {
                countryId = $0.iso2_code
            }
        }
        return countryId
    }
    
    var cancelPaypalCompletion: (()->())? = nil
    var countries: [Country] = []
    var regions: [Region] = []
    var showBackButton: Bool = false
    var discount : String = ""
    var is_appiled : Int = 0
    var coupentxt : String = ""
     var isPushed: Bool = false
    
    /** Create the UILabel */
    var theLabel: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.numberOfLines = 3
        label.font = UIFont(name: "Helvetica-Bold", size: 22)
        return label
    }()
    
    var cars : [Car] = [] {
        didSet {
            cartTableView.reloadData()
        }
    }
    
    var cart: Cart!

    var depositConfirmed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isPushed ? addBackNavigationItem() : addMenuNavigationItem()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.viewCart(.white)
        Observer.addUpdateCart(target: self, selector: #selector(updateCart))
        
    }
    
    @objc func updateCart() {
        self.viewWillAppear(true)
        self.selected_now = -1
        self.viewCart()
        
    }
    
    @IBAction func openEstimateTax(_ sender: Any) {
        
        if ViewEstimateTax.isHidden {
            
            topConstraintsEstimateTax.constant = 15
            ViewEstimateTax.isHidden = false
        }
        else
        {
            topConstraintsEstimateTax.constant = -280
            ViewEstimateTax.isHidden = true
        }
    }
    
    // estimate Tax
    func estimateTax() {
        
        let query = ["addressInformation": ["address" : ["countryId" : selectedCountryId,
                                                         "postcode" : txtZipCode.text!,
                                                         "region" : txtState.text!,
                                                         "regionId" : selectedRegionId]]]
        showLoader()
        WebService.estimateTax(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    self.topConstraintsEstimateTax.constant = -280
                    self.ViewEstimateTax.isHidden = true
                    self.updateCart()
                    AlertManager.showAlert(type: .custom(response.message!))
                case .failure(let error):
                    AlertManager.showAlert(type: .custom(error.message))
                }
            }
        }
    }
    
    @IBAction func applyEstimate(_ sender: Any) {
        estimateTax()
    }
    
     //MARK : get countries data
    func getCountries() {
        WebService.getCountries { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self.countries = response.objects ?? []
                case .failure(_):
                    break
                }
            }
        }
    }
    
    //MARK : get Region data
    func getRegion(_ code: String) {
        let query = ["country_code": code]
        self.txtState.isUserInteractionEnabled = false
        WebService.getRegion(queryItems: query) { result in
            DispatchQueue.main.async {
                self.txtState.isUserInteractionEnabled = true
                switch result {
                case .success(let response):
                    self.regions = response.objects ?? []
                case .failure(_):
                    break
                }
            }
        }
    }
    
    // show country picker
    func showCountryPicker() {
        self.view.endEditing(true)
        if self.countries.isEmpty { return }
        let picker = CustomPickerView()
        picker.list = self.countries.compactMap { return $0.name }
        picker.showPickerView(title: "Countries".localized, tag: 0)
        picker.didSelectRow = { index,_ in
            if self.txtCountry.text == self.countries[index].name { return }
            self.txtState.text = ""
            self.txtCountry.text = self.countries[index].name
            self.getRegion(self.countries[index].iso2_code)
        }
    }
    
    // textfield should begin editing
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCountry {
            self.showCountryPicker()
        }
        if textField == txtState {
            if !countries.isEmpty  {
                if !regions.isEmpty {
                    self.showRegionPicker()
                }
                return regions.isEmpty && !txtCountry.text!.isEmpty
            }
        }
        return !(textField == txtCountry || textField == txtState)
    }
    
    func showRegionPicker() {
        if self.countries.isEmpty { return }
        let picker = CustomPickerView()
        picker.list = self.regions.compactMap { return $0.name }
        picker.showPickerView(title: "State", tag: 0)
        picker.didSelectRow = { index,_ in
            self.txtState.text = self.regions[index].name
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // view cart
    func viewCart(_ loaderColor: UIColor = UIColor.black.withAlphaComponent(0.3)) {
        showLoader(color: loaderColor)
        WebService.viewCart { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    guard let cart = response.object, let cars = cart.cars  else {
                        self.checkoutScroll.isHidden = true
                        return
                    }
                    self.checkoutScroll.isHidden = cars.isEmpty
                    self.cars = cars
                    self.checkoutDetail(cart)
                    self.cart = cart
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    // view cart
    func clearCart() {

        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        WebService.ClearCart { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(_):
                    
                    self.cars.removeAll()
                    self.checkoutScroll.isHidden = true
                    self.cartTableView.reloadData()
                    
                    //Observer.poptoSearch()
                    Observer.updateCart()
                    self.navigationController?.popToRootViewController(animated: true)
                    self.navigationController?.viewControllers.first?.tabBarController?.selectedIndex = 0
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    func checkoutDetail(_ Cart: Cart?){
        
        guard Cart != nil else {
            print("----no data----"); return
        }
        
        let subTotal = Cart?.cars?.first?.subtotal ?? "0"
        let orderTotal = Cart?.cars?.first?.subtotal ?? "0"
        let deposite = Cart?.dipositAmount ?? ""
        let discount = Cart?.discount ?? ""
        let protectionFee = Cart?.extraFee ?? ""
        self.coupentxt = Cart?.couponApplied ?? ""
        
        let subTotal1 = Double(subTotal.roundOff()) ?? 0
        let subTotal2 = CGFloat(subTotal1).formattedWithSeparator
        
        let orderTotal1 = Double(orderTotal.roundOff()) ?? 0
        let orderTotal2 = CGFloat(orderTotal1).formattedWithSeparator
        
        let deposite1 = Double(deposite.roundOff()) ?? 0
        let deposite2 = CGFloat(deposite1).formattedWithSeparator
        
        let discount1 = Double(discount.roundOff()) ?? 0
        let discount2 = CGFloat(discount1).formattedWithSeparator
        
        let protectionFee1 = Double(protectionFee.roundOff()) ?? 0
        let protectionFee2 = CGFloat(protectionFee1).formattedWithSeparator
        
        self.total_order = orderTotal1
        self.lblSubTotal.text = symbol + "\(subTotal2)"
        self.lblOrderTotal.text = symbol + "\(orderTotal2)"
        self.lblShowDepositePrice.text = symbol + "0.0"
        self.lblShowDiscountPrice.text = symbol + "\(discount2)"
       // self.lblShowProtectionFee.text = symbol + "\(protectionFee2)"
        self.discount = Cart?.discount ?? ""
        self.is_appiled = Cart?.isdiscountBool ?? 0
        self.txtcode.text = self.coupentxt
        self.showdiscount()
        
        let gold_amount = Cart?.gold_amount ?? "0.0"
        let platinum_amount = Cart?.platinum_amount ?? "0.0"
        
        self.lbl_gold.text = symbol + gold_amount.trimmingCharacters(in: .whitespaces)
        self.lbl_platinum.text = symbol + platinum_amount.trimmingCharacters(in: .whitespaces)
        print("*******")
        print(symbol + platinum_amount.trimmingCharacters(in: .whitespaces))
        print("*******")
//        deposit_confirmed
        
        
        if Cart?.gold_amount != nil{
            
            var sel = Cart?.type!
            if sel == "" {
                sel = "gold"
            }
            
            selected_now = sel == "gold" ? 0:1
            switch selected_now {
            case 0:
                gold_btn.isSelected = true
                platinum_btn.isSelected = false
            case 1:
                gold_btn.isSelected = false
                platinum_btn.isSelected = true
            default:
                print("default")
            }
            update_cart_values(sender: selected_now, carttt: Cart)
            
            let depositConfir = Cart?.deposit_confirmed ?? 0
            self.depositConfirmed = depositConfir == 0 ? true : false

            self.gold_btn.isUserInteractionEnabled = depositConfirmed
            self.platinum_btn.isUserInteractionEnabled = depositConfirmed
        }
        
    }
    func update_cart_values(sender: Int, carttt : Cart?) {
        if carttt != nil{
            if sender == 0{
                
                var protection = 0.0
                protection = Double(Float(carttt!.gold_amount ?? "0.0")!)
                
                var deposit = 0.0
                deposit = Double(Float(carttt!.gold_amount_deposit ?? "0.0")!)
                
                self.protection_fee_tbl = Float(protection)
                
                var final = 0.0
                final = total_order + protection + deposit
                
                self.lblShowDepositePrice.text = symbol + "\(carttt!.gold_amount_deposit ?? "0.0")"
                let total_now = symbol + "\(Float(final))"
                self.lblOrderTotal.text = total_now
                self.cartTableView.reloadData()
            }else{
                var protection = 0.0
                protection = Double(Float(carttt!.platinum_amount ?? "0.0")!)
                
                var deposit = 0.0
                deposit = Double(Float(carttt!.platinum_amount_deposit ?? "0.0")!)
                
                self.protection_fee_tbl = Float(protection)
                
                var final = 0.0
                final = total_order + protection + deposit
                
                self.lblShowDepositePrice.text = symbol + "\(carttt!.platinum_amount_deposit ?? "0.0")"
                let total_now = symbol + "\(Float(final))"
                self.lblOrderTotal.text = total_now
                self.cartTableView.reloadData()
            }
        }else{
            if sender == 0{
                
                var protection = 0.0
                protection = Double(Float(cart.gold_amount ?? "0.0")!)
                
                var deposit = 0.0
                deposit = Double(Float(cart.gold_amount_deposit ?? "0.0")!)
                
                self.protection_fee_tbl = Float(protection)
                
                var final = 0.0
                final = total_order + protection + deposit
                
                self.lblShowDepositePrice.text = symbol + "\(cart.gold_amount_deposit ?? "0.0")"
                let total_now = symbol + "\(Float(final))"
                self.lblOrderTotal.text = total_now
                self.cartTableView.reloadData()
            }else{
                var protection = 0.0
                protection = Double(Float(cart.platinum_amount ?? "0.0")!)
                
                var deposit = 0.0
                deposit = Double(Float(cart.platinum_amount_deposit ?? "0.0")!)
                
                self.protection_fee_tbl = Float(protection)
                
                var final = 0.0
                final = total_order + protection + deposit
                
                self.lblShowDepositePrice.text = symbol + "\(cart.platinum_amount_deposit ?? "0.0")"
                let total_now = symbol + "\(Float(final))"
                self.lblOrderTotal.text = total_now
                self.cartTableView.reloadData()
            }
        }
       
    }
    
    
    @IBAction func platinum_btn(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        selected_now = sender.tag
        switch sender.tag {
        case 0:
            gold_btn.isSelected = true
            platinum_btn.isSelected = false
        case 1:
            gold_btn.isSelected = false
            platinum_btn.isSelected = true
        default:
            print("default")
        }
        update_cart_values(sender: sender.tag, carttt: nil)
    }
    
    func addMenuNavigationItem() {
        let mainView = UIView()
        mainView.frame = CGRect.init(x: 2, y: 0, width: 148, height: 40)
        let image = UIImage(named: "topbar_Logo_ic")
        let view = UIImageView(image: image)
        
        view.clipsToBounds = true
        mainView.addSubview(view)
        view.contentMode = .scaleAspectFit
        view.frame = CGRect(x: 2, y: 0, width: 148, height: 40)
        
        navigationItem.titleView = nil
        let barItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self, action: #selector(menuItems))
//        let emptyBarItem = UIBarButtonItem(image: nil, style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = barItem
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: mainView)
    }
    
    func addBackNavigationItem() {
        let mainView = UIView()
        mainView.frame = CGRect.init(x: -8, y: 0, width: 148, height: 40)
        let image = UIImage(named: "topbar_Logo_ic")
        let view = UIImageView(image: image)
        
        view.clipsToBounds = true
        mainView.addSubview(view)
        view.contentMode = .scaleAspectFit
        view.frame = CGRect(x: -8, y: 0, width: 148, height: 40)
        
        navigationItem.titleView = mainView
        let barItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(pop))
        navigationItem.leftBarButtonItem = barItem
    }
    
    @IBAction func continueShopping(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.viewControllers.first?.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func expandCouponView(_ sender: Any) {
        
        if ApplyFilterView.isHidden {
            ApplyFilterView.isHidden = false
            
        } else{
            ApplyFilterView.isHidden = true
        }
        cartTableView.reloadData()
        
        self.btnApplyBtn.setTitle(is_appiled == 1 ? "Remove coupon".localized : "Apply coupon".localized, for: .normal)
    }
    
    func showdiscount() {

        if is_appiled == 0
        {
            viewtopDic.isHidden = true
            labelDis.isHidden = true
//            lblbotmDis.isHidden = true
            lblShowDiscountPrice.isHidden = true
            labelTopConstraints.constant = -30
            txtcode.isEnabled = true
        }
        else
        {
           viewtopDic.isHidden = false
            labelDis.isHidden = false
            labelDis.text = "Discount".localized + " (" + coupentxt + ")"
//            lblbotmDis.isHidden = false
            lblShowDiscountPrice.isHidden = false
            labelTopConstraints.constant = 10
            txtcode.isEnabled = false
        }
    }
    
    @IBAction func applyDiscountCode(_ sender: Any) {
        
        if is_appiled == 0
        {
            if txtcode.text == ""
            {
                AlertManager.showAlert(on: self, type: .custom("please fill coupon code".localized))
                
            }
            else {
                is_appiled = is_appiled == 1 ? 0 : 1
                self.applyCoupon()
                ApplyFilterView.isHidden = is_appiled == 1 ? true : false
            }
        }
        else
        {
            is_appiled = is_appiled == 1 ? 0 : 1
            self.applyCoupon()
            ApplyFilterView.isHidden = is_appiled == 1 ? true : false
        }
    }
    
    // apply coupon
    func applyCoupon() {
        let query = ["coupon_code": txtcode.text ?? "",
                     "is_coupon" : is_appiled] as [String : Any]
        
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        WebService.applyCoupon(queryItems: query) { result in
            
            DispatchQueue.main.async {switch result {
            case .success(let response):
                self.hideLoader()
                self.ApplyFilterView.isHidden = true
                AlertManager.showAlert(type: .custom(response.message!), action: {
                    Observer.updateCart()
                })
                
            case .failure(let error):
                AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    @IBAction func navigate(_ sender: Any) {
        
        if showBackButton {
            NavigationHandler.pop()
        } else {
            //NavigationHandler.presentDrawer()
        }
    }
    
    @IBAction func clearShoppingCart(_ sender: Any) {
        clearCart()
    }
    
    @IBAction func updateShoppingCart(_ sender: Any) {
        self.viewCart()
    }
    
    @IBAction func proceedToCheckout(_ sender: Any) {
        
        if selected_now != -1{
            if cart.paymentStatus == 0 {
                
                let controller = UIAlertController.init(title: "Select Payment Method", message: nil, preferredStyle: .actionSheet)
                
                let paypal = UIAlertAction.init(title: "PayPal", style: .default) { (_) in
                    let controller = PaypalCheckoutViewController.initiatefromStoryboard(.orderSummary)
                    controller.order_type = self.selected_now == 0 ? APIConstants.orderSummary_gold : APIConstants.orderSummary_platinum
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                
                let ppimage = UIImage.init(named: "pp_logo")?.withRenderingMode(.alwaysOriginal)
                paypal.setValue(ppimage, forKey: "image")
                paypal.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                paypal.setValue(UIColor.black, forKey: "titleTextColor")
                
                let card = UIAlertAction.init(title: "Credit Card", style: .default) { (_) in
                    let vc = UIStoryboard(name: "OrderSummary", bundle: nil).instantiateViewController(withIdentifier: "OrderSummaryViewController") as! OrderSummaryViewController
                    
                    vc.order_type = self.selected_now == 0 ? APIConstants.orderSummary_gold : APIConstants.orderSummary_platinum
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                let ccimage = UIImage.init(named: "cc_logo")?.withRenderingMode(.alwaysOriginal)
                card.setValue(ccimage, forKey: "image")
                card.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                card.setValue(UIColor.black, forKey: "titleTextColor")
                
                let cancel = UIAlertAction.init(title: "Cancel", style: .cancel)
                controller.addAction(card)
                controller.addAction(paypal)
                controller.addAction(cancel)
                self.present(controller, animated: true, completion: nil)
            }
            else if cart.paymentMethod == "paypal" && cart.paymentStatus == 1 {
                let controller = PaypalCheckoutViewController.initiatefromStoryboard(.orderSummary)
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else if cart.paymentMethod == "openpay" && cart.paymentStatus == 1 {
                self.pushTo(.orderSummary)
            }
        }else{
            self.view.makeToast("please select protection type", duration: 1.5, position: .bottom)
//            self.view.
        }
        
       
    }
}

extension CheckoutViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckoutTableViewCell", for: indexPath) as? CheckoutTableViewCell
        
        cell?.tag = indexPath.row
        cell?.configure(cars[indexPath.row])
        cell?.lblprotectionFee.text = symbol + "\(protection_fee_tbl)"
        cell?.removeItem = { self.removeItem(self.cars[$0]) }
        return cell!
    }
    
    func removeItem(_ cars : Car) {
        // check if there is any deposit of current item in cart
        self.checkDepositStatus {
            let query = ["item_id":cars.itemId!,
                         "prod_id" : cars.prodId!] as [String:Any]
            self.showLoader(color: UIColor.black.withAlphaComponent(0.3))
            WebService.deletefromcart(queryItems: query) { result in
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch result {
                    case .success(_):
                        let carList = self.cars.filter {
                            return $0.itemId != cars.itemId
                        }
                       Observer.updateCart()
                        if carList.isEmpty {
                            Observer.poptoSearch()
                        }
                    case .failure(let error):
                        AlertManager.showAlert(type: .custom(error.message))
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        ConstraintTableHeight.constant = tableView.contentSize.height
    }
}

//MARK:- Refund Deposit
extension CheckoutViewController {
    
    // check cart status
    func checkDepositStatus(completion:@escaping ()->()) {
        
        if cart.paymentMethod!.contains("paypal") && cart.paymentStatus == 1 && cart.isOrderFromWeb {
            showLoader(color: UIColor.black.withAlphaComponent(0.3))
            PaypalSDK.shared.delegate = self
            PaypalSDK.shared.cancelV1(transactionId: cart!.paypal_deposit_order_id!)
            self.cancelPaypalCompletion = completion
        }
        else if cart.paymentMethod!.contains("paypal") && cart.paymentStatus == 1 {
            self.cancelPaypalAuthorization(completion: completion)
        }
        else {
            self.refundOpenPayPayment(completion: completion)
        }
    }
    
    //MARK: Paypal Authorization Cancelation
    func cancelPaypalAuthorization(completion:@escaping ()->()) {
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        PaypalSDK.shared.delegate = self
        PaypalSDK.shared.orderDetail(for: cart!.paypal_deposit_order_id!)
        self.cancelPaypalCompletion = completion
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    //MARK: Openpay Refund
    func refundOpenPayPayment(completion:@escaping ()->()) {
        showLoader(color: UIColor.black.withAlphaComponent(0.3))

        
        let accessToken = Authentication.token
        var httpfiels = ["authorization":"Bearer \(accessToken!)",
                "content-type":"application/json",
                "cache-control":"no-cache"]
        
        Alamofire.request("\(APIConstants.pathLive)getCartStatusvi", method: .get, parameters: nil, headers: httpfiels).responseString { (resp) in
            switch resp.result{
            case .success(_):
                var respo = self.convertToDictionary(text: resp.result.value as! String)
                //
                let dict = respo!["data"] as! NSDictionary
                let depositConfirmed = "\(dict["deposit_confirmed"]!)"
                if Int(depositConfirmed) == 1 {
                    var refund_amount = "0"
                    refund_amount = "\(dict["customer_deposit_amount"]!)"
                    
                    self.showRefundAlert {
                        self.refundAmount(refund_amount, transactionId: "\(dict["openpay_transaction_id"]!)", completion: {
                            self.hideLoader()
                            completion()
                            self.releaseDeposit()
                            self.showToast(message: "Your amount is successfully refunded".localized)
                        })
                    }
                } else {
                    
                    completion()
                }
            case .failure(_):
                completion()
            }
        }
        
//        response { (response) in
//
//
//        }
        
        
        
//        WebService.cartStatus { result in
//            DispatchQueue.main.async {
//                self.hideLoader()
//                switch result {
//                case .success(let response):
//
////                    let getCartStatusvi = try? JSONDecoder().decode(GetCartStatusvi.self, from: jsonData)
//
//                    guard let depositConfirmed = response.object?.depositConfirmed else {
//                        self.showToast(message: "Server error\nPlease try again later.".localized)
//                        return
//                    }
//
//                case .failure(let error):
//                    self.showToast(message: error.message)
//                }
//            }
//        }
    }
    
    // release deposit
    func releaseDeposit() {
        WebService.releaseDeposit { (result) in
            switch result {
            case .success:
                break
            case .failure:
                break
            }
        }
    }
    
    // refund alert
    func showRefundAlert(completion:@escaping ()->()) {
        let alert = UIAlertController(title: "Refund Deposit".localized, message: "Your debited deposit will be refunded. Please wait for sometime".localized, preferredStyle: .alert)
        let action = UIAlertAction(title: title ?? "OK".localized, style: .default) { (_) in
            completion()
        }
        alert.addAction(action)
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else { return }
        guard let controller = delegate.window?.rootViewController else { return }
        controller.present(alert, animated: true, completion: nil)
    }
    
    // refund
    func refundAmount(_ amount: String, transactionId: String, completion: @escaping ()->()) {
        OpenpayPayment.refundAmount(amount: amount, customerId: Authentication.openpayCustomerId!, transactionId: transactionId) { (json, message) in
            DispatchQueue.main.async {
                guard json != nil else {
                    AlertManager.showAlert(type: .custom(message))
                    return
                }
                completion()
            }
        }
    }
}

extension CheckoutViewController: PaymentDelegate {
    
    func didUpdateOrder(with state: PaymentState) {
        switch state {
        case .cancel:
            print("order canceled")
            self.cancelPaypalCompletion?()
        case .detail(let order):
            print("order detail id order_id: \(order.id)")
            if let authId = ((order.purchase_units?[0])?.payments?.authorizations?[0])?.id {
                PaypalSDK.shared.cancel(authorizationId: authId)
            } else {
                AlertManager.showAlert(type: .custom("invalid authorization id"))
            }
        default:
            break
        }
    }
    
    func didFailUpdateOrder(with error: PaymentError) {
        hideLoader()
        AlertManager.showAlert(type: .custom(error.errorDescription))
    }
}
