//
//  CheckoutTableViewCell.swift
//  XRentY
//
//  Created by user on 18/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit

class CheckoutTableViewCell: UITableViewCell {

    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCheckIn: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblNoOfDays: UILabel!
    @IBOutlet weak var lblprotectionFee: UILabel!
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblCheckout: UILabel!
    @IBOutlet weak var btnRemoveItem: UIButton!
    
    var removeItem : ((Int)->())? = nil
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    func configure(_ Car: Car) {
        lblName.text = Car.name
        lblCheckIn.text = Car.checkin
        lblCheckout.text = Car.checkout
        lblNoOfDays.text = Car.totalDays
        
        let carprice = Car.price ?? ""
        let protectFee = Car.protectionFee ?? ""
        let subtotal = Car.subtotal ?? ""
        
        print(subtotal)
        
        let carprice1 = Double(carprice.roundOff()) ?? 0
        let carprice2 = CGFloat(carprice1).formattedWithSeparator
        
        let subtotal1 = Double(subtotal.roundOff()) ?? 0
        let subtotal2 = CGFloat(subtotal1).formattedWithSeparator
        
        let protectionFee1 = Double(protectFee.roundOff()) ?? 0
        let protectionFee2 = CGFloat(protectionFee1).formattedWithSeparator
        
        lblPrice.text = symbol + "\(carprice2)"
        lblprotectionFee.text = symbol + "\(protectionFee2)"
        lblSubtotal.text = symbol + "\(subtotal2)"
        imgCar.kf.indicatorType = .activity
        let image = Car.imageURL
        imgCar.kf.setImage(with: image  , placeholder: #imageLiteral(resourceName: "car_placeholder_images"))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func RemoveItem(_ sender: Any) {
        if let handler = removeItem {
            handler(tag)
        }
    }
}
