//
//  RegisterFieldError.swift
//  XRentY
//
//  Created by user on 05/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.


import Foundation

extension CustomerProfileViewController {

// return required imageString for paramater to send
var imageString: String?
        {
            if imgProfilePhoto.image == #imageLiteral(resourceName: "user_ic") || imgProfilePhoto.image == nil { return nil }
            var imageData : Data?
            var compressedImage : UIImage?
            let max = 2097152
            imageData = imgProfilePhoto.image!.pngData(     )
            if (imageData?.count)! > max {
                imageData = imgProfilePhoto.image!.jpegData(compressionQuality: 0.5)
                compressedImage = UIImage(data: imageData!)
            }
            else {
                compressedImage = imgProfilePhoto.image
            }
            let (base64,format) = compressedImage!.convertImageTobase64()
            let imageString = "data:image/\(format.rawValue);base64," + base64!
            return imageString
    }

// return 0 for invalid form and 1 form valid form details
var isValidForm: Bool {
    guard let error = errorDescription else {
        return true
    }

    AlertManager.showAlert(on: self, type: .custom(error))
    return false
}

// parameters to be used for register user api
var parameters: [String:Any] {
    
    var params: [String:Any] = [:]
    if let firstName = self.txtFirstName.text?.trimmingCharacters(in:
        CharacterSet.whitespacesAndNewlines) {
        params["firstname"] = firstName
    }
    if let lastName = self.txtLastName.text?.trimmingCharacters(in:
        CharacterSet.whitespacesAndNewlines) {
        params["lastname"] = lastName
    }
    if let email = self.txtEmail.text?.trimmingCharacters(in:
        CharacterSet.whitespacesAndNewlines) {
        params["email"] = email
    }
    if let city = self.txtCity.text?.trimmingCharacters(in:
        CharacterSet.whitespacesAndNewlines) {
        params["city"] = city
    }
    if let country = countryCode {
        params["country"] = country
    }
    if let state = self.txtState.text?.trimmingCharacters(in:
        CharacterSet.whitespacesAndNewlines) {
        params["state"] = state
    }
    if let pincode = self.txtPincode.text {
        params["pincode"] = pincode
    }
    if let telephone = self.txtPhone.text {
        params["telephone"] = telephone
    }
    if let currency = self.txtCurrency.text {
        params["customer_currencies"] = currency
    }
    
    
    params["is_seller"] = 0
    params["company"] = ""
    params["account_number"] = ""
    params["account_holder_name"] = ""
    params["bank"] = ""

    return params
}

// register error
enum ProfileError: String {
    
    case empty = "Please enter your details"
    case firtsname = "First name should be of atleast 3 characters"
    case lastname = "Last name should be of atleast 3 characters"
    case email = "Please enter a valid email address. For example customer@xrenty.com."
    case city
    case state
    case pincode
    case phonenumber
    case image
    case licenseDate
    case licenseNumber
    case licenseDoc
    
    var emptydescription: String {
        switch self {
        case .firtsname:
            return "Please enter your first name".localized
        case .lastname:
            return "Please enter your last name".localized
        case .email:
            return "Please enter your email address".localized
        case .city:
            return "Please enter your city".localized
        case .state:
            return "Please enter your state".localized
        case .pincode:
            return "Please enter your pin code".localized
        case .phonenumber:
            return "Please enter your phone number".localized
        case .image:
            return "Please upload your image".localized
        case .empty:
            return "Please enter your details".localized
        case .licenseDate:
            return "Please enter your license Date".localized
        case .licenseNumber:
            return "Please enter your license number".localized
        case .licenseDoc:
            return "Please upload your license doc file".localized
        }
    }
}

// return error description to show
var errorDescription: String? {
    return txtFirstName.text!.empty && txtLastName.text!.empty && txtEmail.text!.empty && txtCity.text!.empty && txtState.text!.empty && (txtCountry.text?.empty)! && txtPincode.text!.empty && (txtPhone.text?.empty)! ? ProfileError.empty.rawValue.localized :
        txtFirstName.text!.empty ? ProfileError.firtsname.emptydescription :
        txtFirstName.text!.count < 3 ? ProfileError.firtsname.rawValue.localized :
        txtLastName.text!.empty ? ProfileError.lastname.emptydescription :
        txtLastName.text!.count < 3 ? ProfileError.lastname.rawValue.localized :
        txtEmail.text!.empty ? ProfileError.email.emptydescription :
        !txtEmail.text!.isValidEmail() ? ProfileError.email.rawValue.localized :
       txtCity.text!.empty ? ProfileError.city.emptydescription :
      txtState.text!.empty ? ProfileError.state.emptydescription :
    txtPincode.text!.empty ? ProfileError.pincode.emptydescription :
    txtPhone.text!.empty ? ProfileError.phonenumber.emptydescription :
    txtDriverLicenseNumber.text!.empty ? ProfileError.licenseNumber.emptydescription :
    licenseExpiryDate.text!.empty ? ProfileError.licenseDate.emptydescription :
      licenseDoc.isEmpty ? ProfileError.licenseDoc.emptydescription :
     (imgProfilePhoto.image == nil) ? ProfileError.image.emptydescription :nil
}
}
