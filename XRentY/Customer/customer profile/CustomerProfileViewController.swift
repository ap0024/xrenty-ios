//
//  CustomerProfileViewController.swift
//  XRentY
//
//  Created by user on 24/09/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
import MobileCoreServices
import iCloudDocumentSync
import SwiftyDropbox
import GoogleAPIClientForREST
import GoogleSignIn

enum uploadDoc: String {
    case registration = "registration"
}

class CustomerProfileViewController: BaseViewController {
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var imgProfilePhoto: UIImageView!
    @IBOutlet weak var txtPincode: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var saveProfile: UIButton!
    @IBOutlet weak var imageBtn: UIButton!
    @IBOutlet weak var uploadfileName: UILabel!
    @IBOutlet weak var txtDriverLicenseNumber: UITextField!
    @IBOutlet weak var licenseExpiryDate: UITextField!
    @IBOutlet weak var docUploadBtn: UIButton!
    @IBOutlet weak var txtCurrency: UITextField!
    
    
    
    //heading
    @IBOutlet weak var lblHeading: UIButton!
    @IBOutlet weak var headingFName: UILabel!
    
    @IBOutlet weak var headingcountry: UILabel!
    @IBOutlet weak var headingcity: UILabel!
    @IBOutlet weak var headingstate: UILabel!
    @IBOutlet weak var headingzipcode: UILabel!
    @IBOutlet weak var headingPnumber: UILabel!
    @IBOutlet weak var headinglicense: UILabel!
    @IBOutlet weak var headinglicensedate: UILabel!
    @IBOutlet weak var headinglicensedoc: UILabel!
    
    @IBOutlet weak var headingLstName: UILabel!
    @IBOutlet weak var headingEmail: UILabel!
    
    var userImage :  String = ""
    var name : String = ""
    var countries: [Country] = []
    var currency: [String] = []
    let dateBirthPicker = UIDatePicker()
    let dateCardPicker = UIDatePicker()
    var isEdit = false
    var isEmailEdit : String = ""
    let licensePicker = UIDatePicker()
    var licenseDoc : String = ""
    
    
    lazy var picker: UIImagePicker = {
        let imagePicker = UIImagePicker()
        imagePicker.pickerHandler = { self.addProfilePhoto($0, $1)}
        return imagePicker
    }()
    
    var reachability1 : Reachability {
        let reachability = Reachability()!
        return reachability
    }
    
    
    let scopes = [kGTLRAuthScopeDriveReadonly]
    let service = GTLRDriveService()
    var listArray = [String]()
    var googledata : Data? = nil
    var filetype : String = ""
    var csvList = [GTLRDrive_File]()
    var selectedIndex = Int()
    var imageType : uploadDoc = .registration
    
    var imagesData = [String: Any]()
    
    
    var filePath : String = ""
    var product_id : String = ""
    var minetype : String = ""
    var fileName : String = ""
    var docName : String = ""
    var fileUrl : URL? = nil
    let datePicker = UIDatePicker()
    
    lazy var docPicker: UIImagePicker = {
        let imagePicker = UIImagePicker()
        imagePicker.pickerHandler = {self.addImages($0, $1)}
        return imagePicker
    }()
    
    
    // return corresponding country code for selected country
    var countryCode: String? {
        var code : String?
        countries.forEach {
            if $0.name == txtCountry.text! { code = $0.country_id }
        }
        return code
    }
    
    var layoutSubviews: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLabel()
        self.txtEmail.isUserInteractionEnabled = false
        self.currency = ["USD","MXN"]
        userEnabled(isEdit: false)
        self.profile()
        self.licenseDatePicker()
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.dbDelegate = self
        
        Observer.uploadDoc(target: self, selector: #selector(uploadlicense))
    }
    
    func setLabel() {
        
        headingFName.setLocalizedAshtrik()
        headingLstName.setLocalizedAshtrik()
        headingEmail.setLocalizedAshtrik()
        headingcountry.setLocalizedAshtrik()
        headingcity.setLocalizedAshtrik()
        headingstate.setLocalizedAshtrik()
        headingzipcode.setLocalizedAshtrik()
        headingPnumber.setLocalizedAshtrik()
        headinglicense.setLocalizedAshtrik()
        headinglicensedate.setLocalizedAshtrik()
        headinglicensedoc.setLocalizedAshtrik()
    }
    
    @objc func uploadlicense() {
        if !(googledata == nil){
            
            if let data =  googledata {
                self.myImageUploadRequest(data, isData: true, name: "")
                
            }
        }
    }
    
    // enable profile button
    func enableRegister(_ show: Bool) {
        saveProfile.alpha = show ? 1 : 0.6
        saveProfile.isUserInteractionEnabled = show
    }
    
    func addProfilePhoto(_ image: UIImage, _ name : String) {
        self.imgProfilePhoto.image = image
        self.name = name
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        layoutSubviews = true
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !layoutSubviews {
            imgProfilePhoto.addShadow()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openCamera(_ sender: Any) {
        
        self.picker.showSheet(false)
    }
    
    @IBAction func browesId(_ sender: Any) {
        
    }
    
    @IBAction func saveProfile(_ sender: Any) {
        if isEdit {
            if isValidForm {
                editProfileApi()
            }
        }
    }
    
    @IBAction func uploadDoc(_ sender: Any) {
        self.view.endEditing(true)
        if reachability1.isReachable {
            imageType = .registration
            self.docPicker.showSheet(true) { self.openFileType($0) }
        }
        else
        {
            
            self.showToast(message: "No Internet Connection".localized)
            self.hideLoader()
        }
    }
    
    
    @IBAction func editProfile(_ sender: Any) {
        isEdit = true
        userEnabled(isEdit: true)
        if isEdit {
            txtFirstName.becomeFirstResponder()
        }
    }
    
    func userEnabled(isEdit:Bool) {
        self.txtFirstName.resignFirstResponder()
        self.enableRegister(isEdit)
        let txtFields = [txtFirstName,txtEmail,txtLastName,txtCity,txtState,txtCountry,txtPincode,txtPhone,txtEmail,txtDriverLicenseNumber,licenseExpiryDate,txtCurrency]
        for textField in txtFields {
            textField?.isUserInteractionEnabled = isEdit
        }
        imgProfilePhoto.isUserInteractionEnabled = isEdit
        imageBtn.isUserInteractionEnabled = isEdit
        docUploadBtn.isUserInteractionEnabled = isEdit
        
        if isEdit {
            if self.isEmailEdit.contains("1")
            {
                self.txtEmail.isUserInteractionEnabled = true
            } else
            {
                self.txtEmail.isUserInteractionEnabled = false
            }
        }
    }
    
    func profile () {
        let query = ["is_seller": "0"]
        print(query)
        let grayColor = UIColor(red: 230, green: 230, blue: 241, alpha: 1)
        showLoader(color: grayColor)
        WebService.profile(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    self.getCountries()
                    let profile = response.object
                    DispatchQueue.main.async {
                        self.txtCity.text = profile?.city ?? "N/A"
                        self.txtCountry.text = profile?.countryName ?? "N/A"
                        self.txtEmail.text = profile?.email ?? "N/A"
                        self.txtFirstName.text = profile?.firstname ?? "N/A"
                        self.imgProfilePhoto.kf.indicatorType = .activity
                        self.userImage = profile?.image ?? ""
                        let targetURL = URL(string: profile?.image ?? "")
                        self.imgProfilePhoto.kf.setImage(with: targetURL , placeholder: #imageLiteral(resourceName: "user_ic"))
                        self.txtLastName.text = profile?.lastname ?? "N/A"
                        self.txtPincode.text = profile?.pincode ?? "N/A"
                        self.txtState.text = profile?.state ?? "N/A"
                        self.isEmailEdit = profile?.iseditable ?? ""
                        self.txtDriverLicenseNumber.text = profile?.licenseNumber ?? ""
                        self.txtCurrency.text = profile?.current_currency ?? ""
                        
                        print(profile?.current_currency ?? "")
                        let currency = profile?.current_currency ?? ""
                        Authentication.currency = currency
                        let language = NSLocale.current.languageCode
                        Authentication.currency = currency
                        
                        if currency == "USD"
                        {
                            Authentication.currencySymbol = "US$"
                        }
                        else if currency == "MXN"
                        {
                            Authentication.currencySymbol = "MX$"
                        }
                        else if currency == ""
                        {
                            if language == "en"
                            {
                                Authentication.currencySymbol = "US$"
                                Authentication.currency = "USD"
                            }
                            else if language == "es"
                            {
                                Authentication.currencySymbol = "MX$"
                                Authentication.currency  = "MXN"
                            }
                            else
                            {
                                Authentication.currencySymbol = "US$"
                                Authentication.currency  = "USD"
                            }
                        }
                        
                        if !(profile?.licencedate == "")
                        {
                            let date = self.profileconvertDateFormater(profile?.licencedate ?? "")
                            self.licenseExpiryDate.text = date
                        }
                        
                        let title = profile?.licenceFile
                        if title == "" {
                            self.uploadfileName.text = "Upload (JPG,PDF,PNG)".localized
                        }
                        else {
                            self.uploadfileName.text = (title! as NSString).lastPathComponent
                            self.licenseDoc = profile?.licenceFile ?? ""
                        }
                        let phone = profile?.telephone ?? "N/A"
                        if phone.isEqualToString(find: "null")
                        {
                            self.txtPhone.text = ""
                        }else
                        {
                            self.txtPhone.text = phone
                        }
                        Authentication.customerName = (profile?.firstname)! + " " + (profile?.lastname)!
                        Authentication.customerImage = profile?.image ?? ""
                        self.hideLoader()
                    }
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    func editProfileApi () {
        
        print(licenseExpiryDate.text ?? "")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from: licenseExpiryDate.text ?? "")
        dateFormatter.dateFormat = "MM-dd-YYYY"
        let show = dateFormatter.string(from: date!)
        print(show)
        
        var query = parameters
        query["image"] = imageString ?? ""
        query["is_editable"] = isEmailEdit
        query["licence_file"] = licenseDoc
        query["licencedate"] = show
        query["license_number"] = txtDriverLicenseNumber.text ?? ""
        showLoader(color: .clear)
        WebService.editProfile(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    self.userEnabled(isEdit: false)
                    self.hideLoader()
                    AlertManager.showAlert(type: .custom("Profile is updated successfully".localized), action: {
                        self.profile()
                        Authentication.LicenceDetail = true
                        Observer.updateCart()
                        Observer.updateMyBookings()
                        Observer.poptoSearch()
                        Observer.updateWishlist()
                    })
                case .failure(let error):
                    self.userEnabled(isEdit: true)
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    func profileconvertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yy-MM-dd"
        if let date = dateFormatter.date(from: date) {
            dateFormatter.dateFormat = "dd-MM-YYYY"
            return  dateFormatter.string(from: date)
        }
        else {
            return "N/A"
        }
    }
    
    func licenseDatePicker() {
        licensePicker.datePickerMode = .date
        licensePicker.backgroundColor = .white
        licensePicker.minimumDate = Date()
        licensePicker.addTarget(self, action: #selector(changelicensePickerValue), for: .valueChanged)
        let toolBar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: view.bounds.width, height: 44))
        toolBar.isTranslucent = false
        toolBar.barTintColor = Color.barTintColor
        
        let barbutton = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(selectLicense))
        barbutton.tintColor = .white
        let flexSpace = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.items = [flexSpace,barbutton]
        licenseExpiryDate.inputView = licensePicker
        licenseExpiryDate.inputAccessoryView = toolBar
    }
    
    @objc func changelicensePickerValue(_ sender: UIDatePicker) {
        let formatter = DateFormatter.init()
        formatter.dateFormat = "dd-MM-yyyy"
        let date = formatter.string(from: sender.date)
        licenseExpiryDate.text = date
    }
    
    @objc func selectLicense(_ sender: UIBarButtonItem) {
        licenseExpiryDate.resignFirstResponder()
    }
    
    // get countries data
    func getCountries() {
        WebService.getCountries { result in
            switch result {
            case .success(let response):
                self.countries = response.objects ?? []
            case .failure(_):
                break
            }
        }
    }
    
    // show country picker
    func showCountryPicker() {
        self.view.endEditing(true)
        if self.countries.isEmpty { return }
        let picker = CustomPickerView()
        picker.list = self.countries.compactMap { return $0.name }
        picker.showPickerView(title: "Countries".localized, tag: 0)
        picker.didSelectRow = { index,_ in  self.txtCountry.text = self.countries[index].name }
    }
    
    // show country picker
    func showCurrencyPicker() {
        self.view.endEditing(true)
        if self.currency.isEmpty { return }
        let picker = CustomPickerView()
        picker.list = self.currency.compactMap { return $0 }
        picker.showPickerView(title: "Currency".localized, tag: 0)
        picker.didSelectRow = { index,_ in  self.txtCurrency.text = self.currency[index]}
    }
    
    // textfield should begin editing
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtCountry {
            self.showCountryPicker()
        }
        else if textField == txtCurrency {
            self.showCurrencyPicker()
        }
        return !(textField == txtCountry || textField == txtCurrency)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtState
        {
            if txtCountry.text == ""
            {
                AlertManager.showAlert(type: .custom("Please select country.".localized))
            }
        }
        else if textField == txtCity
        {
            if txtCountry.text == ""
            {
                AlertManager.showAlert(type: .custom("Please select country.".localized))
            }
            else if txtState.text == ""
            {
                AlertManager.showAlert(type: .custom("Please select country.".localized))
            }
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    func openFileType(_ type: UploadFile) {
        
        switch type {
        case .file:
            self.openiCloudDrive()
        case .dropBox:
            self.dropboxSignIn(self)
            self.selectedIndex = 0
        case .googleDrive:
            self.googleSignIn()
            self.selectedIndex = 1
        }
    }
}

extension CustomerProfileViewController {
    
    
    func addImages(_ image: Any, _ name : String) {
        switch imageType {
        case .registration:
            self.myImageUploadRequest(image, isData: false, name: name)
        }
    }
    
    func myImageUploadRequest(_ image : Any , isData : Bool , name : String)
    {
        
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        
        let myUrl = NSURL(string: APIConstants.pathLive+APIConstants.uploadproductimages.rawValue);
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var imageData : Data?
        
        if isData
        {
            if self.filetype.contains("pdf")
            {
                imageData = googledata ?? image as! Data
                minetype = "application/pdf"
                fileName =  self.docName + "." + self.filetype
            }
            else if self.filetype.contains("docx")
            {
                imageData = googledata ?? image as! Data
                minetype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("doc")
            {
                imageData = googledata ?? image as! Data
                minetype = "application/msword"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("csv")
            {
                imageData = googledata ?? image as! Data
                minetype = "text/csv"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("ppt")
            {
                imageData = googledata ?? image as! Data
                minetype = "application/vnd.ms-powerpoint"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("png")
            {
                imageData = googledata ?? image as! Data
                minetype = "image/png"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("PNG")
            {
                imageData = googledata ?? image as! Data
                minetype = "image/png"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("jpeg")
            {
                imageData = googledata ?? image as! Data
                minetype = "image/jpeg"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("jpg")
            {
                imageData = googledata ?? image as! Data
                minetype = "image/jpeg"
                fileName = self.docName + "." + self.filetype
            }
            else if self.filetype.contains("JPG")
            {
                imageData = googledata ?? image as! Data
                minetype = "image/jpeg"
                fileName = self.docName + "." + self.filetype
            }
            else{
                hideLoader()
                
                self.showToast(message: "This file format is not supported . Please use this file format  jpg,jpeg,png,csv,doc,docx,pdf.".localized)
            }
        }
        else
        {
            if let imageType = image as? UIImage {
                
                let max = 2097152
                imageData = imageType.pngData()
                if (imageData?.count)! > max {
                    imageData = imageType.jpegData(compressionQuality: 0.5)
                }
                
                print(imageData?.count as Any)
                minetype = "image/jpeg"
                fileName = name
                docName = name.fileName()
                filetype = name.fileExtension()
                
            } else {
                do {
                    imageData =  try Data.init(contentsOf: image as! URL)
                    
                    minetype = "image/jpeg"
                    fileName = name
                    docName = name.fileName()
                    filetype = name.fileExtension()
                }
                catch { }
            }
        }
        
        switch self.imageType {
            
        case .registration:
            self.filePath = "licence_file"
        }
        
        if(imageData==nil)  { return; }
        
        if reachability1.isReachable {
            
            request.allHTTPHeaderFields = ["Authorization": "Bearer \(Authentication.token ?? "")"]
            request.httpBody = createBodyWithParameters( parameters: [:], filePathKey: self.filePath, imageDataKey: imageData! as NSData, boundary: boundary) as Data
            
            //            print(request.httpBody)
        }
        else
        {
            
            self.showToast(message: "No Internet Connection".localized)
            self.hideLoader()
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.hideLoader()
                    //                    print("error=\(error)")
                    AlertManager.showAlert(type: .custom("Connection error".localized))
                }
                return
            }
            
            // You can print out response object
            //            print("******* response = \(response)")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            //            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                
                //                print(json!)
                
                DispatchQueue.main.async {
                    if let response = json!["data"] as? [String:Any]
                    {
                        let imagepath = response["image_url"] as? String
                        //                        print(imagepath ?? "")
                        let imgpath = response["img_url"] as? String
                        //                        print(imgpath ?? "")
                        self.licenseDoc = imagepath ?? ""
                        
                        switch self.imageType {
                        case .registration:
                            
                            self.imagesData[self.imageType.rawValue] = imagepath
                            let tittle = "\(self.docName + "." + self.filetype)"
                            
                            AlertManager.showAlert(type: .custom(tittle + " " + "is uploaded successfully".localized))
                            self.hideLoader()
                            self.uploadfileName.text = tittle
                        }
                        
                    }
                    
                }
                
            }catch
            {
                print(error)
            }
        }
        task.resume()
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(fileName)\"\r\n")
        body.appendString(string: "Content-Type: \(minetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
        
    }
}





extension CustomerProfileViewController {
    
    func openiCloudDrive() {
        self.setupiCloud()
        let cloudIsAvailable: Bool = iCloud.shared().checkAvailability()
        if cloudIsAvailable {
            let picker = UIDocumentPickerViewController(documentTypes:["public.content"], in: UIDocumentPickerMode.import)
            picker.delegate = self
            present(picker, animated: true, completion: nil)
            
        } else {
            
            self.showToast(message: "Sorry! Your ICloud Drive is Turn OFF. Download .pdf File from ICloud Drive, you need to turn it ON".localized)
        }
    }
    
    func dropboxSignIn(_ controller: UIViewController) {
        if (DropboxClientsManager.authorizedClient == nil) {
            DropboxClientsManager.authorizeFromController(UIApplication.shared,
                                                          controller: self,
                                                          openURL: { (url: URL) -> Void in
                                                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            })
        } else {
            dropboxListFiles()
        }
    }
    
    //intialize iCloud
    func setupiCloud() {
        
        iCloud.shared().delegate = self
        iCloud.shared().verboseLogging = true
        iCloud.shared().setupiCloudDocumentSync(withUbiquityContainer: nil)
    }
    
    func googleSignIn() {
        
        if let user =  GIDSignIn.sharedInstance()?.currentUser {
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            self.googledriveListFiles()
        }
        else {
            GIDSignIn.sharedInstance().clientID = Constants.kGOOGLE_SIGNIN_CLIENT_KEY
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().scopes = scopes
            GIDSignIn.sharedInstance().signIn()
        }
    }
    
    //MARK:- Google Drive Api
    func googledriveListFiles() {
        
        self.showLoader(color: UIColor.black.withAlphaComponent(0.3))
        let root = "(mimeType = 'application/vnd.google-apps.folder' or  mimeType = 'application/pdf ' or mimeType = 'image/jpeg' or mimeType = 'image/png' or mimeType = 'text/csv'  or mimeType = 'application/msword' or mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' or mimeType = 'application/pdf')"
        
        let query = GTLRDriveQuery_FilesList.query()
        query.pageSize = 100
        query.q = root
        query.fields = "files(id,name,mimeType,modifiedTime,createdTime,fileExtension,size,parents,kind),nextPageToken"
        service.executeQuery(query,
                             delegate: self,
                             didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:))
        )
    }
    
    // Process the response and display list of files in TableView
    @objc func displayResultWithTicket(ticket: GTLRServiceTicket,
                                       finishedWithObject result : GTLRDrive_FileList,
                                       error : NSError?) {
        self.hideLoader()
        if let error = error {
            AlertManager.showAlert(type: .custom(error.localizedDescription))
        }
        else if let files = result.files, !files.isEmpty {
            self.pushToListing(.google(files,service))
        }
    }
    
    func pushToListing(_ document: Document) {
        if let vc = UIStoryboard(name: "Seller", bundle: nil).instantiateViewController(withIdentifier: "DocumentViewController") as? DocumentViewController {
            vc.showFiles(document, dataHandler: { (data,etension,name) in
                if data != nil {
                    self.googledata = data
                    self.filetype = etension
                    self.docName = name
                    for controller in self.navigationController!.viewControllers {
                        if controller.isKind(of: CustomerProfileViewController.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                        }
                    }
                } else {
                    self.showToast(message: "The file type is not valid".localized)
                }
            })
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- DropBox Api
    func dropboxListFiles() {
        showLoader(color: UIColor.black.withAlphaComponent(0.3))
        DropboxClientsManager.authorizedClient?.files.listFolder(path:"").response { response, error in
            DispatchQueue.main.async {
                self.hideLoader()
                if let result = response {
                    if !result.entries.isEmpty  {
                        self.pushToListing(.dropBox(result.entries))
                    }
                    else {
                        
                        self.showToast(message: "No file found".localized)
                    }
                } else {
                    AlertManager.showAlert(type: .custom("authentication error".localized))
                    self.hideLoader()
                }
            }
        }
    }
}

extension CustomerProfileViewController: UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        if controller.documentPickerMode == UIDocumentPickerMode.import {
            
            self.fileName = url.lastPathComponent
            self.filetype = self.fileName.fileExtension()
            
            if self.filetype == "jpg" {
                self.filetype = url.pathExtension
                minetype = "image/jpeg"
                self.addImages(url, self.fileName)
            }
            else if self.filetype == "jpeg" {
                self.filetype = url.pathExtension
                minetype = "image/jpeg"
                self.addImages(url, self.fileName)
            }
                
            else if self.filetype == "png" {
                self.filetype = url.pathExtension
                minetype = "image/png"
                self.addImages(url, self.fileName)
            }
            else if self.filetype == "csv" {
                self.filetype = url.pathExtension
                minetype = "text/csv"
                self.addImages(url, self.fileName)
            }
            else if self.filetype == "doc" {
                self.filetype = url.pathExtension
                minetype = "application/msword"
                self.addImages(url, self.fileName)
            }
            else if self.filetype == "docx" {
                self.filetype = url.pathExtension
                minetype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                self.addImages(url, self.fileName)
            }
            else if self.filetype == "pdf" {
                self.filetype = url.pathExtension
                self.minetype = "application/pdf"
                self.addImages(url, self.fileName)
            }
                
            else {
                
                self.showToast(message: "This file format is not supported . Please use this file format  jpg,jpeg,png,csv,doc,docx,pdf.".localized)
            }
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
    }
}

extension CustomerProfileViewController : iCloudDelegate {
    
    func iCloudDidFinishInitializingWitUbiquityToken(_ cloudToken: Any!, withUbiquityContainer ubiquityContainer: URL!) {
        //        debugPrint("ubiquityContainer == \(ubiquityContainer)")
        let cloud = iCloud.shared().checkUbiquityContainer()
        //        debugPrint("ubiquity container \(cloud)")
    }
    
    func iCloudAvailabilityDidChange(toState cloudIsAvailable: Bool, withUbiquityToken ubiquityToken: Any!, withUbiquityContainer ubiquityContainer: URL!) {
        //        debugPrint("UbiquityToken == \(ubiquityToken)")
    }
}

extension CustomerProfileViewController : GIDSignInDelegate, GIDSignInUIDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        
        guard (error == nil) else {
            service.authorizer = nil
            return
        }
        service.authorizer = user.authentication.fetcherAuthorizer()
        googledriveListFiles()
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        print("dismissing Google SignIn")
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        print("presenting Google SignIn")
    }
}

extension CustomerProfileViewController: DropBoxResponseDelegate {
    
    func didAuthenticateDropBoxUser(_ url: URL) {
        self.dropboxListFiles()
    }
}
