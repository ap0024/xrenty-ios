//
//  MyBookingTableViewCell.swift
//  XRentY
//
//  Created by user on 20/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit

class MyBookingTableViewCell: UITableViewCell {
    
    enum BookingStatus {
        case pickCar
        case returnCar
    }
    
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblCarAddress: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblSellerName: UILabel!
    @IBOutlet weak var lblPhoneno: UILabel!
    @IBOutlet weak var lblCarStatus: UILabel!
    @IBOutlet weak var lblPickupDate: UILabel!
    @IBOutlet weak var lblReturnDate: UILabel!
    @IBOutlet weak var btnPIckUp: UIButton!
    @IBOutlet weak var btnRetun: UIButton!
    @IBOutlet weak var btnTrack: UIButton!
    @IBOutlet weak var btnViewOrder: UIButton!
    @IBOutlet weak var btntrack: UIButton!
    
    var pickupCar : ((Int)->())? = nil
    var returnCar : ((Int)->())? = nil
    var trackMap : ((Int)->())? = nil
    var viewOrder : ((Int)->())? = nil
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    //heading
    
    @IBOutlet weak var lblrenty: UILabel!
    @IBOutlet weak var lblPickup: UILabel!
    @IBOutlet weak var lblTel: UILabel!
    @IBOutlet weak var lblreturn: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnViewbooking: UIButton!
    
    func configure(_ booking: Booking) {
        
        self.changeBookingStatus(booking)
        
        lblCarName.text = booking.product![0].name ?? "N/A"
        lblCarAddress.text = booking.product![0].address ?? "N/A"
        var price = booking.grand_total ?? "N/A"
        
        let price1 = Double(price.roundOff()) ?? 0
        let price2 = CGFloat(price1).formattedWithSeparator
        
        lblPrice.text = symbol + "\(price2)"
        lblSellerName.text = booking.product![0].sellername ?? "N/A"
        lblPhoneno.text = booking.product![0].contactnumber ?? "N/A"
        lblCarStatus.text =  booking.status ?? "N/A"
        lblPickupDate.text = booking.pickup_date ?? "N/A"
        lblReturnDate.text = booking.return_date ?? "N/A"
        imgCar.kf.indicatorType = .activity
        let image = booking.product![0].imageURL
        imgCar.kf.setImage(with: image  , placeholder: #imageLiteral(resourceName: "car_placeholder_images"))
    }
    
    func changeBookingStatus(_ booking: Booking) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        
        let pickUpdate = dateFormatter.date(from: booking.pickup_date!)
        let returnDate = dateFormatter.date(from: booking.return_date!)

        let now = Date()
        let pickupDayOrder = Calendar.current.compare(now, to: pickUpdate!, toGranularity: .day)
        let returnDayOrder = Calendar.current.compare(now, to: returnDate!, toGranularity: .day)
        let pickupCar = pickupDayOrder == .orderedDescending || pickupDayOrder == .orderedSame && !booking.pickupCar
        let returnCar = returnDayOrder == .orderedDescending || returnDayOrder == .orderedSame && !booking.returnCar
        
        let isPickupReturn = pickupCar || returnCar
        
        
        if booking.pickupCar && booking.returnCar && booking.cancelCar
        {
            enablePickup(false)
        }
        else
        {
            enablePickup(isPickupReturn ? (pickupCar ? !booking.pickupCar : false) : false)
        }
        self.enableReturn(booking.pickupCar && !booking.returnCar)

    }
    
    func enablePickup(_ enable: Bool) {
        btnPIckUp.alpha = enable ? 1.0 : 0.4
        btnPIckUp.isUserInteractionEnabled = enable
    }
    
    func enableReturn(_ enable: Bool) {
        btnRetun.alpha = enable ? 1.0 : 0.4
        btnRetun.isUserInteractionEnabled = enable
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func pickupCar(_ sender: Any) {
        if let handler = pickupCar {
            handler(tag)
        }
    }
    
    @IBAction func ReturnCar(_ sender: Any) {
        
        if let handler = returnCar {
            handler(tag)
        }
    }
    
    @IBAction func trackMap(_ sender: Any) {
        if let handler = trackMap {
            handler(tag)
        }
    }
    
    @IBAction func vieworderBooking(_ sender: Any) {
        if let handler = viewOrder {
            handler(tag)
        }
    }
}
