//
//  MyBookingViewController.swift
//  XRentY
//
//  Created by user on 20/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit

class MyBookingViewController: BaseViewController
{

    @IBOutlet weak var myBookingTable: UITableView!
    @IBOutlet weak var noRecordsView: UIView!
    let refreshControl = UIRefreshControl()
    var bookings : [Booking] = [] {
        didSet {
            myBookingTable.reloadData()
        }
    }
    
    @IBOutlet weak var lblMyBooking: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getbookings()
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        let attributes = [NSAttributedString.Key.foregroundColor: Color.barTintColor, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing please wait...".localized, attributes: attributes)
        myBookingTable.refreshControl = refreshControl
        refreshControl.tintColor = Color.barTintColor
        Observer.addMyBookings(target: self, selector: #selector(updateBookings))
        Observer.poptoSearch()
        
    }
    
    @objc func updateBookings() {
        self.getbookings(loaderColor: UIColor.black.withAlphaComponent(0.3))
    }
    
    
    
    @objc func doSomething(refreshControl: UIRefreshControl) {
        self.getbookings(false)
    }
    
    func getbookings(_ showLoader: Bool = true, loaderColor: UIColor = .white) {
        showLoader ? self.showLoader(color: loaderColor) : self.refreshControl.beginRefreshing()
        WebService.getbooking{ (result) in
           DispatchQueue.main.async {
            self.hideLoader()
            self.refreshControl.endRefreshing()
            switch result {
            case .success(let response):
                if let orders = response.object?.bookings, !orders.isEmpty  {
                    self.bookings = orders
                    self.myBookingTable.backgroundView = UIView()
                }
                else if self.bookings.isEmpty {
                    self.myBookingTable.backgroundView = self.noRecordsView
                }
                else {
                    AlertManager.showAlert(on: self, type: .custom(response.message ?? ""))
                }
            case .failure(let error):
                AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MyBookingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return bookings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyBookingTableViewCell", for: indexPath) as? MyBookingTableViewCell
        
        cell?.tag = indexPath.row
        cell?.configure(bookings[indexPath.row])
        cell?.pickupCar = { self.pickupCar(self.bookings[$0]) }
        cell?.returnCar = { self.returnCar(self.bookings[$0]) }
        cell?.viewOrder = { self.viewOrder(self.bookings[$0]) }
        
        return cell!
    }
    
    func pickupCar(_ booking: Booking) {
        
        self.pushTo(.pickupCar(booking,{rmaId in
            var row = 0
            self.bookings.forEach{
                if $0.order_id == booking.order_id {
                    $0.pickup_bool = 1
                    $0.rma_id = rmaId
                    row = self.bookings.index(of: $0)!
                }
            }
            self.myBookingTable.reloadRows(at: [IndexPath.init(row: row, section: 0)], with: .fade)
        }))
    }
    
    func returnCar(_ booking: Booking) {
        self.pushTo(.returnCar(booking,{
            var row = 0
            self.bookings.forEach{
                if $0.order_id == booking.order_id {
                    $0.return_bool = 1
                    row = self.bookings.index(of: $0)!
                }
            }
            self.myBookingTable.reloadRows(at: [IndexPath.init(row: row, section: 0)], with: .fade)
        }))
    }
    
    func viewOrder(_ booking: Booking) {
        self.pushTo(.vieworder(booking))
    }
}
