//
//  BookingHistoryViewController.swift
//  XRentY
//
//  Created by user on 20/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit

class BookingHistoryViewController: BaseViewController {
    
    
    @IBOutlet weak var lblBookingHistory: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension BookingHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingHistoryTableViewCell", for: indexPath) as? BookingHistoryTableViewCell
        return cell!
    }
    
}
