//
//  OpenPay.swift
//  XRentY
//
//  Created by user on 31/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

class OpenpayPayment {
    
    
//        #if PRO
//        static let baseUrl = "https://api.openpay.mx/v1/mezmsxpsvkhy0ayhipck/"
//        static let MERCHANT_ID = "mezmsxpsvkhy0ayhipck"
//        static let PRIVATE_KEY = "sk_9a7e99455e54491b9f9f5ff9f17f9fd5"
//        #elseif DEV
//        static let baseUrl = "https://sandbox-api.openpay.mx/v1/mgb7ejii13aernd1dirx/"
//        static let MERCHANT_ID = "mgb7ejii13aernd1dirx"
//        static let PRIVATE_KEY = "sk_bbc230a990f64b23b1a69154e5077aee"
//        #endif
//
//        #if PRO
//        let isProduction = true
//        #elseif DEV
//        let isProduction = false
//        #endif
    
    
//   Sandbox
     //static let baseUrl = "https://sandbox-api.openpay.mx/v1/mgb7ejii13aernd1dirx/"
     //static let MERCHANT_ID = "mgb7ejii13aernd1dirx"
     //static let PRIVATE_KEY = "sk_bbc230a990f64b23b1a69154e5077aee"

//    Live
      static let baseUrl = "https://api.openpay.mx/v1/mezmsxpsvkhy0ayhipck/"
      static let MERCHANT_ID = "mezmsxpsvkhy0ayhipck"
      static let PRIVATE_KEY = "sk_9a7e99455e54491b9f9f5ff9f17f9fd5"

    typealias TransactionHandler = ((OpenPayTransaction?,String) -> Void)
    var transactionCompletion: TransactionHandler? = nil
    
    var openPay: Openpay?
    var sessionId: String?
    var card: OPCard?
    var customer: OPCustomer?
    var amount: String?
    var customerId: String?
  
    // initialization
    init(customerId: String, amount: String, card: OPCard, details: OPCustomer, isProductionMode: Bool = true, isDebug: Bool = true) {
        self.openPay = Openpay(withMerchantId: OpenpayPayment.MERCHANT_ID, andApiKey: OpenpayPayment.PRIVATE_KEY, isProductionMode: isProductionMode, isDebug: isDebug)
        self.card = card
        self.customer = details
        self.amount = amount
        self.customerId = customerId
        self.sessionId = openPay?.createDeviceSessionId()
    }
    
    //MARK:- payment function
    public func payment(completion: @escaping TransactionHandler) {
        self.transactionCompletion = completion
        self.createToken()
    }
    
    //MARK:- token creation
    private func createToken() {
        openPay?.createTokenWithCard(with: card!, successFunction: { token in
             let id = token.id
            print("Token: \(token)")
            self.createPayment(id)
        }, failureFunction: { error in
            print(error.localizedDescription)
            if let errorString = (error as NSError).userInfo["errors"] as? [String]{
                self.transactionCompletion?(nil, errorString[0])
            }else {
                self.transactionCompletion?(nil, "Card details are not valid".localized )
            }
        })
    }

    //MARK:- charge amount
    private func createPayment(_ token: String) {
        
        
        
        let body = ["source_id": token,
                    "method": "card",
                    "amount": amount ?? "",
                    "description": "",
                    "device_session_id": sessionId!,
                    "capture": false,
                    "currency": "\(Authentication.currency!)"] as [String : Any]
        
        let data = try? JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
        let basicAuth = String(format: "%@:%@", OpenpayPayment.PRIVATE_KEY, "")
        let basicAuthData = basicAuth.data(using: String.Encoding.utf8)!
        let basicAuthString = basicAuthData.base64EncodedString()
        
        let urlString = OpenpayPayment.baseUrl + "customers/\(customerId!)/" + "charges"
        let url = URL(string: urlString)
        var request = URLRequest.init(url: url!)
        request.httpMethod = "POST"
        request.httpBody = data!
        request.setValue("Basic \(basicAuthString)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        
        print("url: \(urlString)")
        print("request: \(body)\n")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let responseData = data else {
                print(error!.localizedDescription)
                self.transactionCompletion?(nil, "Unable to process payment.\n please try again".localized)
                return
            }
            do {
                guard let json = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String : Any] else {
                    self.transactionCompletion?(nil, "Unable to process payment.\n please try again".localized)
                    return
                }
                
                print("response: \(json)")
                
                if let transaction = self.parseTransaction(from: json) {
                    self.transactionCompletion?(transaction, "success")
                } else {
                    var messageString : String?
                    if let errorCode = json["error_code"] as? Int {
                        messageString = ErrorCode(rawValue: errorCode)?.messageString
                    }
                    else {
                        messageString =  json["description"] as? String
                    }
                    self.transactionCompletion?(nil, messageString ?? "Unable to process payment.\n please try again".localized)
                }
            }
            catch {
                self.transactionCompletion?(nil, "Unable to process payment.\n please try again".localized)
            }
        }
        task.resume()
    }
    
    private func parseTransaction(from json: [String:Any]) -> OpenPayTransaction? {
        
        //print(json)
        
        if let transactionId = json["id"] as? String  {
            if let exchangeRate = json["exchange_rate"] as? [String: Any], let exchangeRateValue = exchangeRate["value"] as? Double {
                 return OpenPayTransaction(transactionId: transactionId,exchangeRate: "\(exchangeRateValue)")
            }
            return OpenPayTransaction(transactionId: transactionId,exchangeRate: "1")
        }

        return nil
    }
    
    //MARK:- refund amount
    public class func refundAmount(amount: String, customerId: String, transactionId: String, completion: @escaping ([String:Any]?,_ message: String) -> Void) {
        
        let parameters = ["amount": amount,
                          "description": "refund"]
        
        let data = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        let urlString = baseUrl + "customers/\(customerId)/" + "charges/" + "\(transactionId)/refund"
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = data!
        
        let basicAuth = String(format: "%@:%@", OpenpayPayment.PRIVATE_KEY, "")
        let basicAuthData = basicAuth.data(using: String.Encoding.utf8)!
        let basicAuthString = basicAuthData.base64EncodedString()
        
        request.setValue("Basic \(basicAuthString)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        
        print("url: \(urlString)")
        print("request: \(parameters)\n")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let responseData = data else {
                completion(nil,error!.localizedDescription)
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String : Any]
                print("refund response: \(json ?? [:])")
                if let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode != 200 {
                    let description = json?["description"] as? String ?? ""
                    completion(nil, description)
                } else {
                    completion(json,"Success")
                }
            }
            catch {
                completion(nil, "Openpay Error, Unable to parse :(")
            }
        }
        task.resume()
    }
    
    //MARK:- charge confirmation
    public class func confirmCharge(amount: String, customerId: String, transactionId: String, completion: @escaping ([String:Any]?) -> Void) {
        
        let parameters = ["amount": amount]
        let data = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        let urlString = baseUrl + "customers/\(customerId)/" + "charges/" + "\(transactionId)/capture"
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = data!
        
        let basicAuth = String(format: "%@:%@", OpenpayPayment.PRIVATE_KEY, "")
        let basicAuthData = basicAuth.data(using: String.Encoding.utf8)!
        let basicAuthString = basicAuthData.base64EncodedString()
        
        request.setValue("Basic \(basicAuthString)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        
        print("url: \(urlString)")
        print("request: \(parameters)\n")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let responseData = data else {
                print(error!.localizedDescription)
                completion(nil)
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String : Any]
//                print("confirm charge response: \(json)")
                completion(json)
            }
            catch {
                completion(nil)
            }
        }
        task.resume()
    }
}

