//
//  OrderSummaryTableViewCell.swift
//  XRentY
//
//  Created by user on 24/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit

class OrderSummaryTableViewCell: UITableViewCell {

    @IBOutlet var  lblName: UILabel!
    @IBOutlet var  carImageView: UIImageView!
    @IBOutlet var  lblCheckin: UILabel!
    @IBOutlet var  lblCheckout: UILabel!
    
    //heading
    @IBOutlet var  checkin: UILabel!
    @IBOutlet var  checkout: UILabel!
    @IBOutlet var  bookingSubtotal: UILabel!
    @IBOutlet var  depositAmout: UILabel!
    @IBOutlet var  discount: UILabel!
    @IBOutlet var  orderTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(_ car: Car) {
        lblName.text = car.name?.capitalized
        lblCheckin.text = car.checkin
        lblCheckout.text = car.checkout
        carImageView.kf.setImage(with: URL(string: car.image ?? ""), placeholder: #imageLiteral(resourceName: "car_placeholder.jpg"))
    }
}
