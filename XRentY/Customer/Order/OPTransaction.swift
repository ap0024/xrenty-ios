//
//  OPTransaction.swift
//  XRentY
//
//  Created by user on 13/11/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

struct OpenPayTransaction {
    let transactionId: String
    let exchangeRate: String
}
