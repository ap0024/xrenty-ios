//
//  PaypalCheckoutViewController.swift
//  XRentY
//
//  Created by user on 01/08/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
import IBLocalizable
import PaypalSDK

class PaypalCheckoutViewController: BaseViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var ConstraintTableHeight: NSLayoutConstraint!
    @IBOutlet weak var tableCars: UITableView!
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblDeposite: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var protectionFee: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnPlaceOrder: UIButton!
    @IBOutlet weak var DiscountStack: UIStackView!
    @IBOutlet weak var lblOrderSummary: UILabel!
    @IBOutlet weak var lblNeedHelp: UILabel!
    @IBOutlet weak var btnGotoPayment: UIButton!
    @IBOutlet weak var lblConfirmDeposit: UILabel!
    
    var region_id : String = ""
    var extraFee : String = ""
    var order_type : APIConstants = .orderSummary_gold

    var isDepositConfirmed: Bool = false {
        didSet {
            UserDefaults.standard.set(isDepositConfirmed, forKey: "depositConfirmed")
            if let deposit = order.depositeValue, !isDepositConfirmed {
                let deposit1 = Double(order!.depositeValue!) ?? 0
                let deposit2 = CGFloat(deposit1).formattedWithSeparator
                
                self.btnPlaceOrder.setTitle("Confirm Deposit: ".localized + "\(Authentication.currencySymbol ?? "US$")" + " \(deposit2)", for: .normal)
                self.lblConfirmDeposit.text = "Deposit is only for warranty if something happens with the car, we are just pre-authorizing your card".localized
            }
            else if let rent = order.chargeValue, isDepositConfirmed {
                
                let extraFee1 = Double(order!.extraFee!.roundOff()) ?? 0
                let deposit2 = CGFloat(Double(order!.chargeValue!.roundOff()) ?? 0 + extraFee1).formattedWithSeparator
                
                let rent1 = Double(rent) ?? 0
                let rent2 = CGFloat(rent1).formattedWithSeparator

                
//
//
//                let rent1 = Double(rent) ?? 0
//                let extraFee1 = Double(order!.extraFee!.roundOff()) ?? 0
//
//                let rent2 = CGFloat(rent1+extraFee1).formattedWithSeparator
                self.btnPlaceOrder.setTitle("Place Order: ".localized + symbol + "\(deposit2)", for: .normal)
                self.lblConfirmDeposit.text = "Proceed to rent, we are just pre-authorizing your card".localized
            } else {
                self.btnPlaceOrder.setTitle("Please Wait...".localized, for: .normal)
                self.lblConfirmDeposit.text = ""
            }
        }
    }
    
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    
    var order : Order! {
        didSet {
            self.updateUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTitleView()
        self.orderSummary()
    }
    
    // update ui
    func updateUI() {
        
        let subTotal = order.subtotal ?? ""
        let orderTotal = order.grandTotal ?? ""
        let deposite = order.deposit ?? ""
        let discount = order.discount ?? ""
        
        self.extraFee = order.extraFee ?? ""
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        
        let subTotal1 = Double(subTotal.roundOff()) ?? 0
        let subTotal2 = CGFloat(subTotal1).formattedWithSeparator
        
        let orderTotal1 = Double(orderTotal.roundOff()) ?? 0
        let orderTotal2 = CGFloat(orderTotal1).formattedWithSeparator
        
        let deposite1 = Double(deposite.roundOff()) ?? 0
        let deposite2 = CGFloat(deposite1).formattedWithSeparator
        
        let discount1 = Double(discount.roundOff()) ?? 0
        let discount2 = CGFloat(discount1).formattedWithSeparator
        
        let extraFee1 = Double(self.extraFee.roundOff()) ?? 0
        let extraFee2 = CGFloat(extraFee1).formattedWithSeparator
        
        self.lblSubtotal.text = symbol + "\(subTotal2)"
        self.lblTotal.text = symbol + "\(orderTotal2)"
        self.lblDeposite.text = symbol + "\(deposite2)"
        self.lblDiscount.text = symbol + "\(discount2)"
        self.protectionFee.text = symbol + "\(extraFee2)"
       
        tableCars.reloadData()
        
        if   discount2  == "0"  {
            DiscountStack.isHidden = true
        }
        else {
            DiscountStack.isHidden = false
        }
        
        if let deposit_order_id = order.paypal_deposit_order_id {
            self.isDepositConfirmed = !deposit_order_id.isEmpty
        }
    }
    
    //MARK: Order Summary API
    func orderSummary() {
        self.showLoader(color: .white)
        WebService.orderSummary(APIConstants: order_type) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    guard let order = response.object,
                        order.products?.count != 0 else {
                        self.navigationController?.popToRootViewController(animated: true)
                        return
                    }
                    self.order = order
                case .failure(let error):
                    AlertManager.showAlert(type: .custom(error.message))
                }
            }
        }
    }
    
    // MARK: Place Order Action
    @IBAction func placeOrder(_ sender: Any) {
        guard order != nil else { return }
        isDepositConfirmed ? authorizeCharge() : authorizeDeposit()
    }
    
    @IBAction func supportLink(_ sender: UIButton) {
        if let url = URL(string: "https://xrenty.uvdesk.com/en/blog/how-we-handle-payments") {
            UIApplication.shared.open(url)
        }
    }
}

extension PaypalCheckoutViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order?.products?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrderSummaryTableViewCell
        let car = order!.products![indexPath.row]
        cell.configure(car)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        ConstraintTableHeight.constant = tableView.contentSize.height
    }
}

//MARK: Payment
extension PaypalCheckoutViewController {
    
    //MARK: Authorize Deposit (PAYPAL)
    func authorizeDeposit() {
        guard let currencyCode = Authentication.currency else {
            return
        }
        let extraFee1 = Double(order!.extraFee!.roundOff()) ?? 0
        let deposit1 = Double(order!.depositeValue!) ?? 0
        let deposit2 = CGFloat(deposit1).formattedWithSeparator
        let deposit = "\(deposit2)"

        self.showLoaderOnWindow()
        PaypalSDK.shared.createOrder(withIntent: .Authorize, amount: "\(deposit)", currencyCode: currencyCode, on: self)
    }
    
    //MARK: Capture Deposit (PAYPAL)
    func authorizeCharge() {
        
        let extraFee1 = Double(order!.extraFee!.roundOff()) ?? 0
        let deposit2 = CGFloat(Double(order!.chargeValue!.roundOff()) ?? 0 + extraFee1).formattedWithSeparator
        
        
//        let extraFee1 = Double(order!.extraFee!.roundOff()) ?? 0
//        let chargee = Double(order!.chargeValue!.roundOff()) ?? 0
        let final = deposit2
        let charge = final.replacingOccurrences(of: ",", with: "")
        
        
        guard let _ = order.depositeValue , let currencyCode = Authentication.currency, let _ = order.extraFee, let _ = order.paypal_deposit_order_id else {
            print("Parameter Missing")
            return
        }
        
        guard let number = NumberFormatter().number(from: charge) else {
            print("invalid amount")
            return
        }
        
        let amount = CGFloat(truncating: number)
        print(amount)
        
        if amount > 0 {
            self.showLoaderOnWindow()
            PaypalSDK.shared.createOrder(withIntent: .Authorize, amount: charge, currencyCode: currencyCode, on: self)
        } else {
            self.showLoaderOnWindow()
            self.placeOrder(chargeOrderId: "", paypalPayerId: "", paypalPayerEmail: order.email ?? "", paypalPayerStatus: "Complete", city: order.city ?? "", countryId: order.countryId ?? "" , paypal_address_status: "", paypal_express_checkout_token: "", region: "", postcode: order.Pincode ?? "", regionId: "", street:  order.street ?? "", telephone:  order.telephone ?? "")
        }
    }
    
    //MARK: Hold Deposit (Server Side)
    func holdPaypalDeposit(_ orderId: String) {
        let query = ["paypal_deposit_order_id": orderId]
        WebService.holdPaypalDeposit(query: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success:
                    self.order.paypal_deposit_order_id = orderId
                    self.isDepositConfirmed = true
                    Observer.updateCart()
                case .failure(let error):
                    AlertManager.showAlert(type: .custom(error.message))
                }
            }
        }
    }

    
    //MARK: Place Order (Server Side)
    func placeOrder(chargeOrderId: String,paypalPayerId: String,paypalPayerEmail: String,paypalPayerStatus: String,city : String,countryId : String ,paypal_address_status :String,paypal_express_checkout_token : String,region :String,postcode: String,regionId : String,street :String ,telephone: String) {
        
        // force unwrap optionals as null check is implemented in capture charge method
        let queryItems = [ "token" : Authentication.token!,
                           "city" : city,
                           "country_id" : countryId,
                           "paypal_address_status"  : paypal_address_status,
                           "paypal_express_checkout_token" : paypal_express_checkout_token,
                           "paypal_payer_email" : paypalPayerEmail,
                           "paypal_payer_id" : paypalPayerId,
                           "paypal_payer_status" : paypalPayerStatus,
                           "paypal_protection_eligibility" : "",
                           "postcode" : order.Pincode!,
                           "region" : region,
                           "region_id" : regionId,
                           "street" : order.street!,
                           "telephone" : telephone,
                           "paypal_charge_order_id": chargeOrderId,
                           "paypal_deposit_order_id": order.paypal_deposit_order_id!,
                           "customer_charge_amount": order.chargeValue!,
                           "customer_deposit_amount": order.depositeValue!,
                           "extra_free": order.extraFee!,
                           "pay_method": "paypal_express",
                           "currency" : Authentication.currency!
            ] as [String : Any]
        print(queryItems)
        WebService.placePaypalOrder(query: queryItems) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    //print(response.object?.orderid!)
                    AlertManager.showAlert(type: .custom(response.message ?? "Your payment is successfull".localized), action: {
                        let controller = OrderViewController.initiatefromStoryboard(.orderSummary)
                        controller.id = response.object?.orderid ?? ""
                        print(controller.id)
                        Observer.updateMyBookings()
                        Observer.updateCart()
                        UserDefaults.standard.removeObject(forKey: "selected_protection_now")
                        UserDefaults.standard.removeObject(forKey: "depositConfirmed")
                        self.navigationController?.pushViewController(controller, animated: true)
                    })
                case .failure(let error):
                    AlertManager.showAlert(type: .custom(error.message))
                }
            }
        }
    }
}









// Paypal Payment Depegate
extension PaypalCheckoutViewController: PaymentDelegate {
    
    func didUpdateOrder(with state: PaymentState) {
        switch state {
        case .create:
            self.hideLoader()
        case .authorize(let order):
            if isDepositConfirmed {
                self.placeOrder(chargeOrderId: order.id, paypalPayerId: order.payer?.payer_id ?? "", paypalPayerEmail: order.payer?.email_address ?? "", paypalPayerStatus: order.status ?? "", city: order.payer?.address?.admin_area_2 ?? "", countryId: order.payer?.address?.country_code ?? "" , paypal_address_status: order.status ?? "" , paypal_express_checkout_token: "", region: order.payer?.address?.admin_area_2 ?? "", postcode: order.payer?.address?.postal_code ?? "", regionId: order.payer?.address?.admin_area_1 ?? "", street: order.payer?.address?.address_line_1 ?? "", telephone: order.payer?.phone?.phone_number?.national_number ?? "")
            } else {
                self.holdPaypalDeposit(order.id)
            }
        case .approve:
            self.showLoaderOnWindow()
        default:
            break
        }
    }
    
    func didFailUpdateOrder(with error: PaymentError) {
        self.hideLoader()
        AlertManager.showAlert(type: .custom(error.errorDescription.localized))
    }
}

