//
//  OrderViewController.swift
//  XRentY
//
//  Created by user on 09/08/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit

class OrderViewController: BaseViewController {

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPickUpDate: UILabel!
    @IBOutlet weak var lblReturnDate: UILabel!
    @IBOutlet weak var lblBookingSubTotal: UILabel!
    @IBOutlet weak var lblDepositeAmount: UILabel!
    @IBOutlet weak var lblOrderTotal: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblReturnStatus: UILabel!
    @IBOutlet weak var lblPickUpStatus: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDiscountValue: UILabel!
    @IBOutlet weak var lblProtectionFee: UILabel!
    
    var booking: Booking?
    var product_id : String = ""
    var id : String = ""
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getOrderDetail()
        print(id)
        Observer.poptoSearch(target: self, selector: #selector(poptosearch))
    }
    
    @objc func poptosearch() {
        self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.viewControllers.first?.tabBarController?.selectedIndex = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getOrderDetail() {
        
        let query = ["order_id": self.id]
        let grayColor = UIColor(red: 230, green: 230, blue: 241, alpha: 1)
        showLoader(color: grayColor)
        WebService.getOrderDetail(queryItems: query) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    
                    let order = response.object
                    
                    self.imgProduct.kf.indicatorType = .activity
                    let image = order?.imageURL
                    self.imgProduct.kf.setImage(with: image , placeholder: #imageLiteral(resourceName: "car_placeholder_images"))
                    self.lblName.text = order?.productName
                    self.lblPickUpDate.text = order?.pickupDate
                    self.lblReturnDate.text = order?.returnDate
                    self.product_id = order?.productId ?? ""
                    
                    
                    let subTotal = order?.bookingSubtotal ?? ""
                    let subTotal1 = Double(subTotal.roundOff()) ?? 0
                    let subTotal2 = CGFloat(subTotal1).formattedWithSeparator
                    
                    let orderTotal = order?.OrderTotal ?? ""
                    let orderTotal1 = Double(orderTotal.roundOff()) ?? 0
                    let orderTotal2 = CGFloat(orderTotal1).formattedWithSeparator
                    
                    let deposite = order?.deposit ?? ""
                    let deposite1 = Double(deposite.roundOff()) ?? 0
                    let deposite2 = CGFloat(deposite1).formattedWithSeparator
                    
                    let protectionFee = order?.extraFee ?? ""
                    let protectionFee1 = Double(protectionFee.roundOff()) ?? 0
                    let protectionFee2 = CGFloat(protectionFee1).formattedWithSeparator
                    
                    let discountAmount = order?.discountAmount ?? ""
                    let discountAmount1 = Double(discountAmount.roundOff()) ?? 0
                    let discountAmount2 = CGFloat(discountAmount1).formattedWithSeparator
                    
                    self.lblBookingSubTotal.text = self.symbol + " " + "\(subTotal2)"
                    self.lblOrderTotal.text = self.symbol + " " + "\(orderTotal2)"
                    self.lblDepositeAmount.text = self.symbol + " " + "\(deposite2)"
                    self.lblProtectionFee.text = self.symbol + " " + "\(protectionFee2)"
                    self.lblUserName.text =  "\(order?.customerFirstname ?? "")" +  " " + "\(order?.customerLastname ?? "")"
                    self.lblEmail.text = order?.customerEmail
                    self.lblPhone.text = "\(order?.telephone ?? "N/A")"
                    self.lblAddress.text =  order?.billingAddress ?? "N/A"
                    self.lblOrderStatus.text = order?.orderStatus ?? ""
                    if !(order?.discountAmount == "" )
                    {
                        self.lblDiscountValue.text = self.symbol + " " + "\(discountAmount2)"
                    }
                    else
                    {
                        self.lblDiscountValue.isHidden = true
                        self.lblDiscount.isHidden = true
                    }
                    
                    if(order?.pickupBool == 1)
                    {
                        self.lblPickUpStatus.text = "I accept the car.".localized
                    }
                    else
                    {
                        self.lblPickUpStatus.text = "Not Done".localized
                    }
                    if (order?.returnBool == 1)
                    {
                        self.lblReturnStatus.text = "Return Ok".localized
                    }
                    else
                    {
                        self.lblReturnStatus.text = "Not Done".localized
                    }
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    @IBAction func Back(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.viewControllers.first?.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func didTapAskQuestion(_ sender: Any) {
        
        let controller = PopOverViewController.initiatefromStoryboard(.carDetail)
        controller.productId = product_id
        controller.completionBlock = { message in
            AlertManager.showAlert(on: self, type: .custom(message))
            self.hideLoader()
        }
        self.present(controller, animated: true, completion: nil)
    }
}

