//
//  OrderSummaryViewController.swift
//  XRentY
//
//  Created by user on 24/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit
import IBLocalizable

class OrderSummaryViewController: BaseViewController, UIScrollViewDelegate {

    @IBOutlet weak var ConstraintTableHeight: NSLayoutConstraint!
    @IBOutlet var  tableCars: UITableView!
    @IBOutlet var  lblSubtotal: UILabel!
    @IBOutlet var  lblDeposite: UILabel!
    @IBOutlet var  lblDiscount: UILabel!
    @IBOutlet var  lblTotal: UILabel!
    @IBOutlet var  protectionFee: UILabel!
    @IBOutlet var  txtFirstName: UITextField!
    @IBOutlet var  txtLastName: UITextField!
    @IBOutlet var  txtEmail: UITextField!
    @IBOutlet var  txtAddress: UITextField!
    @IBOutlet var  txtCity: UITextField!
    @IBOutlet var  txtState: UITextField!
    @IBOutlet var  txtZipCode: UITextField!
    @IBOutlet var  txtCountry: UITextField!
    @IBOutlet var  txtPhoneNumber: UITextField!
    @IBOutlet var  txtCardNumber: UITextField!
    @IBOutlet var  txtCardHolderName: UITextField!
    @IBOutlet var  txtExpirationMonth: UITextField!
    @IBOutlet var  txtExpirationYear: UITextField!
    @IBOutlet var  txtCvv: UITextField!
    @IBOutlet var  regionIndicator: UIActivityIndicatorView!
    @IBOutlet var  regionDropdownImage: UIImageView!
    @IBOutlet var  countryDropdownImage: UIImageView!
    @IBOutlet var  countryIndicator: UIActivityIndicatorView!
    @IBOutlet var  scrollView: UIScrollView!
    @IBOutlet var  monthYearPicker: UIPickerView!
    @IBOutlet var  pickerTopConstraint: NSLayoutConstraint!
    @IBOutlet var  btnPlaceOrder: UIButton!
    @IBOutlet weak var DiscountStack: UIStackView!
    @IBOutlet var  lblConfirmDeposit: UILabel!
    
    //heading
    @IBOutlet weak var lblOrderSummary: UILabel!
    @IBOutlet weak var lblPaymentInfo: UILabel!
    @IBOutlet weak var lblFname: UILabel!
    @IBOutlet weak var lblLname: UILabel!
    @IBOutlet weak var lblemail: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblcountry: UILabel!
    @IBOutlet weak var lblstate: UILabel!
    @IBOutlet weak var lblcity: UILabel!
    @IBOutlet weak var lblzipCode: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblpaymentMethod: UILabel!
    @IBOutlet weak var lblcrdHolderName: UILabel!
    @IBOutlet weak var lblcrdCreditNumber: UILabel!
    @IBOutlet weak var lblExpirationDate: UILabel!
    @IBOutlet weak var lblCrdVerificationNumber: UILabel!
    @IBOutlet weak var lblNeedHelp: UILabel!
    @IBOutlet weak var btnGotoPayment: UIButton!
    
    var order_type : APIConstants = .orderSummary_gold
    
    var currentScrollOffset = CGPoint(x: 0, y: 0)
    var region_id : String = ""
    var extraFee : String = ""
    let expMonths = Calendar.current.monthSymbols
    var expYears : [String] {
        let components = Calendar.current.dateComponents([.year], from: Date())
        return (components.year!...2100).map { String($0) }
    }
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    var selectedRegionId: String {
        var regionId = self.region_id
        regions.forEach {
            if $0.name == txtState.text! {
                regionId = $0.region_id!
            }
        }
        return regionId
    }

    var selectedCountryId: String {
        var countryId = ""
        countries.forEach {
            if $0.name == txtCountry.text! {
                countryId = $0.iso2_code
            }
        }
        return countryId
    }
    
    var card: OPCard {

        let card = OPCard()
        card.holderName = txtCardHolderName.text!
        card.number = txtCardNumber.text!
        card.expirationMonth = txtExpirationMonth.text!
        card.expirationYear = txtExpirationYear.text!
        card.cvv2 = txtCvv.text!

        return card
    }
    
    var customer: OPCustomer {
          let customer = OPCustomer(firstName: txtFirstName.text!,
                                  lastName: txtLastName.text!,
                                  phone: txtPhoneNumber.text!,
                                  email: txtEmail.text!)
        return customer
    }
    
    var countries: [Country] = []
    var regions: [Region] = []
    var showPicker: Bool = false {
        didSet {
            pickerTopConstraint.constant = showPicker ? -200 : 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let y_offset = ((txtExpirationYear.superview!.frame.origin.y + txtExpirationYear.superview!.frame.size.height)  - (scrollView.bounds.height - 200)) + 20
            let cgpoint = CGPoint(x: 0, y: y_offset)
            scrollView.setContentOffset(showPicker ? cgpoint : currentScrollOffset, animated: true)
        }
    }
    
    var order : Order? {
        didSet {
            
            let subTotal = order?.products?.first?.subtotal ?? ""
            let orderTotal = order?.grandTotal ?? ""
            let deposite = order?.deposit ?? ""
            let discount = order?.discount ?? ""
            
            self.extraFee = order?.extraFee ?? ""
            
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            
            let subTotal1 = Double(subTotal.roundOff()) ?? 0
            let subTotal2 = CGFloat(subTotal1).formattedWithSeparator
            
            let orderTotal1 = Double(orderTotal.roundOff()) ?? 0
            let orderTotal2 = CGFloat(orderTotal1).formattedWithSeparator
            
            let deposite1 = Double(deposite.roundOff()) ?? 0
            let deposite2 = CGFloat(deposite1).formattedWithSeparator
            
            let discount1 = Double(discount.roundOff()) ?? 0
            let discount2 = CGFloat(discount1).formattedWithSeparator

            let extraFee1 = Double(self.extraFee.roundOff()) ?? 0
            let extraFee2 = CGFloat(extraFee1).formattedWithSeparator
            
            self.lblSubtotal.text = symbol + "\(subTotal2)"
            self.lblTotal.text = symbol + "\(orderTotal2)"
            self.lblDeposite.text = symbol + "\(deposite2)"
            self.lblDiscount.text = symbol + "\(discount2)"
            self.protectionFee.text = symbol + "\(extraFee2)"
            
            txtFirstName.text = order?.firstName ?? ""
            txtLastName.text = order?.lastName ?? ""
            txtEmail.text = order?.email ?? ""
            txtCity.text = order?.city ?? ""
            txtState.text = order?.state ?? ""
            txtZipCode.text = order?.Pincode ?? ""
            txtCountry.text = order?.countryName
            
            let phone = order?.telephone ?? ""
            if phone.isEqualToString(find: "null")
            {
                txtPhoneNumber.text = ""
            }else
            {
                txtPhoneNumber.text = order?.telephone ?? ""
            }
            tableCars.reloadData()
            self.fetchRegionId()
            print(discount2)
            if  discount2  == "0"
            {
                DiscountStack.isHidden = true
            }
            else
            {
                DiscountStack.isHidden = false
            }
        }
    }
    
    var depositeTransaction: OpenPayTransaction? = nil
    var depositConfirmed: Bool = false  {
        
        didSet {
            if let deposit = order?.depositeValue, !depositConfirmed {
                let extraFee1 = Double(order!.extraFee!.roundOff()) ?? 0
                let deposit1 = Double(deposit) ?? 0
                
                self.btnPlaceOrder.setTitle("Confirm Deposit: ".localized + "\(Authentication.currencySymbol ?? "US$")" + " \(deposit1)", for: .normal)
                self.lblConfirmDeposit.text = "Deposit is only for warranty if something happens with the car, we are just pre-authorizing your card".localized
            }
            else if let rent = order?.chargeValue, depositConfirmed {
                
                let extraFee1 = Double(order!.extraFee!.roundOff()) ?? 0
                let deposit2 = CGFloat(Double(order!.chargeValue!.roundOff()) ?? 0 + extraFee1).formattedWithSeparator

                let rent1 = Double(rent) ?? 0
                let rent2 = CGFloat(rent1).formattedWithSeparator
                
                if let exchangeRate = order?.exchangeRate, let transactionId = order?.depositTransactionId {
                    self.depositeTransaction = OpenPayTransaction(transactionId: transactionId, exchangeRate:exchangeRate)
                }
                self.btnPlaceOrder.setTitle("Place Order: ".localized + symbol + "\(deposit2)", for: .normal)
                self.lblConfirmDeposit.text = "Proceed to rent, we are just pre-authorizing your card".localized
            } else {
                self.btnPlaceOrder.setTitle("Please Wait...".localized, for: .normal)
                self.lblConfirmDeposit.text = ""
            }
            
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTitleView()
        setLabel()
        self.showPicker = false
        self.depositConfirmed = false
        Observer.poptoSearch(target: self, selector: #selector(poptosearch))
        
    }
    
    @objc func poptosearch() {
        self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.viewControllers.first?.tabBarController?.selectedIndex = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.orderSummary()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setLabel() {
        
        lblFname.setLocalizedAshtrik()
        lblLname.setLocalizedAshtrik()
        lblemail.setLocalizedAshtrik()
        lblAddress.setLocalizedAshtrik()
        lblcountry.setLocalizedAshtrik()
        lblstate.setLocalizedAshtrik()
        lblcity.setLocalizedAshtrik()
        lblzipCode.setLocalizedAshtrik()
        lblPhone.setLocalizedAshtrik()
        lblcrdHolderName.setLocalizedAshtrik()
        lblcrdCreditNumber.setLocalizedAshtrik()
        lblExpirationDate.setLocalizedAshtrik()
        lblCrdVerificationNumber.setLocalizedAshtrik()
        self.btnGotoPayment.setTitle("Go to Payments Support".localized, for: .normal)
        
    }
    
    // get countries data
    func getCountries(completion:(()->())? = nil) {
        self.countryIndicator.startAnimating()
        self.countryDropdownImage.alpha = 0
        WebService.getCountries { result in
            DispatchQueue.main.async {
                self.countryIndicator.stopAnimating()
                self.countryDropdownImage.alpha = 1
                switch result {
                case .success(let response):
                    self.countries = response.objects ?? []
                    completion?()
                case .failure(_):
                    break
                }
            }
        }
    }
    
    func getRegion(_ code: String, completion:(()->())? = nil) {
        let query = ["country_code": code]
        self.regionIndicator.startAnimating()
        self.regionDropdownImage.alpha = 0
        WebService.getRegion(queryItems: query) { result in
            DispatchQueue.main.async {
                self.regionIndicator.stopAnimating()
                self.regionDropdownImage.alpha = 1
                switch result {
                case .success(let response):
                    self.regions = response.objects ?? []
                    completion?()
                case .failure(_):
                    break
                }
            }
        }
    }
    
    func fetchRegionId() {
        getCountries {
            if let code = self.order?.countryId {
                self.getRegion(code) {
                    self.regions.forEach {
                        if $0.name == self.txtState.text {
                            self.region_id = $0.region_id ?? ""
                        }
                        else {
                            self.txtState.text = ""
                        }
                    }
                }
            }
        }
    }
    
    //MARK: Order Summary API
    func orderSummary() {
        self.showLoader(color: .white)
        WebService.orderSummary(APIConstants: order_type) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    self.order = response.object
                    self.depositConfirmed  = response.object?.depositConfirmed == 1
                    if self.order?.products?.count == 0 {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                case .failure(let error):
                    AlertManager.showAlert(type: .custom(error.message))
                }
            }
        }
    }
    
    @IBAction func pickMonthYear(_ sender: UIButton) {
        self.showPicker = false
    }

    // MARK: Place Order Action
    @IBAction func placeOrder(_ sender: Any) {
        
//        txtAddress.text = "xyz"
//        txtCardNumber.text = "4111111111111111"
//        txtCardHolderName.text = "Personal"
//        txtCvv.text = "123"
        //txtExpirationMonth.text = "02"
        //txtExpirationYear.text = "2020"
        
        guard order != nil else { return }
        guard isValidDetails != false else { return }
        
        
        self.view.endEditing(true)
        depositConfirmed ? confirmOrder() : confirmDeposit()
    }
    
    //MARK: Confirm Deposit
    func confirmDeposit() {
        let deposit1 = Double(order!.depositeValue!) ?? 0

        let myNumber = NSNumber(value: Float(deposit1))
        let deposit2 = "\(myNumber)"
        
        self.openpayTransaction(amount: "\(deposit2)") { (transaction,message) in
            
            guard let transaction = transaction else {
                
                let actionCancel = UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil)
                
                let actionUseAnotherCard = UIAlertAction.init(title: "Use another card".localized, style: .default, handler: { _ in
                    self.txtCardNumber.text = ""
                    self.txtExpirationYear.text = ""
                    self.txtExpirationMonth.text = ""
                    self.txtCvv.text = ""
                    self.txtCardNumber.becomeFirstResponder()
                })
                
                let alert = UIAlertController.init(title: nil, message: message + ".\nWe cannot charge the rent, please call your bank or try with another Card".localized, preferredStyle: .alert)
                alert.addAction(actionCancel)
                alert.addAction(actionUseAnotherCard)
                self.present(alert, animated: true, completion: nil)
                
                return
            }
            
            self.holdDeposit(deposit: deposit2, transaction: transaction, completion: {
                AlertManager.showAlert(type: .custom("Deposit Successful".localized))
                self.depositConfirmed = true
                self.depositeTransaction = transaction
                Observer.updateCart()
            })
        }
    }
    
    //MARK: Confirm Order
    func confirmOrder() {
        
        guard let depositTransaction = depositeTransaction else {
            print("deposit transaction is nil".localized)
            return
        }
        let extraFee1 = Double(order!.extraFee!.roundOff()) ?? 0
        let deposit1 = Double(order!.depositeValue!) ?? 0
        let deposit2 = CGFloat(deposit1).formattedWithSeparator
        let deposit = "\(deposit2)"
        deposit.replacingOccurrences(of: ",", with: "")
        guard  var charge = order?.chargeValue else {
            AlertManager.showAlert(on: self, type: .custom("Charge amount is invalid".localized))
            return
        }
        
        let final_charge = CGFloat((Double(order!.chargeValue!) ?? 0 + extraFee1)).formattedWithSeparator.replacingOccurrences(of: ",", with: "")
        charge = final_charge.replacingOccurrences(of: ",", with: "")
        
        guard let number = NumberFormatter().number(from: "\(final_charge)") else {
            print("invalid amount")
            return
        }
        
        let amount = CGFloat(truncating: number)
    
        if amount > 0 {
            self.openpayTransaction(amount: charge) { (transaction,message) in
                guard let transaction = transaction else {
                    let actionCancel = UIAlertAction.init(title: "Cancel & Refund Deposit".localized, style: .default, handler: { (_) in
                        self.showRefundAlert {
                            if let calculativeDeposit = self.order?.convertedDeposit {
                                self.refundAPI(calculativeDeposit, transactionId: depositTransaction.transactionId) {
                                    self.releaseDeposit()
                                    self.depositConfirmed = false
                                }
                            }
                            else {
                                self.refund(deposit, transaction: depositTransaction, completion: {
                                    self.releaseDeposit()
                                    self.depositConfirmed = false
                                })
                            }
                        }
                    })
                    
                    let actionTryAgain = UIAlertAction.init(title: "Use Another Card".localized, style: .cancel, handler: { _ in
                        self.txtCardNumber.text = ""
                        self.txtExpirationYear.text = ""
                        self.txtExpirationMonth.text = ""
                        self.txtCvv.text = ""
                        self.txtCardNumber.becomeFirstResponder()
                    })
                    
                    let alert = UIAlertController.init(title: nil, message: message + ".\nWe cannot charge the rent, please Call your bank or try with another Card".localized, preferredStyle: .alert)
                    alert.addAction(actionCancel)
                    alert.addAction(actionTryAgain)
                    self.present(alert, animated: true, completion: nil)
                    
                    return
                }
                
                self.placeOrder(chargeTransaction: transaction, depositTransaction: depositTransaction, charge: charge, deposit: deposit)
            }
        }
        else {
            self.placeOrder(depositTransaction: depositTransaction, charge: charge, deposit: deposit)
        }
    }
    
    // release deposit
    func releaseDeposit() {
        WebService.releaseDeposit { (result) in
            switch result {
            case .success:
                break
            case .failure:
                break
            }
        }
    }
    
    //MARK: Hold Deposit
    func holdDeposit(deposit: String, transaction: OpenPayTransaction, completion:@escaping ()->()) {
        
        let param = ["deposit_amount": deposit,
                     "exchange_rate": transaction.exchangeRate,
                     "transaction_id": transaction.transactionId]
        showLoaderOnWindow()
        WebService.holdDeposit(queryItems: param) { (response) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch response {
                case .success:
                    completion()
                case .failure(let error):
                    
                    let actionCancel = UIAlertAction.init(title: "Cancel & Refund Deposit".localized, style: .default, handler: { (_) in
                        self.showRefundAlert {
                            self.refund(deposit, transaction: transaction, completion: {
                                self.releaseDeposit()
                                self.depositConfirmed = false
                            })
                        }
                    })
                    
                    let actionTryAgain = UIAlertAction.init(title: "Try Again".localized, style: .default, handler: { (_) in
                        self.holdDeposit(deposit: deposit, transaction: transaction, completion: completion)
                    })
                    
                    let alert = UIAlertController.init(title: "Deposit Failed".localized, message: error.message, preferredStyle: .alert)
                    alert.addAction(actionCancel)
                    alert.addAction(actionTryAgain)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: OpenPay Transaction
    func openpayTransaction(amount: String, completion: @escaping (OpenPayTransaction?,String)->()) {
        
        guard let customerId = Authentication.openpayCustomerId else {
            completion(nil, "customer is not valid".localized)
            return
        }

        let openPay = OpenpayPayment(customerId: customerId, amount: amount, card: card, details: customer, isProductionMode: true, isDebug: true)

        showLoaderOnWindow()
        openPay.payment { (optransaction, message) in
            DispatchQueue.main.async {
                self.hideLoader()
                completion(optransaction, message)
            }
        }
    }
    
    //MARK: Place Order Backend API
    func placeOrder(chargeTransaction: OpenPayTransaction? = nil, depositTransaction: OpenPayTransaction, charge: String, deposit: String) {
        print (selectedRegionId)
        let shippinAddress = ["firstname": txtFirstName.text!.trimmingCharacters(in:
            CharacterSet.whitespaces),
                              "lastname": txtLastName.text!.trimmingCharacters(in:
                                CharacterSet.whitespaces),
                              "street": txtAddress.text!.trimmingCharacters(in:
                                CharacterSet.whitespaces),
                              "city": txtCity.text!.trimmingCharacters(in:
                                CharacterSet.whitespaces),
                              "country_id": selectedCountryId,
                              "region": txtState.text!.trimmingCharacters(in:
                                CharacterSet.whitespaces),
                              "region_id": selectedRegionId,
                              "postcode": txtZipCode.text!,
                              "telephone": txtPhoneNumber.text!]
        
        let queryItems = ["card_number": txtCardNumber.text!,
                          "exp_month": txtExpirationMonth.text!,
                          "exp_year": txtExpirationYear.text!,
                          "pay_method": "checkmo",
                          "shipping_address": shippinAddress,
                          "charge_transaction_id": chargeTransaction?.transactionId ?? "",
                          "deposit_transaction_id": depositTransaction.transactionId,
                          "customer_charge_amount": charge,
                          "customer_deposit_amount": deposit,
                          "extra_free":self.extraFee,
                          "openpay_exchange_rate": chargeTransaction?.exchangeRate ?? "",
                          "currency" : Authentication.currency!] as [String : Any]
      
        showLoaderOnWindow()
        WebService.placeOrder(queryItems: queryItems) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    print(response)
                    
                    AlertManager.showAlert(type: .custom(response.message ?? "Your payment is successfull".localized), action: {
                        let controller = OrderViewController.initiatefromStoryboard(.orderSummary)
                        controller.id = response.object?.orderid ?? ""
                        Observer.updateMyBookings()
                        Observer.updateCart()

                        self.navigationController?.pushViewController(controller, animated: true)
                    })
                case .failure(let error):
                    AlertManager.showAlert(type: .custom(error.message), action: {
                        // refund alert
                        self.showRefundAlert {
                            
                            guard let chargeTransaction = chargeTransaction else {
                                return
                            }
                            
                            // refund charge amount
                            self.refund(charge, transaction: chargeTransaction) {
                                // refund deposit amount
                                self.refund(deposit, transaction: depositTransaction) {
                                    self.showToast(message: "Your amount is successfully refunded".localized)
                                    self.releaseDeposit()
                                    self.depositConfirmed = false
                                }
                            }
                        }
                    })
                }
            }
        }
    }
    
    //MARK: Refund amount
    /*arguments - amount and optransaction*/
    func refund(_ amount: String, transaction: OpenPayTransaction, completion:@escaping ()->()) {
        
        if let calculativeExchangeRate = Double(transaction.exchangeRate),
            let calculativeAmount = Double(amount) {
            var refundAmount = "\(calculativeAmount*calculativeExchangeRate)"
            let components = refundAmount.components(separatedBy: ".")
            let decimals = components.last!
            if decimals.count >= 3, components.count > 1 {
                let index = refundAmount.firstIndex(of: ".")
                let index2 = refundAmount.index(index!, offsetBy: 2)
                let str = refundAmount[...index2]
                let double = Double(str)! + 0.01
                refundAmount = String.init(format: "%.2f", double)
            }
            
            self.refundAPI("\(refundAmount)", transactionId: transaction.transactionId) {
                completion()
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !showPicker {
            self.currentScrollOffset = scrollView.contentOffset
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCountry {
            showCountryPicker()
            return false 
        }
        if textField == txtState {
            if regionIndicator.isAnimating {
                return false
            }
            if !countries.isEmpty  {
                if !regions.isEmpty {
                    showRegionPicker()
                    return false
                } else {
                    return true
                }
            }
        }
        showPicker = textField == txtExpirationYear || textField == txtExpirationMonth
        return !showPicker
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtState
        {
            if txtCountry.text == ""
            {
                AlertManager.showAlert(type: .custom("Please select country.".localized))
            }
        }
        else if textField == txtCity
        {
            if txtCountry.text == ""
            {
                AlertManager.showAlert(type: .custom("Please select country.".localized))
            }
            else if txtState.text == ""
            {
                AlertManager.showAlert(type: .custom("Please select state.".localized))
            }
        }
    }
    
    // show country picker
    func showCountryPicker() {
        if self.countries.isEmpty { return }
        self.view.endEditing(true)
        let picker = CustomPickerView()
        picker.list = self.countries.compactMap { return $0.name }
        picker.showPickerView(title: "Countries".localized, tag: 0)
        picker.didSelectRow = { index,_ in
            if self.txtCountry.text == self.countries[index].name { return }
            self.txtState.text = ""
            self.txtCountry.text = self.countries[index].name
            self.getRegion(self.countries[index].iso2_code)
        }
    }
    
    func showRegionPicker() {
        if self.countries.isEmpty { return }
        let picker = CustomPickerView()
        picker.list = self.regions.compactMap { return $0.name }
        picker.showPickerView(title: "State".localized, tag: 0)
        picker.didSelectRow = { index,_ in
            self.txtState.text = self.regions[index].name
        }
    }
    
    //MARK: Refund Alert
    func showRefundAlert(completion:@escaping ()->()) {
        let alert = UIAlertController(title: nil, message: "Your debited amount will be refunded. Please wait for sometime".localized, preferredStyle: .alert)
        let action = UIAlertAction(title: title ?? "OK".localized, style: .default) { (_) in
            completion()
        }
        alert.addAction(action)
        
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else { return }
        guard let controller = delegate.window?.rootViewController else { return }
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Refund API
    func refundAPI(_ amount: String, transactionId: String, completion: (()->())? = nil) {
        
        showLoader()
        OpenpayPayment.refundAmount(amount: amount, customerId: Authentication.openpayCustomerId!, transactionId: transactionId) { (json,message) in
            DispatchQueue.main.async {
                self.hideLoader()
                guard json != nil else {
                    AlertManager.showAlert(type: .custom(message))
                    return
                }
                
                guard let completionHandler = completion else {
                    self.showToast(message: "Your amount is successfully refunded".localized)
                    return
                }
                
                completionHandler()
            }
        }
    }
    
    @IBAction func supportLink(_ sender: UIButton) {
        if let url = URL(string: "https://xrenty.uvdesk.com/en/blog/how-we-handle-payments") {
            UIApplication.shared.open(url)
        }
    }
}

extension OrderSummaryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order?.products?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrderSummaryTableViewCell
        let car = order!.products![indexPath.row]
        cell.configure(car)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        ConstraintTableHeight.constant = tableView.contentSize.height
    }
}

extension OrderSummaryViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return component == 0 ? expMonths.count : expYears.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return component == 0 ? expMonths[row] : expYears[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            txtExpirationMonth.text = String(format: "%02d", row+1)
        } else {
            txtExpirationYear.text = "\((Int(expYears[row])!)%100)"
        }
    }
}

extension OrderSummaryViewController {
    
    var isValidDetails: Bool {
        
        if txtFirstName.text!.isEmptyString() && txtLastName.text!.isEmptyString() && txtEmail.text!.isEmptyString() && txtAddress.text!.isEmptyString() && txtCity.text!.isEmptyString() && txtState.text!.isEmptyString() && txtZipCode.text!.isEmptyString() && txtCountry.text!.isEmptyString() && txtPhoneNumber.text!.isEmptyString() {
            AlertManager.showAlert(type: .custom("Please enter payment information".localized))
            return false
        }
        else if txtCardNumber.text!.isEmptyString() && txtExpirationMonth.text!.isEmptyString() && txtExpirationYear.text!.isEmptyString() &&
            txtCvv.text!.isEmptyString() {
            AlertManager.showAlert(type: .custom("Please enter the card details".localized))
            return false
        }
        else if txtFirstName.text!.isEmptyString() {
            AlertManager.showAlert(type: .custom("Please enter your name".localized))
            return false
        }
        else if txtLastName.text!.isEmptyString() {
            AlertManager.showAlert(type: .custom("Please enter your last name".localized))
            return false
        }
        else if txtEmail.text!.isEmptyString() {
            if !(txtEmail.text!.isValidEmail()) {
                AlertManager.showAlert(type: .custom("Please enter a valid email address".localized))
            } else {
                AlertManager.showAlert(type: .custom("Please enter your email address".localized))
            }
            return false
        }
        else if txtAddress.text!.isEmptyString() {
            if (txtAddress.text!.count < 3) {
                AlertManager.showAlert(type: .custom("Please enter a valid address".localized))
            } else {
                AlertManager.showAlert(type: .custom("Please enter your address".localized))
            }
            return false
        }
        else if txtCity.text!.isEmptyString() {
            AlertManager.showAlert(type: .custom("Please enter your city".localized))
            return false
        }
        else if txtState.text!.isEmptyString() {
            AlertManager.showAlert(type: .custom("Please enter your state".localized))
            return false
        }
        else if txtZipCode.text!.isEmptyString() {
            
            if (txtZipCode.text!.count < 5) {
                AlertManager.showAlert(type: .custom("Please enter a valid zipcode".localized))
            } else {
                AlertManager.showAlert(type: .custom("Please enter your zipcode".localized))
            }
            return false
        }
        else if txtCountry.text!.isEmptyString() {
            AlertManager.showAlert(type: .custom("Please select your country".localized))
            return false
        }
        else if txtPhoneNumber.text!.isEmptyString()  {
            AlertManager.showAlert(type: .custom("Please enter your phone number".localized))
            return false
        } else if txtPhoneNumber.text!.count < 10 {
            AlertManager.showAlert(type: .custom("Please enter a valid phone number".localized))
            return false
        }
        else if txtCardHolderName.text!.isEmptyString()  {
            AlertManager.showAlert(type: .custom("Please enter a valid card holder name".localized))
            return false
        }
        else if txtCardNumber.text!.isEmptyString()  {
            AlertManager.showAlert(type: .custom("Please enter your card number".localized))
            return false
        }else if txtCardNumber.text!.count < 9 {
            AlertManager.showAlert(type: .custom("Please enter a valid card number".localized))
            return false
        }
        else if txtExpirationMonth.text!.isEmptyString()  {
            AlertManager.showAlert(type: .custom("Please enter your card expire month".localized))
            return false
        }
        else if txtExpirationYear.text!.isEmptyString()  {
            AlertManager.showAlert(type: .custom("Please enter your card expire year".localized))
            return false
        }
        else if txtCvv.text!.isEmptyString()  {
            AlertManager.showAlert(type: .custom("Please enter your card cvv number".localized))
            return false
        }else if card.cvv2.count < 3 {
            AlertManager.showAlert(type: .custom("Please enter a valid cvv number".localized))
            return false
        }else if card.expired {
           AlertManager.showAlert(type: .custom("Expiry date is not valid.".localized))
            return false
        }else if card.number.count == 15 && card.cvv2.count < 4 {
            AlertManager.showAlert(type: .custom("The CVV number should be of 4 digits.".localized))
            return false
        }else if card.number.count == 16 &&  card.cvv2.count > 3 {
            AlertManager.showAlert(type: .custom("The CVV number should be of 3 digits.".localized))
            return false
        }
        else {
            return true
        }
    }
}

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}

extension UIView {
    func dribble() {
        transform = CGAffineTransform.identity
        UIView.animateKeyframes(withDuration: 1, delay: 0, options: [.autoreverse, .repeat], animations: {
            self.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
        }, completion: nil)
    }
}
