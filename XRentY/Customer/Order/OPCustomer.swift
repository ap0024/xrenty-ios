//
//  OPCustomer.swift
//  XRentY
//
//  Created by user on 13/11/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation

struct OPCustomer {
    let firstName: String
    let lastName: String
    let phone: String
    let email: String
}
