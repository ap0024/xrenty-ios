//
//  WishListViewController.swift
//  XRentY
//
//  Created by user on 19/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit

class WishListViewController: BaseViewController {
    
    var wishlistcars =  [Car]()
    @IBOutlet weak var wishListTable: UITableView!
    @IBOutlet weak var pageHadding : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.wishlist()
        Observer.connectWishlist(target: self, selector: #selector(refreshWishlit))
    }
    
    func label()  {
        self.pageHadding.text = "My Wishlist"
    }
    
    @objc func refreshWishlit() {
        self.wishlist()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension WishListViewController {

    func wishlist() {
        self.showLoader(color: .white)
        WebService.getWishlist { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    self.wishlistcars = response.objects!
                    if (response.objects?.isEmpty)!
                     {
                        self.wishListTable.isHidden = true
                    }
                    
                    self.wishListTable.reloadData()
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
}

extension WishListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishlistcars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WishlistTableViewCell", for: indexPath) as? WishlistTableViewCell
        cell?.tag = indexPath.row
//        cell?.btnGoToCar.setTitle("Go to Car", for: .normal)
        cell?.configure(wishlistcars[indexPath.row])
        cell?.goTocar = { self.pushToPriductDetail(self.wishlistcars[$0]) }
        return cell!
    }
    
    func pushToPriductDetail(_ car: Car) {
        self.pushTo(.productDetail(car))
    }
}
