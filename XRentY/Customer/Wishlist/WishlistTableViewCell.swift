//
//  WishlistTableViewCell.swift
//  XRentY
//
//  Created by user on 20/09/18.
//  Copyright © 2018 softprodigy. All rights reserved.
//

import UIKit

class WishlistTableViewCell: UITableViewCell {

    
    @IBOutlet weak var ImgCar: UIImageView!
    @IBOutlet weak var LblCarAddress: UILabel!
    @IBOutlet weak var LblCarName: UILabel!
    @IBOutlet weak var lblCarPrice: UILabel!
    @IBOutlet weak var btnGoToCar: UIButton!
    
    var goTocar : ((Int)->())? = nil
    var symbol: String {
        return Authentication.currencySymbol ?? ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(_ car: Car) {

        LblCarName.text = car.name
        LblCarAddress.text = car.address
        
        let carprice1 = Double(car.Price.roundOff()) ?? 0
        let carprice2 = CGFloat(carprice1).formattedWithSeparator
        
        lblCarPrice.text = symbol + "\(carprice2)"
        ImgCar.kf.indicatorType = .activity
        let targetURL = URL(string: car.image ?? "")
        ImgCar.kf.indicatorType = .activity
        ImgCar.kf.setImage(with: targetURL , placeholder: #imageLiteral(resourceName: "car_placeholder_images"))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func goToCar(_ sender: UIButton) {
        goTocar?(tag)
    }
    
}
