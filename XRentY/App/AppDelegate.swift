//
//  AppDelegate.swift
//  XRentY
//
//  Created by user on 07/09/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
import IQKeyboardManager
import GooglePlaces
import GoogleMaps
import TwitterKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import SwiftyDropbox
import UserNotifications
import Siren
import PaypalSDK
import PlacesPicker

protocol DropBoxResponseDelegate {
    func didAuthenticateDropBoxUser(_ url: URL)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var dbDelegate: DropBoxResponseDelegate?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
        window?.makeKeyAndVisible()
        // configure paypal 
        PaypalSDK.shared.configure(clientSecret: (id: Constants.KPAYPAL_CLIENT_ID, secret: Constants.KPAYPAL_SECRET), enableDebug: true, environment: .production)
        
        // configure pubnub
        PubNubManager.sharedInstance.configure(publishKey: Constants.PUBNUB_KEYS.0, subscribeKey: Constants.PUBNUB_KEYS.1)
        
        // configure location tracker coordinator
        LTCoordinator.sharedInstance().configure(pubnubKeys: Constants.PUBNUB_KEYS)
        try? LTCoordinator.sharedInstance().enableServices(on: Authentication.customerId)
        
        // Configure Facebook Service
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Configure Google Service
        GMSServices.provideAPIKey(Constants.kGOOGLE_API_KEY)
        GMSPlacesClient.provideAPIKey(Constants.kGOOGLE_API_KEY)
        
        PlacePicker.configure(googleMapsAPIKey: Constants.kGOOGLE_API_KEY, placesAPIKey: Constants.kGOOGLE_API_KEY)
        
        //        application.applicationIconBadgeNumber = Authentication.badge ?? 0
        
        // Configure Twitter Login SDK
        TWTRTwitter.sharedInstance().start(withConsumerKey: Constants.KTWITTER_CONSUMER_KEY, consumerSecret: Constants.KTWITTER_CONSUMER_SECRET)
        
        
        // Configure Keyboard Manager
        IQKeyboardManager.shared().isEnabled = true
        
        // Status bar appearance
        UIApplication.shared.isStatusBarHidden = false
        
        // Google Drive
        setupGoogleDrive()
        
        // DropBox
        setupDropBox()
        
        // push notification
        UNUserNotificationCenter.current()
        registerPushNotifications()
        
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        Authentication.appVersion = appVersion ?? ""
        
        //        Thread.sleep(forTimeInterval: 3)
        
        // App force update popup
        appUpdate()
        
        return true
    }
    
    //MARK:-  App force update popup
    func appUpdate() {
        let siren = Siren.shared
        siren.rulesManager = RulesManager.init(majorUpdateRules: .critical, minorUpdateRules: .annoying, patchUpdateRules: .annoying, revisionUpdateRules: .annoying, showAlertAfterCurrentVersionHasBeenReleasedForDays: 1)
        siren.wail(performCheck: .onDemand) { (result, error) in
            print(result?.localization ?? "")
            print(result.debugDescription)
            print(result?.updateType ?? "")
        }
    }
    
    //MARK:-  push notification
    func registerPushNotifications() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { granted, error in
            DispatchQueue.main.async {
                if granted {
                    UIApplication.shared.registerForRemoteNotifications()
                    // UIApplication.shared.applicationIconBadgeNumber = (Authentication.badge ?? 0)+1
                }
            }
        })
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        Authentication.deviceToken = token
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        guard let info = response.notification.request.content.userInfo as? [String:AnyObject] else { return }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let response = info["aps"] as? [String:Any] {
                var isSeller : Bool = false
                var clabeNotification : Bool = false
                if  let description = response["is_seller"] as? Bool {
                    isSeller = description
                }
                
                if  let clabe = response["clabe_info"] as? Bool {
                    clabeNotification = clabe
                }
                
                if clabeNotification {
                    
                    if let controller = self.window?.rootViewController, controller.isKind(of: BaseNavigationController.self) {
                        for controller in BaseNavigationController.sharedInstance.viewControllers {
                            if controller.isKind(of: BaseTabBarViewController.self) {
                                if (controller as! BaseTabBarViewController).selectedIndex != 4 {
                                    (controller as! BaseTabBarViewController).selectTab(4)
                                }
                            }
                        }
                    }
                }
                else {
                    if let controller = self.window?.rootViewController, controller.isKind(of: BaseNavigationController.self) {
                        for controller in BaseNavigationController.sharedInstance.viewControllers {
                            if controller.isKind(of: BaseTabBarViewController.self) {
                                if (controller as! BaseTabBarViewController).selectedIndex != 2 {
                                    (controller as! BaseTabBarViewController).selectTab(2)
                                }
                                Observer.refreshmessage()
                            }
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- open url
    public func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let sourceApp = options[UIApplication.OpenURLOptionsKey.sourceApplication]
        let annotation = options[UIApplication.OpenURLOptionsKey.annotation]
        var openUrlHandler: Bool = false
        
        if url.absoluteString.contains("li6626144") {
            openUrlHandler = LISDKCallbackHandler.application(app, open: url, sourceApplication: sourceApp as? String, annotation: annotation)
        }
            
        else if url.absoluteString.contains("fb265987010811143") {
            openUrlHandler = FBSDKApplicationDelegate.sharedInstance().application(app,open: url,sourceApplication: sourceApp as? String, annotation: annotation)
        }
            
        else if url.absoluteString.contains("twitterkit") {
            openUrlHandler = TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        }
            
        else if url.absoluteString.range(of: Constants.kDROPBOX_API_KEY) != nil {   //DropBox URL
            if let authResult = DropboxClientsManager.handleRedirectURL(url) {
                switch authResult {
                case .success:
                    self.dbDelegate?.didAuthenticateDropBoxUser(url)
                    debugPrint("Success! User is logged into Dropbox.")
                case .cancel:
                    debugPrint("Authorization flow was manually canceled by user!")
                case .error(_, let description):
                    debugPrint("Error: \(description)")
                }
            }
        }
            
        else if (url.absoluteString.range(of: Constants.kGOOGLEDRIVE_API_KEY) != nil) {
            //GoogleDrive URL
            openUrlHandler = GIDSignIn.sharedInstance().handle(url as URL?,
                                                               sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String!,
                                                               annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        }
        return openUrlHandler || true
    }
    
    //MARK:- DropBox
    func setupDropBox() {
        DropboxClientsManager.setupWithAppKey(Constants.kDROPBOX_API_KEY)
    }
    
    //MARK:- GoogleDrive
    func setupGoogleDrive() {
        GIDSignIn.sharedInstance().clientID = "517661220601-eh6pho7gvihab3d1u1tb5nh6ddkfqr1g.apps.googleusercontent.com"
    }
    
    //MARK:-
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    //MARK:-
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    //MARK:-
    func applicationWillEnterForeground(_ application: UIApplication) {
        LTCoordinator.sharedInstance().checkLocationServices()
        Observer.playvideo()
    }
    
    //MARK:-
    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
    }
    
    //MARK:-
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        if Authentication.isSeller == true
        {
            Authentication.appMode = true
        }
    }
}
