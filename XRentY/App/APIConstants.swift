//
//  API.swift
//  XRentY
//
//  Created by user on 25/06/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation

enum APIConstants: String {
    
//    #if PRO
//    static let pathLive = "https://www.getxrenty.com/minimart/miniapi/"
//    #elseif DEV
//    static let pathLive = "https://test.xrenty.com/minimart/miniapi/"
//    #endif
    
 //static let pathLocal = "http://192.168.0.73/devXrenty/minimart/miniapi/"
//  static let pathLive = "https://test.xrenty.com/minimart/miniapi/"
//    static let pathLive = "https://test.xrenty.com/minimart/miniapi/"
  static let pathLive = "https://www.xrenty.com/minimart/miniapi/"
//    static let pathLive = "https://lamp.softprodigyphp.in/Xrenty/minimart/miniapi/"

//    case login = "login"
//    case sociallogin = "sociallogin"
//    case register = "registration"
//    case forgotPassword = "forgotpassword"
//    case carCategory = "categorylist"
//    case carMake = "getmake"
//    case carModel = "getmodel"
//    case carDetail = "cardetail"
//    case rentdetail = "rentdetail"
//    case applyCoupon = "addcoupon"
//    case viewCart = "viewcart"
//    case updateCart = "updatecart"
//    case addWishlist = "AddToWishList"
//    case addReview = "addProductReview"
//    case wishlist = "wishlist"
//    case GetSearch = "GetSearch"
//    case getCountry = "GetCountry"
//    case Banklist = "Banklist"
//    case OrderList = "OrderList"
//    case pickupcar = "pickupcar"
//    case returnCar = "ReturnCar"
//    case returnCarSeller = "ReturnCarSeller"
//    case sellerList = "SellerList"
//    case becomeSeller = "BecomeSeller"
//    case listcars = "GetSellerCars"
//    case sellerOrderHistory = "SellerOrderHistory"
//    case myProfile = "MyProfile"
//    case addtocart = "addtocart"
//    case orderInfo = "OrderInfo"
//    case editProfile = "EditProfile"
//    case calender = "CalenderUnavailableDates"
//    case deletefromcart = "Deletefromcart"
//    case orderSummary = "orderSummary"
//    case EstimateTax = "EstimateTax"
//    case ClearCart = "ClearCart"
//    case placeOrder = "placeorder"
//    case getRegion = "GetRegion"
//    case getWishlistItems = "GetWishlistItems"
//    case checklogin = "Checklogin"
//    case addOpenpayCustomerId = "addOpenpayCustomerId"
//    case priceSuggest = "PriceSuggest"
//    case addPrice = "AddPrice"
//    case sellerPriceList = "SellerPriceList"
//    case deletePrice = "DeletePrice"
//    case editPrice = "EditPrice"
//    case getCity = "getCity"
//    case getStatus = "getStatus"
//    case gettype = "Gettype"
//    case getSubcategory = "GetSubcategory"
//    case getEngine = "GetEngine"
//    case getSoundSystem = "GetSoundSystem"
//    case getChildSeat = "GetChildSeat"
//    case getAc = "GetAc"
//    case getNumberSeat = "GetNumberSeat"
//    case getYear = "GetYear"
//    case getWheel = "GetWheel"
//    case selleraddcar = "selleraddcar"
//    case sellerdeletecar = "sellerdeletecar"
//    case GetSellerCarDetail = "GetSellerCarDetail"
//    case uploadproductimages = "uploadproductimages"
//    case sendMessage = "Sendmessage"
//    case viewMessageList = "ViewMessageList"
//    case viewMessage = "ViewMessage"
//    case sendReply = "SendReply"
//    case saveAddress = "SaveAddress"
//    case editCar = "sellereditcar"
//    case sellerReview = "SellerReview"
//    case removeproductimage = "removeproductimage"
//    case savebooking = "savebooking"
//    case clearDeviceToken = "ClearDeviceToken"
//    case updateMessageStatus = "UpdatemessageStatus"
//    case holdDeposit = "HoldDeposit"
//    case releaseDeposit = "releasedeposit"
//    case getCartStatus = "getCartStatus"
//    case LanguageSet = "LanguageSet"
//    case GetCurrency = "GetCurrency"
//    case HoldPaypalDeposit = "HoldPaypalDeposit"
//    case PlacePaypalOrder = "placeorderpaypal"

    case login = "loginvi"
    case sociallogin = "socialloginvi"
    case register = "registrationvi"
    case forgotPassword = "forgotpasswordvi"
    case carCategory = "categorylistvi"
    case carMake = "getmakevi"
    case carModel = "getmodelvi"
    case carDetail = "cardetailvi"
    case rentdetail = "rentdetailvi"
    case applyCoupon = "addcouponvi"
    case viewCart = "viewcartvi"
    case updateCart = "updatecartvi"
    case addWishlist = "AddToWishListvi"
    case addReview = "addProductReviewvi"
    case wishlist = "wishlistvi"
    case GetSearch = "GetSearchvi"
    case getCountry = "GetCountryvi"
    case Banklist = "Banklistvi"
    case OrderList = "OrderListvi"
    case pickupcar = "pickupcarvi"
    case returnCar = "ReturnCarvi"
    case returnCarSeller = "ReturnCarSellervi"
    case sellerList = "SellerListvi"
    case becomeSeller = "BecomeSellervi"
    case listcars = "GetSellerCarsvi"
    case sellerOrderHistory = "SellerOrderHistoryvi"
    case myProfile = "MyProfilevi"
    case addtocart = "addtocartvi"
    case orderInfo = "OrderInfovi"
    case editProfile = "EditProfilevi"
    case calender = "CalenderUnavailableDatesvi"
    case deletefromcart = "Deletefromcartvi"
    case orderSummary = "orderSummaryvi"
    case orderSummary_gold = "orderSummaryvi?type=gold"
    case orderSummary_platinum = "orderSummaryvi?type=platinum"
    case EstimateTax = "EstimateTaxvi"
    case ClearCart = "ClearCartvi"
    case placeOrder = "placeordervi"
    case getRegion = "GetRegionvi"
    case getWishlistItems = "GetWishlistItemsvi"
    case checklogin = "Checkloginvi"
    case addOpenpayCustomerId = "addOpenpayCustomerIdvi"
    case priceSuggest = "PriceSuggestvi"
    case addPrice = "AddPricevi"
    case sellerPriceList = "SellerPriceListvi"
    case deletePrice = "DeletePricevi"
    case editPrice = "EditPricevi"
    case getCity = "getCityvi"
    case getStatus = "getStatusvi"
    case gettype = "Gettypevi"
    case getSubcategory = "GetSubcategoryvi"
    case getEngine = "GetEnginevi"
    case getSoundSystem = "GetSoundSystemvi"
    case getChildSeat = "GetChildSeatvi"
    case getAc = "GetAcvi"
    case getNumberSeat = "GetNumberSeatvi"
    case getYear = "GetYearvi"
    case getWheel = "GetWheelvi"
    case selleraddcar = "selleraddcarvi"
    case sellerdeletecar = "sellerdeletecarvi"
    case GetSellerCarDetail = "GetSellerCarDetailvi"
    case uploadproductimages = "uploadproductimagesvi"
    case sendMessage = "Sendmessagevi"
    case viewMessageList = "ViewMessageListvi"
    case viewMessage = "ViewMessagevi"
    case sendReply = "SendReplyvi"
    case saveAddress = "SaveAddressvi"
    case editCar = "sellereditcarvi"
    case sellerReview = "SellerReviewvi"
    case removeproductimage = "removeproductimagevi"
    case savebooking = "savebookingvi"
    case clearDeviceToken = "ClearDeviceTokenvi"
    case updateMessageStatus = "UpdatemessageStatusvi"
    case holdDeposit = "HoldDepositvi"
    case releaseDeposit = "releasedepositvi"
    case getCartStatus = "getCartStatusvi"
    case LanguageSet = "LanguageSetvi"
    case GetCurrency = "GetCurrencyvi"
    case HoldPaypalDeposit = "HoldPaypalDepositvi"
    case PlacePaypalOrder = "PlaceOrderpaypalvi"

    var url: String {
        return APIConstants.pathLive + rawValue
    }
}

