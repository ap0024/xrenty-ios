//
//  Storyboards.swift
//  XRentY
//
//  Created by user on 22/05/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation
import UIKit

enum Storyboards: String {
    
    case login = "Login"
    case base = "Base"
    case drawer = "Drawer"
    case customer = "Customer"
    case home = "Home"
    case seller = "Seller"
    case bookings = "MyBookings"
    case message =  "Messages"
    case carDetail = "CarDetail"
    case orderSummary = "OrderSummary"
    case cart = "MyCart"
    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T: UIViewController>(_ controller: T.Type) -> T {
        let storyBoardId = (controller as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyBoardId) as! T
    }
}

extension UIViewController {
    
    class var storyboardID : String {
        return "\(self)"
    }

    class func initiatefromStoryboard(_ storyboard: Storyboards) -> Self {
        return storyboard.viewController(self)
    }
}
