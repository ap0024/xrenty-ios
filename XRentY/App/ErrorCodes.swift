//
//  ErrorCodes.swift
//  XRentY
//
//  Created by user on 30/11/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation
import UIKit

enum ErrorCode: Int {
    case e1000 = 1000
    case e1001 = 1001
    case e1002 = 1002
    case e1003 = 1003
    case e1004 = 1004
    case e1005 = 1005
    case e1006 = 1006
    case e1007 = 1007
    case e1008 = 1008
    case e1009 = 1009
    case e1010 = 1010
    case e1011 = 1011
    case e1012 = 1012
    case e1013 = 1013
    case e1014 = 1014
    case e1015 = 1015
    case e1016 = 1016
    case e1017 = 1017
    case e1018 = 1018
    case e1020 = 1020
    
    case e2001 = 2001
    case e2002 = 2002
    case e2003 = 2003
    case e2004 = 2004
    case e2005 = 2005
    case e2006 = 2006
    case e2007 = 2007
    case e2008 = 2008
    case e2009 = 2009
    case e2010 = 2010
    case e2011 = 2011
    
    case e3001 = 3001
    case e3002 = 3002
    case e3003 = 3003
    case e3004 = 3004
    case e3005 = 3005
    case e3006 = 3006
    case e3007 = 3007
    case e3008 = 3008
    case e3009 = 3009
    case e3010 = 3010
    case e3011 = 3011
    case e3012 = 3012
    
    case e4001 = 4001
    case e4002 = 4002
    
    case e5001 = 5001
    
    case e6001 = 6001
    case e6002 = 6002
    case e6003 = 6003
    
    var messageString : String{
        switch self {
        case .e1000:
            return "An error happened in the internal Openpay server.".localized
        case .e1001:
            return "The request is not JSON valid format, the fields don’t have the correct format, or the request doesn’t have the required fields.".localized
        case .e1002:
            return "The request is not authenticated or is incorrect.".localized
        case .e1003:
            return "The operation couldn’t be processed because one or more parameters are incorrect.".localized
        case .e1004:
            return "A required service is not available.".localized
        case .e1005:
            return "A required resource doesn’t exist.".localized
        case .e1006:
            return "There is already a transaction with the same ID order.".localized
        case .e1007:
            return "The funds transfer between the bank account or card and the Openpay account was rejected.".localized
        case .e1008:
            return "One of the required accounts is deactivated.".localized
        case .e1009:
            return "The request body is too large.".localized
        case .e1010:
            return "The public key is being used to make a request that requires the private key, or the private key is being using from Javascript.".localized
        case .e1011:
            return "The resource was previously deleted.".localized
        case .e1012:
            return "The transaction amount is out of the limits.".localized
        case .e1013:
            return "The operation is not allowed on the resource.".localized
        case .e1014:
            return "The account is inactive.".localized
        case .e1015:
            return "Could not get any response from gateway.".localized
        case .e1016:
            return "The merchant email has been already processed.".localized
        case .e1017:
            return "The payment gateway is not available at the moment, please try again later.".localized
        case .e1018:
            return "The number of retries of charge is greater than allowed.".localized
        case .e1020:
            return "The number of decimal digits is not valid for this currency".localized
        case .e2001:
            return "The CLABE of the bank account is already registered.".localized
        case .e2002:
            return "The number of this card is already registered.".localized
        case .e2003:
            return "The customer with this external ID already exists.".localized
        case .e2004:
            return "The identifier digit of this card number is invalid according to Luhn algorithm.".localized
        case .e2005:
            return "The card expiration date is prior to the current date.".localized
        case .e2006:
            return "The card security code (CVV2) wasn’t provided.".localized
        case .e2007:
            return "The card number is just for testing, it can only be used in Sandbox.".localized
        case .e2008:
            return "The card is not valid for Santander points.".localized
        case .e2009:
            return "The card security code (CVV2) is invalid.".localized
        case .e2010:
            return "3D Secure authentication failed.".localized
        case .e2011:
            return "Card product type not supported.".localized
        case .e3001:
            return "Card declined.".localized
        case .e3002:
            return "Card is expired.".localized
        case .e3003:
            return "Card has not enough funds.".localized
        case .e3004:
            return "Card has been flagged as stolen.".localized
        case .e3005:
            return "Card has been rejected by the antifraud system.".localized
        case .e3006:
            return "The operation is not allowed for this customer or transaction.".localized
        case .e3007:
            return "Deprecated. The card was rejected.".localized
        case .e3008:
            return "The card doesn’t support online transactions.".localized
        case .e3009:
            return "Card has been flagged as lost.".localized
        case .e3010:
            return "The card has been restricted by the bank.".localized
        case .e3011:
            return "The bank has requested to hold this card. Please contact the bank.".localized
        case .e3012:
            return "Bank authorization is required to make this payment.".localized
        case .e4001:
            return "The Openpay account doesn’t have enough funds.".localized
        case .e4002:
            return "The operation can't be completed until pending fees are paid.".localized
        case .e5001:
            return "The external_order_id already exists.".localized
        case .e6001:
            return "The webhook has already been processed.".localized
        case .e6002:
            return "Could not connect with webhook service.".localized
        case .e6003:
            return "Service responded with an error.".localized
        }
    }
}
