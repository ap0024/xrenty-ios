//
//  DrawerViewController.swift
//  XRentY
//
//  Created by user on 09/07/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
import IBLocalizable


class DrawerViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView?
    @IBOutlet weak var versionLbl: UILabel!
    @IBOutlet var backView: UIView!
    @IBOutlet var trailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
    var user: User!
    var selectItem: ((Drawer)->())? = nil
    var expand: Bool = false
    let tapGesture = UITapGestureRecognizer()
    let swipeGesture = UISwipeGestureRecognizer()
    var items : [DrawerItem] = DrawerItem.customer_seller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addGestures()
        self.tableView?.tableFooterView = UIView()
        self.versionLbl.text = "APP VERSION :".localized + Authentication.appVersion!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // will appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        trailingConstraint.constant = -(view.bounds.width)
        self.lblUsername.text = Authentication.customerName ?? "N/A"
        imgUser.kf.indicatorType = .activity
        let targetURL = URL(string: Authentication.customerImage ?? "")
        imgUser.kf.setImage(with: targetURL , placeholder: #imageLiteral(resourceName: "user_ic"))
    }
    
    // did appear for animation
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView?.isScrollEnabled = self.tableView!.contentSize.height > self.tableView!.bounds.height
        self.presentAnimation()
    }
    
    @IBAction func ViewProfile(_ sender: Any) {

    }
    
    // back (pop)
    @IBAction func btnMenu(_ sender: UIBarButtonItem) {
        self.dismissAnimation(completion: nil)
    }
    
    // configure tap gesture
    func addGestures() {
        tapGesture.addTarget(self, action: #selector(tapView))
        swipeGesture.direction = .left
        swipeGesture.addTarget(self, action: #selector(tapView))
        backView.addGestureRecognizer(tapGesture)
        backView.addGestureRecognizer(swipeGesture)
    }
    
    // tap outside tableview
    @objc func tapView() {
        self.dismissAnimation(completion: nil)
    }
    
    // present view animation
    func presentAnimation() {
        self.trailingConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.backView.alpha = 0.5
        }
    }
    
    // dismiss view animation
    @objc func dismissAnimation(completion: (()->Void)?) {
        self.trailingConstraint.constant = -(view.bounds.width)
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.backView.alpha = 0
        }) { (_) in
            self.navigationController?.dismiss(animated: false, completion: completion)
        }
    }

    func reloadMenu(_ mode: UserMode) {
        switch mode {
        case .customer:
            items = Authentication.isSeller ?  DrawerItem.customer_seller : DrawerItem.customer
        case .seller:
            items = DrawerItem.seller_customer
        }
        tableView?.reloadData()
    }
}

// MARK:- UITableViewDelegate, UITableviewDatasource 
extension DrawerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = items[indexPath.row].item.rawValue.localized
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
        cell.imageView?.image = items[indexPath.row].icon
        cell.imageView?.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        cell.imageView?.contentMode = .scaleAspectFit
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismissAnimation {
            guard let handler = self.selectItem else {
                return
            }
            handler(self.items[indexPath.row].item)
        }
    }
}
