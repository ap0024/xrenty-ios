//
//  Drawer.swift
//  XRentY
//
//  Created by user on 09/07/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation
import UIKit
import IBLocalizable

enum BaseHandler {
    case tab(Int)
    case push(UIViewController)
    case logout
    case mode(UserMode)
    case becomeSeller
}

enum Drawer: String {
    
    case search = "   Search a car"
    case cars = "My Cars"
    case rents = "My Rents"
    case carReviews = " My Car Reviews"
    case rentHistory = "   Rent History"
    case helpSupport = "  Help & Support"
    case logout = " Logout"
    case chat = "  Chat"
    case bookings = "  My Booking"
    case wishlist = "  My Wishlist"
    case cart = "Cart"
    case bookingHistory = "  Booking History"
    case seller = " Seller"
    case customer = " Customer"
    case becomeSeller = "  Become a seller"
//    case ratingHistor = "My Car Reviews"
    
    var basehandler: BaseHandler {
        
        switch self {
        case .search:
            return .tab(0)
        case .cars:
            return .tab(3)
         case.bookings:
            return .tab(1)
        case .rents:
            return .tab(1)
          case .cart:
            return .tab(3)
        case .carReviews:
            let controller = RatingHistoryViewController.initiatefromStoryboard(.seller)
            return .push(controller)
//            let controller = AddCarViewController.initiatefromStoryboard(.seller)
//            return .push(controller)
        case .rentHistory:
            let controller = RentHistoryViewController.initiatefromStoryboard(.seller)
            return .push(controller)
        case .helpSupport:
            let controller = HelpSupportViewController.initiatefromStoryboard(.seller)
            return .push(controller)
        case .logout:
            return .logout
        case .wishlist:
            let controller = WishListViewController.initiatefromStoryboard(.bookings)
            return .push(controller)
        case .chat:
            return .tab(2)
        case .bookingHistory:
            let controller = BookingHistoryViewController.initiatefromStoryboard(.bookings)
            return .push(controller)
        case .seller:
            Authentication.appMode = true
            return .mode(.seller)
        case .customer:
            Authentication.appMode = false
            return .mode(.customer)
        case .becomeSeller:
            Authentication.appMode = true
            return .becomeSeller
        }
    }
}

struct DrawerItem {
    
    let icon: UIImage!
    let item: Drawer!

    static var customer_seller = [DrawerItem(icon: #imageLiteral(resourceName: "customer_ic"), item: .seller),
                           DrawerItem(icon:  #imageLiteral(resourceName: "tabFindBlue_ic"), item: .search),
                           DrawerItem(icon: #imageLiteral(resourceName: "myBookingBlue_ic"), item: .bookings),
                           DrawerItem(icon: #imageLiteral(resourceName: "wishlist_ic"), item: .wishlist),
                           DrawerItem(icon: #imageLiteral(resourceName: "messages"), item: .chat),
                           DrawerItem(icon: #imageLiteral(resourceName: "new_cart"), item: .cart),
                          // DrawerItem(icon: #imageLiteral(resourceName: "history_ic"), item: .bookingHistory),
                           DrawerItem(icon: #imageLiteral(resourceName: "Help&suppot_ic"), item: .helpSupport),
                           DrawerItem(icon: #imageLiteral(resourceName: "logout_ic"), item: .logout)]
    
    static var customer = [DrawerItem(icon: #imageLiteral(resourceName: "customer_ic"), item: .becomeSeller),
                           DrawerItem(icon: #imageLiteral(resourceName: "tabFindBlue_ic"), item: .search),
                           DrawerItem(icon: #imageLiteral(resourceName: "myBookingBlue_ic"), item: .bookings),
                           DrawerItem(icon: #imageLiteral(resourceName: "wishlist_ic"), item: .wishlist),
                           DrawerItem(icon: #imageLiteral(resourceName: "messages"), item: .chat),
                           DrawerItem(icon: #imageLiteral(resourceName: "MyCarBlue_ic"), item: .cart),
                         //  DrawerItem(icon: #imageLiteral(resourceName: "history_ic"), item: .bookingHistory),
                           DrawerItem(icon: #imageLiteral(resourceName: "Help&suppot_ic"), item: .helpSupport),
                           DrawerItem(icon: #imageLiteral(resourceName: "logout_ic"), item: .logout)]
    
    static var seller_customer = [DrawerItem(icon: #imageLiteral(resourceName: "seller_ic"), item: .customer),
                         DrawerItem(icon: #imageLiteral(resourceName: "tabFindBlue_ic"), item: .search),
                         DrawerItem(icon: #imageLiteral(resourceName: "MyCarBlue_ic"), item: .cars),
                         DrawerItem(icon: #imageLiteral(resourceName: "myrent_ic"), item: .rents),
                         DrawerItem(icon: #imageLiteral(resourceName: "review"), item: .carReviews),
                         DrawerItem(icon: #imageLiteral(resourceName: "messages"), item: .chat),
                         //DrawerItem(icon: #imageLiteral(resourceName: "history_ic"), item: .rentHistory),
                         DrawerItem(icon: #imageLiteral(resourceName: "Help&suppot_ic"), item: .helpSupport),
                         DrawerItem(icon: #imageLiteral(resourceName: "logout_ic"), item: .logout)]
    
    static func updateItems(_ mode: UserMode) {
        
    }
}
