//
//  ServiceRequest.swift
//  XRentY
//
//  Created by user on 25/06/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation
import IBLocalizable

class WebService: RequestHandler {
    
    //MARK:- Server Handler -- connect to server -- 
    private class func connectToServer(_ api: APIConstants, httpMethod method: HTTPMethod,query queryItems: Parameters? = nil, completion:@escaping (Result<Data,ErrorResult>)->Void) {
        
        // check rechability
        if !reachability.isReachable {
            self.showRetryAlert(message: "No Internet Connection".localized, api: api, httpMethod: method, query: queryItems, completion: completion)
            return
        }
        
        let cookieStore = HTTPCookieStorage.shared
        for cookie in cookieStore.cookies ?? [] {
            cookieStore.deleteCookie(cookie)
        }

        // url request
        let internalRequest = RequestBuilder.buildRequest(api: api,
                                                          method: method,
                                                          parameters: queryItems)
                
        // check request before session
        guard let request = internalRequest else {
            debugPrint("invalid request")
            return
        }
        
        // session
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard data != nil else {
                let serverError = "Unable to connect to server. please try again after sometime.".localized
                self.showRetryAlert(message: error?.localizedDescription ?? serverError, api: api, httpMethod: method,query: queryItems, completion: completion)
                //print("-----Error: \(error?.localizedDescription ?? serverError)------")
               return
            }
            completion(.success(data!))
        }

        task.resume()
    }
    
    // retry handler
    class func showRetryAlert(message: String, api: APIConstants, httpMethod method: HTTPMethod,query queryItems: Parameters? = nil, completion:@escaping (Result<Data,ErrorResult>)->Void) {
        
        DispatchQueue.main.async {
            guard let delegate = UIApplication.shared.delegate as? AppDelegate else { return }
            guard let controller = delegate.window?.rootViewController else { return }
            
            let alert = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
            let cancelAction = UIAlertAction.init(title: "Cancel".localized, style: .cancel) { (_) in
                completion(Result.failure(ErrorResult.custom(string: "")))
            }
            
            let action = UIAlertAction.init(title: "Retry".localized, style: .default) { (_) in
                self.connectToServer(api, httpMethod: method, query: queryItems, completion: completion)
            }
            
            alert.addAction(action)
            alert.addAction(cancelAction)
            
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- set language
    class func setLanguage(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.LanguageSet, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Get currency
    class func GetCurrency(completion:@escaping ((Result<Response<Currency>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.GetCurrency, httpMethod: .GET,completion: self.networkResult(completion: completion))
    }
    
    //MARK:- login user
    class func loginUser(queryItems: Parameters, completion:@escaping ((Result<Response<User>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.login, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- social login *FB/Twitter/Instagram*
    class func socialLogin(queryItems: Parameters, completion:@escaping ((Result<Response<User>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.sociallogin, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- register user
    class func registerUser(queryItems: Parameters, completion:@escaping ((Result<Response<User>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.register, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- register user
    class func forgotPassword(queryItems: Parameters, completion:@escaping ((Result<Response<User>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.forgotPassword, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car category
    class func getCategory(completion:@escaping ((Result<Response<Category>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.carCategory, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car make
    class func getMake(completion:@escaping ((Result<Response<CarMake>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.carMake, httpMethod: .GET,completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car model
    class func getModel(queryItems: Parameters, completion:@escaping ((Result<Response<CarModel>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.carModel, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car detail
    class func getCarDetail(queryItems: Parameters, completion:@escaping ((Result<Response<Car>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.carDetail, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- rent Detail
    class func getRentDetail(queryItems: Parameters, completion:@escaping ((Result<Response<RentDetail>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.rentdetail, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- apply coupn
    class func applyCoupon(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.applyCoupon, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- clear cart
    class func deletefromcart(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.deletefromcart, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- update cart
    class func updateCart(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.updateCart, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    
    //MARK:- add review
    class func addReview(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.addReview, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- fetch wishlist
    class func fetchWishlist(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.wishlist, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- fetch Countries
    class func getCountries(completion:@escaping ((Result<Response<Country>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.getCountry, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- fetch bank
    class func Banklist(completion:@escaping ((Result<Response<Bank>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.Banklist, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Get search
    class func getSearch(queryItems: Parameters, completion:@escaping ((Result<Response<Car>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.GetSearch, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }

    //MARK:- Get booking

    class func getbooking(completion:@escaping ((Result<Response<BookingResponse>, ErrorResult>) -> Void)) {
        self.connectToServer(.OrderList, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- fetch pickUp
    class func pickUpCar(queryItems: Parameters, completion:@escaping ((Result<Response<PickUpCar>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.pickupcar, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- fetch return
    class func returnCar(queryItems: Parameters, completion:@escaping ((Result<Response<ReturnCar>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.returnCar, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- rent history
    class func rentHistory(completion:@escaping ((Result<Response<Rent>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.sellerList, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- become seller
    class func becomeSeller(completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.becomeSeller, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- list cars
    class func listCars(completion:@escaping ((Result<Response<Car>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.listcars, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- seller order history
    class func sellerOrderHistory(completion:@escaping ((Result<Response<Rent>, ErrorResult>) -> Void)) {

        self.connectToServer(.sellerOrderHistory, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- My profile
    class func profile(queryItems: Parameters, completion:@escaping ((Result<Response<Profile>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.myProfile, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- edit profile
    class func editProfile(queryItems: Parameters, completion:@escaping ((Result<Response<Profile>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.editProfile, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- addtocart
    class func addtocart(queryItems: Parameters, completion:@escaping ((Result<Response<AddtoCart>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.addtocart, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car detail
    class func getOrderDetail(queryItems: Parameters, completion:@escaping ((Result<Response<orderDetail>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.orderInfo, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- fetch return
    class func returnCarSeller(queryItems: Parameters, completion:@escaping ((Result<Response<SellerReturnCar>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.returnCarSeller, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- fetch Calender
    class func getCalender(queryItems: Parameters, completion:@escaping ((Result<Response<CalenderDates>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.calender, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Delete from cart
    class func Deletefromcart(queryItems: Parameters, completion:@escaping ((Result<Response<CalenderDates>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.deletefromcart, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Clear Cart
    class func ClearCart(completion:@escaping ((Result<Response<CalenderDates>, ErrorResult>) -> Void)) {

        self.connectToServer(.ClearCart, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- add to wishlist
    class func addWishlist(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.addWishlist, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- View Cart
    class func viewCart(completion:@escaping ((Result<Response<Cart>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.viewCart, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- View Cart
    class func estimateTax(queryItems: Parameters,completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        self.connectToServer(.EstimateTax, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- View Cart
    class func orderSummary(APIConstants:APIConstants,completion:@escaping ((Result<Response<Order>, ErrorResult>) -> Void)) {
        
        self.connectToServer(APIConstants, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Place order
    class func placeOrder(queryItems: Parameters, completion:@escaping ((Result<Response<Order>, ErrorResult>) -> Void)) {
        self.connectToServer(.placeOrder, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- get region api
    class func getRegion(queryItems: Parameters, completion:@escaping ((Result<Response<Region>, ErrorResult>) -> Void)) {
        self.connectToServer(.getRegion, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- getWishlist
    class func getWishlist(completion:@escaping ((Result<Response<Car>, ErrorResult>) -> Void)) {
        self.connectToServer(.getWishlistItems, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- check login
    class func checkLogin(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        self.connectToServer(.checklogin, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- add Customer Id
    class func addOpenpayCustomerId(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        self.connectToServer(.addOpenpayCustomerId, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    
    //MARK:- Suggest Price
    class func suggestPrice(queryItems: Parameters, completion:@escaping ((Result<Response<SuggestPrice>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.priceSuggest, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Save Price
    class func savePrice(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.addPrice, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    
    //MARK:- Price Listing
    class func getPriceListing(queryItems: Parameters, completion:@escaping ((Result<Response<Price>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.sellerPriceList, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Delete Price
    class func deletePrice(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.deletePrice, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Edit Price
    class func editPrice(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.editPrice, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Get Status
    class func getStatus( completion:@escaping ((Result<Response<Status>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.getStatus, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    //MARK:- Get City
    class func getCity( completion:@escaping ((Result<Response<City>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.getCity, httpMethod: .GET,  completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Seller apis

    
    //MARK:- car type
    class func getTpye(completion:@escaping ((Result<Response<CarType>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.gettype, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car Subcategory
    class func getSubType( completion:@escaping ((Result<Response<CarSubCategory>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.getSubcategory, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car EngineType
    class func getEngineType(completion:@escaping ((Result<Response<Engine>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.getEngine, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car Year
    class func getyear(completion:@escaping ((Result<Response<Year>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.getYear, httpMethod: .GET,completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car AC
    class func getAc(completion:@escaping ((Result<Response<AC>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.getAc, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car soundSystem
    class func getSoundSystem(completion:@escaping ((Result<Response<SoundSystem>, ErrorResult>) -> Void)) {

        self.connectToServer(.getSoundSystem, httpMethod: .GET,completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car childseat
    class func getChildSeat(completion:@escaping ((Result<Response<ChildSeat>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.getChildSeat, httpMethod: .GET,completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car Number Seat
    class func getNumberSeat(completion:@escaping ((Result<Response<NumberSeat>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.getNumberSeat, httpMethod: .GET,completion: self.networkResult(completion: completion))
    }
    
    //MARK:- car Wheel
    class func getWheel(completion:@escaping ((Result<Response<Wheel>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.getWheel, httpMethod: .GET,completion: self.networkResult(completion: completion))
    }
    
    //MARK:- add car
    class func addCar(queryItems: Parameters, completion:@escaping ((Result<Response<CarAdd>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.selleraddcar, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- edit car
    class func editCar(queryItems: Parameters, completion:@escaping ((Result<Response<CarAdd>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.editCar, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- add car
    class func deleteCar(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.sellerdeletecar, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- add car
    class func GetSellerCarDetail(queryItems: Parameters, completion:@escaping ((Result<Response<sellerCar>, ErrorResult>) -> Void)) {

        self.connectToServer(.GetSellerCarDetail, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- add car
    class func uploadproductimages(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.uploadproductimages, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- sendMessage
    class func sendMessage(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        self.connectToServer(.sendMessage, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- get Message list
    class func getMessageList(completion:@escaping ((Result<Response<Message>, ErrorResult>) -> Void)) {
        self.connectToServer(.viewMessageList, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- View Message
    class func viewMessage(queryItems: Parameters, completion:@escaping ((Result<Response<Chat>, ErrorResult>) -> Void)) {
        self.connectToServer(.viewMessage, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }

    //MARK:- replyMessage
    class func replyMessage(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        self.connectToServer(.sendReply, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    //MARK:- add car
    class func saveAddress(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {

        self.connectToServer(.saveAddress, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- list cars
    class func SellerReview(completion:@escaping ((Result<Response<Review>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.sellerReview, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- remove product image
    class func removeimage(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.removeproductimage, httpMethod: .POST, query: queryItems ,  completion: self.networkResult(completion: completion))
    }
    
    //MARK:- save booking
    class func savebooking(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.savebooking, httpMethod: .POST, query: queryItems ,  completion: self.networkResult(completion: completion))
    }
    
    //MARK:- save booking
    class func clearDeviceToken(completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.clearDeviceToken, httpMethod: .GET ,completion: self.networkResult(completion: completion))
    }
    
    //MARK:- save booking
    class func updateMessageStatus(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.updateMessageStatus, httpMethod: .POST, query: queryItems ,  completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Hold Deposit
    class func holdDeposit(queryItems: Parameters, completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.holdDeposit, httpMethod: .POST, query: queryItems, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Release Deposit
    class func releaseDeposit(completion:@escaping ((Result<Response<Empty>, ErrorResult>) -> Void)) {
        
        self.connectToServer(.releaseDeposit, httpMethod: .POST, query: [:], completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Cart Status
    class func cartStatus(completion:@escaping ((Result<Response<Order>, ErrorResult>) -> Void)) {
        self.connectToServer(.getCartStatus, httpMethod: .GET, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Cart Status
    class func holdPaypalDeposit(query: [String:Any], completion:@escaping ((Result<Response<Order>, ErrorResult>) -> Void)) {
        self.connectToServer(.HoldPaypalDeposit, httpMethod: .POST,query: query, completion: self.networkResult(completion: completion))
    }
    
    //MARK:- Place paypal order
    class func placePaypalOrder(query: [String:Any], completion:@escaping ((Result<Response<Order>, ErrorResult>) -> Void)) {
        self.connectToServer(.PlacePaypalOrder, httpMethod: .POST,query: query, completion: self.networkResult(completion: completion))
    }
}
