//
//  URLRequest.swift
//  XRentY
//
//  Created by user on 25/06/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case POST
    case GET
}

final class RequestBuilder {
    
    static let timeoutInterval : TimeInterval = 120
    
    //MARK:- url request builder method
    class func buildRequest(api: APIConstants, method: HTTPMethod = .GET, parameters: [String:Any]?) -> URLRequest? {
        
        guard let requestUrl = URL(string: api.url) else {
            debugPrint("-wrong url format-") 
            return nil
        }
        
        var request = URLRequest(url: requestUrl)
        request.httpMethod = method.rawValue
        request.timeoutInterval = RequestBuilder.timeoutInterval
        
        if method == .POST {
            guard parameters != nil else { return nil }
            self.addPostRequest(parameters!, request: &request)
        } else {
            self.addGetRequest(request: &request, parameters: parameters ?? [:])
        }
        
        self.printRequest(request, parameters: parameters)
        
        return request
    }
    
    //MARK:- post request method to add data
    class func addPostRequest(_ parameters: [String:Any], request: inout URLRequest) {
        
        do {
            let data = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            request.httpBody = data
            var fields = ["content-type":"application/json",
                          "cache-control":"no-cache"]
            if let accessToken = Authentication.token {
                fields["authorization"] = "Bearer \(accessToken)"
            }
            //print(fields)
            request.allHTTPHeaderFields = fields
        }
        catch {
            assertionFailure("-invalid json format-")
        }
    }
    
    //MARK:- get request method
    class func addGetRequest(request: inout URLRequest, parameters: [String:Any]) {
        
        if !parameters.isEmpty {
            var getUrl = "\(request.url!.absoluteString)?"
            for (key,value) in parameters {
                getUrl.append("\(key)=\(value)")
            }
            request.url = URL(string: getUrl)
        }
        
        if let accessToken = Authentication.token {
            request.allHTTPHeaderFields = ["authorization":"Bearer \(accessToken)",
                                           "content-type":"application/json",
                                           "cache-control":"no-cache"]
        }
    }
    
    class func printRequest(_ request: URLRequest, parameters: [String:Any]?) {
        print("******************************")
        print("url: \(request.url?.absoluteString ?? "invalid url")")
        print("method:\(request.httpMethod ?? "GET")")
        print("headers: \(request.allHTTPHeaderFields ?? [:])")
        if request.httpMethod  != "GET" {
            print("request: \(parameters ?? [:])")
        }
        print("******************************")
    }
}
