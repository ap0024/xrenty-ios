//
//  Response.swift
//  XRentY
//
//  Created by user on 04/07/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import Foundation
import IBLocalizable

final class Empty : Parceable {
    
    var response : [String:AnyObject] = [:]
    init(_ json: [String:AnyObject]) {
        self.response = json
    }
    
    static func parseObject(dictionary: [String : AnyObject]) -> Empty? {
        return Empty(dictionary)
    }
}

class Response<T: Parceable> {
    
    var status: String?
    var statusCode: Int?
    var message: String?
    var object: T?
    var objects: [T]?
    
    func parse(json dictionary: [String:AnyObject]) -> (Result<Response<T>, ErrorResult>) {
        
        print("Response: \(dictionary)")
        
        // parse json
        if let message = dictionary["message"] as? String {
            self.message = message
        }
        
        // check response for status code
        if let status_code = dictionary["status_code"] as? Int  {
            if status_code == 401 {
                DispatchQueue.main.async {
                AlertManager.showAlert(type: .custom("Your session has been expired \n Please login to continue".localized), actionTitle: "ok".localized) {
                    Authentication.clearData()
                    NavigationHandler.logOut()
                }
                }
            }
            else {
                self.statusCode = status_code
            }
        }
        
        // check response for dictionary
        if let response = dictionary["data"] as? [String:AnyObject]  {
            self.object = T.parseObject(dictionary: response)
        }
        
        // check response for array
        if let response = dictionary["data"] as? [[String:AnyObject]]  {
            self.objects = []
            response.forEach {
                if let obj = T.parseObject(dictionary:$0) {
                    self.objects?.append(obj)
                }
            }
        }

        // check status
        if let status = dictionary["status"] as? String {
            self.status = status
        }
        
        // response check
        if let statusCode = statusCode, statusCode == 200 {
            return Result.success(self)
        }
        else {
            return Result.failure(ErrorResult.custom(string: message ?? "Please try again later.".localized))
        }
    }
}
