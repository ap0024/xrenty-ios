//
//  CustomPickerView.swift
//  BURSTV2
//
//  Created by user on 26/06/18.
//  Copyright © 2018 Harish. All rights reserved.
//

import Foundation


class CustomPickerView: NSObject {
    
    var list: [String] = []
    var didSelectRow: ((Int,Int)->Void)?
    var tapCancel: (()->Void)?
    var pickerTag: Int!

    override init() {
        super.init()
    }
    
    func showPickerView(title: String, cancelTitle: String = "Cancel", confirmTitle: String = "Confirm", tag: Int) {
        var picker = CZPickerView()
        picker = CZPickerView(headerTitle: title, cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
        pickerTag = tag
        picker.delegate = self
        picker.dataSource = self
        picker.needFooterView = false
        picker.allowMultipleSelection = false
        picker.reloadData()
        picker.show()
    }
}

extension CustomPickerView: CZPickerViewDelegate, CZPickerViewDataSource {
    
    // MARK: - picker
    func czpickerViewImageURL(_ pickerView: CZPickerView!, imageForRow row: Int) -> String! {
        return ""
    }
    
    func numberOfRows(in pickerView: CZPickerView!) -> Int {
        return list.count
    }
    
    func czpickerView(_ pickerView: CZPickerView!, titleForRow row: Int) -> String! {
        return list[row]
    }
    
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemAtRow row: Int) {
        guard didSelectRow != nil else { return }
        didSelectRow!(row, pickerTag)
    }
    
    func czpickerViewDidClickCancelButton(_ pickerView: CZPickerView!) {
        guard tapCancel != nil else { return }
        tapCancel!()
    }
    
    private func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemsAtRows rows: [AnyObject]!) {
    }
}
