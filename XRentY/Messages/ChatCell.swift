//
//  IncomingMessageCell.swift
//  XRentY
//
//  Created by user on 03/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    private static let incoming = "incomingCell"
    private static let incomingLocation = "incomingLocationCell"
    private static let outgoing = "outgoingCell"
    private static let outgoingLocation = "outgoingLocationCell"

    class func identifier(_ message: Chat) -> String {
        if message.isReceived  {
            return message.islocationIncluded ? ChatCell.incomingLocation : ChatCell.incoming
        } else {
            return message.islocationIncluded ? ChatCell.outgoingLocation : ChatCell.outgoing
        }
    }

    @IBOutlet weak var messageLabel: UILabel?
    @IBOutlet weak var locationView: UIImageView?
    @IBOutlet weak var lblTime: UILabel!

    
    var locationHandler: (()->Void)? = nil
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.selectionStyle = . none
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(tapLocation))
        gesture.numberOfTapsRequired = 1
        locationView?.isUserInteractionEnabled = true
        locationView?.addGestureRecognizer(gesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @objc func tapLocation(_ sender: UITapGestureRecognizer) {
        self.locationHandler?()
    }
}
