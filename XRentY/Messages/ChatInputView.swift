//
//  SBInputView.swift
//  SBMessageInputView
//
//  Created by user on 31/12/18.
//  Copyright © 2018 SB. All rights reserved.
//

import UIKit

@objc protocol ChatInputDelegate {
    func sendMessage(_ message: String)
    func didChangeHeight()
}

class ChatInputView: UIView {

    @IBOutlet var txtView: UITextView!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var delegate: ChatInputDelegate?
    @IBOutlet var lblPlaceHolder: UILabel?

    let maxLines: CGFloat = 4
    var lineHeight: CGFloat = 0.0
    var lines: CGFloat = 1
    
    override func awakeFromNib() {
        heightConstraint.constant = txtView.font!.lineHeight
        txtView.textContainer.lineFragmentPadding = 0
        txtView.textContainerInset = .zero
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        txtView.setContentOffset(CGPoint(x: 0, y: -0), animated: false)
    }
    
    func clearText() {
        self.txtView.text = nil
        self.heightConstraint.constant = txtView.font!.lineHeight
        self.lblPlaceHolder?.textColor = .lightGray
    }
    
    @IBAction func btnSend(_ sender: UIButton) {
        delegate?.sendMessage(txtView.text!)
    }
}

extension ChatInputView: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        guard let font = textView.font else { return }
        let fontLineHeight = font.lineHeight
        let lines = round((textView.contentSize.height - textView.textContainerInset.top - textView.textContainerInset.bottom) / fontLineHeight)
        if lines <= maxLines {
            heightConstraint.constant = lines*fontLineHeight
        }
        if self.lines != lines {
            delegate?.didChangeHeight()
        }
        self.lines = lines
 
        lblPlaceHolder?.textColor = textView.text.isEmpty ? .lightGray : .clear
    }
}
