//
//  ChatViewController.swift
//  XRentY
//
//  Created by user on 03/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit
import IQKeyboardManager
import CoreLocation
import GooglePlaces
//import GooglePlacePicker
import SDWebImage
import PlacesPicker

class ChatViewController: BaseViewController {

    var messagesArray = [Chat]()
    var message: Message?
    var dismissHandler: (()->())? = nil
    var scrolledUp: Bool = false
//    var didScroll: Bool {
//        let cell = self.messageTable.visibleCells.last
//        let index = cell!.tag
//        return index != messagesArray.count
//    }
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var dayTitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTable: UITableView!
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var inputChatView: ChatInputView!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var userLabel: UILabel!

    var isScrolling: Bool = false
    var showTitle = false {
        didSet {
            self.dayTitleTopConstraint.constant = self.showTitle ? 5 : -30
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
        }
    }

    var textView: UITextView {
        return inputChatView!.txtView
    }
    var animatedCellIndex = [Int]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.getMessagesApi()
        self.dayTitleTopConstraint.constant = -30
        self.senderNameLabel.text = message?.sender_name
        self.productName.text = message?.product_name
        self.profileImageView.sd_setImage(with: URL(string:
            message?.sender_image ?? ""), placeholderImage: UIImage(named: "user_ic"))
        self.userLabel.text = message?.usercontact_id == Authentication.customerId ? "Xrent:" : "rentY:"
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.addListner()
        
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    
    @IBAction func customerDetail(_ sender: Any) {
        
        let query = ["user_id": message?.receiver_id!]
        self.showLoader(color: .clear)
        WebService.getRentDetail(queryItems: query as Parameters) { result in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    let controller = CustomerDetailViewController.initiatefromStoryboard(.seller)
                    controller.detail = response.object
                    self.presentDetail(controller)
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    @objc func keyboardWillAppear(notification: NSNotification){
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.updateBottomViewConstraint(constant:keyboardHeight - 45)
            self.updateContentOffset()
        }
    }

    @objc func keyboardWillDisappear(){
        self.updateContentOffset()
        self.updateBottomViewConstraint(constant: 0)
    }
    
    func addListner() {
        PubNubManager.sharedInstance.listner = { (message, channel) in
            print(message)
            print(channel)
            if channel == "\(Authentication.customerId!)" {
                if let message = message as? [String:Any] {
                    self.receivedMessage(message)
                }
            }
        }
    }
    
    func updateContentOffset() {
        let yOffset = self.messageTable.contentSize.height > self.messageTable.bounds.size.height ? self.messageTable.contentSize.height - self.messageTable.bounds.height : 0
        let point = CGPoint(x: 0, y: yOffset)
        self.messageTable.setContentOffset(point, animated: false)
    }
    
    func updateBottomViewConstraint(constant: CGFloat) {
        bottomConstraint.constant = constant
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func getMessagesApi(_ showloader: Bool = true, completion:(()->())? = nil) {
        showloader ? self.showLoader() : nil
        let queryItems = ["messages_id": message?.messages_id ?? ""]
        WebService.viewMessage(queryItems: queryItems){ (result) in
            DispatchQueue.main.async {
                showloader ? self.hideLoader() : nil
                completion?()
                switch result {
                case .success(let response):
                    self.messagesArray = response.objects ?? []
                    self.messageTable.reloadData()
                    self.messageTable.scrollToBottom(animated: false)
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.getMessagesApi {
            self.dismissHandler?()
        }
    }
    
    @IBAction func location(_ sender: UIButton) {
        
        let controller = PlacePicker.placePickerController()
        controller.delegate = self
        let navigationController = UINavigationController(rootViewController: controller)
        self.show(navigationController, sender: nil)
        
        /*let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)*/
    }
    
    @objc func dismissKeyboard() {
        if textView.isFirstResponder {
            textView.resignFirstResponder()
            bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // send message
    func sendMessage(_ location: CLLocationCoordinate2D? = nil) {
        
        let text = location != nil ? "" : textView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if (messagesArray.isEmpty || text.isEmptyString()) && location == nil {
            return
        }
        
        self.publishMessage(text, coordinate: location)
        self.addMessage(text, coordinate: location)
        self.inputChatView.clearText()
        
        let queryItems = ["message": text,
                          "messages_id": self.message?.messages_id ?? "",
                          "device_token" : Authentication.deviceToken ?? "" ,
                          "lat": location != nil ? "\(location!.latitude)" : "",
                          "long": location != nil ? "\(location!.longitude)" : "",
                          "device_type" : "iphone"]
        
        WebService.replyMessage(queryItems: queryItems ) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(_):
                    print("message sent")
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        }
    }
    
    // add message
    func addMessage(_ description: String, coordinate: CLLocationCoordinate2D?) {
        if !messagesArray.isEmpty {
            let chat = Chat(description: description,location: coordinate, chat: messagesArray.last!, sending: false,userId: "\(Authentication.customerId!)")
            self.messagesArray.append(chat)
            self.messageTable.reloadData()
            let indexPath = IndexPath(row: messagesArray.count - 1, section: 0)
            self.messageTable.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    // recived message from listner
    func receivedMessage(_ message: [String:Any]) {
        let messageId = message["message_id"] as! String
        if messageId == self.message?.messages_id {
            let message = message["message"] as! [String:Any]
            let data = try! JSONSerialization.data(withJSONObject: message, options: .prettyPrinted)
            let chat = try! JSONDecoder().decode(Chat.self, from: data)
            self.messagesArray.append(chat)
            self.messageTable.reloadData()
            if !scrolledUp {
                let indexPath = IndexPath.init(row: messagesArray.count - 1, section: 0)
                self.messageTable.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }
    
    /*func updateList(_ chat: Chat) {
        self.messagesArray.append(chat)
        self.messageTable.reloadData()
        let indexPath = IndexPath.init(row: messagesArray.count - 1, section: 0)
        self.messageTable.scrollToRow(at: indexPath, at: .bottom, animated: true)
        //self.scrollToBottom(force: !chat.isReceived)
    }*/
    
    // publish message
    func publishMessage(_ description: String, coordinate: CLLocationCoordinate2D?) {
        let id = self.message!.usercontact_id! != Authentication.customerId ?  self.message!.usercontact_id! : self.message!.user_id!
        if let message = Chat(description: description,location: coordinate, chat: messagesArray.last!, sending: true, userId: "\(Authentication.customerId!)").json {
            let dict = ["message_id": self.message!.messages_id!,
                        "message": message] as [String : Any]
            print(dict)
            PubNubManager.sharedInstance.sendMessage(to: "\(id)", message: dict)
        }
    }
}


extension ChatViewController: PlacesPickerDelegate {
    
    func placePickerController(controller: PlacePickerController, didSelectPlace place: GMSPlace) {
        controller.dismiss(animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.sendMessage(place.coordinate)
        }
    }
    
    func placePickerControllerDidCancel(controller: PlacePickerController) {
         controller.dismiss(animated: true, completion: nil)
    }
    
    /*func placePicker(_ viewController: PlacesPickerDelegate, didPick place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.sendMessage(place.coordinate)
        }
    }
    
    func placePickerDidCancel(_ viewController: PlacesPickerDelegate) {
        viewController.dismiss(animated: true, completion: nil)
    }*/
}

extension ChatViewController: ChatInputDelegate {
    
    func sendMessage(_ message: String) {
        self.sendMessage()
    }
    
    func didChangeHeight() {
        self.scrollToBottom()
    }
    
    func scrollToBottom(force: Bool = false) {
        if !self.scrolledUp || force  {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                self.messageTable.scrollToBottom(animated: true)
            }
        }
    }
}

extension ChatViewController: UITableViewDataSource, UITableViewDelegate{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messagesArray[indexPath.row]
        let identifier = ChatCell.identifier(message)
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! ChatCell
        cell.tag = indexPath.row
        cell.messageLabel?.text = messagesArray[indexPath.row].descriptionValue
        cell.locationHandler = message.islocationIncluded ? { self.pushToMap(message) } : nil
        cell.lblTime.text = messagesArray[indexPath.row].dayTime.1
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if textView.isFirstResponder {
            textView.resignFirstResponder()
            bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func pushToMap(_ message: Chat) {
        
//        let storyBoard = UIStoryboard(name: "LocationTracking", bundle: nil)
//        let controller1 = storyBoard.instantiateViewController(withIdentifier: "LTMapViewController") as! LTMapViewController
        
        guard let coordinate = message.coordinate else {
            
            self.showToast(message: "Tracking is disabled for this location.".localized)
            return
        }
        
        let startTracking = TrackList.contains(self.message!.messages_id!)
        let controller = LTMapViewController.instantiate()
        controller.addLocation(isLocationShared: !message.isReceived,
                               channel: self.message!.receiver_id,
                               location: coordinate,
                               startTracking: startTracking)
        controller.trackingStopHandler = { self.getMessagesApi() }
        controller.messageId = self.message!.messages_id!
        self.present(controller, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.lblDay.text = self.messagesArray[indexPath.row].dayTime.0
        if let cell = tableView.visibleCells.last {
            self.scrolledUp = cell.tag + 2 <= messagesArray.count - 1
        }
    }
}

extension ChatViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.isScrolling = true
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        self.showTitle = true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.isScrolling = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            if !self.isScrolling {
                self.showTitle = false
            }
        }
    }
}

extension UITableView {
    func scrollToBottom(animated: Bool = true) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        if (rows > 0){
            self.scrollToRow(at: IndexPath(row: rows - 1, section: sections - 1), at: .bottom, animated: animated)
        }
    }
}
