//
//  MessageCell.swift
//  XRentY
//
//  Created by user on 03/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.

import UIKit
import SDWebImage

class MessageCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var countLabelView: UIView!
    @IBOutlet weak var userLabel: UILabel!

    class var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.countLabelView.cornerRadius = self.countLabelView.bounds.width/2
    }
    
    var object : Message?{
        didSet{
            profileImageView.sd_setImage(with: URL(string:
                object?.sender_image ?? ""), placeholderImage: UIImage(named: "user_ic"))
            senderName.text = object?.sender_name
            carNameLabel.text = object?.product_name
            carNameLabel.sizeToFit()
            carNameLabel.adjustsFontSizeToFitWidth = true
            //countLabel.text = "\(object?.count_of_message ?? 0)"
            //countLabel.isHidden = object?.count_of_message == 0
            countLabelView.isHidden = object?.count_of_message == 0
            if let count = object?.count_of_message {
                countLabel.text = count >= 100 ? "99+" : "\(count)"
            } else {
                countLabel.text = ""
            }
            
            userLabel.text = object?.usercontact_id == Authentication.customerId ? "Xrent:" : "rentY:"
        }
    }
}
