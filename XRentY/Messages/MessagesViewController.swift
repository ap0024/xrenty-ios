//
//  MessagesViewController.swift
//  XRentY
//
//  Created by user on 03/12/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import UIKit

class MessagesViewController: BaseViewController {
    
    @IBOutlet weak var mesageTableView: UITableView!
    @IBOutlet var noRecordsView: UIView!
    let refreshControl = UIRefreshControl()
    var messagesArray : [Message] = [] {
        didSet {
            mesageTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        getMessagesApi()
        Observer.refreshmessage(target: self, selector: #selector(refreshMessageList))
        refreshControl.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        let attributes = [NSAttributedString.Key.foregroundColor: Color.barTintColor, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing please wait...".localized, attributes: attributes)
        mesageTableView.refreshControl = refreshControl
        refreshControl.tintColor = Color.barTintColor
        PubNubManager.sharedInstance.subscribe(to: "\(Authentication.customerId!)")
    }
    
    @objc func refreshMessageList() {
        getMessagesApi(false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        PubNubManager.sharedInstance.listner = { (message, channel) in
            if channel == "\(Authentication.customerId!)" {
                if let message = message as? [String:Any] {
                    self.receivedMessage(message)
                }
            }
        }
    }
    
    func receivedMessage(_ message: [String:Any]) {
        let messageId = message["message_id"] as! String
        var idMatched = false
        self.messagesArray.forEach {
            if $0.messages_id == messageId {
                $0.count_of_message! += 1
                mesageTableView.reloadData()
                idMatched = true
            }
        }
        !idMatched ? self.getMessagesApi(false) : nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func refreshTable(refreshControl: UIRefreshControl) {
        self.getMessagesApi(false)
    }
    
    @objc func getMessagesApi(_ showLoader: Bool = true) {
        showLoader ? self.showLoader() : self.refreshControl.beginRefreshing()
        WebService.getMessageList{ (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                self.refreshControl.endRefreshing()
                switch result {
                case .success(let response):
                    if let messages = response.objects,!messages.isEmpty  {
                        self.mesageTableView.backgroundView = nil
                        self.messagesArray = messages
                    } else if self.messagesArray.isEmpty {
                        self.mesageTableView.backgroundView = self.noRecordsView
                    }
                    else {
                        AlertManager.showAlert(on: self, type: .custom(response.message!))
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
}

extension MessagesViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MessageCell.identifier) as! MessageCell
        cell.object = messagesArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        messagesArray[indexPath.row].count_of_message = messagesArray[indexPath.row].count_of_message! + 10
//        tableView.reloadData()
        
        let controller = ChatViewController.initiatefromStoryboard(.message)
        controller.message = messagesArray[indexPath.row]
        controller.dismissHandler = { self.getMessagesApi(false) }
        self.navigationController?.pushViewController(controller, animated: true)
        
        self.messagesArray[indexPath.row].count_of_message = 0
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
