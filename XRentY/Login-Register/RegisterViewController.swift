//
//  customerRegisterViewController.swift
//  XRentY
//
//  Created by user on 07/09/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit


class RegisterViewController: BaseViewController {
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtDob: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var imgProfilePhoto: UIImageView!
    @IBOutlet weak var imgCheckTerms: UIImageView!
    @IBOutlet weak var btnRegister: UIButton!
    
   
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblDob: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblConfirmPassword: UILabel!
   
    
    
    let datePicker = UIDatePicker()
    var isSeller = false
    var bank: [Bank] = []
    var name: String?
    var acceptterms: Bool = false
    
    lazy var picker: UIImagePicker = {
        let imagePicker = UIImagePicker()
        imagePicker.pickerHandler = { self.addProfilePhoto($0, $1)}
        
        return imagePicker
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTitleView()
        self.addBirthdayPicker()
        //self.enableRegister(true)
        self.setLabel()
    }
    
    func setLabel() {
        
        lblFirstName.setLocalizedAshtrik()
        lblLastName.setLocalizedAshtrik()
        lblEmail.setLocalizedAshtrik()
        lblPassword.setLocalizedAshtrik()
        lblConfirmPassword.setLocalizedAshtrik()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.imgProfilePhoto.addShadow(color: UIColor.white.withAlphaComponent(0.5), radius: 5, opacity: 2)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addProfilePhoto(_ image: UIImage, _ name : String) {
        imgProfilePhoto.image = image
        self.name = name
    }
    
    func addBirthdayPicker() {
        
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = .white
        datePicker.maximumDate = Date()
        datePicker.addTarget(self, action: #selector(changePickerValue), for: .valueChanged)
        let toolBar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: view.bounds.width, height: 44))
        toolBar.isTranslucent = false
        toolBar.barTintColor = Color.barTintColor
        
        let barbutton = UIBarButtonItem.init(title: "Done".localized, style: .plain, target: self, action: #selector(selectBirthday))
        barbutton.tintColor = .white
        let flexSpace = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.items = [flexSpace,barbutton]
        txtDob.inputView = datePicker
        txtDob.inputAccessoryView = toolBar
    }
    
    @objc func changePickerValue(_ sender: UIDatePicker) {
        let formatter = DateFormatter.init()
        formatter.dateFormat = "dd-MM-YYYY"
        let date = formatter.string(from: sender.date)
        txtDob.text = date
    }
    
    @objc func selectBirthday(_ sender: UIBarButtonItem) {
        txtDob.resignFirstResponder()
    }
    
    @IBAction func registerUser(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if isValidForm
        {
            if acceptterms
            {
               self.registerUser()
            }
            else{
                self.showToast(message: "Please agree to our terms and conditions before register".localized)
            }
        }
    }
    
    // register user api 
    func registerUser() {
        
        var langCode : String = ""
        let language = NSLocale.current.languageCode
        
        if language == "en"
        {
            langCode = "es_US"
        }
        else if language == "es"
        {
            langCode = "es_ES"
        }
        else
        {
            langCode = "es_US"
        }

        self.showLoader()
        var query = parameters
        
        query["language"] = langCode
        
        WebService.registerUser(queryItems: query, completion: { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    if let user = response.object {
                        user.isSeller = self.isSeller ? 1 : 0
                        Authentication.autheticateUser(user)
                        NavigationHandler.loggedIn()
                        Authentication.customerName = user.userName ?? ""
                        Authentication.customerImage = user.image ?? ""
                        Authentication.openpayCustomerId = user.openpayId ?? ""
                        Authentication.clabeAccountDetail = user.clabeAccount
                        Authentication.appMode = user.isSeller == 1
                        
                    }
                    else {
                        AlertManager.showAlert(on: self, type: .custom(response.message!))
                    }
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
            }
        })
    }
    
    @IBAction func termsConditions(_ sender: UIButton) {
        acceptterms = self.setCheckUncheck(imgCheckTerms)
        //self.enableRegister(acceptTermsCondition)
    }
    
    // enable register button if user accepts terms and condition
    func enableRegister(_ show: Bool) {
        btnRegister.alpha = show ? 1 : 0.6
        btnRegister.isUserInteractionEnabled = show
    }
    
    @IBAction func profilePhoto(_ sender: UIButton) {
        self.picker.showSheet(false)
    }
    
    @IBAction func moveToLogin(_ sender: Any) {
        BaseNavigationController.sharedInstance.setRoot(.mainScreen)
    }

    func setCheckUncheck(_ view: UIImageView) -> Bool {
        view.tag = view.tag == 1 ? 0 : 1
        let checkboxImage = UIImage(named: "check")
        view.image = view.tag == 1 ? checkboxImage : nil
        return view.tag == 1
    }
}

