//
//  ForgetPasswordViewController.swift
//  XRentY
//
//  Created by user on 07/09/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
import IBLocalizable

class ForgetPasswordViewController: BaseViewController {

    @IBOutlet var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTitleView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func forgotPassword(_ sender: UIButton) {
        if (self.txtEmail.text!.isValidEmail()) {
            let queryItems = ["email":txtEmail.text!]
            self.showLoader()
            WebService.forgotPassword(queryItems: queryItems) { (result) in
                DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    AlertManager.showAlert(on: self, type: .custom(response.message ?? "Please check your email. we have sent you a reset link.".localized))
                case .failure(let error):
                    AlertManager.showAlert(on: self, type: .custom(error.message))
                }
                }
            }
        } else {
            AlertManager.showAlert(type: .custom("Please enter valid email address".localized))
        }
    }
}
