//
//  RegisterFieldError.swift
//  XRentY
//
//  Created by user on 05/10/18.
//  Copyright © 2018 Softprodigy. All rights reserved.
//

import Foundation
import IBLocalizable

extension RegisterViewController {
    
    // return required imageString for paramater to send
    var imageString: String? {
        if imgProfilePhoto.image == #imageLiteral(resourceName: "user_ic") { return nil }
        
        var imageData : Data?
        var compressedImage : UIImage?
        let max = 2097152
        imageData = imgProfilePhoto.image!.pngData()
        if (imageData?.count)! > max {
            imageData = imgProfilePhoto.image!.jpegData(compressionQuality: 0.5)
            compressedImage = UIImage(data: imageData!)
        }
        else {
            compressedImage = imgProfilePhoto.image
        }
        let (base64,format) = compressedImage!.convertImageTobase64()
        let imageString = "data:image/\(format.rawValue);base64," + base64!
        return imageString
    }
    
    
    
    
    
    // return 0 for invalid form and 1 form valid form details
    var isValidForm: Bool {
        guard let error = errorDescription else {
            return true
        }
        
        self.showToast(message: error)
        return false
    }
    
    // parameters to be used for register user api
    var parameters: [String:Any] {
        return ["firstname":self.txtFirstName.text!,
                "lastname":self.txtLastName.text!,
                "email":self.txtEmail.text!,
                "password":self.txtPassword.text!,
                "phone":txtPhone.text!,
                "dob":txtDob.text!,
                "is_seller":self.isSeller,
                "image": imageString ?? "",
                "device_token" : Authentication.deviceToken ?? "" ,
                "device_type" : "iphone",
                "is_editable" : 1
                 ] as [String : Any]
    }
    
    // register error
    enum RegisterError: String {
        
        case empty = "Please enter your details"
        case firtsname = "First name should be of atleast 3 characters"
        case lastname = "Last name should be of atleast 3 characters"
        case email = "Please enter a valid email address. For example customer@xrenty.com."
        case password = "Password should be of atleast 6 characters"
        case confirmPassword = "Please make sure your passwords match."
        
        var emptydescription: String {
            switch self {
            case .firtsname:
                return "Please enter your name".localized
            case .lastname:
                return "Please enter your last name".localized
            case .email:
                return "Please enter your email address".localized
            case .password:
                return "Please enter your password".localized
            case .confirmPassword:
                return "Please enter confirm password".localized
            case .empty:
                return "Please enter your details".localized
            }
        }
    }
    
    // return error description to show 
    var errorDescription: String? {
        
        return txtFirstName.text!.empty && txtLastName.text!.empty && txtEmail.text!.empty &&  txtPassword.text!.empty ? RegisterError.empty.rawValue.localized :
            txtFirstName.text!.empty ? RegisterError.firtsname.emptydescription :
            txtFirstName.text!.count < 3 ? RegisterError.firtsname.rawValue.localized :
            txtLastName.text!.empty ? RegisterError.firtsname.emptydescription :
            txtLastName.text!.count < 3 ? RegisterError.lastname.rawValue.localized :
            txtEmail.text!.empty ? RegisterError.email.emptydescription :
            !txtEmail.text!.isValidEmail() ? RegisterError.email.rawValue.localized :
            txtPassword.text!.empty ? RegisterError.password.emptydescription :
            txtPassword.text!.count < 6 ? RegisterError.password.rawValue.localized :
            txtConfirmPassword.text!.empty ? RegisterError.confirmPassword.emptydescription :
            txtPassword.text! != txtConfirmPassword.text! ? RegisterError.confirmPassword.rawValue.localized : nil
    }
}
