//
//  SellerRegisterViewController.swift
//  XRentY
//
//  Created by user on 25/09/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit

class SellerRegisterViewController: RegisterViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.isSeller = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
