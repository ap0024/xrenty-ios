//
//  CustomerRegisterViewController.swift
//  XRentY
//
//  Created by user on 25/09/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit

class CustomerRegisterViewController: RegisterViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.isSeller = false
        showNavigation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func listYourCar(_ sender: UIButton) {
        let controller = DisplayFeaturesViewController.initiatefromStoryboard(.login)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
