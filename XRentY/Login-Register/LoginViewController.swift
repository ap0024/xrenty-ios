//
//  loginViewController.swift
//  XRentY
//
//  Created by user on 07/09/18.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
import Kingfisher
import IBLocalizable


enum LoginForm: String {
    
    case empty = "Please fill the required fields"
    case email = "Please enter valid email"
    case password = "Please enter valid password"
    case valid
    
    var valid: Bool {
        switch self {
        case .email,.empty,.password:
            AlertManager.showAlert(type: .custom(rawValue.localized))
            return false
        case .valid:
            return true
        }
    }
}

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    var name : String = ""
    var image : String = ""
    var isEdit : Bool = true
    
    var form: LoginForm {
        return self.txtEmail.text!.isEmpty || self.txtPassword.text!.isEmpty ? .empty :
        !(self.txtEmail.text!.isValidEmail()) ? .email :
        self.txtPassword.text!.count < 6 ? .password : .valid
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTitleView()
    }
    
    @IBAction func back(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: UIViewController.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NavigationHandler.showNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Login
    @IBAction func loginuser(_ sender: UIButton) {
        self.view.endEditing(true)
        
        var langCode : String = ""
        let language = NSLocale.current.languageCode
        langCode = language == "en" ? "es_US" : "es_ES"
        
        if form.valid {
            let queryItems = ["email":txtEmail.text!,
                              "password": txtPassword.text!,
                              "device_token" : Authentication.deviceToken ?? "" ,
                              "device_type" : "iphone",
                              "language" : langCode]
            self.showLoader()
            WebService.loginUser(queryItems: queryItems) { (result) in
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch result {
                    case .success(let response):
                        if let user = response.object {
                            Authentication.autheticateUser(user)
                            Authentication.customerName = user.userName ?? ""
                            Authentication.customerImage = user.image ?? ""
                            Authentication.userId = user.id
                            Authentication.clabeAccountDetail = user.clabeAccount
                            Authentication.openpayCustomerId = user.openpayId ?? ""
                            Authentication.appMode = user.isSeller == 1
                            NavigationHandler.loggedIn()
                        }
                        else {
                            AlertManager.showAlert(on: self, type: .custom(response.message!))
                        }
                    case .failure(let error):
                        AlertManager.showAlert(on: self, type: .custom(error.message))
                    }
                }
            }
        }
    }

    //MARK: Facebook
    @IBAction func facebookLogin(_ sender: UIButton) {

        self.view.endEditing(true)
        Facebook.login(target: self) { result in
            DispatchQueue.main.async {
                self.checkSocialLogin(.facebook(result))
            }
        }
    }
    
    //MARK: Twitter
    @IBAction func twitterLogin(_ sender: UIButton) {
        self.view.endEditing(true)
        Twitter.loginUser() { result in
            DispatchQueue.main.async {
                self.checkSocialLogin(.twitter(result))
            }
        }
    }
    
    //MARK: Instagram
    @IBAction func instaLogin(_ sender: UIButton) {
        LinkedIn.loggedIn(self) { result in
            DispatchQueue.main.async {
                self.checkSocialLogin(.linkedIn(result))
            }
        }
    }
    
    // check for email address
    func checkSocialLogin(_ type: SocialLogin) {
        
        var langCode : String = ""
        let language = NSLocale.current.languageCode
        
        if language == "en"
        {
            langCode = "es_US"
        }
        else if language == "es"
        {
            langCode = "es_ES"
        }
        else
        {
            langCode = "es_US"
        }

        var query = type.query
        print("unique id: \(type.uniqueId)")
        let socialemail = type.query["email"] as? String
        if socialemail?.isEmpty ?? false
        {
            query["is_editable"] =  1
        }
        else
        {
            query["is_editable"] =  0
        }
        
        showLoader()
        WebService.checkLogin(queryItems: ["unique_id": type.uniqueId,"language":langCode]) { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let resp):
                    guard let object = resp.object else {
                        AlertManager.showAlert(type: .custom(resp.message!))
                        return
                    }
                    if socialemail == "" {
                    if let email = object.response["email"] as? String {
                        if email == "" {
                            
                            self.showEnterEmailAlert { (email) in
                                guard let input = email else { return }
                                
                                
                                
                                query["email"] = input
                                query["device_token"] = Authentication.deviceToken
                                query["device_type"] = "iphone"
                                query["language"] = langCode
                                
                                self.socialLogin(query)
                            }
                        }
                        else {
                            query["email"] = email
                            query["device_token"] = Authentication.deviceToken
                            query["device_type"] = "iphone"
                            query["language"] = langCode
                            self.socialLogin(query)
                        }
                    }
                    else {
                        AlertManager.showAlert(type: .custom(resp.message!))
                    }
                    }
                    else
                    {
                        query["email"] = socialemail
                        query["device_token"] = Authentication.deviceToken
                        query["device_type"] = "iphone"
                        query["language"] = langCode
                        self.socialLogin(query)
                        
                    }
                case .failure(let error):
                    if error.message == "unique id is not in table" {
                        self.showEnterEmailAlert { (email) in
                            guard let input = email else { return }
                            query["email"] = input
                            query["device_token"] = Authentication.deviceToken
                            query["device_type"] = "iphone"
                            query["language"] = langCode
                            self.socialLogin(query)
                        }
                    } else {
                        AlertManager.showAlert(type: .custom(error.message))
                    }
                }
            }
        }
    }
    
    // social login
    func socialLogin(_ query: [String:Any]) {
        self.showLoader()
        WebService.socialLogin(queryItems: query) { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let response):
                    if let user = response.object {
                        Authentication.autheticateUser(user)
                        Authentication.customerName = user.userName ?? ""
                        Authentication.customerImage = user.image ?? ""
                        Authentication.openpayCustomerId = user.openpayId ?? ""
                        Authentication.clabeAccountDetail = user.clabeAccount
                        Authentication.appMode = user.isSeller == 1
                        NavigationHandler.loggedIn()
                    }
                    else {
                        AlertManager.showAlert(on: self, type: .custom(response.message!))
                    }
                case .failure(let error):
                    AlertManager.showAlert(type: .custom(error.message))
                }
            }
        }
    }
    
    // present put email alert
    func showEnterEmailAlert(completion:@escaping (String?)->()) {
        
        let alert = UIAlertController(title: "Email".localized, message: "Please enter your email address".localized, preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Email".localized
            textField.returnKeyType = .continue
        }
        
        alert.addAction(UIAlertAction(title: "Add".localized, style: .default, handler: { [weak alert] (_) in
            if let textField = alert?.textFields![0], textField.text!.isValidEmail() {
                completion(textField.text!)
            }
            else {
                AlertManager.showAlert(type: .custom("Please enter valid email address".localized), action: {
                    self.showEnterEmailAlert(completion:completion)
                })
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Forgot Password
    @IBAction func forgotPassword(_ sender: UIButton) {
        NavigationHandler.pushTo(.forgotPassword)
    }
    
    //MARK: Forgot Password
    @IBAction func createAccount(_ sender: UIButton) {
        let controller = CustomerRegisterViewController.initiatefromStoryboard(.login)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
