//
//  MainViewController.swift
//  XRentY
//
//  Created by user on 08/03/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit
import AVFoundation


class MainViewController: BaseViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var landingBgView: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    var player: AVPlayer?
    let pageMaster = PageMaster([])
    @IBOutlet private weak var pageFrameView: UIView! {
        willSet {
            self.addChild(self.pageMaster)
            newValue.addSubview(self.pageMaster.view)
            newValue.fitToSelf(childView: self.pageMaster.view)
            self.pageMaster.didMove(toParent: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPageViewController()
        playBackgoundVideo()
        self.pageControl.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        self.pageMaster.isInfinite.toggle()
        
        Observer.playVideo(target: self, selector: #selector(playVideo))

    }
    
    @objc func playVideo() {
        player?.play()
    }
    
    private func playBackgoundVideo() {
        if let filePath = Bundle.main.path(forResource: "bgVideo", ofType:"mp4") {
            let filePathUrl = NSURL.fileURL(withPath: filePath)
            player = AVPlayer(url: filePathUrl)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.view.bounds
            playerLayer.backgroundColor = UIColor.clear.cgColor
            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: nil) { (_) in
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            }
            self.landingBgView.layer.insertSublayer(playerLayer, at: 0)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        hideNavigation()
        //self.icon.dribble()
        player?.play()
    }
    
    @IBAction func pushToLogin(_ sender: Any) {
        showNavigation()
        player?.pause()
        NavigationHandler.pushTo(.login)
    }

    @IBAction func pushToSignUp(_ sender: Any) {
        player?.pause()
        let controller = CustomerRegisterViewController.initiatefromStoryboard(.login)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - PageMasterDelegate
extension MainViewController: PageMasterDelegate {
    
    private func setupPageViewController() {
        
        self.pageMaster.pageDelegate = self
        let First = FirstVc.initiatefromStoryboard(.login)
        let loginChat = LoginChatVc.initiatefromStoryboard(.login)
        let car = CarVc.initiatefromStoryboard(.login)
        
        let vcList = [First,loginChat,car]
        self.pageControl.numberOfPages = vcList.count
        self.pageMaster.setup(vcList)
    }
    
    func pageMaster(_ master: PageMaster, didChangePage page: Int) {
        self.pageControl.currentPage = page
    }
}

