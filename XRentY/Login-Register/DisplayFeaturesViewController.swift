//
//  DisplayFeaturesViewController.swift
//  XRentY
//
//  Created by user on 29/04/19.
//  Copyright © 2019 Softprodigy. All rights reserved.
//

import UIKit

class DisplayFeaturesViewController: BaseViewController {
    
    let pageMaster = PageMaster([])
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet private weak var pageFrameView: UIView! {
        willSet {
            self.addChild(self.pageMaster)
            newValue.addSubview(self.pageMaster.view)
            newValue.fitToSelf(childView: self.pageMaster.view)
            self.pageMaster.didMove(toParent: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showNavigation()
        self.addTitleView()
        self.setupPageViewController()
        self.pageControl.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        self.pageMaster.isInfinite.toggle()
    }
    
    @IBAction func listYourCar(_ sender: UIButton) {
        let controller = SellerRegisterViewController.initiatefromStoryboard(.login)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - PageMasterDelegate
extension DisplayFeaturesViewController: PageMasterDelegate {
    
    private func setupPageViewController() {
        
        self.pageMaster.pageDelegate = self
        
        let chat = ChatVc.initiatefromStoryboard(.login)
        let doller = DollerVc.initiatefromStoryboard(.login)
        let help = HelpVc.initiatefromStoryboard(.login)
        let lock = LockVc.initiatefromStoryboard(.login)
        let location = LocationVc.initiatefromStoryboard(.login)
        
        let vcList = [lock,chat,doller,help,location]
        self.pageControl.numberOfPages = vcList.count
        self.pageMaster.setup(vcList)
    }
    
    func pageMaster(_ master: PageMaster, didChangePage page: Int) {
        self.pageControl.currentPage = page
    }
}

