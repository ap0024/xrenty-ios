//
//  PaymentDelegate.swift
//  PaypalSDK
//
//  Created by user on 31/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

public enum PaymentState {
    case create(Order)
    case authorize(Order)
    case approve(Order)
    case capture(Order)
    case cancel
    case refund(Order)
    case detail(Order)
}


/*IMPORTANT*/
/*payment delegate callbacks helps to determine the status of payment process*/
public protocol PaymentDelegate {
    
    /*updte order state depend on operation performed*/
    func didUpdateOrder(with state: PaymentState)
    
    /*did failure call when there is some failure occurs within the payment process*/
    func didFailUpdateOrder(with error: PaymentError)
}
