//
//  PaymentResult.swift
//  PaypalSDK
//
//  Created by user on 31/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

public enum PaymentResult<T> {
    case success(T)
    case failure(String)
}
