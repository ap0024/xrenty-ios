//
//  PaymentError.swift
//  PaypalSDK
//
//  Created by user on 31/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

public typealias ErrorMessage = String

public enum PaymentError {
    
    /*handling error conditions with these states*/
    case create(String)
    case approve(String)
    case authorize(String)
    case capture(String)
    case detail(String)
    case cancel(String)
    case refund(String)
    
    public var errorDescription: ErrorMessage {
        switch self {
        case .create(let message):
            return message
        case .approve(let message):
            return message
        case .authorize(let message):
            return message
        case .capture(let message):
            return message
        case .detail(let message):
            return message
        case .cancel(let message):
            return message
        case .refund(let message):
            return message
        }
    }
}
