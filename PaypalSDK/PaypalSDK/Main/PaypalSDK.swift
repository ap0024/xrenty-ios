//
//  PPSDK .swift
//  PaypalSDK
//
//  Created by user on 30/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

public enum Intent: String {
    case Authorize
    case Capture
}

/*client secret use to fetch client user id and cleint secret within tuple values*/
public typealias ClientSecret = (id: String, secret: String)

/*Paypal SDK*/
public final class PaypalSDK {
    
    public static var shared = PaypalSDK() // singleton
    public typealias TokenHandler = (String) -> () // callback for succesfully generated token
    public var delegate: PaymentDelegate? = nil // connects status check with other module
    
    /*Important*/
    /*configure paypal sdk with secret*/
    public func configure(clientSecret: ClientSecret, enableDebug: Bool = true, environment: Environment = .production, completion: TokenHandler? = nil) {
        
        // set default configuration
        Configuration.ENABLE_DEBUG = enableDebug
        Configuration.ENVIRONMENT = .production
        
        // generate access token if required
        generateAccessToken(clientSecret: clientSecret)
    }
    
    // generate access token
    private func generateAccessToken(clientSecret: ClientSecret) {
        //if UserDefaults.standard.isTokenExpire {
        Authorization.basic(username: clientSecret.id, secret: clientSecret.secret).setHeader()
        let resource = Token.generate()
        WebService.load(resource) { (response) in
            switch response {
            case .success(let object):
                UserDefaults.standard.addExpireTokenTime(object.expires_in)
                Authorization.bearer(token: object.access_token).setHeader()
            case .failure(let error):
                print(error.message)
            }
        }
        //}
    }
}

// public
public extension PaypalSDK  {
    
    //MARK: Create with intent
    func createOrder(withIntent intent: Intent, amount: String, currencyCode: String ,on target: PaymentDelegate) {
        self.delegate = target
        var amountt = amount.replacingOccurrences(of: ",", with: "")
        let amount = Amount(currency_code: currencyCode, value: amountt)
        let parameters = ["intent": intent.rawValue.uppercased(), "purchase_units": [["amount": ["currency_code": amount.currency_code, "value": amount.value]]]] as [String : Any]
        let resource = Order.create(parameters: parameters)
        WebService.load(resource) { (response) in
            switch response {
            case .success(let order):
                self.delegate?.didUpdateOrder(with: .create(order))
                if let link = order.links.find(where: "approve") {
                    self.approve(link: link, completion: self.handleApproveOrder(intent: intent, order: order))
                }
                else {
                    self.delegate?.didFailUpdateOrder(with: .create("HATEOSLink not found"))
                }
            case .failure(let error):
                self.delegate?.didFailUpdateOrder(with: .create(error.message))
            }
        }
    }
    
    //MARK: Approve
    func approve(link: Link, completion: ((PaymentResult<String>) -> ())? = nil)  {
        
        if let topController = UIApplication.topViewController() {
            let storyboardBundle = Bundle.init(identifier: "com.softprodigy.PaypalSDK")
            let storyboard = UIStoryboard.init(name: "Checkout", bundle: storyboardBundle)
            let controller = storyboard.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
            controller.urlString = link.href
            controller.handler =  completion
            let navigationController = UINavigationController(rootViewController: controller)
            topController.present(navigationController, animated: true, completion: nil)
        } else {
            fatalError()
        }
    }
    
    // MARK: Authorize
    func authorize(link: Link) {
        let resource = Order.authorize(url: link.href)
        WebService.load(resource) { (response) in
            switch response {
            case .success(let order):
                self.delegate?.didUpdateOrder(with: .authorize(order))
            case .failure(let error):
                self.delegate?.didFailUpdateOrder(with: .authorize(error.message))
            }
        }
    }
    
    // MARK: Capture
    func capture(link: Link) {
        let resource = Order.capture(url: link.href)
        WebService.load(resource) { (response) in
            switch response {
            case .success(let order):
                self.delegate?.didUpdateOrder(with: .capture(order))
            case .failure(let error):
                self.delegate?.didFailUpdateOrder(with: .capture(error.message))
            }
        }
    }
    
    /*
     {“amount”:{“currency”:“USD”,“total”: “100.00”},“is_final_capture”: true}
     */
    /*
     let url = "https://api.sandbox.paypal.com/v1/payments/authorization/\(transactionId)/capture"
     */
    
    func captureV1(transactionId: String, total: String, currency: String) {
        
        let params = ["amount" : ["currency": currency,"total": total], "is_final_capture": "true"] as [String : Any]
        let resource = Order.captureV1(api: API.captureV1(authId: transactionId),
                                       parameters: params)
        WebService.load(resource) { (response) in
            switch response {
            case .success(let order):
                self.delegate?.didUpdateOrder(with: .capture(order))
            case .failure(let error):
                self.delegate?.didFailUpdateOrder(with: .capture(error.message))
            }
        }
    }
    
    // MARK: Detail
    func orderDetail(for orderId: String) {
        let resource = Order.detail(orderId: orderId)
        print(resource.url)
        WebService.load(resource) {  (response) in
            switch response {
            case .success(let order):
                self.delegate?.didUpdateOrder(with: .detail(order))
            case .failure(let error):
                self.delegate?.didFailUpdateOrder(with: .detail(error.message))
            }
        }
    }
    
    // MARK: Cancel / Void
    func cancel(authorizationId: String) {
        
        let resource = Order.cancel(api: API.cancelOrder(authId: authorizationId))
        WebService.load(resource) { (response) in
            switch response {
            case .success:
                self.delegate?.didUpdateOrder(with: .cancel)
            case .failure(let error):
                self.delegate?.didFailUpdateOrder(with: .cancel(error.message))
            }
        }
    }
    
    func cancelV1(transactionId: String) {
        let resource = Order.cancel(api: API.cancelOrderV1(authId: transactionId))
        WebService.load(resource) { (response) in
            switch response {
            case .success:
                self.delegate?.didUpdateOrder(with: .cancel)
            case .failure(let error):
                self.delegate?.didFailUpdateOrder(with: .cancel(error.message))
            }
        }
    }
    
    // MARK: Refund 
    func refund(captureId: String) {
        let resource = Order.refund(captureId: captureId)
        WebService.load(resource) { (response) in
            switch response {
            case .success(let order):
                self.delegate?.didUpdateOrder(with: .refund(order))
            case .failure(let error):
                self.delegate?.didFailUpdateOrder(with: .refund(error.message))
            }
        }
    }
}

// private
private extension PaypalSDK {
    
    //MARK: handle aprove orders
    func handleApproveOrder(intent: Intent, order: Order) -> (PaymentResult<String>) -> () {
        return { result in
            switch result {
            case .success:
                self.delegate?.didUpdateOrder(with: .approve(order))
                self.hanldeIntent(intent: intent, order: order)
            case .failure(let message):
                self.delegate?.didFailUpdateOrder(with: .approve(message))
            }
        }
    }
    
    //MARK: handle intent cases
    func hanldeIntent(intent: Intent, order: Order) {
        switch intent {
        case .Authorize:
            if let link = order.links.find(where: "authorize") {
                self.authorize(link: link)
            }
            else {
                self.delegate?.didFailUpdateOrder(with: .authorize("HATEOSLink not found"))
            }
        case .Capture:
            if let link = order.links.find(where: "capture") {
                self.capture(link: link)

            } else {
                self.delegate?.didFailUpdateOrder(with: .capture("HATEOSLink not found"))
            }
        }
    }
}

