//
//  API.swift
//  Paypal
//
//  Created by user on 24/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

enum API {

    static let baseUrlV1 = Configuration.ENVIRONMENT.url + "v1/"
    static let baseUrlV2 = Configuration.ENVIRONMENT.url + "v2/"

    case token
    case createOrder
    case orderDetail(orderId: String)
    case authorizeOrder(orderId: String)
    case cancelOrder(authId: String)
    case refund(captureId: String)
    case cancelOrderV1(authId: String)
    case captureV1(authId: String)
    
    // return url object
    public var url: URL? {
        return URL(string: urlString)
    }
    
    // url String
    public var urlString: String {
        switch self {
        case .token:
            return API.baseUrlV1 + endpoint
        case .cancelOrderV1, .captureV1:
            return API.baseUrlV1 + endpoint
        default:
            return API.baseUrlV2 + endpoint
        }
    }
    
    // api end point
    private var endpoint: String {
        switch self {
        case .token:
            return "oauth2/token"
        case .createOrder:
            return "checkout/orders"
        case .orderDetail(let id):
            return "checkout/orders/\(id)"
        case .authorizeOrder(let id):
            return "orders/\(id)/authorize"
        case .cancelOrder(let authId):
            return "payments/authorizations/\(authId)/void"
        case .refund(let captureId):
            return "payments/captures/\(captureId)/refund"
        case .cancelOrderV1(let transactionId):
            return "payments/authorization/\(transactionId)/void"
        case .captureV1(let transactionId):
            return "payments/authorization/\(transactionId)/capture"
        }
    }
    
    //https://api.sandbox.paypal.com/v1/payments/authorization/\(transactionId)/capture
    
    //headers
    var headers: [String:String] {
        
        var header = ["Authorization" : UserDefaults.standard.value(forKey: "authorization") as? String ?? "",
                      "Content-Type": "application/json",
                      "Accept": "application/json"]
        switch self {
        case .token:
            header["Content-Type"] = "application/x-www-form-urlencoded"
        default:
            break
        }
        return header
    }
}
