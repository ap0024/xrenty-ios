//
//  Environment.swift
//  PaypalSDK
//
//  Created by user on 30/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

public enum Environment {
    
    case production
    case sandbox
    
    var url: String {
        switch self {
        case .production:
            return "https://api.paypal.com/"
        case .sandbox:
            return "https://api.sandbox.paypal.com/"
        }
    }    
}
