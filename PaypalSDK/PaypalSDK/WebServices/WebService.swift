//
//  ServiceRequest.swift
//  Toro
//
//  Created by user on 25/06/18.
//  Copyright © 2018 user. All rights reserved.
//

import Foundation
import AdSupport
import UIKit
import MobileCoreServices

let unknownError = "Paypal is not responding now. Please try after sometime"

public struct `void` : Codable {
    init(){ }
}

struct APIError {
    let statusCode: Int
    let message: String
    let name: String
}

enum Result<T> {
    case success(T)
    case failure(APIError)
}

struct ServerResponse {
    let data: Data?
    let response: HTTPURLResponse?
    let error: Error?
}


struct Resource<T: Decodable> {
    
    enum Method: String {
        case get = "GET"
        case post = "POST"
        case put  = "PUT"
    }
    
    let url: String
    let parse: (ServerResponse) -> Result<T>?
    let method: Method
    let parameters: [String: Any]?
    var headers: [String: String]?
    var urlencoded: [String:String]?
}

extension Resource {
    
    init(url: String, method: Method = .get, parameters: [String: Any]? = nil,urlEncodedParams: [String: String]? = nil , headers: [String:String]? = nil, parseJSON: ((Any) -> T?)? = nil) {
        
        self.url = url
        
        self.parse = { response in
            
            guard let urlResponse = response.response,
                let data = response.data else {
                    let apiError = APIError(statusCode: response.response?.statusCode ?? 0, message: response.error?.localizedDescription ?? unknownError, name: "")
                    return Result.failure(apiError)
            }
            
            if Configuration.ENABLE_DEBUG {
                // for debugging
                if let response = try? JSONSerialization.jsonObject(with: data, options: [.mutableLeaves]) {
                    print("Response: \(response)")
                } else {
                    print("Response: \(String(data: data, encoding: .utf8) ?? "invalid")")
                }
            }
           

            switch urlResponse.statusCode {
            case 300...600:
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    let message = json["message"] as? String
                    let name = json["name"] as? String
                    return Result.failure(APIError(statusCode: urlResponse.statusCode, message: message ?? unknownError, name: name ?? ""))
                } else {
                    return Result.failure(APIError(statusCode: urlResponse.statusCode, message: unknownError, name: ""))
                }
            case 204:
                if data.count == 0 {
                    let object = `void`()
                    return Result.success(object) as? Result<T>
                }
                else if let value = try? JSONDecoder().decode(T.self, from: data) {
                    return Result.success(value)
                }
                else {
                    return Result.failure(APIError(statusCode: 204, message: unknownError, name: ""))
                }
            default:
                if let value = try? JSONDecoder().decode(T.self, from: data) {
                    return Result.success(value)
                }
                else {
                    return Result.failure(APIError(statusCode: 0, message: unknownError, name: ""))
                }
            }
        }
        
        self.method = method
        self.parameters = parameters
        self.headers = headers ?? constructHeaders()
        self.urlencoded = urlEncodedParams
    }
    
    func isErrorResponse(response: URLResponse?) -> Bool {
        guard let response = response as? HTTPURLResponse else { return false }
        return response.statusCode >= 400
    }
}

extension Resource {
    
    func constructHeaders() -> [String: String] {
        var headers = commonHeaders()
        if let authorization = UserDefaults.standard.value(forKey: "authorization") as? String {
            headers["Authorization"] = authorization
        }
        return headers
    }
    
    private func commonHeaders() -> [String: String] {
        var myIDFA = "0000-0000-00000-00000-0000"
        var strIDFV = "0000-0000-00000-00000-0000"
        if ASIdentifierManager.shared().isAdvertisingTrackingEnabled {
            myIDFA = ASIdentifierManager.shared().advertisingIdentifier.uuidString
            strIDFV = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        
        return ["Accept" : "application/json",
                "Content-Type" : "application/json",
                "Device-Type" : "1",
                "Device-Name" : UIDevice.current.localizedModel,
                "App-Version" : (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)!,
                "Timezone" : TimeZone.current.identifier,
                "IDFA": myIDFA,
                "IDFV": strIDFV,
                "Country": Locale.current.regionCode ?? "",
                "OS-Version": UIDevice.current.systemVersion
        ]
    }
    
}

final class WebService {
    
    static func load<T>(_ resource: Resource<T>, completion: @escaping (Result<T>) -> Void) {
        
        var urlRequest = URLRequest(url: URL(string: resource.url)!)
        urlRequest.httpMethod = resource.method.rawValue
        
        resource.headers?.forEach({ (key, value) in
            urlRequest.addValue(value, forHTTPHeaderField: key)
        })
        
        switch resource.method {
        case .post, .put:
            if let params = resource.parameters {
                urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
            }
            else if let params = resource.urlencoded {
                urlRequest.encodeParameters(parameters: params)
            }
        case .get:
            var queryParameters = "?"
            resource.parameters?.forEach({ (key, value) in
                var stringValue = value as? String
                stringValue = stringValue?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)?.replacingOccurrences(of: ":", with: "%3A")
                queryParameters += key + "=\(stringValue ?? value)"
                queryParameters += "&"
            })
            queryParameters.removeLast()
            
            let urlString = urlRequest.url!.absoluteString + queryParameters
            urlRequest.url = URL(string: urlString)
        }
        
        if Configuration.ENABLE_DEBUG {
            print("url: \(urlRequest.url?.absoluteString ?? "")")
            print("headers: \(urlRequest.allHTTPHeaderFields ?? [:])")
            print("method: \(urlRequest.httpMethod ?? "")")
            print("params: \(resource.parameters ?? [:])")
        }
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            let serverResponse = ServerResponse(data: data, response: response as? HTTPURLResponse, error: error)
            if let result = resource.parse(serverResponse) {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            }.resume()
    }
}

extension String {
    var ns: NSString {
        return self as NSString
    }
    var pathExtension: String? {
        return ns.pathExtension
    }
    var lastPathComponent: String? {
        return ns.lastPathComponent
    }
}

extension URLRequest {
    
    private func percentEscapeString(_ string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")
        
        return string
            .addingPercentEncoding(withAllowedCharacters: characterSet)!
            .replacingOccurrences(of: " ", with: "+")
            .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
    }
    
    mutating func encodeParameters(parameters: [String : String]) {
        httpMethod = "POST"
        
        let parameterArray = parameters.map { (arg) -> String in
            let (key, value) = arg
            return "\(key)=\(self.percentEscapeString(value))"
        }
        
        httpBody = parameterArray.joined(separator: "&").data(using: String.Encoding.utf8)
    }
}
