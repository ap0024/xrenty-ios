//
//  Configuration.swift
//  PaypalSDK
//
//  Created by user on 30/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

class Configuration {
    static var ENABLE_DEBUG: Bool = true
    static var ENVIRONMENT: Environment = .production
}
