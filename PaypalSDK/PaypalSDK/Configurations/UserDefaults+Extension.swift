//
//  UserDefaults+Extension.swift
//  Paypal
//
//  Created by user on 25/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    // add expire token time
    func addExpireTokenTime(_ seconds: Int) {
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .second, value: seconds, to: Date())
        setValue(date, forKey: "token_expire_time")
    }
    
    // return true if token expires or vice-versa
    var isTokenExpire: Bool {
        
        guard let expire_date = value(forKey: "token_expire_time") as? Date else {
            return true
        }

        return Date() > expire_date
    }
    
    var accessToken: String {
        return (value(forKey: "authorization") as? String ?? "")
    }
}
