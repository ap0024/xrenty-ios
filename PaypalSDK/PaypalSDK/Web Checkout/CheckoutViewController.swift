//
//  WebViewController.swift
//  SingersBabel
//
//  Created by user on 22/05/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import UIKit
import WebKit

enum ApproveStatus {
    case approved
    case cancel
}

class CheckoutViewController: UIViewController {
    
    typealias WebHandler = (PaymentResult<String>)->()

    var showMenu: Bool = false
    var urlString: String? = nil
    var showNavigationAlways: Bool = true
    var barbutton: UIBarButtonItem!
    var handler: WebHandler? = nil
    
    @IBOutlet var webview: WKWebView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.loadRequest()
        
        navigationController?.navigationBar.tintColor = .blue
        navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(!showNavigationAlways, animated: true)
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        handler?(PaymentResult.failure("Payment Approval Cancel"))
        self.navigationController?.dismiss(animated: true, completion:nil)
    }
    
    @objc func selector(_ button : UIBarButtonItem) {
        if webview.canGoBack {
            webview.goBack()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }

    func loadRequest() {
        
        guard let url = URL.init(string: urlString!) else {
            return
        }
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = ["Authorization" : UserDefaults.standard.value(forKey: "authorization") as? String ?? "",
                                       "Content-Type": "application/json",
                                       "Accept": "text/html"]
        activityIndicator.startAnimating()
        webview.navigationDelegate = self
        webview.load(request)
        webview.allowsBackForwardNavigationGestures = true
        webview.uiDelegate = self
        webview.scrollView.showsVerticalScrollIndicator = false
        webview.scrollView.bounces = false
    }
}

extension CheckoutViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        activityIndicator.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        handler?(PaymentResult.failure(error.localizedDescription))
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        handler?(PaymentResult.failure(error.localizedDescription))
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url, url.absoluteString.contains("code=UkVUUlk%3D") ||  url.absoluteString.contains("&arc=1") {
            self.navigationController?.dismiss(animated: true, completion:nil)
            handler?(PaymentResult.success(urlString!))
            decisionHandler(.cancel)
        }
        else {
            decisionHandler(.allow)
        }
    }
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        decisionHandler(.allow)
    }
}

extension CheckoutViewController: WKUIDelegate {
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        return nil
    }
}
