//
//  PurchaseUnit.swift
//  Paypal
//
//  Created by user on 26/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

public struct PurchaseUnit:Codable {
    public let reference_id: String?
    public let amount: Amount?
    public let payee: Payee?
    public let shipping: Shipping?
    public let payments: Payment?
}

public struct Amount: Codable {
    public let currency_code: String
    public let value: String
}

public struct Payee: Codable {
    public let email_address: String?
    public let merchant_id: String?
}

public struct Shipping: Codable {
    public let name:FullName?
    public let address: Address?
}

public struct FullName: Codable {
    public let full_name: String?
}

