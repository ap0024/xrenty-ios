//
//  Refund.swift
//  Paypal
//
//  Created by user on 29/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation


public struct Refund: Codable {
    public let amount: Amount
    public let create_time: String
    public let id: String
    public let links: [Link]
    public let seller_payable_breakdown: PayableBreakdown?
}
