//
//  Capture.swift
//  Paypal
//
//  Created by user on 29/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

public struct Capture: Codable {
    public let id: String
    public let links: [Link]
    public let status: String
    public let create_time: String
    public let amount: Amount
    public let seller_receivable_breakdown: ReceivableBreakdown?
}
