//
//  Token.swift
//  Paypal
//
//  Created by user on 24/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

struct Token: Codable {
    let scope: String
    let access_token: String
    let token_type: String
    let app_id: String
    let expires_in: Int
    let nonce: String
}

extension Token {
    
    static func generate() -> Resource<Token> {
        let urlEncoded = ["grant_type":"client_credentials"]
        let resource = Resource<Token>(url: API.token.urlString, method: .post, urlEncodedParams: urlEncoded)
        return resource
    }
}
