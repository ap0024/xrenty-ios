//
//  Breakdown.swift
//  Paypal
//
//  Created by user on 29/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

public struct ReceivableBreakdown: Codable {
    public let gross_amount : Amount
    public let net_amount : Amount
    public let paypal_fee : Amount
}
