//
//  Web.swift
//  Paypal
//
//  Created by user on 24/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

@objc public class Link: NSObject, Codable {
    
    public let href: String
    public let rel: String
    public let method: String
    
    override init() {
        href = ""
        rel = ""
        method = ""
    }
}

public extension Array where Element: Link {
    
    //let emptyLink = Link()
    
    func find(where value: String) -> Element? {
        if isEmpty {
            return nil
        } else {
            return (filter { return $0.rel == value }).first
        }
    }
}
