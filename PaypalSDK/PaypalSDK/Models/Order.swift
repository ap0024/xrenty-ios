//
//  Order.swift
//  Paypal
//
//  Created by user on 24/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

public class Order: NSObject, Codable {
    public let id: String
    public let status: String?
    public let links: [Link]
    public let intent: String?
    public let payer: Payer?
    public let purchase_units: [PurchaseUnit]?
}

extension Order {
    
    // create order
    static func create(parameters: [String:Any]) -> Resource<Order> {
        let resource = Resource<Order>(url: API.createOrder.urlString, method: .post, parameters: parameters)
        return resource
    }
    
    // create order
    static func detail(orderId: String) -> Resource<Order> {
        let resource = Resource<Order>(url: API.orderDetail(orderId: orderId).urlString, method: .get)
        return resource
    }
    
    // authorize order
    static func authorize(url: String) -> Resource<Order> {
        let resource = Resource<Order>(url: url, method: .post)
        return resource
    }
    
    // capture order
    static func capture(url: String) -> Resource<Order> {
        let resource = Resource<Order>(url: url, method: .post, parameters: ["rel": "capture"])
        return resource
    }
    
    // capture order
    static func captureV1(api: API, parameters: [String: Any]) -> Resource<Order> {
        let resource = Resource<Order>(url: api.urlString, method: .post, parameters: parameters)
        return resource
    }
    
    // cancel order
    static func cancel(api: API) -> Resource<`void`> {
        let resource = Resource<`void`>(url: api.urlString, method: .post)
        return resource
    }
    
    // refund order
    static func refund(captureId: String) -> Resource<Order> {
        let resource = Resource<Order>(url: API.refund(captureId: captureId).urlString, method: .post)
        return resource
    }
}
