//
//  Payer.swift
//  Paypal
//
//  Created by user on 26/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

public struct Payer: Codable {
    public var address: Address?
    public var email_address: String?
    public var name: Name?
    public var payer_id: String?
    public var phone: Phone?
}

public struct Address: Codable {
    public let country_code: String?
    public let address_line_1: String?
    public let admin_area_2:String?
    public let admin_area_1: String?
    public let postal_code: String?
}

public struct Name: Codable {
    public let given_name: String?
    public let surname: String?
}

public struct PhoneNumber: Codable {
    public let national_number: String?
}

public struct Phone: Codable {
    public let phone_number: PhoneNumber?
}
