//
//  Payment.swift
//  Paypal
//
//  Created by user on 29/07/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation

public struct Payment: Codable {
    public let authorizations: [PPAuthorization]?
    public let captures: [Capture]?
    public let  refunds: [Refund]?
}
